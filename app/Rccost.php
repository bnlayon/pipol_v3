<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rccost extends Model
{
    protected $fillable = ['proj_id', 'rcyear', 'rccost'];
}
