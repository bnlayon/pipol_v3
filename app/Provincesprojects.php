<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincesprojects extends Model
{
    protected $fillable = ['proj_id', 'province_id'];
}
