<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regionsprojects extends Model
{
    protected $fillable = ['proj_id', 'region_id'];
}
