<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investments extends Model
{
    protected $fillable = ['proj_id', 'year', 'local', 'loan', 'grant', 'gocc', 'lgu', 'private', 'others',];
}
