<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rowacost extends Model
{
    protected $fillable = ['proj_id', 'rowayear', 'rowacost'];
}
