<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facost extends Model
{
    protected $fillable = ['proj_id', 'fayear', 'facost_nep', 'facost_all', 'facost_ad'];
}
