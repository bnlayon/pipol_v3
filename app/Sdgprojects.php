<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sdgprojects extends Model
{
    protected $fillable = ['proj_id', 'sdg_id'];
}
