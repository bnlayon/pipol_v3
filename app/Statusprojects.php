<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statusprojects extends Model
{
    protected $fillable = ['proj_id', 'status_id'];
}
