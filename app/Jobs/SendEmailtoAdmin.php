<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailtoAdmin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = collect($user);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        Mail::send('emails.adminNotif', ['user' => $user], function ($message) use ($user) {
            $message->from('john@johndoe.com', 'PIP Online');
            $message->to('pipol.neda@gmail.com', 'PIP Secretariat');
            $message->cc('sakaseya@gmail.com', 'PIP Secretariat');
            $message->subject('PIPOL Account Activation ' . $user['lname'] . ', ' . $user['fname'] . ' ' . $user['mname'] . ' ');
            $message->priority(1);
        });
    }
}
