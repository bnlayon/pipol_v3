<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailDisapprove implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $params;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $params)
    {
        $this->params = collect( $params);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $params = $this->params;
        Mail::send('emails.userRejected',['params' => $params], 
        function ($message) use ($params) {
            $message->from('john@johndoe.com', 'PIP Online');
            $message->to( $params['item']['email'], 'PIP Secretariat');
            // $message->to('pipol.neda@gmail.com', 'PIP Secretariat');
            $message->cc( 'pipol.neda@gmail.com', 'PIP Secretariat');
            $message->cc( 'sakaseya@gmail.com', 'PIP Secretariat');
            $message->subject($params['item']['UACS_AGY_DSC'] . ': PIPOL User Account Request Status');
            $message->priority(1);
        });
    }
}
