<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailtoUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $user)
    {
        $this->user = collect($user);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        Mail::send('emails.userNotif', ['user' => $user], function ($message) use ($user) {
            $message->from('john@johndoe.com', 'PIP Online');
            $message->to($user['email'], $user['lname'] . ', ' . $user['fname'] . ' ' . $user['mname']);
            $message->cc('pipol.neda@gmail.com', 'PIP Secretariat Email1');
            $message->subject( $user['agency']->UACS_AGY_DSC . ': PIPOL User Account Request Status');
            $message->priority(1);
        });
    }
}
