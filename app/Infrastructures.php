<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Infrastructures extends Model
{
    protected $fillable = ['proj_id', 'year', 'local'];
}
