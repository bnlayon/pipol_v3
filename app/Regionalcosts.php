<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regionalcosts extends Model
{
    protected $fillable = ['region', 'year', 'proj_id', 'cost'];
}
