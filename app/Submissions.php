<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submissions extends Model
{
    public function getAgencyDetails($id){
        $agencyDetails = \App\Agencies::where('id', $id)->get();
        return $agencyDetails;
    }

    public function getUserDetails($id){
        $usersEmails = \App\Users::where('submission_id', $id)->get();
        return $usersEmails;
    }

    public function getProjectCompleted($id){
        $completedprojects = \App\Projects::where('agency_id', '=', $id)->where('category', '=', 'Completed')->where('statusofsubmission', '=', 'Endorsed')->get();
        return $completedprojects;
    }

    public function getProjectTier1($id){
        $tier1projects = \App\Projects::where('agency_id', '=', $id)->where('category', '=', 'Tier 1')->where('SS_statusofsubmission', '=', 'Validated')->get();
        return $tier1projects;
    }

    public function getProjectTier2($id){
        $tier2projects = \App\Projects::where('agency_id', '=', $id)->where('category', '=', 'Tier 2')->where('SS_statusofsubmission', '=', 'Validated')->get();
        return $tier2projects;
    }

    public function getProjectDrafts($id){
        $draftprojects = \App\Projects::where('agency_id', '=', $id)->where('statusofsubmission', '=', 'Draft')->where('category', '!=', 'Completed')->get();
        return $draftprojects;
    }

    public function getProjectUncategorized($id){
        $droppedprojects = \App\Projects::where('agency_id', '=', $id)->where('SS_statusofsubmission', '=', 'Uncategorized')->get();
        return $droppedprojects;
    }

    public function getProjectDropped($id){
        $droppedprojects = \App\Projects::where('agency_id', '=', $id)->where('category', '=', 'Dropped')->get();
        return $droppedprojects;
    }

    public function getProjectCompleted2($id){
        $droppedprojects = \App\Projects::where('agency_id', '=', $id)->where('category', '=', 'Completed')->get();
        return $droppedprojects;
    }

    public function getDroppingReasons($id){
        $droppingreasons = \App\Droppingreasons::where('proj_id', '=', $id)->first();
        return $droppingreasons;
    }

    public function getCost($id){
        $projectcost = \App\Investments::where('proj_id', '=', $id)->selectRaw('sum(local) as sum')->selectRaw('sum(loan) as sum1')->selectRaw("sum('grant') as sum2")->selectRaw('sum(gocc) as sum3')->selectRaw('sum(lgu) as sum4')->selectRaw('sum(private) as sum5')->selectRaw('sum(others) as sum6')->get();
        return $projectcost;
    }

    public function getProjectCompleted_v2($id){
        $tier1projects = \App\Projects::where('agency_id', '=', $id)->where('SS_implementationreadiness', '=', '3')->where('statusofsubmission', '=', 'Endorsed')->get();
        return $tier1projects;
    }

    public function getProjectOngoing_v2($id){
        $tier1projects = \App\Projects::where('agency_id', '=', $id)->where('SS_implementationreadiness', '=', '1')->where('statusofsubmission', '=', 'Endorsed')->where('SS_statusofsubmission', '=', 'Validated')->get();
        return $tier1projects;
    }

    public function getProjectProposed_v2($id){
        $tier1projects = \App\Projects::where('agency_id', '=', $id)->where('SS_implementationreadiness', '=', '2')->where('statusofsubmission', '=', 'Endorsed')->where('SS_statusofsubmission', '=', 'Validated')->get();
        return $tier1projects;
    }

    public function getProjectFS_v2($id){
        $tier1projects = \App\FinanceSourceProjects::where('proj_id', '=', $id)->get();
        return $tier1projects;
    }

    protected $guarded = ['id'];
}
