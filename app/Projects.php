<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{

    public function projects_investments() {
        return $this->hasMany('App\Investments', 'proj_id');
    }

    public function projects_infrastructures() {
        return $this->hasMany('App\Infrastructures', 'proj_id');
    }

    public function getAgencyDetails($id){
        $agencyDetails = \App\Agencies::where('id', $id)->get();
        return $agencyDetails;
    }

    public function projects_bases() {
        return $this->belongsToMany('App\Basis', 'basisprojects', 'proj_id', 'basis_id')->withTimestamps();
    }

    public function projects_attagency() {
    return $this->belongsToMany('App\Agencies', 'attagenciesprojects', 'proj_id', 'agency_id')->withTimestamps();
    }

    public function projects_coimpagency() {
    return $this->belongsToMany('App\Agencies', 'agenciesprojects', 'proj_id', 'agency_id')->withTimestamps();
    }

    public function projects_sectors() {
        return $this->belongsToMany('App\Sectors', 'sectorprojects', 'proj_id', 'sector_id')->withTimestamps();
    }

    public function projects_subsectors() {
        return $this->belongsToMany('App\Subsectors', 'subsectorprojects', 'proj_id', 'subsector_id')->withTimestamps();
    }

    public function projects_statuses() {
        return $this->belongsToMany('App\Statuses', 'statusprojects', 'proj_id', 'status_id')->withTimestamps();
    }

    public function projects_otherpdps() {
        return $this->belongsToMany('App\Chapters', 'otherpdpprojects', 'proj_id', 'chap_id')->withTimestamps();
    }

    public function projects_agendas() {
        return $this->belongsToMany('App\Agendas', 'agendaprojects', 'proj_id', 'agenda_id')->withTimestamps();
    }

    public function projects_sdgs() {
        return $this->belongsToMany('App\Goals', 'sdgprojects', 'proj_id', 'sdg_id')->withTimestamps();
    }

    public function projects_funding_s() {
        return $this->belongsToMany('App\FinanceSourceProjects', 'finance_source_projects', 'proj_id', 'fsource_id')->withTimestamps();
    }

    public function projects_fundings() {
        return $this->belongsToMany('App\Fundingsources', 'fprojects', 'proj_id', 'fsource_no')->withTimestamps();
    }

    public function projects_1rms() {
        return $this->belongsToMany('App\_1matrices', '_1matricesprojects', 'proj_id', '_1rm_id')->withTimestamps();
    }

    public function projects_2rms() {
        return $this->belongsToMany('App\_2matrices', '_2matricesprojects', 'proj_id', '_2rm_id')->withTimestamps();
    }

    public function projects_3rms() {
        return $this->belongsToMany('App\_3matrices', '_3matricesprojects', 'proj_id', '_3rm_id')->withTimestamps();
    }

    public function projects_4rms() {
        return $this->belongsToMany('App\_4matrices', '_4matricesprojects', 'proj_id', '_4rm_id')->withTimestamps();
    }

    public function projects_states() {
        return $this->belongsToMany('App\Regions', 'regionsprojects', 'proj_id', 'region_id')->withTimestamps();
    }

    public function projects_provinces() {
        return $this->belongsToMany('App\Provinces', 'provincesprojects', 'proj_id', 'province_id')->withTimestamps();
    }

    public function projects_cities() {
        return $this->belongsToMany('App\Cities', 'citiesprojects', 'proj_id', 'cities_id')->withTimestamps();
    }

    public function projects_provinces2() {
        return $this->belongsToMany('App\Provinces', 'provincesprojects', 'proj_id', 'province_id')->withTimestamps();
    }

    public function getRegionsProjects($id){
        $regionsProjects = \App\Regionsprojects::where('proj_id', $id)->get();
        return $regionsProjects;
    }

    public function getSectorProjects($id){
        $sectorProjects = \App\Sectorprojects::where('proj_id', $id)->get();
        return $sectorProjects;
    }

    public function getScores($id){
        $sectorProjects = \App\Tripcat2projects::where('proj_id', $id)->get();
        return $sectorProjects;
    }

    public function getScores1($id){
        $sectorProjects = \App\Tripcat1projects::where('proj_id', $id)->get();
        return $sectorProjects;
    }

    public function getScores2($id){
        $sectorProjects = \App\Tripcat2projects::where('proj_id', $id)->get();
        return $sectorProjects;
    }

    public function getProjCost($id){
        $projCosts = \App\Investments::where('proj_id', $id)->get();
        $localsum = 0;
        $loansum = 0;
        $grantsum = 0;
        $goccsum = 0;
        $lgusum = 0;
        $privatesum = 0;
        $othersum = 0;
        foreach($projCosts as $projCost){
            $localsum += (int)$projCost->local;
            $loansum += (int)$projCost->loan;
            $grantsum += (int)$projCost->grant;
            $goccsum += (int)$projCost->gocc;
            $lgusum += (int)$projCost->lgu;
            $privatesum += (int)$projCost->private;
            $othersum += (int)$projCost->others;
        }
        $total = $localsum + $loansum + $grantsum + $goccsum + $lgusum + $privatesum + $othersum;
        $total2 = number_format($total);
        return $total;
    }
    
}

