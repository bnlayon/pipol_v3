<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public function getSubmissiondetails($id){
    	$submissionDetails = \App\Submissions::where('id', $id)->get();
    	return $submissionDetails;
    }

    public function getAgencyDetails($id){
    	$agencyDetails = \App\Agencies::where('id', $id)->get();
    	return $agencyDetails;
    }
    protected $fillable = [
        'lname',
        'fname',
        'mname',
        'designation',
        'telnumber',
        'faxnumber',
        'email',
        'password',
        'user_type',
        'submission_id',
        'status',
        'focal_no',
        'username',
        'is_first',
        'gender'
    ];
}
