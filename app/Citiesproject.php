<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citiesproject extends Model
{
    protected $fillable = ['proj_id', 'cities_id'];
}
