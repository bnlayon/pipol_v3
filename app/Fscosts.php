<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fscosts extends Model
{
    protected $fillable = ['proj_id', 'fsyear', 'fscost'];
}
