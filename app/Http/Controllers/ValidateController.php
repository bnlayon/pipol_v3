<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ValidateController extends Controller
{

    public function validateproject(){
        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        return view('validateproject', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices'));
    }

    public function tripvalidateproject(){
        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        return view('tripvalidateproject', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices'));
    }

    public function reclassification(){
        $chapters = \App\Chapters::all();
        $requests = \App\Reclassification::all();
        $myrequests = \App\Reclassification::all();

        $number = auth()->user()->focal_no;
        $chapters_focals = \App\Focals::where('focal_id', $number)->get();

        return view('reclassification', compact('chapters','requests', 'myrequests','chapters_focals'));
    }

}
