<?php

namespace App\Http\Controllers;

use Alert;
use App\Facost;
use App\Rccost;
use App\Fscosts;
use App\Projects;
use App\Rowacost;
use App\Investments;
use App\Sdgprojects;
use App\Citiesproject;
use App\Statusprojects;
use App\Infrastructures;
use App\Regionsprojects;
use App\Provincesprojects;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\FinanceSourceProjects;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AddProject1Controller extends Controller
{
    public function savetodb(Request $request)
    {
        // dd($request);
        //Code
        $codeforUpdating = 2020;
        $agency = DB::table('submissions as A')
            ->join('agencies as B', 'A.agency_id', '=', 'B.id')
            ->where('A.id', '=', auth()->user()->submission_id)
            ->select('agency_id', 'UACS_DPT_ID', 'UACS_AGY_ID')
            ->first();
        $uacs = $agency->UACS_DPT_ID . $agency->UACS_AGY_ID;
        $partialCode = $codeforUpdating . '-' . $uacs . '-';
        $project = Projects::select('code')->where('agency_id', $agency->agency_id)->get();
        if ($project->count() == 0) {
            $code = "000001";
        }else {
            $code = Str::substr($project->last()->code, -6);
        }
        $finalcode = sprintf("%06d", $code + 1);
        //Level of approval
        $LevelofApproval = $this->getLevelofApproval(Request('selectedLevelofApproval'));
        //Save Project
        $project = new Projects;
        $project->code              = $partialCode . $finalcode;

        if(Request('selectedAttachAgency') == []){
            $project->agency_id         = $agency->agency_id;
            $project->motheragency_id   = 0;
        }else{
            $project->agency_id         = Request('selectedAttachAgency')[0]['id'];
            $project->motheragency_id   = Request('selectedAttachAgency')[0]['motheragency_id']; 
        }


        $project->title             = Request('projtitle');
        $project->prog_proj         = Request('progproj');
        $project->description       = Request('project_components');
        $project->output            = Request('expectedoutput');
        $project->spatial           = Request('selectedCoverage')['name'];
        $project->iccable           = (int) Request('iccable');
        $project->currentlevel      = $LevelofApproval;
        if (Request('selectedLevelofApproval') == 1) {
            $project->approval_date1    = Request('levelofapprovaldate');
        } else if (Request('selectedLevelofApproval') == 2) {
            $project->approval_date2    = Request('levelofapprovaldate');
        } else if (Request('selectedLevelofApproval') == 3) {
            $project->approval_date3    = Request('levelofapprovaldate');
        } else if (Request('selectedLevelofApproval') == 4) {
            $project->approval_date4    = Request('levelofapprovaldate');
        } else if (Request('selectedLevelofApproval') == 5) {
            $project->approval_date5    = Request('levelofapprovaldate');
        }
        $project->pip               = 1;
        $project->pip_typo          = Request('selectedPIP');
        $project->cip               = Request('cipRadio');
        $project->cip_typo          = Request('selectedCIP');
        $project->rdip              = Request('selectedRDIP');
        if (in_array("3", Request('selectedProgDocument1'))) {
            $project->trip          = 1;
        } else {
            $project->trip          = 0;
        }
        $project->rdc_endorsement   = Request('selectedEndorsement');
        if (Request('selectedEndorsement') == 1) {
            $project->rdc_endorsed_notendorsed = "Endorsed";
            $project->rdc_date_endorsement = Request('dateEndorse');
        } else if (Request('selectedEndorsement') == 2) {
            $project->rdc_endorsed_notendorsed = "Yet to be Endorsed";
        }
        //TRIP
        $project->risk                  = Request('ImplementationRisks');
        $project->mainpdp               = Request('selectedPdpChapter')['chap_no'];
        $project->start                 = Request('startYear');
        $project->end                   = Request('endYear');
        // Project Preparation
        $project->ppdetails             = Request('selectedProjectPrep')['id'];
        $project->ppdetails_others      = Request('othersProjectPrep');
        $project->fsstatus              = Request('selectedFSStatus')['id'];
        $project->fsassistance          = Request('selectedFSAssistance');
        $project->fsstatus_ongoing      = Request('fsstatus_ongoing');
        $project->fsstatus_prep         = Request('fsstatus_prep');
        $project->rowa                  = Request('rowa1');
        $project->rowa_affected         = Request('rowa_affected');
        $project->rc                    = Request('rowa2');
        $project->rc_affected           = Request('rc_affected');
        $project->wrrc                  = Request('rowa3');
        $project->employment            = Request('employmentgeneration');
        $project->modeofimplementation  = Request('selectedModeofImplementation')['mode_no'];
        $project->category              = Request('selectedCategory')['name'];
        $project->tier1_uacs            = Request('tier1Uacs');
        $project->tier2_uacs            = Request('tier2Uacs');
        $project->tier2_type            = Request('selectedTier2Option')['name'];
        //Level of Readiness CIP/NON CIP
        $project->tier2_status          = Request('selectedLevelofReadiness')['id'];
        //Physical and Financial Status Updates 
        $project->updates               = Request('updates');
        $project->asof                  = Request('asof');
        $project->other_fs              = Request('other_fs');
        $project->other_mode            = Request('otherMode');
        $project->reg_prog              = Request('selectedRegProg');
        $project->rd                    = Request('selectedRND');
        $project->NA_rm                 = Request('NA_rm');
        $project->NA_rm                 = Request('NA_rm');
        //New Field
        $project->regprogtitle          =Request('regprogtitle');
        //Physical Accomplishment
        $project->implementationreadiness   =Request('selectedImplementation')['id'];
        $project->statusofsubmission    = Request('statusofsubmission');

        $project->gender                = Request('selectedLevelofGad');
        $project->rdc_endorsement       = Request('chkrdcyesno');
        $project->rdc_date_endorsement  = Request('dateEndorse');
        $project->rdc_endorsed_notendorsed  = Request('selectedEndorsement');

        $project->save();

        //Spatial Coverage
        if (Request('selectedCoverage')['name'] == "Interregional") {
            $this->SaveInterregional(
                $project->id,
                Request('selectedRegions'),
                Request('selectedProvinces'),
                Request('selectedCities')
            );
        }
        if (Request('selectedCoverage')['name'] == "Region Specific") {
            $this->SaveRegionSpec(
                $project->id,
                Request('selectedRegionsRegional'),
                Request('selectedProvincesRegional'),
                Request('selectedCitiesRegionspecific')
            );
        }
        // Basis of Implementation
        if (!is_null(Request('selectedBasis'))) {
            foreach (Request('selectedBasis') as $basis_to_save) {
                $project_basis                       = new \App\Basisprojects();
                $project_basis->proj_id              = $project->id;
                $project_basis->basis_id             = $basis_to_save['id'];
                $project_basis->save();
            }
        }
        //Infra Sector
        if (!is_null(Request('selectedSector'))) {
            foreach (Request('selectedSector') as $infras_sector_to_save) {
                $project_sector                       = new \App\Sectorprojects();
                $project_sector->proj_id              = $project->id;
                $project_sector->sector_id            = $infras_sector_to_save['id'];
                $project_sector->save();
            }
        }
        //Infra Subsector
        if (!is_null(Request('selectedSubSector'))) {
            foreach (Request('selectedSubSector') as $infras_subsector_to_save) {
                $project_subsector                       = new \App\Subsectorprojects();
                $project_subsector->proj_id              = $project->id;
                $project_subsector->subsector_id         = $infras_subsector_to_save['id'];
                $project_subsector->save();
            }
        }
        //Status of Implementation  TRIP
        if (Request('selectedStatus') != null) {
            $this->SaveStatuses($project->id, Request('selectedStatus'));
        }
        // Other PDP Chapter
        if (!is_null(Request('selectedOtherPdpChapter'))) {
            foreach (Request('selectedOtherPdpChapter') as $otherpdp_to_save) {
                $project_otherpdp                       = new \App\Otherpdpprojects();
                $project_otherpdp->proj_id              = $project->id;
                $project_otherpdp->chap_id              = $otherpdp_to_save['chap_no'];
                $project_otherpdp->save();
            }
        }
        // Ten Point Agenda
        if (!is_null(Request('selectedTenPoint'))) {
            foreach (Request('selectedTenPoint') as $agenda_to_save) {
                $project_agenda_to_save                       = new \App\Agendaprojects();
                $project_agenda_to_save->proj_id              = $project->id;
                $project_agenda_to_save->agenda_id           = $agenda_to_save['id'];
                $project_agenda_to_save->save();
            }
        }
        // SDG Projects
        if (Request('selectedSdg') != null) {
            $this->SaveSdg($project->id, Request('selectedSdg'));
        }
        //ODA Funding
        if (!is_null(Request('selectedODAFunding'))) {
            foreach (Request('selectedODAFunding') as $oda_to_save) {
                $project_oda_to_save                       = new \App\Fprojects();
                $project_oda_to_save->proj_id              = $project->id;
                $project_oda_to_save->fsource_no           = $oda_to_save['fsource_no'];
                $project_oda_to_save->save();
            }
        }
        //Co Implementing Agency
        if (!is_null(Request('selectedCoAgency'))) {
            foreach (Request('selectedCoAgency') as $comimp_to_save) {
                $project_coimp                      = new \App\Agenciesproject();
                $project_coimp->proj_id             = $project->id;
                $project_coimp->agency_id           = $comimp_to_save['id'];
                $project_coimp->save();
            }
        }
        //Attach Implementing Agency
        if (!is_null(Request('selectedAttachAgency'))) {
            foreach (Request('selectedAttachAgency') as $att_to_save) {
                $project_att                     = new \App\Attagenciesproject();
                $project_att->proj_id             = $project->id;
                $project_att->agency_id           = $att_to_save['id'];
                $project_att->save();
            }
        }
        // Project Prep FS Cost Table
        if (Request('selectedProjectPrep')['id'] == 1) {
            $this->SaveFSCost(
                $project->id,
                Request('fs_2017'),
                Request('fs_2018'),
                Request('fs_2019'),
                Request('fs_2020'),
                Request('fs_2021'),
                Request('fs_2022')
            );
        }
        // ROWA
        if (Request('rowa1') == 1) {
            $this->SaveROWA(
                $project->id,
                Request('rowa1_2017'),Request('rowa1_2018'),Request('rowa1_2019'),Request('rowa1_2020'),
                Request('rowa1_2021'),Request('rowa1_2022')
            );
        }
        // RC
        if (Request('rowa2') == 1) {
            $this->SaveRC(
                $project->id,
                Request('rowa2_2017'),Request('rowa2_2018'),Request('rowa2_2019'),Request('rowa2_2020'),
                Request('rowa2_2021'),Request('rowa2_2022')
            );
        }
        // Financial Accomplishments
        $this->FinancialAccomplishments(
            $project->id,
            Request('nep_2017'),Request('nep_2018'),Request('nep_2019'),Request('nep_2020'),
            Request('nep_2021'),Request('nep_2022'),
            Request('all_2017'),Request('all_2018'),Request('all_2019'),Request('all_2020'),
            Request('all_2021'),Request('all_2022'),
            Request('ad_2017'),Request('ad_2018'),Request('ad_2019'),Request('ad_2020'),
            Request('ad_2021'),Request('ad_2022')
        );
        //Investment Targets (Project Cost)
        $this->ProjectCost(
            $project->id,
            Request('nglocal2016'),Request('nglocal2017'),Request('nglocal2018'),Request('nglocal2019'),Request('nglocal2020'),
            Request('nglocal2021'),Request('nglocal2022'),Request('nglocalCont'),
            Request('ngloan2016'),Request('ngloan2017'),Request('ngloan2018'),Request('ngloan2019'),Request('ngloan2020'),
            Request('ngloan2021'),Request('ngloan2022'),Request('ngloanCont'),
            Request('nggrant2016'),Request('nggrant2017'),Request('nggrant2018'),Request('nggrant2019'),Request('nggrant2020'),
            Request('nggrant2021'),Request('nggrant2022'),Request('nggrantCont'),
            Request('gocc2016'),Request('gocc2017'),Request('gocc2018'),Request('gocc2019'),Request('gocc2020'),
            Request('gocc2021'),Request('gocc2022'),Request('goccCont'),
            Request('lgu2016'),Request('lgu2017'),Request('lgu2018'),Request('lgu2019'),Request('lgu2020'),
            Request('lgu2021'),Request('lgu2022'),Request('lguCont'),
            Request('private2016'),Request('private2017'),Request('private2018'),Request('private2019'),Request('private2020'),
            Request('private2021'),Request('private2022'),Request('privateCont'),
            Request('others2016'),Request('others2017'),Request('others2018'),Request('others2019'),Request('others2020'),
            Request('others2021'),Request('others2022'),Request('othersCont')
        );
        //Infrastructure Cost
        $this->InfraCost(
            $project->id,
            Request('icnglocal2016'),Request('icnglocal2017'),Request('icnglocal2018'),Request('icnglocal2019'),Request('icnglocal2020'),
            Request('icnglocal2021'),Request('icnglocal2022'),Request('icnglocalCont')
        );
        // Regional Breakdown
        $this->SaveRegionalBreakdown($project->id, Request('selectedInterregionals'));
        // Results Matrices
        $this->SaveRM($project->id, Request('selectedRM1'),Request('selectedRM2'),
        Request('selectedRM3'),Request('selectedRM4'));

        //Funding Source
        if (!is_null(request('selectedFundingSource'))) {
            foreach (request('selectedFundingSource') as $key => $fs) {
                FinanceSourceProjects::create([
                    'proj_id' => $project->id,
                    'fsource_id' => $fs
                ]);
            }
        }

        // Project Logs
        if (request('statusofsubmission') == 'Draft') {
            $projectlogs                         = new \App\Projectlogs();
            $projectlogs->username                   = auth()->user()->SiderbarName();
            $projectlogs->ipaddress                  = request()->ip();
            $projectlogs->activity                   = "Added Project as Draft";
            $projectlogs->proj_id                    = $project->id;
            $projectlogs->save();
        } else if (request('statusofsubmission') == 'Endorsed') {
            $projectlogs                         = new \App\Projectlogs();
            $projectlogs->username                   = auth()->user()->SiderbarName();
            $projectlogs->ipaddress                  = request()->ip();
            $projectlogs->activity                   = "Added Project as Endorsed";
            $projectlogs->proj_id                    = $project->id;
            $projectlogs->save();
        }

        return response()->json(Request('selectedStatus'));
    }
    function getLevelofApproval($selectedLevelofApproval)
    {
        if ($selectedLevelofApproval == 1) {
            return $selected = "Yet to be submitted to NEDA Secretariat";
        } elseif ($selectedLevelofApproval == 2) {
            return $selected = "Under the NEDA Secretariat Review";
        } elseif ($selectedLevelofApproval == 3) {
            return $selected = "ICC-TB Endorsed";
        } elseif ($selectedLevelofApproval == 4) {
            return $selected = "ICC-CC Approved";
        } elseif ($selectedLevelofApproval == 5) {
            return $selected = "NEDA Board Confirmed";
        }
    }
    //TRIP Status
    function SaveStatuses($projectid, $selectedStatus)
    {
        foreach ($selectedStatus as $key => $status) {
            Statusprojects::create([
                'proj_id'   => $projectid,
                'status_id' => $status['id']
            ]);
        }
    }
    // SDG Projects
    function SaveSdg($projectid, $selectedSdg)
    {
        foreach ($selectedSdg as $key => $sdg) {
            Sdgprojects::create([
                'proj_id'   => $projectid,
                'sdg_id'    => $sdg['goal_id']
            ]);
        }
    }
    //Interregional
    function SaveInterregional($projectid, $regions, $provinces, $cities)
    {
        if (!is_null($regions)) {
            foreach ($regions as $region) {
                Regionsprojects::create([
                    'proj_id' => $projectid,
                    'region_id' => $region['regional_code']
                ]);
            }
        }
        if (!is_null($provinces)) {
            foreach ($provinces as $province) {
                Provincesprojects::create([
                    'proj_id' => $projectid,
                    'province_id' => $province['id']
                ]);
            }
        }
        if (!is_null($cities)) {
            foreach ($cities as $city) {
                Citiesproject::create([
                    'proj_id' => $projectid,
                    'cities_id' => $city['id']
                ]);
            }
        }
    }
    //Regional
    function SaveRegionSpec($projectid, $rsregions, $rsprovinces, $rscities)
    {
        if (!is_null($rsregions)) {
            Regionsprojects::create([
                'proj_id'   => $projectid,
                'region_id' => $rsregions['regional_code']
            ]);
        }
        if (!is_null($rsprovinces)) {
            foreach ($rsprovinces as $rsprovince) {
                Provincesprojects::create([
                    'proj_id' => $projectid,
                    'province_id' => $rsprovince['id']
                ]);
            }
        }
        if (!is_null($rscities)) {
            foreach ($rscities as $rscity) {
                Citiesproject::create([
                    'proj_id' => $projectid,
                    'cities_id' => $rscity['id']
                ]);
            }
        }
    }
    // Project Prep FS Cost
    function SaveFSCost($projectid, $fs_2017, $fs_2018, $fs_2019, $fs_2020, $fs_2021, $fs_2022)
    {
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2017',
            'fscost'    => $fs_2017
        ]);
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2018',
            'fscost'    => $fs_2018
        ]);
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2019',
            'fscost'    => $fs_2019
        ]);
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2020',
            'fscost'    => $fs_2020
        ]);
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2021',
            'fscost'    => $fs_2021
        ]);
        Fscosts::create([
            'proj_id'   => $projectid,
            'fsyear'    => '2022',
            'fscost'    => $fs_2022
        ]);
    }
    // ROWA
    function SaveROWA($projectid, $rowa_2017, $rowa_2018, $rowa_2019, $rowa_2020, $rowa_2021, $rowa_2022) {
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2017',
            'fscost'    => $rowa_2017
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2018',
            'fscost'    => $rowa_2018
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2019',
            'rowacost'    => $rowa_2019
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2019',
            'rowacost'    => $rowa_2019
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2020',
            'rowacost'    => $rowa_2020
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2021',
            'rowacost'    => $rowa_2021
        ]);
        Rowacost::create([
            'proj_id'   => $projectid,
            'rowayear'    => '2022',
            'rowacost'    => $rowa_2022
        ]);
    }
    // RC
    function SaveRC($projectid, $rc_2017, $rc_2018, $rc_2019, $rc_2020, $rc_2021, $rc_2022) {
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2017',
            'rccost'    => $rc_2017
        ]);
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2018',
            'rccost'    => $rc_2018
        ]);
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2019',
            'rccost'    => $rc_2019
        ]);
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2020',
            'rccost'    => $rc_2020
        ]);
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2021',
            'rccost'    => $rc_2021
        ]);
        Rccost::create([
            'proj_id'   => $projectid,
            'rcyear'    => '2022',
            'rccost'    => $rc_2022
        ]);
    }
    // Financial Accomplishments
    function FinancialAccomplishments(
        $projectid,
        $nep_2017, $nep_2018, $nep_2019, $nep_2020, $nep_2021, $nep_2022,
        $all_2017, $all_2018, $all_2019, $all_2020, $all_2021, $all_2022,
        $ad_2017, $ad_2018, $ad_2019, $ad_2020, $ad_2021, $ad_2022) 
    {
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2017,
            'facost_nep'    => $nep_2017,
            'facost_all'    => $all_2017,
            'facost_ad'     => $ad_2017
        ]);
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2018,
            'facost_nep'    => $nep_2018,
            'facost_all'    => $all_2018,
            'facost_ad'     => $ad_2018
        ]);
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2019,
            'facost_nep'    => $nep_2019,
            'facost_all'    => $all_2019,
            'facost_ad'     => $ad_2019
        ]);
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2020,
            'facost_nep'    => $nep_2020,
            'facost_all'    => $all_2020,
            'facost_ad'     => $ad_2020
        ]);
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2021,
            'facost_nep'    => $nep_2021,
            'facost_all'    => $all_2021,
            'facost_ad'     => $ad_2021
        ]);
        Facost::create([
            'proj_id'       => $projectid,
            'fayear'        => 2022,
            'facost_nep'    => $nep_2022,
            'facost_all'    => $all_2022,
            'facost_ad'     => $ad_2022
        ]);
    }
    // Investment Targets (Project Cost)
    function ProjectCost(
        $projectid,
        $nglocal2016, $nglocal2017, $nglocal2018, $nglocal2019, $nglocal2020, $nglocal2021, $nglocal2022, $nglocalCont,
        $ngloan2016, $ngloan2017, $ngloan2018, $ngloan2019, $ngloan2020, $ngloan2021, $ngloan2022, $ngloanCont,
        $nggrant2016, $nggrant2017, $nggrant2018, $nggrant2019, $nggrant2020, $nggrant2021, $nggrant2022, $nggrantCont,
        $gocc2016, $gocc2017, $gocc2018, $gocc2019, $gocc2020, $gocc2021, $gocc2022, $goccCont,
        $lgu2016, $lgu2017, $lgu2018, $lgu2019, $lgu2020, $lgu2021, $lgu2022, $lguCont,
        $private2016, $private2017, $private2018, $private2019, $private2020, $private2021, $private2022, $privateCont,
        $others2016, $others2017, $others2018, $others2019, $others2020, $others2021, $others2022, $othersCont
    ) 
    {
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2016,
            'local'         => $nglocal2016,
            'loan'          => $ngloan2016,
            'grant'         => $nggrant2016,
            'gocc'          => $gocc2016,
            'lgu'           => $lgu2016,
            'private'       => $private2016,
            'others'        => $others2016
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2017,
            'local'         => $nglocal2017,
            'loan'          => $ngloan2017,
            'grant'         => $nggrant2017,
            'gocc'          => $gocc2017,
            'lgu'           => $lgu2017,
            'private'       => $private2017,
            'others'        => $others2017
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2018,
            'local'         => $nglocal2018,
            'loan'          => $ngloan2018,
            'grant'         => $nggrant2018,
            'gocc'          => $gocc2018,
            'lgu'           => $lgu2018,
            'private'       => $private2018,
            'others'        => $others2018
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2019,
            'local'         => $nglocal2019,
            'loan'          => $ngloan2019,
            'grant'         => $nggrant2019,
            'gocc'          => $gocc2019,
            'lgu'           => $lgu2019,
            'private'       => $private2019,
            'others'        => $others2019
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2020,
            'local'         => $nglocal2020,
            'loan'          => $ngloan2020,
            'grant'         => $nggrant2020,
            'gocc'          => $gocc2020,
            'lgu'           => $lgu2020,
            'private'       => $private2020,
            'others'        => $others2020
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2021,
            'local'         => $nglocal2021,
            'loan'          => $ngloan2021,
            'grant'         => $nggrant2021,
            'gocc'          => $gocc2021,
            'lgu'           => $lgu2021,
            'private'       => $private2021,
            'others'        => $others2021
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2022,
            'local'         => $nglocal2022,
            'loan'          => $ngloan2022,
            'grant'         => $nggrant2022,
            'gocc'          => $gocc2022,
            'lgu'           => $lgu2022,
            'private'       => $private2022,
            'others'        => $others2022
        ]);
        Investments::create([
            'proj_id'       => $projectid,
            'year'          => 2023,
            'local'         => $nglocalCont,
            'loan'          => $ngloanCont,
            'grant'         => $nggrantCont,
            'gocc'          => $goccCont,
            'lgu'           => $lguCont,
            'private'       => $privateCont,
            'others'        => $othersCont
        ]);
    }
    // Infrastructure Cost
    function InfraCost(
        $projectid,
        $nglocal2016, $nglocal2017, $nglocal2018, $nglocal2019, $nglocal2020, $nglocal2021, $nglocal2022, 
        $nglocalCont) 
    {
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2016,
            'local'     => $nglocal2016
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2017,
            'local'     => $nglocal2017
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2018,
            'local'     => $nglocal2018
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2019,
            'local'     => $nglocal2019
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2020,
            'local'     => $nglocal2020
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2021,
            'local'     => $nglocal2021
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2022,
            'local'     => $nglocal2022
        ]);
        Infrastructures::create([
            'proj_id'   => $projectid,
            'year'      => 2023,
            'local'     => $nglocalCont
        ]);
    }
    // Regional Breakdown
    function SaveRegionalBreakdown($projectid, $selectedInterregionals) {
        if (!is_null($selectedInterregionals)) {
            foreach ($selectedInterregionals as $region) {
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2016,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2016']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2017,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2017']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2018,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2018']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2019,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2019']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2020,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2020']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2021,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2021']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2022,
                    'proj_id'       => $projectid,
                    'cost'          => $region['value2022']
                ]);
                \App\Regionalcosts::create([
                    'region'        => $region['regional_code'],
                    'year'          => 2023,
                    'proj_id'       => $projectid,
                    'cost'          => $region['valueCont']
                ]);
            }
        }
    }
    function SaveRM($projectid, $selectedRM1, $selectedRM2, $selectedRM3, $selectedRM4) {
        if (!is_null($selectedRM1)) {
            foreach ($selectedRM1 as $_1rm_to_save) {
                $project_1rm                      = new \App\_1matricesprojects();
                $project_1rm->proj_id             = $projectid;
                $project_1rm->_1rm_id             = $_1rm_to_save['rm1_id'];
                $project_1rm->save();
            }
        }
        if (!is_null($selectedRM2)) {
            foreach ($selectedRM2 as $_2rm_to_save) {
                $project_2rm                      = new \App\_2matricesprojects();
                $project_2rm->proj_id             = $projectid;
                $project_2rm->_2rm_id             = $_2rm_to_save['rm2_id'];
                $project_2rm->save();
            }
        }
        if (!is_null($selectedRM3)) {
            foreach ($selectedRM3 as $_3rm_to_save) {
                $project_3rm                      = new \App\_3matricesprojects();
                $project_3rm->proj_id             = $projectid;
                $project_3rm->_3rm_id             = $_3rm_to_save['rm3_id'];
                $project_3rm->save();
            }
        }
        if (!is_null($selectedRM4)) {
            foreach ($selectedRM4 as $_4rm_to_save) {
                $project_4rm                      = new \App\_4matricesprojects();
                $project_4rm->proj_id             = $projectid;
                $project_4rm->_4rm_id             = $_4rm_to_save['rm4_id'];
                $project_4rm->save();
            }
        }
    }
}
