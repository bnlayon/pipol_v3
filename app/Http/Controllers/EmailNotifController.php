<?php

namespace App\Http\Controllers;

use Alert;
use App\Http\Requests;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Http\Controllers\Controller;

class EmailNotifController extends Controller
{
    public function emailnotif(){
        $uniqueAgencys = \App\Submissions::select('agency_id', 'head_email', 'id', 'mother_email', 'midpoint', 'midpoint_time', 
        'final', 'final_time', 'validated', 'validated_time')
            ->whereNotNull('agency_id')
            ->groupBy('agency_id', 'head_email', 'id', 'mother_email', 'midpoint', 'midpoint_time', 'final', 'final_time', 'validated', 'validated_time')
            ->get();
        $agencytype = $this->getAgencyType();
        return view('emailnotif', compact('uniqueAgencys', 'agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
