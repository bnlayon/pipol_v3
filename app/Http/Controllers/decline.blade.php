<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		p{
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

<p><b>Agency:</b> <?php echo $agencies->UACS_AGY_DSC; ?></p>

<p><b>Agency PIP/TRIP Focal:</b> <?php echo $user->lname.", ".$user->fname." ".$user->mname; ?></p>

<p>Dear Sir/Ma'am: </p>

<p>This is to inform you that you were unable to successfully accomplish the PIPOL System Online Sign-up Sheet due to the following reason/s:</p>

<ul>
	<li><p><?php echo $reason; ?></p></li>
</ul>                                                                     

<p>Kindly address the above-mentioned deficiency as soon as possible and accomplish again the PIP System Online Sign-up Sheet so the Authorized Agency PIP/TRIP Focal(s) can be issued with their respective username and password. Please be informed that the PIPOL System v2.0 will be open to Agency PIP/TRIP Focals <b>starting September 19, 2018 until October 26, 2018</b>for the submission of priority programs and projects (PAPs) for inclusion in the Updated 2017-2022 PIP and TRIP for FY 2020-2022 as input to the FY 2020 Budget Preparation.</p>

<p>For any inquiries, please do not hesitate to coordinate with the PIP Secretariat of the NEDA-Public Investment Staff at contact numbers: DL 631-2165 or TL 631-0945 local 404 or through e-mail address: pip@neda.gov.ph. </p>

<p>Thank you. </p>

<p><b>PIP Secretariat</p>

</body>
</html>