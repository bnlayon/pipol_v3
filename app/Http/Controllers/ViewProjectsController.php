<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ViewProjectsController extends Controller
{

    public function viewprojects(){
        if(auth()->user()->user_type == 'AG'){
            $submission = \App\Submissions::find(auth()->user()->submission_id);
            $pipprojects = \App\Projects::
            leftJoin('implementations','projects.implementationreadiness','=','implementations.implementation')
            ->where('agency_id', '=', $submission->agency_id)->where('pip', '=', 1)
                // ->where(function($q) {
                //     $q->where('SS_statusofsubmission', 'Validated');
                // })
                ->where(function ($q) {
                    $q->where('category', '!=', 'Completed')
                    ->where('category', '!=', 'Dropped')
                    ->orWhereNull('category');
                })
            ->select('projects.id','projects.code','projects.title','projects.spatial','projects.mainpdp','projects.statusofsubmission','implementations.implementation')
            ->get();

            $tripprojects = \App\Projects::
            leftJoin('implementations','projects.implementationreadiness','=','implementations.implementation')
            ->where('agency_id', '=', $submission->agency_id)->where('trip', '=', 1)
            ->where(function($q) {
                $q->where('SS_statusofsubmission', 'Validated');
            })->get();

            $droppedprojects = \App\Projects::
             leftJoin('implementations','projects.implementationreadiness','=','implementations.implementation')
             ->where('agency_id', '=', $submission->agency_id)->where('category', '=', 'Dropped')->get();

            $completedprojects = \App\Projects::
             leftJoin('implementations','projects.implementationreadiness','=','implementations.implementation')
             ->where('agency_id', '=', $submission->agency_id)->where('category', '=', 'Completed')->get();

            $draftprojects = \App\Projects::
             leftJoin('implementations','projects.implementationreadiness','=','implementations.implementation')
             ->where('agency_id', '=', $submission->agency_id)->where('pip', '=', 0)
            //->where(function($q) {
            //    $q->where('statusofsubmission', 'Endorsed')
            //    ->where('SS_statusofsubmission', '!=', 'Validated')
            //    ->orWhereNull('SS_statusofsubmission');
            //})
            ->get();
        }

        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        
        return view('viewprojects', compact('submission', 'pipprojects', 'tripprojects', 'droppedprojects', 'completedprojects','fundingsources', 'agencytype','draftprojects'));
    }

    public function viewprojectsattached(){
        if(auth()->user()->user_type == 'AG'){
            $submission = \App\Submissions::find(auth()->user()->submission_id);

            $getIdsOfAttached = \App\Agencies::select('id')->where('motheragency_id', $submission->agency_id)->get();

            $pipprojects = \App\Projects::whereIn('agency_id', $getIdsOfAttached)->where('pip', '=', 1)
            // ->where(function($q) {
            //     $q->where('SS_statusofsubmission', 'Validated');
            // })
            ->get();

            $tripprojects = \App\Projects::whereIn('agency_id', $getIdsOfAttached)->where('trip', '=', 1)
            ->where(function($q) {
                $q->where('statusofsubmission', 'Draft')
                ->orWhere('statusofsubmission', 'Endorsed');
            })->get();

            $droppedprojects = \App\Projects::whereIn('agency_id', $getIdsOfAttached)->where('category', '=', 'Dropped')->get();
            $completedprojects = \App\Projects::whereIn('agency_id', $getIdsOfAttached)->where('category', '=', 'Completed')->get();
        }

        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        
        return view('viewprojectsattached', compact('submission', 'pipprojects', 'tripprojects', 'droppedprojects', 'completedprojects','fundingsources', 'agencytype', 'getIdsOfAttached'));
    }

    public function viewprojectsprint($id) {

 $id = decrypt($id);
        $project = \App\Projects::find($id);
        $imp_agency = \App\Agencies::find($project->agency_id);

        $co_agenciesIds = \App\Agenciesproject::where('proj_id', $project->id)->get();
        $co_agencies = array();
        foreach ($co_agenciesIds as $agencies) {
            $temp = \App\Agencies::find($agencies->agency_id); 
            if($temp!=null) 
                $co_agencies[] = $temp->Abbreviation;
        }

        $att_agenciesIds = \App\Attagenciesproject::where('proj_id', $project->id)->get();
        $att_agencies = array();
        foreach ($att_agenciesIds as $agencies) {
            $temp = \App\Agencies::find($agencies->agency_id); 
            if($temp!=null)
                $att_agencies[] = $temp->Abbreviation;
        }

        $region_projectIds = \App\Regionsprojects::where('proj_id', $project->id)->get();
        $regions = array();
        foreach ($region_projectIds as $region) {
            $temp = \App\Regions::where('regional_code',$region->region_id)->first();
            if($temp!=null)
                $regions[] = $temp->region_description;
        }

        $province_projectIds = \App\Provincesprojects::where('proj_id', $project->id)->get();
        $provinces = array();
        foreach ($province_projectIds as $province) {
            $temp = \App\Provinces::where('id', $province->province_id)->first();
            if($temp!=null)
                $provinces[] = $temp->provName;
        }

        $temp = \App\Chapters::where('chap_no', $project->mainpdp)->first();
        $chapter="";
        if($temp!=null)
            $chapter = $temp->chap_description;
        // dd($chapter);
        $basesIds = \App\Basisprojects::where('proj_id', $project->id)->get();
        $bases = array();
        foreach ($basesIds as $base) {
            $bases[] = \App\Basis::find($base->basis_id)->basis;
        }
        
        $agendaIds = \App\Agendaprojects::where('proj_id', $project->id)->get();
        $agendas = array();
        foreach ($agendaIds as $agenda) {
            $agendas[] = \App\Agendas::find($agenda->agenda_id);
        }

        $mainfsource = $project->mainfsource;
        $temp = \App\Fundingsources::where('fsource_no', $mainfsource)->first();
        $fsource = null;
        if($temp!=null)
            $fsource = $temp->fsource_description;

        $fsourcecountries = null;
        if($mainfsource == 2 || $mainfsource == 3) {
            $fsourceIds = \App\Fprojects::where('proj_id', $project->id)->get();
            foreach ($fsourceIds as $fs) {
                $temp = \App\Fundings::where('fsource_no', $fs->fsource_no)->first();
                if($temp!=null)
                    $fsourcecountries[] = $temp->fsource_description;
            }
        }

        $finance_source_projects = \App\FinanceSourceProjects::where('proj_id', $project->id)->get();

        $progproj = ($project->prog_proj == 1) ? "Program" : "Project";

        $projectcost2016 = \App\Investments::where('proj_id', $project->id)->where('year', '2016')->get();

        $projectcost2017 = \App\Investments::where('proj_id', $project->id)->where('year', '2017')->get();

        $projectcost2018 = \App\Investments::where('proj_id', $project->id)->where('year', '2018')->get();

        $projectcost2019 = \App\Investments::where('proj_id', $project->id)->where('year', '2019')->get();

        $projectcost2020 = \App\Investments::where('proj_id', $project->id)->where('year', '2020')->get();

        $projectcost2021 = \App\Investments::where('proj_id', $project->id)->where('year', '2021')->get();

        $projectcost2022 = \App\Investments::where('proj_id', $project->id)->where('year', '2022')->get();

        $projectcost2023 = \App\Investments::where('proj_id', $project->id)->where('year', '2023')->get();

        $agencytype = $this->getAgencyType();

        return view('viewprojectsprint', compact('project', 'imp_agency', 'co_agencies', 'att_agencies', 'regions', 'provinces', 'chapter', 'bases', 'agendas', 'fsource', 'fsourcecountries', 'progproj', 'regions', 'agencytype','finance_source_projects', 'projectcost2016','projectcost2017', 'projectcost2018', 'projectcost2019', 'projectcost2020', 'projectcost2021', 'projectcost2022', 'projectcost2023'));
    }
    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
