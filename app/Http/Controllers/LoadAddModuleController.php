<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Alert;
use App\Citiesproject;
use App\City;
use App\Fscosts;
use App\Product;
use App\Document;
use App\Projects;
use App\Province;
use App\Sdgprojects;
use App\Submissions;
use App\Http\Requests;
use App\Statusprojects;
use App\Regionsprojects;
use App\Provincesprojects;
use Illuminate\Support\Str;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LoadAddModuleController extends Controller
{

    public function addproject(){

        if(auth()->user()->user_type == 'AG'){
            $submission = \App\Submissions::find(auth()->user()->submission_id);
            if($submission->motheragency_id == 0){
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = "";
            }else{
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = \App\Agencies::where('id', $submission->motheragency_id)->first();
            }
        }else{

        }
        
        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        $tier2statuses = \App\Tier2statuses::all();
        $tier1statuses = \App\Tier1statuses::all();
        $agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        $provinces = \App\Provinces::all();
        $fsstatuses = \App\Fsstatuses::all();

        $agencytype = $this->getAgencyType();

        return view('addproject', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices', 'tier2statuses', 'submission', 'agency_agency', 'agency_mother', 'agencies', 'provinces', 'tier1statuses', 'fsstatuses', 'agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }

    public function addproject1()
    {

        if (auth()->user()->user_type == 'AG') {
            $submission = \App\Submissions::find(auth()->user()->submission_id);
            if ($submission->motheragency_id == 0) {
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = "";
            } else {
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = \App\Agencies::where('id', $submission->motheragency_id)->first();
            }
        } else {
            $submission = '';
            $agency_agency = '';
            $agency_mother = '';
        }

        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        $tier2statuses = \App\Tier2statuses::all();
        $tier1statuses = \App\Tier1statuses::all();
        $agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        $provinces = \App\Provinces::all();
        $fsstatuses = \App\Fsstatuses::all();

        $agencytype = $this->getAgencyType();

        return view('addproject1', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices', 'tier2statuses', 'submission', 'agency_agency', 'agency_mother', 'agencies', 'provinces', 'tier1statuses', 'fsstatuses', 'agencytype'));
    }

    public function asyncFindRegions(Request $request)
    {
        $regions =\App\Regions::where('region_description', 'like', '%' . Request(' query') . '%')->get();
        return response()->json($regions);
    }

    public function getProvinces(Request $request)
    {
        if ($request->remove) {
            foreach ($request->region as $region) {
                $provinces[] = \App\Provinces::where('regionNo', $region['regional_code'])
                    ->select('id', 'provName', 'regionNo')
                    ->get();
            }
            return response()->json( $provinces);
        }

        $provinces =\App\Provinces::where('regionNo', $request->region['regional_code'])
        ->select('id', 'provName', 'regionNo')
        ->get();
        return response()->json( $provinces);
    }

    public function getCities(Request $request)
    {
        //Remove Options
        if ($request->remove) {
            foreach ( $request->city as $city) {
                $cities[] = \App\City::where('provNo', $city['id'])
                    ->select('id', 'cityName', 'provNo')
                    ->get();
            }
            return response()->json( $cities);
        }
        //Add Options
        $cities =\App\City::where('provNo', $request->city['id'])
        ->select('id', 'cityName', 'provNo')
        ->get();
        return response()->json( $cities);
        // return response()->json( $request);
    }
    public function getRegions()
    {
        $regions = \App\Regions::all();
        return response()->json($regions);
    }
    public function getLevelsOfGAD()
    {
        $levels = \App\Levels::all();
        return response()->json($levels);
    }
    public function getTier1Statuses()
    {
        $tier1statuses = \App\Tier1statuses::all();
        $tier2statuses = \App\Tier2statuses::all();
        return response()->json([
            "tier1statuses" => $tier1statuses,
            "tier2statuses" => $tier2statuses
            ]);
    }
    public function edit()
    {
        if (auth()->user()->user_type == 'AG') {
            $submission = \App\Submissions::find(auth()->user()->submission_id);
            if ($submission->motheragency_id == 0) {
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = "";
            } else {
                $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
                $agency_mother = \App\Agencies::where('id', $submission->motheragency_id)->first();
            }
        } else { }

        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        $tier2statuses = \App\Tier2statuses::all();
        $tier1statuses = \App\Tier1statuses::all();
        $agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        $provinces = \App\Provinces::all();
        $fsstatuses = \App\Fsstatuses::all();

        $agencytype = $this->getAgencyType();

        return view('editproject1', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices', 'tier2statuses', 'submission', 'agency_agency', 'agency_mother', 'agencies', 'provinces', 'tier1statuses', 'fsstatuses', 'agencytype'));
    }
}
