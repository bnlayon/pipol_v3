<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LoginUserController extends Controller
{
    public function login_user(Request $request) {
       
        $username = request('loginUsername');
        $password = request('loginPassword');
        $token = $request->input('g-recaptcha-response');
        
    if($token){
            if(!auth()->attempt(['username' => $username, 'password' => $password])) {
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);        
                
            }

            $user = \App\Users::where('username', $username)->first();

            if($user->status == 1) {
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
                return back()->withErrors([
                    'message' => 'Account is locked. Please contact PIP Secretariat.'
                ]);
                
            }

            if($user->is_first == 1){
               return redirect('/changepassword');
            }else{
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Successful';
                $logs->username  = $username;
                $logs->save();
                return redirect('/dashboard');
            }
        }else{
            return back()->withErrors([
                    'message' => 'Please fill up the captcha'
            ]);
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
        }
    } 

    public function logout() {
        auth()->logout();
        Session::flush();
        return redirect('/');
    }


}
