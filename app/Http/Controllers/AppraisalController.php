<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use App\Province;

use App\Projects;
use App\Chapters;
use App\Fundingsources;

use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class AppraisalController extends Controller
{

    public function appraisal(){
        $focals = \App\Focals::where('focal_id','=', auth()->user()->focal_no)->get();

        foreach($focals as $focal){
            $chaps[] = $focal->chap;
        }

        $allpips = \App\Projects::where('pip', '=', 1)
                ->where('statusofsubmission', 'Endorsed')
                ->whereIn('mainpdp', $chaps)
                ->get();

        //$allpips = \App\Projects::where('pip', '=', 1)->where(function($q) {
                //$q->where('SS_statusofsubmission', 'Validated');
                // ->orWhere('statusofsubmission', 'Reviewed')
                // ->orWhere('statusofsubmission', 'Validated');
            //})
        //->where('category', '!=' , 'Dropped')
        //->where('category', '!=' , 'Completed')
        // ->where('SS_statusofsubmission', '!=' , 'Uncategorized')
        
        
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('appraisal', compact('chapters', 'allpips','fundingsources','agencytype','focals'));
    }

    public function cip(){


        $allpips = \App\Projects::where('cip', '=', 1)
                ->where('statusofsubmission', 'Endorsed')
                ->get();

        //$allpips = \App\Projects::where('pip', '=', 1)->where(function($q) {
                //$q->where('SS_statusofsubmission', 'Validated');
                // ->orWhere('statusofsubmission', 'Reviewed')
                // ->orWhere('statusofsubmission', 'Validated');
            //})
        //->where('category', '!=' , 'Dropped')
        //->where('category', '!=' , 'Completed')
        // ->where('SS_statusofsubmission', '!=' , 'Uncategorized')
        
        
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('cip', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    // public function appraisal2(){
    //     $allpips = \App\Projects::where('trip', '=', 1)->where(function($q) {
    //             $q->where('statusofsubmission', 'Endorsed');
    //         })
    //     ->where('category', '!=' , 'Dropped')
    //     ->where('category', '!=' , 'Completed')
    //     ->get();
    //     $chapters = \App\Chapters::all();
    //     $fundingsources = \App\Fundingsources::all();
    //     $agencytype = $this->getAgencyType();
    //     return view('appraisal2', compact('chapters', 'allpips','fundingsources','agencytype'));
    // }

     public function appraisal2()
    {
        return view('appraisal2');
    }

    public function allTripProjects()
    {     
                  
            $allpips = DB::table('projects as P')
            ->join('agencies as A', 'P.agency_id', '=', 'A.id')
            ->join('chapters as C', 'P.mainpdp', '=', 'C.chap_no')
            ->join('sectorprojects as S', 'P.id', '=', 'S.proj_id')
            ->join('sectors as SS', 'S.sector_id', '=', 'SS.id')            

            ->where([
                ['P.trip', '=', 1],
            ])
                       
            ->select('P.id as id', 'code', 'title', 'UACS_DPT_DSC', 'UACS_AGY_DSC', 'spatial', 
                'chap_description', 'P.category as cat', 'NRO_category', 'SS_category', 
                'SS_statusofsubmission', DB::raw("GROUP_CONCAT(distinct sector separator ',') as sector"))      
                        

            ->groupBy('id', 'code', 'title', 'UACS_DPT_DSC', 'UACS_AGY_DSC', 'spatial','chap_description', 'cat', 'NRO_category', 'SS_category', 'SS_statusofsubmission')
            ->get();       
            
            return Datatables()::of($allpips)  
                        
            ->addColumn('actions', function($allpips) {                
                return '<a target="_blank" href="reclassify/'. $allpips->id .'"><button disabled type="button" class="btn btn-primary">R</button></a>
                <a target="_blank" href="/editproject/'. $allpips->id .'"><button type="button" class="btn btn-primary">V</button></a>
               <a href="/viewprojectsprint/'. $allpips->id .'" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>';
              })  

            ->rawColumns(['actions'])
            ->make(true);        
    }



    public function allCompletedProjects()
        {     
                  
            $allpips = DB::table('projects as P')
            ->join('agencies as A', 'P.agency_id', '=', 'A.id')
          

            ->where([
                ['P.implementationreadiness', '=', 3]
            ])
                       

            ->get();       
            
            return Datatables()::of($allpips)  
                        
            ->addColumn('actions', function($allpips) {                
                return '<a target="_blank" href="reclassify/'. $allpips->id .'"><button disabled type="button" class="btn btn-primary">R</button></a>
                <a target="_blank" href="/editproject/'. $allpips->id .'"><button type="button" class="btn btn-primary">V</button></a>
               <a href="/viewprojectsprint/'. $allpips->id .'" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>';
              })  

            ->rawColumns(['actions'])
            ->make(true);        
    }



    public function appraisal4(){
        return view('appraisal4');
    }

    public function appraisal5(){
        $allpips = \App\Projects::where('SS_statusofsubmission', '=', 'Uncategorized')->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('appraisal5', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function drafts(){
        $allpips = \App\Projects::where('statusofsubmission', '=', 'Draft')->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('drafts', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function nationwide(){
        $allpips = \App\Projects::where('spatial', '=', 'Nationwide')->where('statusofsubmission', 'Endorsed')->get();
            
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('nationwide', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function interregional(){
        $allpips = \App\Projects::where('spatial', '=', 'Interregional')->where('statusofsubmission', 'Endorsed')->get();
            
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('interregional', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function region(){
        $rprojects = \App\Regionsprojects::where('region_id','=', auth()->user()->focal_no)->get();

        foreach($rprojects as $rproject){
            $nos[] = $rproject->proj_id;
        }

        $allpips = \App\Projects::where('pip', '=', 1)
                ->where('statusofsubmission', 'Endorsed')
                ->where('spatial', 'Region Specific')
                ->whereIn('id', $nos)
                ->get();

        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('region', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }

    // public function oversight(){
    //     $allpips_dbm = \App\Projects::where('pip', '=', 1)->where(function($q) {
    //             $q->where('SS_statusofsubmission', 'Validated');
    //         })
    //     ->get();
    //     $chapters = \App\Chapters::all();
    //     $fundingsources = \App\Fundingsources::all();
    //     $agencytype = $this->getAgencyType();
    //     return view('oversight', compact('chapters', 'allpips_dbm','fundingsources','agencytype'));
    // }
    public function allValidatedPIP()
        {     
                  
            $allpips = DB::table('projects')
            ->join('agencies', 'projects.agency_id', '=', 'agencies.id')
            ->select('projects.id','projects.code','projects.title','projects.spatial','agencies.UACS_AGY_DSC')
            ->where([
                ['projects.SS_statusofsubmission', '=', 'Validated'],
                ['projects.pip', '=', 1],
            ])
                       

            ->get();       
            
            return Datatables()::of($allpips)  
                        
            ->addColumn('actions', function($allpips) {                
                return '<a target="_blank" href="/editproject/'. $allpips->id .'"><button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button></a>
               <a href="/viewprojectsprint/'. $allpips->id .'" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>';
              })  

            ->rawColumns(['actions'])
            ->make(true);        
    }

    public function oversight()
    {
        return view('oversight');
    }

    public function allDroppedProjects()
         {     
                  
            $allpips = DB::table('projects')
            ->join('agencies', 'projects.agency_id', '=', 'agencies.id')
            ->select('projects.id','projects.code','projects.title','projects.SS_statusofsubmission','agencies.UACS_AGY_DSC')
            ->where([
                ['projects.category', '=', 'Dropped'],
            ])
                       

            ->get();       
            
            return Datatables()::of($allpips)  
                        
            ->addColumn('actions', function($allpips) {                
                return '<a target="_blank" href="reclassify/'. $allpips->id .'"><button disabled type="button" class="btn btn-primary">R</button></a>
                <a target="_blank" href="/editproject/'. $allpips->id .'"><button type="button" class="btn btn-primary">V</button></a>
               <a href="/viewprojectsprint/'. $allpips->id .'" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>';
              })  

            ->rawColumns(['actions'])
            ->make(true);        
    }

    public function appraisal3(){
        return view('appraisal3');
    }

    public function getProjs(Request $request){
        print_r($request->all());
    }
}
