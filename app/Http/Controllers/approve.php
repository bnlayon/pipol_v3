<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		p{
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

<p><b>Agency:</b>
@foreach ($userrequest->getAgencyDetails($submission->agency_id) as $agencydetail)
    {{ $agencydetail->UACS_AGY_DSC }}
@endforeach
</p>

<p><b>Agency PIP/TRIP Focal:</b> <?php echo $user->lname.", ".$user->fname." ".$user->mname.":"; ?></p>

<p>Dear Sir/Ma'am: </p>

<p>As the authorized Agency PIP/TRIP Focal, this is to provide your user account information to access the PIPOL System.  Please be informed that the PIPOL System v2.0 will be open to Agency PIP/TRIP Focals <b>starting September 19, 2018 until October 26, 2018</b> for the submission of priority programs and projects (PAPs) for inclusion in the Updated 2017-2022 PIP and TRIP for FY 2020-2022 as input to the FY 2020 Budget Preparation.</p>

<p><b>Username:</b></p>

<p><b>Password:</b></p>

<p>Kindly note as well the following reminders: </p>
<p>A) Each user account cannot be used/accessed simultaneously by more than one user; </p>
<p>B) The PIPOL system generates <b>system logs</b>, which contain electronic record of transactions/interactions of each user account in the system for monitoring and accountability purposes; and</p>
<p>C) User names and passwords are <b>case sensitive</b>.</p>

<p>For reference, you may refer to the Terms of Reference of Agency PIP/TRIP Focals uploaded in the PIP Online System. Should you have any inquiries, please do not hesitate to coordinate with the PIP Secretariat of the NEDA-Public Investment Staff at contact numbers: DL 631-2165 or TL 631-0945 local 404 or through e-mail address: pip@neda.gov.ph</p>

<p>Thank you. </p>

<p><b>PIP Secretariat</p>

</body>
</html>