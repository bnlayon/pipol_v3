<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Alert;
use App\Users;
use App\Agencies;
use Carbon\Carbon;
use App\Submissions;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Jobs\SendEmailtoAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    private $submissionid;
    public function users(){
        $userrequests = \App\Users::where(function($query){
            $query->where('status', '1')->whereNull('username');
        })
        ->get();

        $app_userrequests = \App\Users::where(function($query){
            $query->where('user_type', 'AG')->whereNotNull('username');
        })
        ->get();

        return view('users', compact('userrequests', 'app_userrequests'));
    }
    public function index()
    {
        $users = Users::all();
        return $users;
    }
    public function signup()
    {
        return view('signup');
    }
    public function addUser(Request $request)
    {
        // dd($request->users);
        foreach ($request->users as $key => $user) {
            //head of agency
            if ($key == 0) {
                if ($request->agency['motheragency_id'] == 0) {
                    $user['mlname'] = '';
                    $user['mfname'] = '';
                    $user['mmname'] = '';
                    $user['mdesignation'] = '';
                    $user['mtelnumber'] = '';
                    $user['mfaxnumber'] = '';
                    $user['memail'] = '';
                }
                $submission = Submissions::create([
                    'agency_id' => $request->agency['id'],
                    'motheragency_id' => $request->agency['motheragency_id'],
                    'head_lname' => $user['lname'],
                    'head_fname' => $user['fname'],
                    'head_mname' => $user['mname'],
                    'head_designation' => $user['designation'],
                    'head_telnumber' => $user['telnumber'],
                    'head_faxnumber' => $user['faxnumber'],
                    'head_email' => $user['email'],
                    'mother_lname' => $user['mlname'],
                    'mother_fname' => $user['mfname'],
                    'mother_mname' => $user['mmname'],
                    'mother_designation' => $user['mdesignation'],
                    'mother_telnumber' => $user['mtelnumber'],
                    'mother_faxnumber' => $user['mfaxnumber'],
                    'mother_email' => $user['memail'],
                    'authorization_form' => $this->submissionid
                ]);
                //focals
            } else if ($key >= 1) {
                Users::create([
                    'lname' => $user['lname'],
                    'fname' => $user['fname'],
                    'mname' => $user['mname'],
                    'designation' => $user['designation'],
                    'telnumber' => $user['telnumber'],
                    'faxnumber' => $user['faxnumber'],
                    'email' => $user['email'],
                    'password' => bcrypt($user['password']),
                    'user_type' => 'AG',
                    'status' => 1,
                    'username' => $request->agency['Abbreviation'] . '_' . $user['lname'],
                    'submission_id' => $submission->id,
                    'is_first' => true,
                    'gender' => $user['gender']
                ]);
                $job = (new SendEmailtoAdmin($user))
                    ->delay(Carbon::now()->addSeconds(5));
                dispatch($job);
            }
        }
        return $this->submissionid;
    }
    public function asyncfindagency(Request $request)
    {
        $agency = Agencies::where('UACS_AGY_DSC', 'like', '%' . Request(' query') . '%')->get();
        return response()->json($agency);
    }
    public function asyncfindagencyold(Request $request)
    {
        $agency = DB::table('submissions AS A')
            ->select('A.agency_id', 'B.*')
            ->join('agencies as B', 'B.id', '=', 'A.agency_id')
            ->where('UACS_AGY_DSC', 'like', '%' . Request(' query') . '%')
            ->get();
        return response()->json($agency);
    }
    public function upload(Request $request)
    {
        if (count($request->images)) {
            foreach ($request->images as $image) {
                $ext = $image->getClientOriginalExtension();
                if ($request->submission != 'undefined') {
                    $submission = Submissions::where('agency_id', $request->submission)->first();
                    $submission->authorization_form = $submission->id . '.' . $ext;
                    $submission->save();
                }
                // elseif (!$request->submissionOld != 'undefined') {
                //     $submission = Submission::where('agency_id', $request->submissionOld)->first();
                //     $submission->authorization_form = $imageName;
                //     $submission->save();
                // }
                // $imageName = $image->getClientOriginalName();
                // $ext = $image->getClientOriginalExtension();
                $imageName = $submission->id . '.' . $ext;
                $image->move(public_path('images'), $imageName);
            }
        }
        return response()->json($request);
        // return response()->json([
        //     "message" => "Done"
        // ]);
    }
    public function getMother(Request $request)
    {
        $mother = Agencies::findOrFail($request->id);
        return response()->json($mother);
    }
}
