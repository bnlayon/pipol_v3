<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Http\Requests;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Http\Controllers\Controller;

class LoadEditModuleController extends Controller
{

    public function editproject(){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        if($submission->motheragency_id == 0){
            $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
            $agency_mother = "";
        }else{
            $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
            $agency_mother = \App\Agencies::where('id', $submission->motheragency_id)->first();
        }
        
        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        $tier2statuses = \App\Tier2statuses::all();
        $tier1statuses = \App\Tier1statuses::all();
        $agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        $province = \App\Provinces::all();
        $fsstatuses = \App\Fsstatuses::all();

        $agencytype = $this->getAgencyType();

        return view('editproject', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices', 'tier2statuses', 'submission', 'agency_agency', 'agency_mother', 'agencies', 'province', 'tier1statuses', 'fsstatuses','agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }

}
