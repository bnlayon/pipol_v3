<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Http\Controllers\Controller;

class PipolController extends Controller
{
    public function login(){
        
        return view('login');
    }
    
    public function register(){
        $all_agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        return view('register', compact('m_agencies', 'all_agencies'));
    }

    public function links(){
        $links = \App\Links::where('archive', '=', 0)->get();
        $links_archives = \App\Links::where('archive', '=', 1)->get();
        $agencytype = $this->getAgencyType();

        return view('links', compact('links', 'links_archives', 'agencytype'));
    }

    public function manual(){
         $agencytype = $this->getAgencyType();
        return view('manual', compact('agencytype'));
    }

    public function video(){
         $agencytype = $this->getAgencyType();
        return view('video', compact('agencytype'));
    }

    public function getProvince(Request $request){
        $data = \App\Provinces::where('regionNo', $request->id)->get();
        return $data;
    }

    public function getProjects(Request $request){
        $data = \App\Projects::select('id', 'agency_id', 'title')->get();
        return $data;

        // return $request;
    }

    public function getCity(Request $request){
        $data = \App\City::where('provNo', $request->id)->get();
        return $data;
    }

    public function changepassword() {
        return view('changepassword');
    }

    public function trip() {
        return view('trip');
    }

    public function rdip() {
        return view('rdip');
    }

    public function changepassword_user(Request $request) {

        $user = auth()->user();

        if (!(Hash::check(request('oldPassword'), $user->password))) {
            return back()->withErrors([
                'message' => 'Your current password does not match with the password you provided.'
            ]); 
        }

        if(strcmp(request('newPassword'), request('confirmPassword')) != 0){
            return back()->withErrors([
                'message' => 'New Password and Confirm Password does not match.'
            ]); 
        }

        $user->password = bcrypt($request->get('newPassword'));
        if($user->is_first == 1)
            $user->is_first = 0;
        $user->save();
 
        return redirect('/dashboard');
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
