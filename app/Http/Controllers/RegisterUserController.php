<?php

namespace App\Http\Controllers;

use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class RegisterUserController extends Controller{
    public function register_user(Request $request){

        $token = $request->input('g-recaptcha-response');

        if($token){
            $submission                          = new \App\Submissions();
            $submission->agency_id               = request('agency');
            $submission->motheragency_id         = request('mother_agency');
            $submission->head_lname              = request('4_loginLastname');
            $submission->head_fname              = request('4_loginFirstname');
            $submission->head_mname              = request('4_loginMiddlename');
            $submission->head_designation        = request('4_loginDes');
            $submission->head_telnumber          = request('4_loginTel');
            $submission->head_faxnumber          = request('4_loginMFax');
            $submission->head_email              = request('4_email');
            $submission->mother_lname            = request('5_loginLastname');
            $submission->mother_fname            = request('5_loginFirstname');
            $submission->mother_mname            = request('5_loginMiddlename');
            $submission->mother_designation      = request('5_loginDes');
            $submission->mother_telnumber        = request('5_loginTel');
            $submission->mother_faxnumber        = request('5_loginMFax');
            $submission->mother_email            = request('5_email');

            $filenameWithExt = $request->file('registerAttachment')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('registerAttachment')->getClientOriginalExtension();
            $fileNametoStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('registerAttachment')->StoreAs('public/storage/registration', $fileNametoStore);
            $submission->authorization_form = $fileNametoStore;

            $submission->save();

            $user                   = new \App\Users();
            $user->lname            = request('1_loginLastname');
            $user->fname            = request('1_loginFirstname');
            $user->mname            = request('1_loginMiddlename');
            $user->designation      = request('1_loginDes');
            $user->telnumber        = request('1_loginTel');
            $user->faxnumber        = request('1_loginMFax');
            $user->email            = request('1_email');
            $user->user_type        = "AG";
            $user->submission_id    = $submission->id;
            $user->focal_no         = "1";
            $user->save();

            $user                   = new \App\Users();
            $user->lname            = request('2_loginLastname');
            $user->fname            = request('2_loginFirstname');
            $user->mname            = request('2_loginMiddlename');
            $user->designation      = request('2_loginDes');
            $user->telnumber        = request('2_loginTel');
            $user->faxnumber        = request('2_loginMFax');
            $user->email            = request('2_email');
            $user->user_type        = "AG";
            $user->focal_no         = "2";
            $user->submission_id    = $submission->id;
            $user->save();

            $user                   = new \App\Users();
            $user->lname            = request('3_loginLastname');
            $user->fname            = request('3_loginFirstname');
            $user->mname            = request('3_loginMiddlename');
            $user->designation      = request('3_loginDes');
            $user->telnumber        = request('3_loginTel');
            $user->faxnumber        = request('3_loginMFax');
            $user->email            = request('3_email');
            $user->user_type        = "AG";
            $user->focal_no         = "3";
            $user->submission_id    = $submission->id;
            $user->save();

            Alert::success('Registration Successful', '');

            return redirect('login');
        } else{
            
            return back()->withErrors([
                    'message' => 'Please fill up the captcha'
                ]);
        }
    }

    public function get_mother(Request $request) {
        $mother = \App\Agencies::where('id', $request->id)->first();
        
        return $mother;
    }

    public function activate(Request $request, $id){
        
        \App\Users::where("id", $id)
        ->update(['status'=> '2']);

        $user = \App\Users::find($id);
        $emailofagencyuser = $user->email;

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = "pipol.neda@gmail.com";
        $mail->Password = "parasabayan";
        $mail->IsHTML(true);
        
        $user = \App\Users::find($id);
        $submissions = \App\Submissions::find($user->submission_id);
        $agencies = \App\Agencies::find($submissions->agency_id);
        $generatedusername = $agencies->Abbreviation."_".$user->lname;
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(100, 999) . mt_rand(100, 999) . $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)];
        $generatedpassword = str_shuffle($pin);

        \App\Users::where("id", $id)
        ->update([
            'username' => $generatedusername,
            'password'=> bcrypt($generatedpassword)]);

        if(!empty($submissions->head_email)){
          $mail->AddCC($submissions->head_email); 
        }

        if(!empty($submissions->mother_email)){
            $mail->AddCC($submissions->mother_email); 
        }
        
        ob_start(); //STARTS THE OUTPUT BUFFER
        include('approve.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->SetFrom('pipol.neda@gmail.com');
        $mail->Subject = 'PIPOL Account Activation';
        $mail->AddAddress($emailofagencyuser);
        $mail->AddCC("BNLayon@neda.gov.ph");
        $mail->AddCC("pip@neda.gov.ph");
        $mail->Send();
        
        Alert::success('Account Approved', '');

        return back();
    }

    public function decline(Request $request, $id){
        
        \App\Users::where("id", $id)
            ->update(['status'=> '3']);
    
        $user = \App\Users::find($id);
        $emailofagencyuser = $user->email;

        $reason = request('reason');

        if($reason == "Others"){
            $reason = request('reason_others');
        }

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = "pipol.neda@gmail.com";
        $mail->Password = "parasabayan";
        $mail->IsHTML(true);
        $user = \App\Users::find($id);
        $submissions = \App\Submissions::find($user->submission_id);
        $agencies = \App\Agencies::find($submissions->agency_id);

        if(!empty($submissions->head_email)){
           $mail->AddCC($submissions->head_email); 
        }

        if(!empty($submissions->mother_email)){
           $mail->AddCC($submissions->mother_email); 
        }

        ob_start(); //STARTS THE OUTPUT BUFFER
        include('decline.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->SetFrom('pipol.neda@gmail.com');
        $mail->Subject = 'PIPOL Account Activation';
        $mail->AddAddress($emailofagencyuser);
        $mail->AddCC("BNLayon@neda.gov.ph");
        $mail->AddCC("pip@neda.gov.ph");
        $mail->Send();

        Alert::success('Account Declined', '');
        return back();
    }

    public function deactivate(Request $request, $id){
        \App\Users::where("id", $id)
            ->update(['status'=> '1']);

        Alert::success('Account Deactivated', '');
        return back();
    }

    public function reactivate(Request $request, $id){
        \App\Users::where("id", $id)
            ->update(['status'=> '2']);

        Alert::success('Account Reactivated', '');
        return back();
    }

}
