<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class EditProjectController extends Controller
{

    public function editproject($id){
        $id = decrypt($id);
      if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        if($submission->motheragency_id == 0){
            $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
            $agency_mother = "";
        }else{
            $agency_agency = \App\Agencies::where('id', $submission->agency_id)->first();
            $agency_mother = \App\Agencies::where('id', $submission->motheragency_id)->first();
        }
      }

        $basis_ = \App\Basis::all();
        $piptypologies = \App\Piptypologies::all();
        $ciptypologies = \App\Ciptypologies::all();
        $agendas = \App\Agendas::all();
        $goals = \App\Goals::all();
        $chapters = \App\Chapters::all();
        $projectdocuments = \App\Projectdocuments::all();
        $fundingsources = \App\Fundingsources::all();
        $modes = \App\Modes::all();
        $sectors = \App\Sectors::all();
        $statuses = \App\Statuses::all();
        $regions = \App\Regions::all();
        $provinces = \App\Provinces::all();
        $cities = \App\City::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $province = \App\Provinces::all();
        $fundings = \App\Fundings::all();
        $levels = \App\Levels::all();
        $subsectors = \App\Subsectors::all();
        $_1matrices = \App\_1matrices::all();
        $_2matrices = \App\_2matrices::all();
        $_3matrices = \App\_3matrices::all();
        $_4matrices = \App\_4matrices::all();
        $tier2statuses = \App\Tier2statuses::all();
        $tier1statuses = \App\Tier1statuses::all();
        $agencies = \App\Agencies::all()->sortBy('UACS_AGY_DSC');
        $provinces = \App\Provinces::all();
        $fsstatuses = \App\Fsstatuses::all();

        //Project Details
        $projects = \App\Projects::find($id);
        $basis_projects = \App\Basisprojects::where('proj_id', $id)->get();
        $agencies_projects = \App\Agenciesproject::where('proj_id', $id)->get();
        $agenda_projects = \App\Agendaprojects::where('proj_id', $id)->get();
        $sdg_projects = \App\Sdgprojects::where('proj_id', $id)->get();
        $chapters_projects = \App\Otherpdpprojects::where('proj_id', $id)->get();
        $f_projects = \App\Fprojects::where('proj_id', $id)->get();
        $status_projects = \App\Statusprojects::where('proj_id', $id)->get();
        $sector_projects = \App\Sectorprojects::where('proj_id', $id)->get();
        $subsector_projects = \App\Subsectorprojects::where('proj_id', $id)->get();
        $attagencies_projects = \App\Attagenciesproject::where('proj_id', $id)->get();
        $states_projects = \App\Regionsprojects::where('proj_id', $id)->get();
        $province_projects = \App\Provincesprojects::where('proj_id', $id)->get();
        $city_projects = \App\Citiesproject::where('proj_id', $id)->get();
        $logs_projects = \App\Projectlogs::where('proj_id', $id)->get();
        $finance_source_projects = \App\FinanceSourceProjects::where('proj_id', $id)->get();

        //FSCOST ROWA RC
        $fscost_projects = \App\Fscosts::where('proj_id', $id)->get();
        $rowa_projects = \App\Rowacost::where('proj_id', $id)->get();
        $rc_projects = \App\Rccost::where('proj_id', $id)->get();
        $fa_projects = \App\Facost::where('proj_id', $id)->get();
        $it_projects = \App\Investments::where('proj_id', $id)->get();

        $it_projects2 = \App\Investments::where('proj_id', $id)->where('year', 2024)->get();


        $ic_projects = \App\Infrastructures::where('proj_id', $id)->get();

        //RESULTS MATRICES
        $_1matrices_projects = \App\_1matricesprojects::where('proj_id', $id)->get();
        $_2matrices_projects = \App\_2matricesprojects::where('proj_id', $id)->get();
        $_3matrices_projects = \App\_3matricesprojects::where('proj_id', $id)->get();
        $_4matrices_projects = \App\_4matricesprojects::where('proj_id', $id)->get();

        $agencytype = $this->getAgencyType();
        if(auth()->user()->user_type == 'AG'){
        $getIdsOfAttached = \App\Agencies::select('id')->where('motheragency_id', $agencytype->id)->get();
      }
        return view('editproject', compact('basis_', 'piptypologies', 'ciptypologies', 'agendas', 'goals', 'chapters', 'projectdocuments', 'fundingsources', 'modes', 'sectors', 'statuses', 'regions', 'fundings', 'levels', 'subsectors', '_1matrices', '_2matrices', '_3matrices', '_4matrices', 'tier2statuses', 'submission', 'agency_agency', 'agency_mother', 'agencies', 'province', 'tier1statuses', 'fsstatuses', 'projects', 'basis_projects', 'agencies_projects', 'agenda_projects', 'sdg_projects', 'chapters_projects', 'f_projects', 'status_projects', 'sector_projects', 'subsector_projects','attagencies_projects', 'fscost_projects', 'rowa_projects','rc_projects', 'fa_projects', 'it_projects', 'ic_projects', 'states_projects','province_projects','city_projects','_1matrices_projects','_2matrices_projects','_3matrices_projects','_4matrices_projects', 'provinces', 'cities','agencytype','getIdsOfAttached', 'logs_projects', 'finance_source_projects','it_projects2'));
    }

    public function editsave(Request $request, $id){
        $project                             = \App\Projects::find($id);
        $project->title                      = request('title');
        $project->prog_proj                  = request('prog_proj');
        $project->description                = request('description');
        $project->output                     = request('output');
        $project->spatial                    = request('spatial');
        $project->iccable                    = request('iccable');
        $project->currentlevel               = request('currentlevel');
        $project->approval_date1             = request('approval_date1');
        $project->approval_date2             = request('approval_date2');
        $project->approval_date3             = request('approval_date3');
        $project->approval_date4             = request('approval_date4');
        $project->approval_date5             = request('approval_date5');
        $project->pip                        = request('pip');
        $project->pip_typo                   = request('pip_typo');
        $project->cip                        = request('cip');
        $project->cip_typo                   = request('cip_typo');
        $project->trip                       = request('trip');
        $project->rdip                       = request('rdip');
        $project->rdc_endorsement            = request('rdc_endorsement');
        $project->rdc_endorsed_notendorsed   = request('rdc_endorsed_notendorsed');
        $project->rdc_date_endorsement       = request('rdc_date_endorsement');
        $project->mainpdp                    = request('mainpdp');
        $project->risk                       = request('risk');
        $project->gender                     = request('gender');
        $project->start                      = request('start');
        $project->end                        = request('end');
        $project->ppdetails                  = request('ppdetails');
        $project->ppdetails_others           = request('ppdetails_others');
        $project->rowa                       = request('rowa');
        $project->rc                         = request('rc');
        $project->wrrc                       = request('wrrc');
        $project->rowa_affected              = request('rowa_affected');
        $project->rc_affected                = request('rc_affected');
        $project->employment                 = request('employment');
        $project->mainfsource                = request('mainfsource');
        $project->modeofimplementation       = request('modeofimplementation');
        $project->category                   = request('category');
        $project->tier2_status               = request('status2');
        $project->updates                    = request('updates');
        $project->asof                       = request('asof');
        $project->tier1_uacs                 = request('tier1_uacs');
        $project->tier2_uacs                 = request('tier2_uacs');
        $project->agency_id                  = request('agency_id');
        $project->motheragency_id            = request('motheragency_id');
        $project->statusofsubmission         = request('statusofsubmission');
        $project->fsstatus                   = request('fsstatus');
        $project->fsstatus_ongoing           = request('fsstatus_ongoing');
        $project->fsstatus_prep              = request('fsstatus_prep');
        $project->tier1_type                 = request('status1');
        $project->fsassistance               = request('fsassistance');
        $project->NA_fs                      = request('NA_fs');
        $project->tier2_type                 = request('tier2_type');
        $project->other_fs                   = request('other_fs');
        $project->other_mode                 = request('other_mode');
        $project->reg_prog                   = request('reg_prog');
        $project->rd                         = request('rd');
        $project->NA_rm                      = request('NA_rm');
        $project->implementationreadiness    = request('implementationreadiness');

        if(auth()->user()->user_type == 'NRO'){
          if(request('NRO_statusofsubmission') == "Reviewed"){
            if(request('NFI') == 1){
                $project->NRO_implementationreadiness = request('implementationreadiness');
                $project->NRO_updates                 = request('updates');
                $project->NRO_remarks                 = request('NRO_remarks');
                $project->NRO_status2                 = request('NRO_status2');
                $project->NRO_no                      = request('NFI');
                $project->NRO_statusofsubmission      = request('NRO_statusofsubmission');
            }else{
                $project->NRO_implementationreadiness = request('NRO_implementationreadiness');
                $project->NRO_updates                 = request('NRO_updates');
                $project->NRO_remarks                 = request('NRO_remarks');
                $project->NRO_status2                 = request('NRO_status2');
                $project->NRO_no                      = request('NFI');
                $project->NRO_statusofsubmission      = request('NRO_statusofsubmission');
            }
          }elseif(request('NRO_statusofsubmission') == "Validated"){
            if(request('NFI') == 1){
                $project->NRO_implementationreadiness = request('implementationreadiness');
                $project->NRO_updates                 = request('updates');
                $project->NRO_remarks                 = request('NRO_remarks');
                $project->NRO_status2                 = request('NRO_status2');
                $project->NRO_no                      = request('NFI');
                $project->NRO_statusofsubmission      = request('NRO_statusofsubmission');
            }else{
                $project->NRO_implementationreadiness = request('NRO_implementationreadiness');
                $project->NRO_updates                 = request('NRO_updates');
                $project->NRO_remarks                 = request('NRO_remarks');
                $project->NRO_status2                 = request('NRO_status2');
                $project->NRO_no                      = request('NFI');
                $project->NRO_statusofsubmission      = request('NRO_statusofsubmission');
            }
          }
        }
 
        if(auth()->user()->user_type == "SS"){
          if(request('SS_statusofsubmission') == "Reviewed"){
            if(request('NFI') == 1){
                $project->SS_implementationreadiness = request('implementationreadiness');
                $project->SS_updates                 = request('updates');
                $project->SS_remarks                 = request('SS_remarks');
                $project->SS_no                      = request('NFI');
                $project->SS_status2                 = request('SS_status2');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }else{
                $project->SS_implementationreadiness = request('SS_implementationreadiness');
                $project->SS_updates                 = request('SS_updates');
                $project->SS_remarks                 = request('SS_remarks');
                $project->SS_status2                 = request('SS_status2');
                $project->SS_no                      = request('NFI');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }
          }elseif(request('SS_statusofsubmission') == "Validated"){
            if(request('NFI') == 1){
                $project->SS_implementationreadiness = request('implementationreadiness');
                $project->SS_updates                 = request('updates');
                $project->SS_remarks                 = request('SS_remarks');
                $project->SS_no                      = request('NFI');
                $project->SS_status2                 = request('SS_status2');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }else{
                $project->SS_implementationreadiness = request('SS_implementationreadiness');
                $project->SS_updates                 = request('SS_updates');
                $project->SS_remarks                 = request('SS_remarks');
                $project->SS_status2                 = request('SS_status2');
                $project->SS_no                      = request('NFI');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }
          }elseif(request('SS_statusofsubmission') == "Uncategorized"){
            if(request('NFI') == 1){
                $project->SS_implementationreadiness = request('implementationreadiness');
                $project->SS_updates                 = request('updates');
                $project->SS_no                      = request('NFI');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }else{
                $project->SS_implementationreadiness = request('SS_implementationreadiness');
                $project->SS_updates                 = request('SS_updates');
                $project->SS_remarks                 = request('SS_remarks');
                $project->SS_no                      = request('NFI');
                $project->SS_statusofsubmission      = request('SS_statusofsubmission');
            }
          }
        }

        if(auth()->user()->user_type == "PIS"){
          if(request('PIS_statusofsubmission') == "Reviewed"){
            if(request('NFI') == 1){
                $project->PIS_implementationreadiness = request('implementationreadiness');
                $project->PIS_updates                 = request('updates');
                $project->PIS_remarks                 = request('PIS_remarks');
                $project->PIS_no                      = request('NFI');
                $project->PIS_status2                 = request('PIS_status2');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }else{
                $project->PIS_implementationreadiness = request('PIS_implementationreadiness');
                $project->PIS_updates                 = request('PIS_updates');
                $project->PIS_remarks                 = request('PIS_remarks');
                $project->PIS_status2                 = request('PIS_status2');
                $project->PIS_no                      = request('NFI');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }
          }elseif(request('PIS_statusofsubmission') == "Validated"){
            if(request('NFI') == 1){
                $project->PIS_implementationreadiness = request('implementationreadiness');
                $project->PIS_updates                 = request('updates');
                $project->PIS_remarks                 = request('PIS_remarks');
                $project->PIS_no                      = request('NFI');
                $project->PIS_status2                 = request('PIS_status2');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }else{
                $project->PIS_implementationreadiness = request('PIS_implementationreadiness');
                $project->PIS_updates                 = request('PIS_updates');
                $project->PIS_remarks                 = request('PIS_remarks');
                $project->PIS_status2                 = request('PIS_status2');
                $project->PIS_no                      = request('NFI');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }
          }elseif(request('PIS_statusofsubmission') == "Uncategorized"){
            if(request('NFI') == 1){
                $project->PIS_implementationreadiness = request('implementationreadiness');
                $project->PIS_updates                 = request('updates');
                $project->PIS_no                      = request('NFI');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }else{
                $project->PIS_implementationreadiness = request('PIS_implementationreadiness');
                $project->PIS_updates                 = request('PIS_updates');
                $project->PIS_remarks                 = request('PIS_remarks');
                $project->PIS_no                      = request('NFI');
                $project->PIS_statusofsubmission      = request('PIS_statusofsubmission');
            }
          }
        }
         
        if(empty($project->code)){
          //GET PROJECT CODE

        $ip = "localhost";
        $user = "pipolv2user";
        $pass = "Dranreb0792";
        $db = "pipolv2";

            
          $conn = mysqli_connect($ip,$user,$pass,$db);
          $sql = "SELECT code, SUBSTRING_INDEX(`code`,'-',-1)+1 as code1 FROM projects WHERE agency_id = $project->agency_id ORDER BY code DESC LIMIT 1";
          $result = mysqli_query($conn,$sql);

          if (mysqli_num_rows($result)==0) { $finalcode = "000001"; 
          }

          while($row12=mysqli_fetch_array($result)){
              if($row12['code1'] == 0){
                  $finalcode = "000001";
              }else{
                  $finalcode = sprintf("%06d", $row12['code1']);
              }
           
          }

          $project->code                       = request('code').$finalcode;
        }

        $project->save();

//EDIT ROWA
        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2017)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2017'))]);

        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2018)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2018'))]);

        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2019)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2019'))]);

        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2020)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2020'))]);

        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2021)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2021'))]);

        \App\Rowacost::where("proj_id", $id)->where("rowayear", 2022)
            ->update(['rowacost'=> str_replace(',', '',request('rowa_2022'))]);

//EDIT RC

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2017)
            ->update(['rccost'=> str_replace(',', '',request('rc_2017'))]);

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2018)
            ->update(['rccost'=> str_replace(',', '',request('rc_2018'))]);

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2019)
            ->update(['rccost'=> str_replace(',', '',request('rc_2019'))]);

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2020)
            ->update(['rccost'=> str_replace(',', '',request('rc_2020'))]);

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2021)
            ->update(['rccost'=> str_replace(',', '',request('rc_2021'))]);

        \App\Rccost::where("proj_id", $id)->where("rcyear", 2022)
            ->update(['rccost'=> str_replace(',', '',request('rc_2022'))]);

//EDIT FS COST

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2017)
            ->update(['fscost'=> str_replace(',', '',request('fs_2017'))]);

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2018)
            ->update(['fscost'=> str_replace(',', '',request('fs_2018'))]);

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2019)
            ->update(['fscost'=> str_replace(',', '',request('fs_2019'))]);

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2020)
            ->update(['fscost'=> str_replace(',', '',request('fs_2020'))]);

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2021)
            ->update(['fscost'=> str_replace(',', '',request('fs_2021'))]);

        \App\Fscosts::where("proj_id", $id)->where("fsyear", 2022)
            ->update(['fscost'=> str_replace(',', '',request('fs_2022'))]);


//EDIT INVESTMENT
        // \App\Investments::where("proj_id", $id)->where("year", 2016)
        //     ->update(['local'=> str_replace(',', '',request('it_2016_nglocal')),
        //               'loan'=> str_replace(',', '',request('it_2016_ngloan')),
        //               'grant'=> str_replace(',', '',request('it_2016_odagrant')),
        //               'gocc'=> str_replace(',', '',request('it_2016_gocc')),
        //               'lgu'=> str_replace(',', '',request('it_2016_lgu')),
        //               'private'=> str_replace(',', '',request('it_2016_ps')),
        //               'others'=> str_replace(',', '',request('it_2016_others'))]);

        // \App\Investments::where("proj_id", $id)->where("year", 2017)
        //     ->update(['local'=> str_replace(',', '',request('it_2017_nglocal')),
        //               'loan'=> str_replace(',', '',request('it_2017_ngloan')),
        //               'grant'=> str_replace(',', '',request('it_2017_odagrant')),
        //               'gocc'=> str_replace(',', '',request('it_2017_gocc')),
        //               'lgu'=> str_replace(',', '',request('it_2017_lgu')),
        //               'private'=> str_replace(',', '',request('it_2017_ps')),
        //               'others'=> str_replace(',', '',request('it_2017_others'))]);

        // \App\Investments::where("proj_id", $id)->where("year", 2018)
        //     ->update(['local'=> str_replace(',', '',request('it_2018_nglocal')),
        //               'loan'=> str_replace(',', '',request('it_2018_ngloan')),
        //               'grant'=> str_replace(',', '',request('it_2018_odagrant')),
        //               'gocc'=> str_replace(',', '',request('it_2018_gocc')),
        //               'lgu'=> str_replace(',', '',request('it_2018_lgu')),
        //               'private'=> str_replace(',', '',request('it_2018_ps')),
        //               'others'=> str_replace(',', '',request('it_2018_others'))]);

        // \App\Investments::where("proj_id", $id)->where("year", 2019)
        //     ->update(['local'=> str_replace(',', '',request('it_2019_nglocal')),
        //               'loan'=> str_replace(',', '',request('it_2019_ngloan')),
        //               'grant'=> str_replace(',', '',request('it_2019_odagrant')),
        //               'gocc'=> str_replace(',', '',request('it_2019_gocc')),
        //               'lgu'=> str_replace(',', '',request('it_2019_lgu')),
        //               'private'=> str_replace(',', '',request('it_2019_ps')),
        //               'others'=> str_replace(',', '',request('it_2019_others'))]);

        // \App\Investments::where("proj_id", $id)->where("year", 2020)
        //     ->update(['local'=> str_replace(',', '',request('it_2020_nglocal')),
        //               'loan'=> str_replace(',', '',request('it_2020_ngloan')),
        //               'grant'=> str_replace(',', '',request('it_2020_odagrant')),
        //               'gocc'=> str_replace(',', '',request('it_2020_gocc')),
        //               'lgu'=> str_replace(',', '',request('it_2020_lgu')),
        //               'private'=> str_replace(',', '',request('it_2020_ps')),
        //               'others'=> str_replace(',', '',request('it_2020_others'))]);

        \App\Investments::where("proj_id", $id)->where("year", 2021)
            ->update(['local'=> str_replace(',', '',request('it_2021_nglocal')),
                      'loan'=> str_replace(',', '',request('it_2021_ngloan')),
                      'grant'=> str_replace(',', '',request('it_2021_odagrant')),
                      'gocc'=> str_replace(',', '',request('it_2021_gocc')),
                      'lgu'=> str_replace(',', '',request('it_2021_lgu')),
                      'private'=> str_replace(',', '',request('it_2021_ps')),
                      'others'=> str_replace(',', '',request('it_2021_others'))]);

        \App\Investments::where("proj_id", $id)->where("year", 2022)
            ->update(['local'=> str_replace(',', '',request('it_2022_nglocal')),
                      'loan'=> str_replace(',', '',request('it_2022_ngloan')),
                      'grant'=> str_replace(',', '',request('it_2022_odagrant')),
                      'gocc'=> str_replace(',', '',request('it_2022_gocc')),
                      'lgu'=> str_replace(',', '',request('it_2022_lgu')),
                      'private'=> str_replace(',', '',request('it_2022_ps')),
                      'others'=> str_replace(',', '',request('it_2022_others'))]);

        \App\Investments::where("proj_id", $id)->where("year", 2023)
            ->update(['local'=> str_replace(',', '',request('it_2023_nglocal')),
                      'loan'=> str_replace(',', '',request('it_2023_ngloan')),
                      'grant'=> str_replace(',', '',request('it_2023_odagrant')),
                      'gocc'=> str_replace(',', '',request('it_2023_gocc')),
                      'lgu'=> str_replace(',', '',request('it_2023_lgu')),
                      'private'=> str_replace(',', '',request('it_2023_ps')),
                      'others'=> str_replace(',', '',request('it_2023_others'))]);


        if(request('checker_if_new') == 1){
        \App\Investments::where("proj_id", $id)->where("year", 2024)
            ->update(['local'=> str_replace(',', '',request('it_2024_nglocal')),
                      'loan'=> str_replace(',', '',request('it_2024_ngloan')),
                      'grant'=> str_replace(',', '',request('it_2024_odagrant')),
                      'gocc'=> str_replace(',', '',request('it_2024_gocc')),
                      'lgu'=> str_replace(',', '',request('it_2024_lgu')),
                      'private'=> str_replace(',', '',request('it_2024_ps')),
                      'others'=> str_replace(',', '',request('it_2024_others'))]);  


         \App\Investments::where("proj_id", $id)->where("year", 2025)
            ->update(['local'=> str_replace(',', '',request('it_2025_nglocal')),
                      'loan'=> str_replace(',', '',request('it_2025_ngloan')),
                      'grant'=> str_replace(',', '',request('it_2025_odagrant')),
                      'gocc'=> str_replace(',', '',request('it_2025_gocc')),
                      'lgu'=> str_replace(',', '',request('it_2025_lgu')),
                      'private'=> str_replace(',', '',request('it_2025_ps')),
                      'others'=> str_replace(',', '',request('it_2025_others'))]);  


        }else{
        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2024;
        $it->local                      = request('it_2024_nglocal');
        $it->loan                       = request('it_2024_ngloan');
        $it->grant                      = request('it_2024_odagrant');
        $it->gocc                       = request('it_2024_gocc');
        $it->lgu                        = request('it_2024_lgu');
        $it->private                    = request('it_2024_ps');
        $it->others                     = request('it_2024_others');
        $it->save();

        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2025;
        $it->local                      = request('it_2025_nglocal');
        $it->loan                       = request('it_2025_ngloan');
        $it->grant                      = request('it_2025_odagrant');
        $it->gocc                       = request('it_2025_gocc');
        $it->lgu                        = request('it_2025_lgu');
        $it->private                    = request('it_2025_ps');
        $it->others                     = request('it_2025_others');
        $it->save();
        }



//EDIT FINANCIAL ACCOMPLISHMENTS

        \App\Facost::where("proj_id", $id)->where("fayear", 2017)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2017')),
                      'facost_ad'=> str_replace(',', '',request('ad_2017')),
                      'facost_all'=> str_replace(',', '',request('all_2017'))]);

        \App\Facost::where("proj_id", $id)->where("fayear", 2018)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2018')),
                      'facost_ad'=> str_replace(',', '',request('ad_2018')),
                      'facost_all'=> str_replace(',', '',request('all_2018'))]);

        \App\Facost::where("proj_id", $id)->where("fayear", 2019)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2019')),
                      'facost_ad'=> str_replace(',', '',request('ad_2019')),
                      'facost_all'=> str_replace(',', '',request('all_2019'))]);

        \App\Facost::where("proj_id", $id)->where("fayear", 2020)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2020')),
                      'facost_ad'=> str_replace(',', '',request('ad_2020')),
                      'facost_all'=> str_replace(',', '',request('all_2020'))]);

        \App\Facost::where("proj_id", $id)->where("fayear", 2021)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2021')),
                      'facost_ad'=> str_replace(',', '',request('ad_2021')),
                      'facost_all'=> str_replace(',', '',request('all_2021'))]);

        \App\Facost::where("proj_id", $id)->where("fayear", 2022)
            ->update(['facost_nep'=> str_replace(',', '',request('nep_2022')),
                      'facost_ad'=> str_replace(',', '',request('ad_2022')),
                      'facost_all'=> str_replace(',', '',request('all_2022'))]);

// EDIT INFRASTRUCTURE COST

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2016)
        //     ->update(['local'=> str_replace(',', '',request('ic_2016_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2016_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2016_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2016_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2016_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2016_ps')),
        //               'others'=> str_replace(',', '',request('ic_2016_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2017)
        //     ->update(['local'=> str_replace(',', '',request('ic_2017_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2017_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2017_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2017_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2017_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2017_ps')),
        //               'others'=> str_replace(',', '',request('ic_2017_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2018)
        //     ->update(['local'=> str_replace(',', '',request('ic_2018_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2018_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2018_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2018_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2018_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2018_ps')),
        //               'others'=> str_replace(',', '',request('ic_2018_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2019)
        //     ->update(['local'=> str_replace(',', '',request('ic_2019_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2019_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2019_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2019_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2019_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2019_ps')),
        //               'others'=> str_replace(',', '',request('ic_2019_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2020)
        //     ->update(['local'=> str_replace(',', '',request('ic_2020_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2020_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2020_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2020_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2020_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2020_ps')),
        //               'others'=> str_replace(',', '',request('ic_2020_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2021)
        //     ->update(['local'=> str_replace(',', '',request('ic_2021_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2021_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2021_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2021_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2021_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2021_ps')),
        //               'others'=> str_replace(',', '',request('ic_2021_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2022)
        //     ->update(['local'=> str_replace(',', '',request('ic_2022_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2022_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2022_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2022_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2022_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2022_ps')),
        //               'others'=> str_replace(',', '',request('ic_2022_others'))]);

        // \App\Infrastructures::where("proj_id", $id)->where("year", 2023)
        //     ->update(['local'=> str_replace(',', '',request('ic_2023_nglocal')),
        //               'loan'=> str_replace(',', '',request('ic_2023_ngloan')),
        //               'grant'=> str_replace(',', '',request('ic_2023_odagrant')),
        //               'gocc'=> str_replace(',', '',request('ic_2023_gocc')),
        //               'lgu'=> str_replace(',', '',request('ic_2023_lgu')),
        //               'private'=> str_replace(',', '',request('ic_2023_ps')),
        //               'others'=> str_replace(',', '',request('ic_2023_others'))]);

//EDIT CHECKBOXES

            if(request('spatial') == 'Interregional'){
              $states = array();
              $states_to_save = request('states_id');
              if(!is_null($states_to_save)){
                  foreach ($states_to_save as $states_to_sav) {

                      if($states_to_sav!=0)
                          $states[] = $states_to_sav;
                  }
              }
              $project->projects_states()->sync($states);
              $project->save();

              $provinces = array();
              $provinces_to_save = request('provinces_id');
              if(!is_null($provinces_to_save)){
                  foreach ($provinces_to_save as $provinces_to_sav) {

                      if($provinces_to_sav!=0)
                          $provinces[] = $provinces_to_sav;
                  }
              }
              $project->projects_provinces()->sync($provinces);
              $project->save();

              $cities = array();
              $cities_to_save = request('cities_id');
              if(!is_null($cities_to_save)){
                  foreach ($cities_to_save as $cities_to_sav) {

                      if($cities_to_sav!=0)
                          $cities[] = $cities_to_sav;
                  }
              }
              $project->projects_cities()->sync($cities);
              $project->save();


            }

            if(request('spatial') == 'Region Specific'){
                \App\Regionsprojects::where("proj_id", $id)
                  ->update(['region_id'=> request('region')]);

                $provinces = array();
                $provinces_to_save = request('RegionSpecProvincesList');
                if(!is_null($provinces_to_save)){
                    foreach ($provinces_to_save as $provinces_to_sav) {

                        if($provinces_to_sav!=0)
                            $provinces[] = $provinces_to_sav;
                    }
                }
                $project->projects_provinces2()->sync($provinces);
                $project->save();
            }
            
            $bases = array();
            $bases_to_save = request('basis_id');
            if(!is_null($bases_to_save)){
                foreach ($bases_to_save as $basis) {

                    if($basis!=0)
                        $bases[] = $basis;
                }
            }
            $project->projects_bases()->sync($bases);
            $project->save();

            $coimps = array();
            $comips_to_save = request('coimp');
            if(!is_null($comips_to_save)){
                foreach ($comips_to_save as $coimp) {

                    if($coimp!=0)
                        $coimps[] = $coimp;
                }
            }
            $project->projects_coimpagency()->sync($coimps);
            $project->save();

        
            $atts = array();
            $atts_to_save = request('att');
            if(!is_null($atts_to_save)){
                foreach ($atts_to_save as $att) {

                    if($att!=0)
                        $atts[] = $att;
                }
            }
            $project->projects_attagency()->sync($atts);
            $project->save();

            $sectors = array();
            $sectors_to_save = request('sectors');
            if(!is_null($sectors_to_save)){
                foreach ($sectors_to_save as $sector) {

                    if($sector!=0)
                        $sectors[] = $sector;
                }
            }
            $project->projects_sectors()->sync($sectors);
            $project->save();

            $subsectors = array();
            $subsectors_to_save = request('subsectors');
            if(!is_null($subsectors_to_save)){
                foreach ($subsectors_to_save as $subsector) {

                    if($subsector!=0 && $this->CheckIfSectorExist($project->id, $subsector))
                        $subsectors[] = $subsector;
                }
            }
            $project->projects_subsectors()->sync($subsectors);
            $project->save();

            $statuses = array();
            $statuses_to_save = request('status');
            if(!is_null($statuses_to_save)){
                foreach ($statuses_to_save as $status) {

                    if($status!=0)
                        $statuses[] = $status;
                }
            }
            $project->projects_statuses()->sync($statuses);
            $project->save();

            $otherpdps = array();
            $otherpdps_to_save = request('otherpdp');
            if(!is_null($otherpdps_to_save)){
                foreach ($otherpdps_to_save as $otherpdp) {

                    if($otherpdp!=0)
                        $otherpdps[] = $otherpdp;
                }
            }
            $project->projects_otherpdps()->sync($otherpdps);
            $project->save();

            $agendas = array();
            $agendas_to_save = request('agenda');
            if(!is_null($agendas_to_save)){
                foreach ($agendas_to_save as $agenda) {

                    if($agenda!=0)
                        $agendas[] = $agenda;
                }
            }
            $project->projects_agendas()->sync($agendas);
            $project->save();

            $sdgs = array();
            $sdgs_to_save = request('sdg');
            if(!is_null($sdgs_to_save)){
                foreach ($sdgs_to_save as $sdg) {

                    if($sdg!=0)
                        $sdgs[] = $sdg;
                }
            }
            $project->projects_sdgs()->sync($sdgs);
            $project->save();

            $finance_source_projects = array();
            $finance_source_projects_to_save = request('finance_source_projects');
            if(!is_null($finance_source_projects_to_save)){
                foreach ($finance_source_projects_to_save as $finance_source_project) {

                    if($finance_source_project!=0)
                        $finance_source_projects[] = $finance_source_project;
                }
            }
            $project->projects_funding_s()->sync($finance_source_projects);
            $project->save();

            $oda_fundings = array();
            $oda_fundings_to_save = request('oda_funding');
            if(!is_null($oda_fundings_to_save)){
                foreach ($oda_fundings_to_save as $oda_funding) {
                        $oda_fundings[] = $oda_funding;
                }
            }
            $project->projects_fundings()->sync($oda_fundings);
            $project->save();

            $_1rms = array();
            $_1rm_to_save = request('_1rm'.$project->mainpdp);
            if(!is_null($_1rm_to_save)){
               foreach ($_1rm_to_save as $_1rm) {

                    if($_1rm!=0)
                        $_1rms[] = $_1rm;
                }
            }
            $project->projects_1rms()->sync($_1rms);
            $project->save();

            $_2rms = array();
            $_2rm_to_save = request('_2rm'.$project->mainpdp);
            if(!is_null($_2rm_to_save)){
               foreach ($_2rm_to_save as $_2rm) {

                    if($_2rm!=0)
                        $_2rms[] = $_2rm;
                }
            }
            $project->projects_2rms()->sync($_2rms);
            $project->save();

            $_3rms = array();
            $_3rm_to_save = request('_3rm'.$project->mainpdp);
            if(!is_null($_3rm_to_save)){
               foreach ($_3rm_to_save as $_3rm) {

                    if($_3rm!=0)
                        $_3rms[] = $_3rm;
                }
            }
            $project->projects_3rms()->sync($_3rms);
            $project->save();

            $_4rms = array();
            $_4rm_to_save = request('_4rm'.$project->mainpdp);
            if(!is_null($_4rm_to_save)){
               foreach ($_4rm_to_save as $_4rm) {

                    if($_4rm!=0)
                        $_4rms[] = $_4rm;
                }
            }
            $project->projects_4rms()->sync($_4rms);
            $project->save();

        if(auth()->user()->user_type == 'AG'){
          if(request('statusofsubmission') == 'Draft'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Draft";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Draft', '');
              return back();
          }else if(request('statusofsubmission') == 'Endorsed'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Endorsed";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Endorsed', '');
              return back();
          }
        }

        if(auth()->user()->user_type == 'NRO'){
          if(request('NRO_statusofsubmission') == 'Reviewed'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Reviewed";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Reviewed', '');
              return back();
          }elseif(request('NRO_statusofsubmission') == 'Validated'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Validated";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Validated', '');
              return back();
          }
        }

        if(auth()->user()->user_type == "SS"){
          if(request('SS_statusofsubmission') == 'Reviewed'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Reviewed";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Reviewed', '');
              return back();
          }elseif(request('SS_statusofsubmission') == 'Validated'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Validated";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Validated', '');
              return back();
          }elseif(request('SS_statusofsubmission') == 'Uncategorized'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Uncategorized";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Uncategorized', '');
              return back();
          }
        }

        if(auth()->user()->user_type == "PIS"){
          if(request('PIS_statusofsubmission') == 'Reviewed'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Reviewed";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Reviewed', '');
              return back();
          }elseif(request('PIS_statusofsubmission') == 'Validated'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Validated";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Validated', '');
              return back();
          }elseif(request('PIS_statusofsubmission') == 'Uncategorized'){
              $projectlogs                         = new \App\Projectlogs();
              $projectlogs->username                   = auth()->user()->SiderbarName();
              $projectlogs->ipaddress                  = request()->ip();
              $projectlogs->activity                   = "Edited Project as Uncategorized";
              $projectlogs->proj_id                    = $id;
              $projectlogs->save();
              Alert::success('Project Edited as Uncategorized', '');
              return back();
          }
        }
    }

    public function CheckIfSectorExist($projId, $subSecId) {
        $secId = \App\Subsectors::where('id', $subSecId)->first()->sector;
        $projsec = \App\Sectorprojects::where('proj_id', $projId)->where('sector_id', $secId)->first();

        if($projsec!=null)
            return true;

        return false;
    }

    public function FindProvinceOnTable(Request $request) {
        //$provDes = Array();
        $prov_id = \App\Provincesprojects::where('proj_id', $request->id)->pluck('province_id');
        foreach ($prov_id as $pid) {
          $provDes = \App\Provinces::where('id', $pid)->pluck('provName');
        }
        return $provDes;
    }

    public function getAgencyType(){
      if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();

        return $agencytype;
      }
    }

    public function uncatproject(Request $request, $id){
        $project                             = \App\Projects::find($id);
        $project->SS_statusofsubmission      = "Uncategorized";
        $project->uncat                      = request('uncat');
        $project->save();

        $projectlogs                         = new \App\Projectlogs();
        $projectlogs->username               = auth()->user()->SiderbarName();
        $projectlogs->ipaddress              = request()->ip();
        $projectlogs->activity               = "Edited Project as Uncategorized";
        $projectlogs->proj_id                = $id;
        $projectlogs->save();
        
        Alert::success('Project Edited as Uncategorized', '');
        return back();
    }

}
