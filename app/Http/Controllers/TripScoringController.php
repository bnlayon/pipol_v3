<?php

namespace App\Http\Controllers;

use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class TripScoringController extends Controller
{
    public function validatedtrip(){
        $alltrips = \App\Projects::where('tripcategory', '=', 1)->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('trip', compact('chapters', 'alltrips','fundingsources','agencytype'));
    }

    public function validatedtrip2(){
        $alltrips = \App\Projects::where('tripcategory', '=', 2)->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('tripcat2', compact('chapters', 'alltrips','fundingsources','agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }


    public function cat2_save(Request $request, $id){
        $one = 0;
        foreach ($this->getScores($id) as $sectorDetail){
            if($sectorDetail->id != ""){
                $one = 1;
            }
        }
        
        if($one == 1){
            $hey = "criteria3_".$id;
            \App\Tripcat2projects::where("proj_id", $id)
            ->update(['criteria1'=> $request->criteria1,
                      'criteria2'=> $request->criteria2,
                      'criteria3'=> $request->$hey
                        ]);
            Alert::success('TRIP Score Edited', '');
            return back();
        }else{
            $add = new \App\Tripcat2projects;
            $add->proj_id = $id;
            $add->criteria1 = $request->criteria1;
            $add->criteria2 = $request->criteria2;
            $hey = "criteria3_".$id;
            $add->criteria3 = $request->$hey;
            $add->save();
            Alert::success('TRIP Score Saved', '');
            return back();
        }

    }

    public function cat1_save(Request $request, $id){
            $add = new \App\Tripcat1projects;
            $add->proj_id = $id;
            $add->criteria1 = $request->criteria1;
            $add->criteria2 = $request->criteria2;
            $add->criteria3 = $request->criteria3;
            $add->criteria4 = $request->criteria4;
            $add->criteria5 = $request->criteria5;
            $add->criteria6 = $request->criteria6;
            $add->criteria7 = $request->criteria7;
            $add->criteria8 = $request->criteria8;
            $add->criteria9 = $request->criteria9;
            $add->criteria10 = $request->criteria10;
            $add->criteria11 = $request->criteria11;
            $add->criteria12 = $request->criteria12;
            $add->criteria13 = $request->criteria13;
            $add->criteria14 = $request->criteria14;
            $add->criteria15 = $request->criteria15;
            $hey = "criteria16_".$id;
            $add->criteria16 = $request->$hey;
            $add->save();
            Alert::success('TRIP Score Saved', '');
            return back();
    }

    public function getScores($id){
        $sectorProjects = \App\Tripcat2projects::where('proj_id', $id)->get();
        return $sectorProjects;
    }
}
