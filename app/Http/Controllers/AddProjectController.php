<?php

namespace App\Http\Controllers;

use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class AddProjectController extends Controller
{
    public function addproject(Request $request){
        $project                             = new \App\Projects();
        $project->title                      = request('title');
        $project->prog_proj                  = request('prog_proj');
        $project->description                = request('description');
        $project->output                     = request('output');
        $project->spatial                    = request('spatial');
        $project->iccable                    = request('iccable');
        $project->currentlevel               = request('currentlevel');
        $project->approval_date1             = request('approval_date1');
        $project->approval_date2             = request('approval_date2');
        $project->approval_date3             = request('approval_date3');
        $project->approval_date4             = request('approval_date4');
        $project->approval_date5             = request('approval_date5');
        $project->pip                        = request('pip');
        $project->pip_typo                   = request('pip_typo');
        $project->cip                        = request('cip');
        $project->cip_typo                   = request('cip_typo');
        $project->trip                       = request('trip');
        $project->rdip                       = request('rdip');
        $project->rdc_endorsement            = request('rdc_endorsement');
        $project->rdc_endorsed_notendorsed   = request('rdc_endorsed_notendorsed');
        $project->rdc_date_endorsement       = request('rdc_date_endorsement');
        $project->mainpdp                    = request('mainpdp');
        $project->risk                       = request('risk');
        $project->gender                     = request('gender');
        $project->start                      = request('start');
        $project->end                        = request('end');
        $project->ppdetails                  = request('ppdetails');
        $project->ppdetails_others           = request('ppdetails_others');
        $project->rowa                       = request('rowa');
        $project->rc                         = request('rc');
        $project->wrrc                       = request('wrrc');
        $project->rowa_affected              = request('rowa_affected');
        $project->rc_affected                = request('rc_affected');
        $project->employment                 = request('employment');
        $project->mainfsource                = request('mainfsource');
        $project->modeofimplementation       = request('modeofimplementation');
        $project->category                   = request('category');
        $project->tier2_status               = request('status2');
        $project->updates                    = request('updates');
        $project->asof                       = request('asof');
        $project->tier1_uacs                 = request('tier1_uacs');
        $project->tier2_uacs                 = request('tier2_uacs');
        $project->agency_id                  = request('agency_id');

        //GET PROJECT CODE
        $ip = "localhost";
        $user = "pipolv2user";
        $pass = "Dranreb0792";
        $db = "pipolv2";
                            
        $conn = mysqli_connect($ip,$user,$pass,$db);
        $sql = "SELECT code, SUBSTRING_INDEX(`code`,'-',-1)+1 as code1 FROM projects WHERE agency_id = $project->agency_id ORDER BY code DESC LIMIT 1";
        $result = mysqli_query($conn,$sql);

        if (mysqli_num_rows($result)==0) { $finalcode = "000001"; 
        }

        while($row12=mysqli_fetch_array($result)){
            if($row12['code1'] == 0){
                $finalcode = "000001";
            }else{
                $finalcode = sprintf("%06d", $row12['code1']);
            }
         
        }

        $project->motheragency_id            = request('motheragency_id');
        $project->statusofsubmission         = request('statusofsubmission');
        $project->fsstatus                   = request('fsstatus');
        $project->fsstatus_ongoing           = request('fsstatus_ongoing');
        $project->fsstatus_prep              = request('fsstatus_prep');
        $project->tier1_type                 = request('status1');
        $project->fsassistance               = request('fsassistance');
        $project->NA_fs                      = request('NA_fs');
        $project->tier2_type                 = request('tier2_type');
        $project->other_fs                   = request('other_fs');
        $project->other_mode                 = request('other_mode');
        $project->reg_prog                   = request('reg_prog');
        $project->rd                         = request('rd');
        $project->NA_rm                      = request('NA_rm');

        if(auth()->user()->user_type == 'AG'){
            $project->code                   = request('code').$finalcode;
        }else{
            $getAgencyUACS = \App\Agencies::find(request('agency_id'));
            $project->code = $getAgencyUACS->UACS_DPT_ID.$getAgencyUACS->UACS_AGY_ID."-".$finalcode;
        }

        $project->save();

//INTERREGIONAL
        if(request('spatial') == 'Interregional'){
            if(!is_null(request('states'))){
                $regions_to_saves = request('states');
                foreach ($regions_to_saves as $regions_to_save) {
                    $project_region                        = new \App\Regionsprojects();
                    $project_region->proj_id               = $project->id;
                    $project_region->region_id             = $regions_to_save;
                    $project_region->save();
                }
            }

            if(!is_null(request('provinces'))){
                $provinces_to_saves = request('provinces');
                foreach ($provinces_to_saves as $provinces_to_save) {
                    $project_province                        = new \App\Provincesprojects();
                    $project_province->proj_id               = $project->id;
                    $project_province->province_id           = $provinces_to_save;
                    $project_province->save();
                }
            }

            if(!is_null(request('cities'))){
                $cities_to_saves = request('cities');
                foreach ($cities_to_saves as $cities_to_save) {
                    $project_cities                        = new \App\Citiesproject();
                    $project_cities->proj_id               = $project->id;
                    $project_cities->cities_id           = $cities_to_save;
                    $project_cities->save();
                }
            }
        }

//REGION SPECIFIC
        if(request('spatial') == 'Region Specific'){
            $project_region                        = new \App\Regionsprojects();
            $project_region->proj_id               = $project->id;
            $project_region->region_id             = request('region');
            $project_region->save();
        }

//OTHER TABLES
        if(!is_null(request('basis_id'))){
            $basis_to_saves = request('basis_id');
            foreach ($basis_to_saves as $basis_to_save) {
                $project_basis                       = new \App\Basisprojects();
                $project_basis->proj_id              = $project->id;
                $project_basis->basis_id             = $basis_to_save;
                $project_basis->save();
            }
        }

        if(!is_null(request('sectors'))){
            $infras_sector_to_saves = request('sectors');
            foreach ($infras_sector_to_saves as $infras_sector_to_save) {
                $project_sector                       = new \App\Sectorprojects();
                $project_sector->proj_id              = $project->id;
                $project_sector->sector_id            = $infras_sector_to_save;
                $project_sector->save();
            }
        }   

        if(!is_null(request('subsectors'))){
            $infras_subsector_to_saves = request('subsectors');
            foreach ($infras_subsector_to_saves as $infras_subsector_to_save) {
                $project_subsector                       = new \App\Subsectorprojects();
                $project_subsector->proj_id              = $project->id;
                $project_subsector->subsector_id         = $infras_subsector_to_save;
                $project_subsector->save();
            }
        }

        if(!is_null(request('status'))){
            $statuses_to_saves = request('status');
            foreach ($statuses_to_saves as $statuses_to_save) {
                $project_status                       = new \App\Statusprojects();
                $project_status->proj_id              = $project->id;
                $project_status->status_id            = $statuses_to_save;
                $project_status->save();
            }
        }

        if(!is_null(request('otherpdp'))){
            $otherpdp_to_saves = request('otherpdp');
            foreach ($otherpdp_to_saves as $otherpdp_to_save) {
                $project_otherpdp                       = new \App\Otherpdpprojects();
                $project_otherpdp->proj_id              = $project->id;
                $project_otherpdp->chap_id           = $otherpdp_to_save;
                $project_otherpdp->save();
            }
        }

        if(!is_null(request('agenda'))){
            $agenda_to_saves = request('agenda');
            foreach ($agenda_to_saves as $agenda_to_save) {
                $project_agenda_to_save                       = new \App\Agendaprojects();
                $project_agenda_to_save->proj_id              = $project->id;
                $project_agenda_to_save->agenda_id           = $agenda_to_save;
                $project_agenda_to_save->save();
            }
        }

        if(!is_null(request('sdg'))){
            $sdg_to_saves = request('sdg');
            foreach ($sdg_to_saves as $sdg_to_save) {
                $project_sdg_to_save                       = new \App\Sdgprojects();
                $project_sdg_to_save->proj_id              = $project->id;
                $project_sdg_to_save->sdg_id               = $sdg_to_save;
                $project_sdg_to_save->save();
            }
        }

        if(!is_null(request('oda_funding'))){
            $oda_to_saves = request('oda_funding');
            foreach ($oda_to_saves as $oda_to_save) {
                $project_oda_to_save                       = new \App\Fprojects();
                $project_oda_to_save->proj_id              = $project->id;
                $project_oda_to_save->fsource_no           = $oda_to_save;
                $project_oda_to_save->save();
            }
        }

        if(!is_null(request('coimp'))){
            $comimp_to_saves = request('coimp');
            foreach ($comimp_to_saves as $comimp_to_save) {
                $project_coimp                      = new \App\Agenciesproject();
                $project_coimp->proj_id             = $project->id;
                $project_coimp->agency_id           = $comimp_to_save;
                $project_coimp->save();
            }
        }

        if(!is_null(request('att'))){
            $att_to_saves = request('att');
            foreach ($att_to_saves as $att_to_save) {
                $project_att                     = new \App\Attagenciesproject();
                $project_att->proj_id             = $project->id;
                $project_att->agency_id           = $att_to_save;
                $project_att->save();
            }
        }

//FEASIB COST
            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2017;
            $fs->fscost                     = request('fs_2017');
            $fs->save();

            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2018;
            $fs->fscost                     = request('fs_2018');
            $fs->save();

            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2019;
            $fs->fscost                     = request('fs_2019');
            $fs->save();

            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2020;
            $fs->fscost                     = request('fs_2020');
            $fs->save();

            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2021;
            $fs->fscost                     = request('fs_2021');
            $fs->save();

            $fs                             = new \App\Fscosts();
            $fs->proj_id                    = $project->id;
            $fs->fsyear                     = 2022;
            $fs->fscost                     = request('fs_2022');
            $fs->save();

//ROWA COST
            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2017;
            $rowa->rowacost                   = request('rowa_2017');
            $rowa->save();

            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2018;
            $rowa->rowacost                   = request('rowa_2018');
            $rowa->save();

            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2019;
            $rowa->rowacost                   = request('rowa_2019');
            $rowa->save();

            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2020;
            $rowa->rowacost                   = request('rowa_2020');
            $rowa->save();

            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2021;
            $rowa->rowacost                   = request('rowa_2021');
            $rowa->save();

            $rowa                             = new \App\Rowacost();
            $rowa->proj_id                    = $project->id;
            $rowa->rowayear                   = 2022;
            $rowa->rowacost                   = request('rowa_2022');
            $rowa->save();

//RC
            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2017;
            $rc->rccost                     = request('rc_2017');
            $rc->save();

            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2018;
            $rc->rccost                     = request('rc_2018');
            $rc->save();

            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2019;
            $rc->rccost                     = request('rc_2019');
            $rc->save();

            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2020;
            $rc->rccost                     = request('rc_2020');
            $rc->save();

            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2021;
            $rc->rccost                     = request('rc_2021');
            $rc->save();

            $rc                             = new \App\Rccost();
            $rc->proj_id                    = $project->id;
            $rc->rcyear                     = 2022;
            $rc->rccost                     = request('rc_2022');
            $rc->save();

//FINANCIAL ACCOMPLISHMENTS
        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2017;
        $fa->facost_nep                 = request('nep_2017');
        $fa->facost_all                 = request('all_2017');
        $fa->facost_ad                  = request('ad_2017');
        $fa->save();

        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2018;
        $fa->facost_nep                 = request('nep_2018');
        $fa->facost_all                 = request('all_2018');
        $fa->facost_ad                  = request('ad_2018');
        $fa->save();

        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2019;
        $fa->facost_nep                 = request('nep_2019');
        $fa->facost_all                 = request('all_2019');
        $fa->facost_ad                  = request('ad_2019');
        $fa->save();

        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2020;
        $fa->facost_nep                 = request('nep_2020');
        $fa->facost_all                 = request('all_2020');
        $fa->facost_ad                  = request('ad_2020');
        $fa->save();

        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2021;
        $fa->facost_nep                 = request('nep_2021');
        $fa->facost_all                 = request('all_2021');
        $fa->facost_ad                  = request('ad_2021');
        $fa->save();

        $fa                             = new \App\Facost();
        $fa->proj_id                    = $project->id;
        $fa->fayear                     = 2022;
        $fa->facost_nep                 = request('nep_2022');
        $fa->facost_all                 = request('all_2022');
        $fa->facost_ad                  = request('ad_2022');
        $fa->save();

//INVESTMENT TARGETS COST
        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2016;
        // $it->local                      = request('it_2016_nglocal');
        // $it->loan                       = request('it_2016_ngloan');
        // $it->grant                      = request('it_2016_odagrant');
        // $it->gocc                       = request('it_2016_gocc');
        // $it->lgu                        = request('it_2016_lgu');
        // $it->private                    = request('it_2016_ps');
        // $it->others                     = request('it_2016_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2017;
        // $it->local                      = request('it_2017_nglocal');
        // $it->loan                       = request('it_2017_ngloan');
        // $it->grant                      = request('it_2017_odagrant');
        // $it->gocc                       = request('it_2017_gocc');
        // $it->lgu                        = request('it_2017_lgu');
        // $it->private                    = request('it_2017_ps');
        // $it->others                     = request('it_2017_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2018;
        // $it->local                      = request('it_2018_nglocal');
        // $it->loan                       = request('it_2018_ngloan');
        // $it->grant                      = request('it_2018_odagrant');
        // $it->gocc                       = request('it_2018_gocc');
        // $it->lgu                        = request('it_2018_lgu');
        // $it->private                    = request('it_2018_ps');
        // $it->others                     = request('it_2018_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2019;
        // $it->local                      = request('it_2019_nglocal');
        // $it->loan                       = request('it_2019_ngloan');
        // $it->grant                      = request('it_2019_odagrant');
        // $it->gocc                       = request('it_2019_gocc');
        // $it->lgu                        = request('it_2019_lgu');
        // $it->private                    = request('it_2019_ps');
        // $it->others                     = request('it_2019_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2020;
        // $it->local                      = request('it_2020_nglocal');
        // $it->loan                       = request('it_2020_ngloan');
        // $it->grant                      = request('it_2020_odagrant');
        // $it->gocc                       = request('it_2020_gocc');
        // $it->lgu                        = request('it_2020_lgu');
        // $it->private                    = request('it_2020_ps');
        // $it->others                     = request('it_2020_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2021;
        // $it->local                      = request('it_2021_nglocal');
        // $it->loan                       = request('it_2021_ngloan');
        // $it->grant                      = request('it_2021_odagrant');
        // $it->gocc                       = request('it_2021_gocc');
        // $it->lgu                        = request('it_2021_lgu');
        // $it->private                    = request('it_2021_ps');
        // $it->others                     = request('it_2021_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2022;
        // $it->local                      = request('it_2022_nglocal');
        // $it->loan                       = request('it_2022_ngloan');
        // $it->grant                      = request('it_2022_odagrant');
        // $it->gocc                       = request('it_2022_gocc');
        // $it->lgu                        = request('it_2022_lgu');
        // $it->private                    = request('it_2022_ps');
        // $it->others                     = request('it_2022_others');
        // $it->save();

        // $it                             = new \App\Investments();
        // $it->proj_id                    = $project->id;
        // $it->year                       = 2023;
        // $it->local                      = request('it_2023_nglocal');
        // $it->loan                       = request('it_2023_ngloan');
        // $it->grant                      = request('it_2023_odagrant');
        // $it->gocc                       = request('it_2023_gocc');
        // $it->lgu                        = request('it_2023_lgu');
        // $it->private                    = request('it_2023_ps');
        // $it->others                     = request('it_2023_others');
        // $it->save();

         $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2021;
        $it->local                      = request('it_2021_nglocal');
        $it->loan                       = request('it_2021_ngloan');
        $it->grant                      = request('it_2021_odagrant');
        $it->gocc                       = request('it_2021_gocc');
        $it->lgu                        = request('it_2021_lgu');
        $it->private                    = request('it_2021_ps');
        $it->others                     = request('it_2021_others');
        $it->save();

        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2022;
        $it->local                      = request('it_2022_nglocal');
        $it->loan                       = request('it_2022_ngloan');
        $it->grant                      = request('it_2022_odagrant');
        $it->gocc                       = request('it_2022_gocc');
        $it->lgu                        = request('it_2022_lgu');
        $it->private                    = request('it_2022_ps');
        $it->others                     = request('it_2022_others');
        $it->save();

        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2023;
        $it->local                      = request('it_2023_nglocal');
        $it->loan                       = request('it_2023_ngloan');
        $it->grant                      = request('it_2023_odagrant');
        $it->gocc                       = request('it_2023_gocc');
        $it->lgu                        = request('it_2023_lgu');
        $it->private                    = request('it_2023_ps');
        $it->others                     = request('it_2023_others');
        $it->save();

        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2024;
        $it->local                      = request('it_2024_nglocal');
        $it->loan                       = request('it_2024_ngloan');
        $it->grant                      = request('it_2024_odagrant');
        $it->gocc                       = request('it_2024_gocc');
        $it->lgu                        = request('it_2024_lgu');
        $it->private                    = request('it_2024_ps');
        $it->others                     = request('it_2024_others');
        $it->save();

        $it                             = new \App\Investments();
        $it->proj_id                    = $project->id;
        $it->year                       = 2025;
        $it->local                      = request('it_2025_nglocal');
        $it->loan                       = request('it_2025_ngloan');
        $it->grant                      = request('it_2025_odagrant');
        $it->gocc                       = request('it_2025_gocc');
        $it->lgu                        = request('it_2025_lgu');
        $it->private                    = request('it_2025_ps');
        $it->others                     = request('it_2025_others');
        $it->save();

// INFRA COST
        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2016;
        //     $ic->local                      = request('ic_2016_nglocal');
        //     $ic->loan                       = request('ic_2016_ngloan');
        //     $ic->grant                      = request('ic_2016_odagrant');
        //     $ic->gocc                       = request('ic_2016_gocc');
        //     $ic->lgu                        = request('ic_2016_lgu');
        //     $ic->private                    = request('ic_2016_ps');
        //     $ic->others                     = request('ic_2016_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2017;
        //     $ic->local                      = request('ic_2017_nglocal');
        //     $ic->loan                       = request('ic_2017_ngloan');
        //     $ic->grant                      = request('ic_2017_odagrant');
        //     $ic->gocc                       = request('ic_2017_gocc');
        //     $ic->lgu                        = request('ic_2017_lgu');
        //     $ic->private                    = request('ic_2017_ps');
        //     $ic->others                     = request('ic_2017_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2018;
        //     $ic->local                      = request('ic_2018_nglocal');
        //     $ic->loan                       = request('ic_2018_ngloan');
        //     $ic->grant                      = request('ic_2018_odagrant');
        //     $ic->gocc                       = request('ic_2018_gocc');
        //     $ic->lgu                        = request('ic_2018_lgu');
        //     $ic->private                    = request('ic_2018_ps');
        //     $ic->others                     = request('ic_2018_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2019;
        //     $ic->local                      = request('ic_2019_nglocal');
        //     $ic->loan                       = request('ic_2019_ngloan');
        //     $ic->grant                      = request('ic_2019_odagrant');
        //     $ic->gocc                       = request('ic_2019_gocc');
        //     $ic->lgu                        = request('ic_2019_lgu');
        //     $ic->private                    = request('ic_2019_ps');
        //     $ic->others                     = request('ic_2019_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2020;
        //     $ic->local                      = request('ic_2020_nglocal');
        //     $ic->loan                       = request('ic_2020_ngloan');
        //     $ic->grant                      = request('ic_2020_odagrant');
        //     $ic->gocc                       = request('ic_2020_gocc');
        //     $ic->lgu                        = request('ic_2020_lgu');
        //     $ic->private                    = request('ic_2020_ps');
        //     $ic->others                     = request('ic_2020_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2021;
        //     $ic->local                      = request('ic_2021_nglocal');
        //     $ic->loan                       = request('ic_2021_ngloan');
        //     $ic->grant                      = request('ic_2021_odagrant');
        //     $ic->gocc                       = request('ic_2021_gocc');
        //     $ic->lgu                        = request('ic_2021_lgu');
        //     $ic->private                    = request('ic_2021_ps');
        //     $ic->others                     = request('ic_2021_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2022;
        //     $ic->local                      = request('ic_2022_nglocal');
        //     $ic->loan                       = request('ic_2022_ngloan');
        //     $ic->grant                      = request('ic_2022_odagrant');
        //     $ic->gocc                       = request('ic_2022_gocc');
        //     $ic->lgu                        = request('ic_2022_lgu');
        //     $ic->private                    = request('ic_2022_ps');
        //     $ic->others                     = request('ic_2022_others');
        //     $ic->save();

        //     $ic                             = new \App\Infrastructures();
        //     $ic->proj_id                    = $project->id;
        //     $ic->year                       = 2023;
        //     $ic->local                      = request('ic_2023_nglocal');
        //     $ic->loan                       = request('ic_2023_ngloan');
        //     $ic->grant                      = request('ic_2023_odagrant');
        //     $ic->gocc                       = request('ic_2023_gocc');
        //     $ic->lgu                        = request('ic_2023_lgu');
        //     $ic->private                    = request('ic_2023_ps');
        //     $ic->others                     = request('ic_2023_others');
        //     $ic->save();

        // $regions = \App\Regions::all();
        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2016;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2016_'.$region->region_code);
        //     if(!is_null(request('2016_'.$region->region_code))){
        //         $regioncost->save();
        //     }
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2017;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2017_'.$region->region_code);
        //     if(!is_null(request('2017_'.$region->region_code))){
        //         $regioncost->save();
        //     }
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2018;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2018_'.$region->region_code);
        //     if(!is_null(request('2018_'.$region->region_code))){
        //         $regioncost->save();
        //     }      
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2019;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2019_'.$region->region_code);
        //     if(!is_null(request('2019_'.$region->region_code))){
        //         $regioncost->save();
        //     }       
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2020;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2020_'.$region->region_code);
        //     if(!is_null(request('2020_'.$region->region_code))){
        //         $regioncost->save();
        //     }       
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2021;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2021_'.$region->region_code);
        //     if(!is_null(request('2021_'.$region->region_code))){
        //         $regioncost->save();
        //     }       
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2022;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2022_'.$region->region_code);
        //     if(!is_null(request('2022_'.$region->region_code))){
        //         $regioncost->save();
        //     }    
        // }

        // foreach ($regions as $region) {
        //     $regioncost                     = new \App\Regionalcosts();
        //     $regioncost->region             = $region->region_no;
        //     $regioncost->year               = 2023;
        //     $regioncost->proj_id            = $project->id;
        //     $regioncost->cost               = request('2023_'.$region->region_code);
        //     if(!is_null(request('2023_'.$region->region_code))){
        //         $regioncost->save();
        //     }      
        // }


//RMs
        if(!is_null(request('_1rm'.$project->mainpdp))){
            $_1rm_to_saves = request('_1rm'.$project->mainpdp);
            foreach ($_1rm_to_saves as $_1rm_to_save) {
                $project_1rm                      = new \App\_1matricesprojects();
                $project_1rm->proj_id             = $project->id;
                $project_1rm->_1rm_id             = $_1rm_to_save;
                $project_1rm->save();
            }
        }

        if(!is_null(request('_2rm'.$project->mainpdp))){
            $_2rm_to_saves = request('_2rm'.$project->mainpdp);
            foreach ($_2rm_to_saves as $_2rm_to_save) {
                $project_2rm                      = new \App\_2matricesprojects();
                $project_2rm->proj_id             = $project->id;
                $project_2rm->_2rm_id             = $_2rm_to_save;
                $project_2rm->save();
            }
        }

        if(!is_null(request('_3rm'.$project->mainpdp))){
            $_3rm_to_saves = request('_3rm'.$project->mainpdp);
            foreach ($_3rm_to_saves as $_3rm_to_save) {
                $project_3rm                      = new \App\_3matricesprojects();
                $project_3rm->proj_id             = $project->id;
                $project_3rm->_3rm_id             = $_3rm_to_save;
                $project_3rm->save();
            }
        }

        if(!is_null(request('_4rm'.$project->mainpdp))){
            $_4rm_to_saves = request('_4rm'.$project->mainpdp);
            foreach ($_4rm_to_saves as $_4rm_to_save) {
                $project_4rm                      = new \App\_4matricesprojects();
                $project_4rm->proj_id             = $project->id;
                $project_4rm->_4rm_id             = $_4rm_to_save;
                $project_4rm->save();
            }
        }

        $addedprojectid = $project->id;

        if(request('statusofsubmission') == 'Draft'){
            $projectlogs                         = new \App\Projectlogs();
            $projectlogs->username                   = auth()->user()->SiderbarName();
            $projectlogs->ipaddress                  = request()->ip();
            $projectlogs->activity                   = "Added Project as Draft";
            $projectlogs->proj_id                    = $addedprojectid;
            $projectlogs->save();
            Alert::success('Project Added as Draft', '');
            return back();
        }else if(request('statusofsubmission') == 'Endorsed'){
            $projectlogs                         = new \App\Projectlogs();
            $projectlogs->username                   = auth()->user()->SiderbarName();
            $projectlogs->ipaddress                  = request()->ip();
            $projectlogs->activity                   = "Added Project as Endorsed";
            $projectlogs->proj_id                    = $addedprojectid;
            $projectlogs->save();
            Alert::success('Project Added as Endorsed', '');
            return back();
        }

    }
}
