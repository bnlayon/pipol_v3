<?php

namespace App\Http\Controllers;

use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class ViewAllMinda extends Controller
{
    public function allminda(){

        $allpips = \App\Projects::where('pip', '=', 1)
            ->where(function($q) {
                $q->where('statusofsubmission', 'Endorsed')
                ->whereIn('id',
                    \App\Regionsprojects::whereIn('region_id', array(9,10,11,12,13,14))
                            ->pluck('proj_id')
                          );
            })->get();

        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('allminda', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
