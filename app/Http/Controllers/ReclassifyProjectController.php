<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Email;
use App\Http\Controllers\Controller;

class ReclassifyProjectController extends Controller
{
    public function reclassifyproject($id){
        $projects = \App\Projects::find($id);
        $chapters = \App\Chapters::all();
        $chapters_projects = \App\Otherpdpprojects::where('proj_id', $id)->get();
        return view('reclassify', compact('projects','chapters', 'chapters_projects'));
    }

    public function reclassifysave($id){
        $reclassify                             = new \App\Reclassification();
        $reclassify->requestor                  = auth()->user()->fname.' '.auth()->user()->lname;
        $reclassify->proj_id                    = $id;
        $reclassify->orig_pdp                   = request('orig_pdp');
        $reclassify->orig_other_pdp             = request('orig_other_pdp');
        $reclassify->proposed_pdp               = request('proposed_pdp');
        if(!empty(request('proposed_other_pdp'))){
        $imploded = implode(", ",request('proposed_other_pdp'));
        $reclassify->proposed_other_pdp         = $imploded;
        }
        $reclassify->remarks_proposed           = request('remarks_proposed');
        $reclassify->status                     = "For Action";
        $reclassify->save();

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = "pipol.neda@gmail.com";
        $mail->Password = "parasabayan";
        $mail->IsHTML(true);
        $mail->SetFrom('pipol.neda@gmail.com');
        $mail->Subject = 'PIPOL PROJECT RECLASSIFICATION'; 
        // VALIDATED PAPS IN THE PIPOL SYSTEM
        //ob_start(); //STARTS THE OUTPUT BUFFER
        //include('midpointmessage.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        //$some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        //ob_clean();  //CLEAN OUT THE OUTPUT BUFFER

        $recl = \App\Projects::find($reclassify->proj_id);
        $focals = \App\Focals::where('chap','=', $reclassify->proposed_pdp)->first();
        // $users = \App\Users::select('email')->where('focal_no','=', $focals->focal_id)->where('user_type','=', 'SS')->get();
        
        $mail->Body = "Project Title: ".$recl->title."<br>";
        $mail->Body .= "Remarks: ".$reclassify->remarks_proposed."<br>";
        $mail->Body .= "Reclassification Request from: ".$reclassify->requestor."<br>";

        // $mail->AddReplyTo('pip@neda.gov.ph');
        // //$mail->AddAddress($submission->head_email);
        // //$mail->AddAddress($submission->mother_email);

        // foreach($users as $user){
        //     $mail->AddAddress($user->email);            
        // }
        // // foreach ($submission->getUserDetails($submission->id) as $useremail){
        // //     $mail->AddCC($useremail->email);
        // // }
        // // $mail->AddCC("BNLayon@neda.gov.ph");
        // // $mail->AddCC("pip@neda.gov.ph");
        // // $mail->AddCC("MLDelRosario@neda.gov.ph");            
        // $mail->Send();

        \App\Projects::where("id", $id)
            ->update(['SS_statusofsubmission'=> 'For Reclassification']);
        Alert::success('Request Sent', '');
        return back();
    }

    public function cancelrequest($id){
        \App\Reclassification::where("id", $id)
            ->update(['status'=> 'Cancelled']);
        Alert::success('Request Cancelled', '');
        return back();
    }

    public function acceptrequest($id){
        \App\Reclassification::where("id", $id)
            ->update(['status'=> 'Accepted']);

        $recl = \App\Reclassification::find($id);

        \App\Projects::where("id", $recl->proj_id)
            ->update(['mainpdp' => request('proposed_pdp'), 'SS_statusofsubmission'=> 'Reclassified']);

            $project = \App\Projects::find($recl->proj_id);
            $otherpdps = array();
            $otherpdps_to_save = explode(", ",request('proposed_other_pdp'));
            if(!is_null($otherpdps_to_save)){
                foreach ($otherpdps_to_save as $otherpdp) {

                    if($otherpdp!=0)
                        $otherpdps[] = $otherpdp;
                }
            }
            $project->projects_otherpdps()->sync($otherpdps);
            $project->save();

            $focals = \App\Focals::where('chap','=', $recl->orig_pdp)->first();
            $users = \App\Users::select('email')->where('focal_no','=', $focals->focal_id)->get();

            $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
            $mail->Username = "pipol.neda@gmail.com";
            $mail->Password = "parasabayan";
            $mail->IsHTML(true);
            $mail->SetFrom('pipol.neda@gmail.com');
            $mail->Subject = 'PIPOL PROJECT RECLASSIFICATION'; 
            // VALIDATED PAPS IN THE PIPOL SYSTEM
            //ob_start(); //STARTS THE OUTPUT BUFFER
            //include('midpointmessage.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
            //$some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
            //ob_clean();  //CLEAN OUT THE OUTPUT BUFFER

            $recl2 = \App\Projects::find($recl->proj_id);
            $focals = \App\Focals::where('chap','=', $recl->orig_pdp)->first();
            $users = \App\Users::select('email')->where('focal_no','=', $focals->focal_id)->where('user_type','=', 'SS')->get();
            
            $mail->Body = "Project Title: ".$recl2->title."<br>";
            $mail->Body .= "Project Reclassification: ACCEPTED <br>";

            $mail->AddReplyTo('pip@neda.gov.ph');
            //$mail->AddAddress($submission->head_email);
            //$mail->AddAddress($submission->mother_email);

            foreach($users as $user){
                $mail->AddAddress($user->email);            
            }
            // foreach ($submission->getUserDetails($submission->id) as $useremail){
            //     $mail->AddCC($useremail->email);
            // }
            // $mail->AddCC("BNLayon@neda.gov.ph");
            // $mail->AddCC("pip@neda.gov.ph");
            // $mail->AddCC("MLDelRosario@neda.gov.ph");            
            $mail->Send();

            $projectlogs                             = new \App\Projectlogs();
            $projectlogs->username                   = auth()->user()->SiderbarName();
            $projectlogs->ipaddress                  = request()->ip();
            $projectlogs->activity                   = "Reclassified PDP Chapter";
            $projectlogs->proj_id                    = $recl->proj_id;
            $projectlogs->save();

        Alert::success('Request Accepted', '');
        return back();
    }

    public function declinerequest($id){
        \App\Reclassification::where("id", $id)
            ->update(['status' => 'Declined',
                      'reasons_declined' => request('reasons_declined')]);

            $recl = \App\Reclassification::find($id);
            $focals = \App\Focals::where('chap','=', $recl->orig_pdp)->first();
            $users = \App\Users::select('email')->where('focal_no','=', $focals->focal_id)->get();

            $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
            $mail->Username = "pipol.neda@gmail.com";
            $mail->Password = "parasabayan";
            $mail->IsHTML(true);
            $mail->SetFrom('pipol.neda@gmail.com');
            $mail->Subject = 'PIPOL PROJECT RECLASSIFICATION'; 
            // VALIDATED PAPS IN THE PIPOL SYSTEM
            //ob_start(); //STARTS THE OUTPUT BUFFER
            //include('midpointmessage.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
            //$some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
            //ob_clean();  //CLEAN OUT THE OUTPUT BUFFER

            $recl2 = \App\Projects::find($recl->proj_id);
            $focals = \App\Focals::where('chap','=', $recl->orig_pdp)->first();
            $users = \App\Users::select('email')->where('focal_no','=', $focals->focal_id)->where('user_type','=', 'SS')->get();
            
            $mail->Body = "Project Title: ".$recl2->title."<br>";
            $mail->Body .= "Project Reclassification: DECLINED <br>";
            $mail->Body .= "Reasons: DECLINED, ".$recl->reasons_declined."<br>";
            
            $mail->AddReplyTo('pip@neda.gov.ph');
            //$mail->AddAddress($submission->head_email);
            //$mail->AddAddress($submission->mother_email);

            foreach($users as $user){
                $mail->AddAddress($user->email);            
            }
            // foreach ($submission->getUserDetails($submission->id) as $useremail){
            //     $mail->AddCC($useremail->email);
            // }
            //$mail->AddCC("BNLayon@neda.gov.ph");
            // $mail->AddCC("pip@neda.gov.ph");
            // $mail->AddCC("MLDelRosario@neda.gov.ph");            
            $mail->Send();

        Alert::success('Request Declined', '');
        return back();
    }

    public function editrequest($id){
        $imploded = "";
        if(!empty(request('proposed_other_pdp_edit'))){
        $imploded = implode(", ",request('proposed_other_pdp_edit'));
        }
        \App\Reclassification::where("id", $id)
            ->update(['proposed_pdp' => request('mainpdp_r'),
                      'remarks_proposed' => request('remarks_proposed_r'),
                      'proposed_other_pdp' => $imploded]);
        Alert::success('Request Edited', '');
        return back();
    }
}
