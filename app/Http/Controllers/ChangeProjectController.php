<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class ChangeProjectController extends Controller
{
    public function complete(Request $request, $id){
        \App\Projects::where("id", $id)
            ->update(['category'=> 'Completed',
                      'statusofsubmission'=> 'Endorsed']);

        Alert::success('Project Marked as Completed', '');
        return back();
    }

    public function dropped(Request $request, $id){
        \App\Projects::where("id", $id)
            ->update(['category'=> 'Dropped',
                      'statusofsubmission'=> 'Dropped']);

        $dropping                   = new \App\Droppingreasons();
        $dropping->proj_id          = $id;
        $dropping->reasons          = request('droppingreasons');
        $dropping->save();

        Alert::success('Project Marked as Dropped', '');
        return back();
    }
}
