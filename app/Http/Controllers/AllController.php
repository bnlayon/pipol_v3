<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class AllController extends Controller
{

    public function all(){
        $allpips = \App\Projects::where('pip', '=', 1)
            ->where(function($q) {
                $q->where('statusofsubmission', 'Draft')
                ->orWhere('statusofsubmission', 'Endorsed');
            })->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('all', compact('chapters', 'allpips','fundingsources','agencytype'));
    }

    public function all2(){
        $alltrips = \App\Projects::where('trip', '=', 1)
            ->where(function($q) {
                $q->where('statusofsubmission', 'Draft')
                ->orWhere('statusofsubmission', 'Endorsed');
            })->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('all2', compact('chapters', 'alltrips','fundingsources','agencytype'));
    }

    public function all3(){
        $droppedprojects = \App\Projects::where('category', '=', 'Dropped')->get();
            
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('all3', compact('chapters', 'droppedprojects','fundingsources','agencytype'));
    }

    public function all4(){
        $completedprojects = \App\Projects::where('category', '=', 'Completed')->get();

        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('all4', compact('chapters', 'completedprojects','fundingsources','agencytype'));
    }

    public function all5(){
        $alluncat = \App\Projects::where('pip', '=', 0)->where('trip', '=', 0)->get();
        $chapters = \App\Chapters::all();
        $fundingsources = \App\Fundingsources::all();
        $agencytype = $this->getAgencyType();
        return view('all5', compact('chapters', 'alluncat','fundingsources','agencytype'));
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
