<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Document;
use App\Http\Requests;
use App\Product;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function dashboard(){
        if(auth()->user()->user_type == 'AG'){
            $submission = \App\Submissions::find(auth()->user()->submission_id);
            $countdraftproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('statusofsubmission', '=', 'Draft')->count();
            $countendorsedproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('statusofsubmission', '=', 'Endorsed')->count();
            $countcompletedproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('statusofsubmission', '=', 'Completed')->count();
            $countpipproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('pip', '=', 1)->count();
            $countcipproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('cip', '=', 1)->count();
            $counttripproject = \App\Projects::where('agency_id', '=', $submission->agency_id)->where('trip', '=', 1)->count();
            
            $agencytype = $this->getAgencyType();
        }

        if(auth()->user()->user_type == 'SS'){
            $focals = \App\Focals::where('focal_id','=', auth()->user()->focal_no)->get();

            foreach($focals as $focal){
                $chaps[] = $focal->chap;
            }

            $countmychap = \App\Projects::where('statusofsubmission', '=', 'Endorsed')->whereIn('mainpdp', $chaps)->count();

            $countvalidated = \App\Projects::where('SS_statusofsubmission', '=', 'Validated')->whereIn('mainpdp', $chaps)->count();

            $countforreclassification = \App\Projects::where('SS_statusofsubmission', '=', 'For Reclassification')->whereIn('mainpdp', $chaps)->count();

            $countforyouraction = \App\Reclassification::where('status', '=', 'For Action')->whereIn('proposed_pdp', $chaps)->count();

            $countaccepted = \App\Reclassification::where('status', '=', 'Accepted')->whereIn('proposed_pdp', $chaps)->count();

            $countdeclined = \App\Reclassification::where('status', '=', 'Declined')->whereIn('proposed_pdp', $chaps)->count();

        }

        if(auth()->user()->user_type == 'NRO'){
            $countnationwide = \App\Projects::where('statusofsubmission', '=', 'Endorsed')->where('spatial', '=', 'Nationwide')->count();

            $countinter = \App\Projects::where('statusofsubmission', '=', 'Endorsed')->where('spatial', '=', 'Interregional')->count();

            $rprojects = \App\Regionsprojects::where('region_id','=', auth()->user()->focal_no)->get();

            foreach($rprojects as $rproject){
                $nos[] = $rproject->proj_id;
            }

            $countreg = \App\Projects::where('pip', '=', 1)
                ->where('statusofsubmission', 'Endorsed')
                ->where('spatial', 'Region Specific')
            ->whereIn('id', $nos)
                ->count();

        }

        return view('dashboard', compact('countdraftproject', 'countendorsedproject', 'countcompletedproject', 'countpipproject', 'countcipproject', 'counttripproject','agencytype','countmychap','countvalidated','countforreclassification','countforyouraction', 'countaccepted','countdeclined', 'countnationwide', 'countinter', 'countreg'));    
    }

    public function getAgencyType(){
        if(auth()->user()->user_type == 'AG'){
        $submission = \App\Submissions::find(auth()->user()->submission_id);
        $agencytype = \App\Agencies::where('id', $submission->agency_id)->first();
        return $agencytype;
        }else{

        }
    }
}
