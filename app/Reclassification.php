<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reclassification extends Model
{
    public function getProjectDetails($id){
        $projectDetails = \App\Projects::where('id', $id)->get();
        return $projectDetails;
    }

    public function getAgencyDetails($id){
    	$agencyDetails = \App\Agencies::where('id', $id)->get();
    	return $agencyDetails;
    }
}
