<?php

//PIPOL Landing PAge
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/login', 'PipolController@login')->name('login');
Route::post('/login', 'LoginUserController@login_user');
Route::get('/logout', 'LoginUserController@logout');

Route::get('/changepassword', 'PipolController@changepassword');
Route::post('/changepassworduser', 'PipolController@changepassword_user');

// Route::get('/register', 'PipolController@register');
// Route::post('/register', 'RegisterUserController@register_user');
Route::get('/getmother', 'RegisterUserController@get_mother');

Route::post('/activate/{id}', 'RegisterUserController@activate');
Route::post('/decline/{id}', 'RegisterUserController@decline');

Route::post('/deactivate/{id}', 'RegisterUserController@deactivate');
Route::post('/reactivate/{id}', 'RegisterUserController@reactivate');

Route::get('/addproject', 'LoadAddModuleController@addproject');
Route::get('/addproject1', 'LoadAddModuleController@addproject1');
Route::post('/addproject', 'AddProjectController@addproject');

Route::get('/sendemail', 'RegisterUserController@sendemail');

Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

Route::get('/editproject/{id}', 'EditProjectController@editproject');
Route::post('/editproject/{id}', 'EditProjectController@editsave');
Route::post('/uncatproject/{id}', 'EditProjectController@uncatproject');

Route::post('/markascomplete/{id}', 'ChangeProjectController@complete');
Route::post('/markasdropped/{id}', 'ChangeProjectController@dropped');

Route::get('/validateproject', 'ValidateController@validateproject');
Route::get('/reclassification', 'ValidateController@reclassification');
Route::get('/tripvalidateproject', 'ValidateController@tripvalidateproject');

Route::get('/appraisal', 'AppraisalController@appraisal');
Route::get('/cip', 'AppraisalController@cip');

// Route::get('/appraisal2', 'AppraisalController@appraisal2');
Route::get('/appraisal2', 'AppraisalController@appraisal2');
Route::get('appraisal2-list', 'AppraisalController@allTripProjects');


Route::get('/appraisal3', 'AppraisalController@appraisal3');
Route::post('appraisal3-list', 'AppraisalController@allDroppedProjects')->name('appraisal_dropped');

Route::get('/appraisal4', 'AppraisalController@appraisal4');
Route::get('appraisal4-list', 'AppraisalController@allCompletedProjects');

Route::get('/oversight', 'AppraisalController@oversight');
Route::post('oversight-list', 'AppraisalController@allValidatedPIP')->name('product_catalog');

Route::get('/appraisal5', 'AppraisalController@appraisal5');

Route::get('/drafts', 'AppraisalController@drafts');
Route::get('/nationwide', 'AppraisalController@nationwide');
Route::get('/interregional', 'AppraisalController@interregional');
Route::get('/region', 'AppraisalController@region');

Route::get('/all', 'AllController@all');
Route::get('/all2', 'AllController@all2');
Route::get('/all3', 'AllController@all3');
Route::get('/all4', 'AllController@all4');
Route::get('/all5', 'AllController@all5');

Route::get('/viewprojects', 'ViewProjectsController@viewprojects');
Route::get('/viewprojectsattached', 'ViewProjectsController@viewprojectsattached');
Route::get('/viewprojectsprint/{id}', 'ViewProjectsController@viewprojectsprint');

Route::get('/links', 'PipolController@links');
Route::get('/report', 'PipolController@report');
Route::get('/manual', 'PipolController@manual');
Route::get('/video', 'PipolController@video');
// Route::get('/users', 'UsersController@users');
Route::get('/users', 'UserCrudController@users2')->middleware('auth');

Route::post('/getProvince','PipolController@getProvince');
Route::post('/getCity','PipolController@getCity');

Route::post('/findProvince','EditProjectController@FindProvinceOnTable');

Route::get('/getAgencyType', 'PipolController@get_agencytype');

//MINDA PROJECTS
Route::get('/allminda', 'ViewAllMinda@allminda');

//Email Notification
Route::get('/emailnotif', 'EmailNotifController@emailnotif');

Route::get('/midpointmessage/{id}', 'SendEmailController@midpointmessage');
Route::post('/midpointmessage/{id}', 'SendEmailController@sendemail');

//Reclassify
Route::get('/reclassify/{id}', 'ReclassifyProjectController@reclassifyproject');
Route::post('/reclassify/{id}', 'ReclassifyProjectController@reclassifysave');

Route::post('/cancelrequest/{id}', 'ReclassifyProjectController@cancelrequest');
Route::post('/acceptrequest/{id}', 'ReclassifyProjectController@acceptrequest');
Route::post('/declinerequest/{id}', 'ReclassifyProjectController@declinerequest');
Route::post('/editrequest/{id}', 'ReclassifyProjectController@editrequest');

//TRIP scoring
Route::get('/trip', 'TripScoringController@validatedtrip');
Route::get('/tripcat2', 'TripScoringController@validatedtrip2');
Route::post('/cat2_save/{id}', 'TripScoringController@cat2_save');
Route::post('/cat1_save/{id}', 'TripScoringController@cat1_save');

Route::get('/rdip', 'PipolController@rdip');

Route::get('/oversight', 'AppraisalController@oversight');

Route::post('/adduser', 'UsersController@adduser')->name('adduser');
// Route::get('/signup', 'UsersController@signup')->name('signup');
Route::get('/register', 'UsersController@signup')->name('register');
//Async Find Agency
Route::post('/asyncfindagency', 'UsersController@asyncfindagency');
Route::post('/asyncfindagencyold', 'UsersController@asyncfindagencyold');
Route::post('/images-upload', 'UsersController@upload');
Route::post('getMother', 'UsersController@getMother');

Route::get('ajax-crud', 'UserCrudController@index');
Route::get('userrequest', 'UserCrudController@userrequest');
Route::get('disabledusers', 'UserCrudController@disabledusers');
Route::post('asyncfindagencysubid', 'UserCrudController@asyncfindagencysubid');
Route::post('getSubmissionID', 'UserCrudController@getSubmissionID');
Route::post('adduserman', 'UserCrudController@store');

Route::post('ajax-crud/update', 'UserCrudController@update')->name('ajax-crud.update');

Route::get('ajax-crud/destroy/{id}', 'UserCrudController@destroy');
Route::get('ajax-crud/approve/{id}', 'UserCrudController@approve');
Route::get('disable/{id}', 'UserCrudController@disable');
Route::get('/getAgency', 'UserCrudController@getAgency');
// Route::get('/users2', 'UserCrudController@users2')->middleware('auth');
Route::post('openauth', 'UserCrudController@openauth');
Route::post( 'disapprove', 'UserCrudController@disapprove');

//Asyncs
Route::post( '/asyncFindRegions', 'LoadAddModuleController@asyncFindRegions');
Route::post( '{id}/asyncFindRegions', 'LoadAddModuleController@asyncFindRegions');
Route::post( 'getProvinces', 'LoadAddModuleController@getProvinces');
Route::post('{id}/getProvinces', 'LoadAddModuleController@getProvinces');
Route::post( 'getCities', 'LoadAddModuleController@getCities');
Route::post('{id}/getCities', 'LoadAddModuleController@getCities');
Route::post( 'getRegions', 'LoadAddModuleController@getRegions');
Route::post('{id}/getRegions', 'LoadAddModuleController@getRegions');
Route::post('getLevelsOfGAD', 'LoadAddModuleController@getLevelsOfGAD');
Route::post('{id}/getLevelsOfGAD', 'LoadAddModuleController@getLevelsOfGAD');
Route::post('getTier1Statuses', 'LoadAddModuleController@getTier1Statuses');
Route::post('{id}/getTier1Statuses', 'LoadAddModuleController@getTier1Statuses');

//AddModuleSave
Route::post('savetodb', 'AddProject1Controller@savetodb');
Route::post('{id}/savetodb', 'AddProject1Controller@savetodb');
// Route::get('edit/{id}', 'LoadAddModuleController@edit');
Route::get('editproject1/{id}', 'EditProjectController1@editproject');
Route::post('{id}/editsave', 'EditProjectController1@editsave');