
require('./bootstrap');

window.Vue = require('vue');
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);
import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)
import VModal from 'vue-js-modal'
Vue.use(VModal)
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('add-user', require('./components/AddUser.vue'));
Vue.component('ImageUploader', require('./components/ImageUploader.vue'));
Vue.component('user-crud', require('./components/UserCrud.vue'));
Vue.component('addproject', require('./components/AddProject.vue'));
Vue.component('editproject', require('./components/Editproject/EditProject.vue'));

const app = new Vue({
    el: '#app'
});
