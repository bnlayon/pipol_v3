@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Validate Projects </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validate PIP/CIP Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">PIP/CIP</a>
        </li>
      </ul>

      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="example" class="table" style="width: 100%;">
        <thead>
            <tr>
              <th>Project Title</th>
              <th>Agency</th>
              <th>Spatial Coverage</th>
              <th>Region</th>
              <th>Head Chapter</th>
              <th>Funding Source</th>
              <th>Category</th>
              <th>Action</th>
              <th>Function</th>
            </tr>
        </thead>
        <tbody>
           @foreach ($allpips as $allpip)
            <tr>
              <td>{{$allpip->title}}</td>
              <td>@foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                @endforeach
              </td>
              <td>{{$allpip->spatial}}</td>
                <td></td>
                <td>Chapter {{$allpip->mainpdp}}</td>
              <td>
                @foreach ($fundingsources as $fundingsource)
                    @if($fundingsource->fsource_no == $allpip->mainfsource)
                      {{$fundingsource->fsource_description}}
                    @endif
                  @endforeach
              </td>
              <td>{{$allpip->category}}</td>
              <td>
                <a href="{{ asset('/editproject') }}/{{$allpip->id}}" target="_blank"><button type="button" class="btn btn-primary"><i class="icon-list-1"></i></button></a>
              </td>
              <td>
                <a href="{{ asset('/viewprojectsprint') }}/{{$allpip->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
              </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>  
              <th>Project Title</th>
              <th>Agency</th>
              <th>Spatial Coverage</th>
              <th>Region</th>
              <th>Head Chapter</th>
              <th>Funding Source</th>
              <th>Category</th>
              <th>Action</th>
              <th>Function</th>
            </tr>
        </tfoot>
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>

    <div id="reclassify" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
      <div role="document" class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 id="exampleModalLabel" class="modal-title">Reclassify Project</h5>
                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                          </div>
                          <div class="modal-body">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam mauris nibh, bibendum ac aliquam ac, dignissim sagittis sapien. Fusce tempor lectus quis lectus fermentum, id faucibus orci consectetur. Proin neque nunc, pellentesque condimentum magna facilisis, ultricies dictum odio. Integer et lobortis lectus. Proin vitae est vel turpis ultricies vehicula. Morbi vel augue eleifend, efficitur tortor sit amet, faucibus ex. Donec malesuada aliquet turpis, vel vehicula nulla convallis eget.</p>

                            <p>In eget velit consectetur, condimentum massa eu, iaculis lectus. Donec at eleifend nisl. Quisque venenatis dolor at tincidunt vehicula. Suspendisse consectetur justo vel purus lacinia, id accumsan nibh lobortis. In eget interdum nulla. Sed sed dictum leo, a blandit lorem. Nam id tempor sem. Sed tincidunt massa vehicula est interdum commodo. Aenean non mi tortor. In massa est, imperdiet ut odio quis, dignissim rhoncus metus. Mauris bibendum elit metus, eget laoreet est ullamcorper vel. Nam elit nunc, tristique eu tempor sed, blandit et metus. Morbi nulla quam, accumsan nec auctor vel, tristique in dolor.</p>
                                          <p>Lorem ipsum dolor sit amet consectetur.</p>
                            <form>
        <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Main PDP Chapter </label>
                        <div class="col-sm-10">
                          <select name="mainpdp" class="form-control" onchange="java_script_:show5(this.options[this.selectedIndex].value)">
                            <option disabled selected>Choose Chapter</option>
                            @foreach ($chapters as $chapter)
                            <option value="{{ $chapter->chap_no }}">{{ $chapter->chap_description }}</option>
                            @endforeach
                          </select>
                        </div>
        </div>
        <div class="form-group row">
        <label class="col-sm-2 form-control-label">Other PDP Chapters <br><small class="text-primary">Select as many as applicable</small></label>
        <div class="col-sm-10">
          <div class="form-control" onclick="show_proj_co_agency()">
              <label id="proj_co_agency_output"></label>
          </div>
          <div class="col-sm-10 scroll-div-coagency" id="proj_co_agency" name="proj_co_agency">
              @foreach ($chapters as $chapter)
              <input type='checkbox' id='checkChapter{{ $chapter->chap_no }}' name='coimpagency[]' value="{{ $chapter->chap_no }}" class="form-control-custom checkbox-custom">
              <label for="checkChapter{{ $chapter->chap_no }}">{{ $chapter->chap_description }}</label>
              @endforeach
            </div>
          </div>
        </div>

        <script type="text/javascript">
          var expanded = false;
          function show_proj_co_agency() {
            var checkboxes = document.getElementById("proj_co_agency");
            if (!expanded) {
              checkboxes.style.display = "block";
              expanded = true;
            } else {
              checkboxes.style.display = "none";
              expanded = false;
            }
          }
          $("#proj_co_agency input").click(function () {
          $("#proj_co_agency_output").text($("#proj_co_agency input:checked").map(function () {
            return $(this).val();
          }).get().join());
        }).click().click();

        </script>

        <div class="form-group row">
          <label class="col-sm-2 form-control-label">Remarks <br></label>
          <div class="col-sm-10">
            <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
          </div>
        </div>
        </form>
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
          <a href="/validateproject" target="_blank"><button type="button" data-toggle="modal" class="btn btn-info">View Entire Project Details</button></a>
          <button type="button" class="btn btn-primary">Submit Request</button>
        </div>
        </div>
      </div>
    </div>
@endsection