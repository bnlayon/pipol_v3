@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Add Project</li>
      </ul>
    </div>
  </div>
    

<form class="text-left needs-validation" action="{{ asset('/addproject') }}" method="POST" enctype="multipart/form-data" novalidate id="addProjForm">{{ csrf_field() }}
  <span class="errorTxt">
    <button type="button" class="btn btn-secondary btn-xs">For Draft Projects, please fill up the project title</button>
    <button type="button" class="btn btn-primary btn-xs">For Finalized Projects, please fill up fields with <i class="fas fa-flag"></i> </button>
  </span>

  <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Add Project 
        </h1>
      </header>
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>General Information</h4>
                </div>
                <div class="card-body">

                    <div class="form-group row">
                      <label class="col-sm-2 control-label" for="title">Project Title <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" required>
                        <span class="text-small text-gray help-block-none">The project title should be identical with the project's title in the budget proposal submitted to DBM.</span>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Is it a Program or a Project? <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <div class="i-checks">
                          <input id="radioProg" type="radio" value="1" name="prog_proj" class="form-control-custom radio-custom form-check-input" required>
                          <label for="radioProg" data-toggle="tooltip" data-placement="top" title="A program is a group of activities and projects that contribute to a common particular outcome. A program should have the following: (a) unique expected results or outcomes; (b) a clear target population or client group external to the agency; (c) a defined method of intervention to achieve the desired result; and (d) a clear management structure that defines accountabilities.">Program </label>
                          &nbsp;&nbsp;&nbsp;<input id="radioProj" type="radio" value="2" name="prog_proj" class="form-control-custom radio-custom form-check-input" required>
                          <label for="radioProj" data-toggle="tooltip" data-placement="top" title="A project is a special undertaking carried out within a definite time frame and intended to result in some pre -determined measure of goods and services.">Project</label>
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>
                      </div>
                        <label class="col-sm-2 form-control-label" id="reg_prog_choices" style="display: none;">Is it a Regular Program? <label data-toggle="tooltip" data-placement="top" title="A regular program refers to a program being implemented by agencies on a regular basis."><i class="fas fa-question-circle flag_tomato"></i></label>&nbsp;<i class="fas fa-flag flag_tomato"></i></label>
                        <div class="col-sm-10"  id="reg_prog_choices2" style="display: none;">
                          <div class="i-checks">
                            <input id="radioProgYes" type="radio" value="1" name="reg_prog" class="form-control-custom radio-custom form-check-input">
                            <label for="radioProgYes">Yes </label>
                            &nbsp;&nbsp;&nbsp;<input id="radioProgNo" type="radio" value="0" name="reg_prog" class="form-control-custom radio-custom form-check-input">
                            <label for="radioProgNo">No</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                      $(document).ready(function () {
                          $('#radioProg').click(function () {
                              $('#reg_prog_choices').show('fast');
                              $('#reg_prog_choices2').show('fast');
                          });
                          $('#radioProj').click(function () {
                              $('#reg_prog_choices').hide('fast');
                              $('#reg_prog_choices2').hide('fast');
                          });
                          $('#radioCustomPIPtypo2').click(function () {
                              $('#RDYesNo').show('fast');
                          });
                      });
                    </script>

                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Basis for Implementation <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">included in any of the following documents</small></label>
                      <div class="col-sm-10">
                         <div class="i-checks basisImp">
                        @foreach ($basis_ as $basis)                       
                          <input id="checkboxCustomBasis{{ $basis->id }}" type="checkbox" value="{{ $basis->id }}" class="form-control-custom form-check-input" name="basis_id[]" onchange="java_script_:BasisImpCheckboxes(this.checked ? '1' : '0')" required>
                          <label for="checkboxCustomBasis{{ $basis->id }}">{{ $basis->basis }}</label>
                          <br />                                    
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>  
                                              
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Description <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Overview, Purpose, and/or Rationale of the Undertaking, Sub-programs/Components</small></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="description" required></textarea>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementing Agency</h4>
                </div>
                <div class="card-body">
                  @switch(auth()->user()->user_type)
                    @case("AG")
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Mother/Oversight Agency </label>
                      <div class="col-sm-10">
                        @if($submission->motheragency_id == 0)
                          {{$agency_agency->UACS_AGY_DSC}}
                          <input type="hidden" name="agency_id" value="{{$submission->agency_id}}">
                          <input type="hidden" name="motheragency_id" value="0">
                          <input type="hidden" name="code" value="{{$agency_agency->UACS_DPT_ID}}-">
                        @else
                          {{$agency_mother->UACS_AGY_DSC}}
                          <input type="hidden" name="agency_id" value="{{$submission->agency_id}}">
                          <input type="hidden" name="motheragency_id" value="{{$submission->motheragency_id}}">
                          <input type="hidden" name="code" value="{{$agency_agency->UACS_DPT_ID}}{{$agency_agency->UACS_AGY_ID}}-">
                        @endif
                      </div>
                      <label class="col-sm-2 form-control-label">Co-Implementing Agency </label>
                      <div class="col-sm-10">
                        <select id="coimp" name="coimp[]" multiple="multiple">
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}">{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                      </div>
                      
                      <label class="col-sm-2 form-control-label">Attached Agency </label>
                        @if($submission->motheragency_id == 0)
                          <div class="col-sm-10">
                            <select id="att" name="att[]" multiple="multiple">
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}">{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                          </div>
                        @else
                          <div class="col-sm-10">
                          {{$agency_agency->UACS_AGY_DSC}}
                          </div>
                        @endif
                    </div>
                    @break

                    @default
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Mother/Oversight Agency </label>
                      <div class="col-sm-10">
                        <select id="agency_id" name="agency_id" class="form-control">
                          <option disabled selected value="">Choose Coverage</option>
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}">{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Co-Implementing Agency </label>
                      <div class="col-sm-10">
                        <select id="coimp" name="coimp[]" multiple="multiple">
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}">{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Attached Agency </label>
                      <div class="col-sm-10">
                        <select id="att" name="att[]" multiple="multiple">
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}">{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  @endswitch

                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Spatial Coverage <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Coverage<br><small class="text-primary">Nationwide <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or project covers all regions (in parts or as a whole)."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"> Interregional <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or project pertains to more than one region (in parts or as a whole) but not all regions."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Region-Specific <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or a project pertains to one region (in parts or as a whole)."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"> Abroad <label data-toggle="tooltip" data-placement="top" title="If spatial coverage of a program or project is outside the country that will have an impact to Filipinos outside of the country (e.g., Overseas Filipino Workers)."><i class="fas fa-question-circle flag_tomato"></i></label></small></label>
                      <div class="col-sm-10">
                        <select id="proj_cov_main" name="spatial" class="form-control" onchange="java_script_:show(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Coverage</option>
                          <label data-toggle="tooltip" data-placement="top" title="A program is a group of activities and projects that contribute to a common particular outcome. A program should have the following: (a) unique expected results or outcomes; (b) a clear target population or client group external to the agency; (c) a defined method of intervention to achieve the desired result; and (d) a clear management structure that defines accountabilities.">A</label><option value="Nationwide">Nationwide</option>
                          <option value="Interregional">Interregional</option>
                          <option value="Region Specific">Region Specific</option>
                          <option value="Abroad">Abroad</option>
                        </select>
                      </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group row" id="inter" style="display: none;">
                      <label class="col-sm-2 form-control-label">Interregional</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input class="interRegion" id="checkCustomInterRegion{{ $region->region_no }}" type="checkbox" value="{{ $region->regional_code }}" name="inter">
                          <label for="checkCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                            {{-- <div class="card" id="reg{{ $region->regional_code }}" style="display: none;">
                              <div class="card-body reg{{ $region->regional_code }}">
                              </div>
                            </div> --}}
                        </div>
                        @endforeach
                      </div>
                    </div>
                    {{-- <div class="form-group row" id="rs" style="display: none;">
                      <label class="col-sm-2 form-control-label">Region Specific</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input id="radioCustomInterRegion{{ $region->region_no }}" type="radio" value="{{ $region->region_no }}" name="rs" class="form-control-custom radio-custom">
                          <label for="radioCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div> --}}
                </div>
              </div>
        </div>
      </div>

      <script>
        function show(proj_cov_main) {
          if (proj_cov_main == 'Interregional') {
            //inter.style.display='block';
            //rs.style.display='none';
          } else if(proj_cov_main == 'Region Specific') {
            inter.style.display='none';
            //rs.style.display='block';
          } else {
            inter.style.display='none';
            //rs.style.display='none';
          }
        }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Level of Approval <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="ICCNBcheckboxYes" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="iccable" required>
                            <label for="ICCNBcheckboxYes">Will require Investment Coordination Committee/NEDA Board Approval (ICC-able)? </label> &nbsp;&nbsp;&nbsp;
                            <input id="ICCNBcheckboxNo" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="iccable" required>
                            <label for="ICCNBcheckboxNo">Not Applicable </label>
                            <div class="invalid-feedback">
                             This is a required field.
                           </div>
                          </div>
                          <div style="display:none" id="icccheckboxes">
                          <div class="i-checks">
                            <input id="ICCNBradio1" type="radio" value="Yet to be submitted to NEDA Secretariat" name="currentlevel" class="form-control-custom radio-custom form-check-input">
                            <label for="ICCNBradio1">Yet to be submitted to the NEDA Secretariat</label>
                            <div id="tdos" style="display:none">
                               <label for="tdos">Target Date of Submission</label>
                                  <div class="col-sm-4">
                                  <input type="date" class="form-control" name="approval_date1">
                                  </div>
                            </div>
                            <br />
                            <input id="ICCNBradio2" type="radio" value="Under the NEDA Secretariat Review" name="currentlevel" class="form-control-custom radio-custom form-check-input">
                            <label for="ICCNBradio2">Under the NEDA Secretariat Review</label>
                            <div id="dostns" style="display:none">
                               <label for="dostns">Date of Submission to NEDA Secretariat</label>
                               <div class="col-sm-4">
                                  <input type="date" class="form-control" name="approval_date2">
                                </div>
                            </div>
                            <br />
                            <input id="ICCNBradio3" type="radio" value="ICC-TB Endorsed" name="currentlevel" class="form-control-custom radio-custom form-check-input">
                            <label for="ICCNBradio3">ICC-TB Endorsed</label>
                            <div id="doa" style="display:none">
                               <label for="doa">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="date" class="form-control" name="approval_date3">
                                </div>
                            </div>
                            <br />
                            <input id="ICCNBradio4" type="radio" value="ICC-CC Approved" name="currentlevel" class="form-control-custom radio-custom form-check-input">
                            <label for="ICCNBradio4">ICC-CC Approved</label>
                            <div id="doa2" style="display:none">
                               <label for="doa2">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="date" class="form-control" name="approval_date4">
                                </div>
                            </div>
                            <br />
                            <input id="ICCNBradio5" type="radio" value="NEDA Board Confirmed" name="currentlevel" class="form-control-custom radio-custom form-check-input">
                            <label for="ICCNBradio5">NEDA Board Confirmed</label>
                            <div id="doa3" style="display:none">
                               <label for="doa3">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="date" class="form-control" name="approval_date5">
                                </div>
                            </div>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                          
                        </div>
                        
                      </div>
                    </div>
                    <script src="http://code.jquery.com/jquery-latest.js"></script>
                    <script type="text/javascript">
                      $(document).ready(function () {
                          $('#ICCNBcheckboxYes').click(function () {
                              $('#icccheckboxes').show('fast');
                          });
                          $('#ICCNBcheckboxNo').click(function () {
                              $('#icccheckboxes').hide('fast');
                          });
                          $('#ICCNBradio1').click(function () {
                              $('#tdos').show('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio2').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').show('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio3').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').show('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio4').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').show('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio5').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').show('fast');
                          });
                      });
                    </script>
                </div>
              </div>
        </div>
      </div>

      <script type="text/javascript">
        function iccCheckboxFunction() {
        var checkBox = document.getElementById("ICCNBcheckbox");
        var text = document.getElementById("icccheckboxes");
        if (checkBox.checked == true){
          text.style.display = "block";
        } else {
          text.style.display = "none";
        }
      }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project for Inclusion in Which Programming Document </h4>
                </div>
                <div class="card-body">
                  
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <div style="pointer-events: none;">
                          <input id="PIPcheckbox" type="checkbox" value="1" checked class="form-control-custom" name="pip">
                          <label for="PIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains priority programs and projects to be implemented by NG, GOCC, GFI and other national government offices that are responsive to the PDP and RM"><b>Public Investment Program (PIP)</b> <i class="fas fa-flag flag_tomato"></i></label>
                          </div>
                          <div class="i-checks">
                          @foreach ($piptypologies as $piptypology)
                          <input id="radioCustomPIPtypo{{ $piptypology->id }}" type="radio" value="{{ $piptypology->id }}" name="pip_typo" class="form-control-custom radio-custom form-check-input" required>
                          <label for="radioCustomPIPtypo{{ $piptypology->id }}">{{ $piptypology->typologies }}
                            @if ($piptypology->id == 7)
                              <label data-toggle="tooltip" data-placement="top" title="Government Buildings be used as offices, housing (military and civilian), and for other technical purposes by NGAs, GOCCs, GFI, other NG offices and instrumentalities and SUCs"><i class="fas fa-question-circle flag_tomato"></i></label>
                            @endif
                          </label>
                          <br/>
                          @if ($piptypology->id == 2)
                            <div id="RDYesNo" style="display: none;">
                              <div class="i-checks">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Is it a Research and Development Program/Project </label><br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="RDYes" type="radio" value="1" name="rd" class="form-control-custom radio-custom form-check-input">
                                <label for="RDYes">Yes </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="RDNo" type="radio" value="0" name="rd" class="form-control-custom radio-custom form-check-input">
                                <label for="RDNo">No</label>
                              </div>
                            </div>
                          @endif
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                           </div>
                          </div>
                        </div>
                      </div>

                      <script type="text/javascript">
                      $(document).ready(function () {
                          $('#radioCustomPIPtypo2').click(function () {
                              $('#RDYesNo').show('fast');
                          });
                          $('#radioCustomPIPtypo1').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                          $('#radioCustomPIPtypo6').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                          $('#radioCustomPIPtypo7').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                      });
                    </script>
                      

                      <div class="col-sm-6">
                        <div class="i-checks">
                          <input id="CIPcheckbox" type="checkbox" value="1" class="form-control-custom" onclick="CIPCheckboxFunction()" name="cip">
                          <label for="CIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains the big ticket programs and projects of the PIP that serves as pipeline for the NEDA Board and Investment Coordination Committee (ICC)"><b>Core Investment Programs/Projects (CIP)</b></label>
                          <div id="cipcheckboxes" style="display:none" >
                            <div class="i-checks cipRbtn">
                            @foreach ($ciptypologies as $ciptypology)
                            <input id="radioCustomCIPtypo{{ $ciptypology->id }}" type="radio" value="{{ $ciptypology->id }}" name="cip_typo" class="form-control-custom radio-custom form-check-input"><label for="radioCustomCIPtypo{{ $ciptypology->id }}">{{ $ciptypology->typologies }}</label><br/>
                            @endforeach
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <script type="text/javascript">
                        function CIPCheckboxFunction() {
                        var checkBox = document.getElementById("CIPcheckbox");
                        var text = document.getElementById("cipcheckboxes");
                        var text2 = document.getElementById("gad");
                        var cipRbtn = $('.cipRbtn :radio');
                        var gadRbtn = $('.gadRbtn :radio');
                        var rowa1Rbtn = $('.rowa1Rbtn :radio');
                        var rowa2Rbtn = $('.rowa2Rbtn :radio');
                        var rowa3Rbtn = $('.rowa3Rbtn :radio');
                        var ppdSelect = $('#projdocument');

                        if (checkBox.checked == true){
                          text.style.display = "block";
                          text2.style.display = "block";
                          cipRbtn.attr('required', 'required');
                          gadRbtn.attr('required', 'required');
                          ppdSelect.attr('required', 'required');
                          rowa1Rbtn.attr('required', 'required');
                          rowa2Rbtn.attr('required', 'required');
                          rowa3Rbtn.attr('required', 'required');
                        } else {
                          text.style.display = "none";
                          text2.style.display = "none";
                          cipRbtn.removeAttr('required');
                          gadRbtn.removeAttr('required');
                          ppdSelect.removeAttr('required');
                          rowa1Rbtn.removeAttr('required');
                          rowa2Rbtn.removeAttr('required');
                          rowa3Rbtn.removeAttr('required');
                        }
                      }
                      </script>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6">
                          <div class="i-checks">
                            <input id="TRIPcheckbox" type="checkbox" value="1" class="form-control-custom" onclick="TRIPCheckboxFunction()" name="trip">
                            <label for="TRIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains nationally funded infrastructure projects irrespective of cost and financing with emphasis on immediate priorities to be undertaken in three-year periods"><b>Three-year Rolling Infrastructure Program (TRIP)</b></label>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="i-checks">
                            <label for="TRIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains nationally funded infrastructure projects irrespective of cost and financing with emphasis on immediate priorities to be undertaken in three-year periods"><b>Is the Program/Project included in the RDIP? <i class="fas fa-flag flag_tomato"></i></b></label>
                            <input id="RDIPcheckboxYes" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="rdip" required>
                            <label for="RDIPcheckboxYes">Yes </label> &nbsp;&nbsp;&nbsp;
                            <input id="RDIPcheckboxNo" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="rdip" required>
                            <label for="RDIPcheckboxNo">No </label>
                            <div class="invalid-feedback">
                             This is a required field.
                            </div>
                          </div>
                          <div class="i-checks" style="display:none" id="rdipcheckboxes">
                            <input id="RDIPcheckbox1" type="checkbox" value="1" class="form-control-custom" onclick="RDIPCheckboxFunction2()" name="rdc_endorsement">
                            <label for="RDIPcheckbox1">Will require Regional Development Council (RDC) Endorsement?</label>
                          </div>
                          <div style="display:none" id="rdipcheckboxes2">
                            <div class="i-checks">
                              <input id="RDCEndorseed1" type="radio" value="Endorsed" name="rdc_endorsed_notendorsed" class="form-control-custom radio-custom">
                              <label for="RDCEndorseed1">Endorsed</label><br/>
                              <div id="RDCEndorseedDate" style="display:none">
                              <label for="RDCEndorseedDate">Date of Endorsement</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" name="rdc_date_endorsement">
                              </div>
                              </div>
                              
                            </div>
                            <div class="i-checks">
                              <input id="RDCEndorseed2" type="radio" value="Yet to be Endorsed" name="rdc_endorsed_notendorsed" class="form-control-custom radio-custom">
                              <label for="RDCEndorseed2">Yet to be Endorsed</label>
                            </div>
                          </div>
                      </div>
                      
                      <script type="text/javascript">
                        function TRIPCheckboxFunction() {
                        var checkBox = document.getElementById("TRIPcheckbox");
                        var text = document.getElementById("tripdiv");
                        var infraSecChbxs = $('.infraSecChbxs :checkbox[name="sectors[]"]');
                        var impReadinessChbxs = $('.impReadiness :checkbox[name="status[]"]');
                        var risk =  $('textarea#risk');

                        if (checkBox.checked == true){
                          text.style.display = "block";
                          infraSecChbxs.attr('required', 'required');
                          impReadinessChbxs.attr('required', 'required');
                          risk.attr('required', 'required');
                        } else {
                          text.style.display = "none";
                          infraSecChbxs.removeAttr('required');
                          impReadinessChbxs.removeAttr('required');
                          risk.removeAttr('required');
                        }
                      }

                        function RDIPCheckboxFunction() {
                        var checkBox = document.getElementById("RDIPcheckbox");
                        var text = document.getElementById("rdipcheckboxes");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      $(document).ready(function () {
                          $('#RDIPcheckboxYes').click(function () {
                              $('#rdipcheckboxes').show('fast');
                          });
                          $('#RDIPcheckboxNo').click(function () {
                              $('#rdipcheckboxes').hide('fast');
                              $('#rdipcheckboxes2').hide('fast');
                              RDIPcheckbox1.removeAttr('checked');
                          });
                        });

                      function RDIPCheckboxFunction2() {
                        var checkBox = document.getElementById("RDIPcheckbox1");
                        var text = document.getElementById("rdipcheckboxes2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      $(document).ready(function () {
                          $('#RDCEndorseed1').click(function () {
                              $('#RDCEndorseedDate').show('fast');
                          });
                          $('#RDCEndorseed2').click(function () {
                              $('#RDCEndorseedDate').hide('fast');
                          });
                        });
                      </script>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row" style="display: none;" id="tripdiv">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Three-year Rolling Infrastructure Program</h4>
                </div>
                <div class="card-body">
                  
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Infrastructure Sector </label>
                          <div class="i-checks infraSecChbxs">
                          @foreach ($sectors as $sector)
                          <input id="checkboxCustomSector{{ $sector->id }}" type="checkbox" value="{{ $sector->id }}" class="form-control-custom form-check-input testsec" onclick="InfraCheckboxFunction{{ $sector->id }}()" name="sectors[]" onchange="java_script_:InfraSectorCheckboxes()">
                          <label for="checkboxCustomSector{{ $sector->id }}">{{ $sector->sector }}</label>
                          <br />
                          <div style="display: none;" id="mothersector{{ $sector->id }}">
                            <div class="i-checks infraSubSecChbxs{{ $sector->id }}">
                          @foreach ($subsectors as $subsector)
                          @if ($sector->id == $subsector->sector)
                            &nbsp;&nbsp;&nbsp;<input id="checkboxCustomSubSector{{ $subsector->id }}" type="checkbox" value="{{ $subsector->id }}" class="form-control-custom form-check-input" name="subsectors[]" onchange="java_script_:InfraSubSectorCheckboxes{{ $sector->id }}()">
                            <label for="checkboxCustomSubSector{{ $subsector->id }}">{{ $subsector->subsector }}</label>
                            <br />
                          @endif
                          @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                          </div>
                        
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Status of Implementation Readiness</label>
                          <div class="i-checks impReadiness">
                         @foreach ($statuses as $status)
                          <input id="checkboxCustomStatus{{ $status->id }}" type="checkbox" value="{{ $status->id }}" class="form-control-custom form-check-input" name="status[]" onchange="java_script_:ImpReadinessCheckboxes()">
                          <label for="checkboxCustomStatus{{ $status->id }}">{{ $status->statuses }}</label>
                          <br />
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                         </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Implementation Risks and Mitigation Strategies</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="risk" name="risk"></textarea>
                      </div>
                    </div>
                    <div class="form-group row" align="center">
                      <div class="col-sm-10">
                      <button type="button" data-toggle="modal" data-target="#ic" class="btn btn-primary">Infrastructure Cost</button> 
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <script type="text/javascript">
        function InfraCheckboxFunction1() {

          if (checkboxCustomSector1.checked == true){
            mothersector1.style.display = "block";
          } else {
            mothersector1.style.display = "none";
          }

          InfraSubSectorCheckboxes1();

        }

        function InfraCheckboxFunction2() {

        }

        function InfraCheckboxFunction3() {
          var infraSubSecChbxs3 = $('.infraSubSecChbxs3 :checkbox');
          if (checkboxCustomSector3.checked == true){
            mothersector3.style.display = "block";
          } else {
            mothersector3.style.display = "none";
          }

          InfraSubSectorCheckboxes3();
        }

        function InfraCheckboxFunction4() {
          var infraSubSecChbxs4 = $('.infraSubSecChbxs4 :checkbox');
          if (checkboxCustomSector4.checked == true){
            mothersector4.style.display = "block";
          } else {
            mothersector4.style.display = "none";
          }

          InfraSubSectorCheckboxes4();
        }

        function InfraCheckboxFunction5() {

        }

        function InfraCheckboxFunction6() {
          var infraSubSecChbxs6 = $('.infraSubSecChbxs6 :checkbox');
          if (checkboxCustomSector6.checked == true){
            mothersector6.style.display = "block";
          } else {
            mothersector6.style.display = "none";
          }

          InfraSubSectorCheckboxes6();
        }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Philippine Development Plan (PDP) Chapter</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main PDP Chapter <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="mainpdp" class="form-control" onchange="java_script_:show5(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Chapter</option>
                          @foreach ($chapters as $chapter)
                          <option value="{{ $chapter->chap_no }}">{{ $chapter->chap_description }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Other PDP Chapters <br><small class="text-primary">Select as many as applicable</small></label>
                      <div class="col-sm-10">
                        <select id="otherpdp"name="otherpdp[]" multiple="multiple">
                          @foreach ($chapters as $chapter)
                            <option value="{{$chapter->chap_no}}">{{$chapter->chap_description}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <script type="text/javascript">
        var expanded = false;
        function show_proj_co_agency() {
          var checkboxes = document.getElementById("proj_co_agency");
          if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
          } else {
            checkboxes.style.display = "none";
            expanded = false;
          }
        }
        $("#proj_co_agency input").click(function () {
        $("#proj_co_agency_output").text($("#proj_co_agency input:checked").map(function () {
          return $(this).val();
        }).get().join());
      }).click().click();

      </script>

      @for ($i = 5; $i <= 20; $i++)
        <div class="row" style="display: none;" id="ResultsChapter{{ $i }}">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>PDP Chapter {{ $i }} Outcome Statements/Outputs <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-10">
                        @foreach ($_1matrices as $_1matrice)
                          @if ($_1matrice->chapter == $i)
                          <div class="i-checks">
                          <input id="checkCustom1RM{{ $_1matrice->id }}" type="checkbox" value="{{ $_1matrice->id }}" name="_1rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"><label for="checkCustom1RM{{ $_1matrice->id }}" >{{ $_1matrice->chapter_outcome }}</label>
                          </div>
                          @foreach ($_2matrices as $_2matrice)
                            @if ($_2matrice->chapter_outcome_id == $_1matrice->id)
                            <div class="i-checks">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom2RM{{ $_2matrice->id }}" type="checkbox" value="{{ $_2matrice->id }}" name="_2rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"><label for="checkCustom2RM{{ $_2matrice->id }}">{{ $_2matrice->intermediate_outcome }}</label>
                            </div>
                            @foreach ($_3matrices as $_3matrice)
                              @if ($_3matrice->intermediate_outcome_id == $_2matrice->id)
                              <div class="i-checks">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom3RM{{ $_3matrice->id }}" type="checkbox" value="{{ $_3matrice->id }}" name="_3rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"><label for="checkCustom3RM{{ $_3matrice->id }}">{{ $_3matrice->intermediate_outcome }}</label>
                              </div>
                                @foreach ($_4matrices as $_4matrice)
                                @if ($_4matrice->intermediate_outcome_id == $_3matrice->id)
                                <div class="i-checks">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom4RM{{ $_4matrice->id }}" type="checkbox" value="{{ $_4matrice->id }}" name="_4rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"><label for="checkCustom4RM{{ $_4matrice->id }}">{{ $_4matrice->intermediate_outcome }}</label>
                                </div>
                                @endif
                                @endforeach
                              @endif
                            @endforeach
                            @endif
                          @endforeach
                          @endif
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
      @endfor

      <div class="form-group row">
        <div class="col-sm-12">
          <div class="i-checks">
            <input id="NA_rm" type="checkbox" value="1" class="form-control-custom" name="NA_rm">
            <label for="NA_rm"><strong>No PDP Output Statement applicable</strong></label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-2 form-control-label">Expected Outputs <label data-toggle="tooltip" data-placement="top" title="Expected outputs should directly contribute to identified RM outcome statement/output"><i class="fas fa-question-circle flag_tomato"></i></label> <br><small class="text-primary">Actual Deliverables, i.e. 100km of paved roads</small></label>
        <div class="col-sm-10">
          <textarea class="form-control" rows="5" id="output" name="output" required></textarea>
        </div>
      </div>


      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>0-10 Point Socioeconomic Agenda <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <div class="i-checks socoPoints">
                          @foreach ($agendas as $agenda)
                          <input id="checkCustomAgenda{{ $agenda->id }}" type="checkbox" value="{{ $agenda->id }}" name="agenda[]" class="form-control-custom checkbox-custom form-check-input" onchange="java_script_:SocioPointsCheckboxes()" required><label for="checkCustomAgenda{{ $agenda->id }}">{{ $agenda->agenda }}</label>
                          <br />
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Sustainable Development Goals (SDG) <i class="fas fa-flag flag_tomato"></i> <br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <div class="i-checks susPoints">
                          @foreach ($goals as $goal)
                          <input id="checkCustomGoals{{ $goal->goal_id }}" type="checkbox" value="{{ $goal->goal_id }}" name="sdg[]" class="form-control-custom checkbox-custom form-check-input" onchange="java_script_:SusPointsCheckboxes()" required><label for="checkCustomGoals{{ $goal->goal_id }}">{{ $goal->goal_description }}</label>
                          <br />
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row" style="display: none;" id="gad">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center"> 
                  <h4>Level of GAD Responsiveness <br><small class="text-primary">Based on the score of the program/ project using the GAD checklist accessible through this <a href="http://www.neda.gov.ph/wp-content/uploads/2015/10/DEVELOPMENT-PLANNING-CHECKLIST.pdf" target="_blank"><u>link</u></a>, kindly identify the GAD responsiveness of the program/project. </small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                          <div class="i-checks gadRbtn">
                          @foreach ($levels as $level)
                          <input id="radioCustomLevel{{ $level->level_id }}" type="radio" value="{{ $level->level_id }}" name="gender" class="form-control-custom radio-custom form-check-input"><label for="radioCustomLevel{{ $level->level_id }}">{{ $level->level_description }}</label>
                          <br />
                          @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementation Period <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Start of Project Implementation</label>
                      <div class="col-sm-2">
                        <input type="number" class="form-control" name="start" required
                        placeholder="e.g. 2019">
                      </div>
                      <label class="col-sm-2 form-control-label">Year of Project Completion</label>
                      <div class="col-sm-2">
                        <input type="number" class="form-control" name="end" required
                        placeholder="e.g. 2019">
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project Preparation Details <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Preparation Document </label>
                      <div class="col-sm-10">
                        <select id="projdocument" class="form-control" onchange="java_script_:show2(this.options[this.selectedIndex].value)" name="ppdetails">
                          <option disabled selected value="">Choose Project Document</option>
                          @foreach ($projectdocuments as $projectdocument)
                          <option value="{{ $projectdocument->id }}">{{ $projectdocument->projectdocument }}</option>
                          @endforeach
                        </select>
                      </div>
                      
                    </div>
                    <div class="invalid-feedback">
                        This is a required field.
                      </div>

                    <div class="form-group row" id="fs" style="display: none;">
                      <div class="col-sm-12">
                        <input id="fsassistance" type="checkbox" value="1" class="form-control-custom" name="fsassistance">
                        <label for="fsassistance">Will require assistance for the conduct of Feasibility Study?</label>
                      </div>
                      <div class="col-sm-12">
                        <label class="col-sm-2 form-control-label">Status </label>
                        <div class="col-sm-10">
                          <div class="i-checks">
                            @foreach ($fsstatuses as $fsstatus)
                            <div class="i-checks">
                            <input id="radioCustomFS{{ $fsstatus->id }}" type="radio" value="{{ $fsstatus->id }}" name="fsstatus" class="form-control-custom radio-custom"><label for="radioCustomFS{{ $fsstatus->id }}">{{ $fsstatus->fs_status }}</label>
                            </div>
                            @if ($fsstatus->id == '2')
                            <div class="col-sm-6" style="display: none;" id="fsongoing_expected_date">
                            <div class="i-checks">
                              Expected completion date: <input type="date" name="fsstatus_ongoing" class="form-control">
                            </div>
                            </div>
                            @endif
                            @if ($fsstatus->id == '3')
                            <div class="col-sm-6" style="display: none;" id="fsongoing_forprep_date">
                            <div class="i-checks">
                              Start date: <input type="date" name="fsstatus_prep" class="form-control">
                            </div>
                            </div>
                            @endif
                            @endforeach
                          </div>
                        </div>
                      </div>
                        <script type="text/javascript">
                        $(document).ready(function () {
                          $('#radioCustomFS1').click(function () {
                              $('#fsongoing_expected_date').hide('fast');
                              $('#fsongoing_forprep_date').hide('fast');
                          });
                          $('#radioCustomFS2').click(function () {
                              $('#fsongoing_expected_date').show('fast');
                              $('#fsongoing_forprep_date').hide('fast');
                          });
                          $('#radioCustomFS3').click(function () {
                              $('#fsongoing_expected_date').hide('fast');
                              $('#fsongoing_forprep_date').show('fast');
                          });
                        });
                        </script>
                      
                      <label class="col-sm-12 form-control-label">Schedule of F/S Cost (In Exact Amount in PhP) </label>
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2017">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2018">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2019">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2020">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2021">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2022">
                                      </td>
                                      <td>
                                        
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      
                    </div>
                    <div class="form-group row" id="others" style="display: none;">
                      <label class="col-sm-2 form-control-label">Others</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="ppdetails_others">
                      </div>                    
                    </div>
                </div>
              </div>
        </div>
      </div>

      <script>
        function show2(projdocument) {
          if (projdocument == "1") {
            fs.style.display='block';
            others.style.display='none';
          } else if (projdocument == "5") {
            fs.style.display='none';
            others.style.display='flex';
          } else {
            fs.style.display='none';
            others.style.display='none';
          }
        }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Pre-construction Costs</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa1Rbtn">
                            <input id="ROWA1" type="radio" value="1" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox()" name="rowa">
                            <label for="ROWA1">With ROWA Component?</label>&nbsp;&nbsp;&nbsp;
                            <input id="ROWA1_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox()" name="rowa">
                            <label for="ROWA1_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement" style="display: none;">
                      <label class="col-sm-12 form-control-label">Schedule of ROWA Cost (In Exact Amount in PhP) </label>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2017">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2018">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2019">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2020">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2021">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2022">
                                      </td>
                                      <td>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="number" class="form-control" name="rowa_affected">
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox() {
                        var checkBox = document.getElementById("ROWA1");
                        var text = document.getElementById("rowarequirement");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa2Rbtn">
                            <input id="ROWA2" type="radio" value="1" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox2()" name="rc">
                            <label for="ROWA2">With Resettlement Component?</label>&nbsp;&nbsp;&nbsp;
                            <input id="ROWA2_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox2()" name="rc">
                            <label for="ROWA2_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement2" style="display: none;">
                    
                    <label class="col-sm-12 form-control-label">Schedule of Resettlement Cost (In Exact Amount in PhP) </label>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2017">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2018">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2019">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2020">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2021">
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2022">
                                      </td>
                                      <td>
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="text" class="form-control" name="rc_affected">
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox2() {
                        var checkBox = document.getElementById("ROWA2");
                        var text = document.getElementById("rowarequirement2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa3Rbtn">
                            <input id="ROWA3" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="wrrc">
                            <label for="ROWA3">With ROWA and Resettlement Action Plan?</label>
                            <input id="ROWA3_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="wrrc">
                            <label for="ROWA3_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Employment Generation<br><small class="text-primary">Please indicate the no. of persons to be employed by the project outside of the implementing agency</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of persons to be employed:</label>
                      <div class="col-sm-4">
                        <input type="number" class="form-control" name="employment">
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project Cost <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                <br>
                <div class="row" align="center">
                
                 <!--  <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Financing Source</th>
                                    <th>2016 and Prior</th>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>2017-2022</th>
                                    <th>Continuing Years</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2016" name="it_2016_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2017" name="it_2017_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2018" name="it_2018_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2019" name="it_2019_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2020" name="it_2020_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2021" name="it_2021_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2022" name="it_2022_nglocal">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="nglocal2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2023" name="it_2023_nglocal">
                                    </td>
                                    <td>
                                       <span class="it-total_all" id="nglocal_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2016" name="it_2016_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2017" name="it_2017_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2018" name="it_2018_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2019" name="it_2019_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2020" name="it_2020_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2021" name="it_2021_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2022" name="it_2022_ngloan">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="ngloan2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2023" name="it_2023_ngloan">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ngloan_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2016" name="it_2016_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2017" name="it_2017_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2018" name="it_2018_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2019" name="it_2019_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2020" name="it_2020_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2021" name="it_2021_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2022" name="it_2022_odagrant">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="odagrant2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2023" name="it_2023_odagrant">
                                    </td>
                                    <td>
                                     <span class="it-total_all" id="odagrant_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2016" name="it_2016_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2017" name="it_2017_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2018" name="it_2018_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2019" name="it_2019_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2020" name="it_2020_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2021" name="it_2021_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2022" name="it_2022_gocc">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="gocc2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2023" name="it_2023_gocc">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="gocc_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2016" name="it_2016_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2017" name="it_2017_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2018" name="it_2018_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2019" name="it_2019_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2020" name="it_2020_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2021" name="it_2021_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2022" name="it_2022_lgu">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="lgu2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2023" name="it_2023_lgu">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="lgu_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2016" name="it_2016_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2017" name="it_2017_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2018" name="it_2018_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2019" name="it_2019_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2020" name="it_2020_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2021" name="it_2021_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2022" name="it_2022_ps">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="ps2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2023" name="it_2023_ps">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ps_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2016" name="it_2016_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2017" name="it_2017_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2018" name="it_2018_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2019" name="it_2019_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2020" name="it_2020_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2021" name="it_2021_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2022" name="it_2022_others">
                                    </td>
                                    <td>
                                      <span class="it-total2" id="others2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2023" name="it_2023_others">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="others_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="it_2016_total">0</span></th>
                                    <th><span id="it_2017_total">0</span></th>
                                    <th><span id="it_2018_total">0</span></th>
                                    <th><span id="it_2019_total">0</span></th>
                                    <th><span id="it_2020_total">0</span></th>
                                    <th><span id="it_2021_total">0</span></th>
                                    <th><span id="it_2022_total">0</span></th>
                                    <th><span id="it_total2">0</span></th>
                                    <th><span id="it_2023_total">0</span></th>
                                    <th><span id="it_total_all">0</span></th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div> -->

                       <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Financing Source</th>
                                    <th>2021 and Prior</th>
                                    <th>2022</th>
                                    <th>2023</th>
                                    <th>2024</th>
                                    <th>Continuing Years</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2021" name="it_2021_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2022" name="it_2022_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2023" name="it_2023_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2024" name="it_2024_nglocal">
                                    </td>                                   
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2025" name="it_2025_nglocal">
                                    </td>
                                    <td>
                                       <span class="it-total_all" id="nglocal_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2021" name="it_2021_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2022" name="it_2022_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2023" name="it_2023_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2024" name="it_2024_ngloan">
                                    </td>                                    
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2025" name="it_2025_ngloan">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ngloan_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2021" name="it_2021_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2022" name="it_2022_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2023" name="it_2023_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2024" name="it_2024_odagrant">
                                    </td>                                    
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2025" name="it_2025_odagrant">
                                    </td>
                                    <td>
                                     <span class="it-total_all" id="odagrant_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2021" name="it_2021_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2022" name="it_2022_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2023" name="it_2023_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2024" name="it_2024_gocc">
                                    </td>                                    
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2025" name="it_2025_gocc">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="gocc_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2021" name="it_2021_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2022" name="it_2022_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2023" name="it_2023_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2024" name="it_2024_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2025" name="it_2025_lgu">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="lgu_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2021" name="it_2021_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2022" name="it_2022_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2023" name="it_2023_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2024" name="it_2024_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2025" name="it_2025_ps">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ps_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2021" name="it_2021_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2022" name="it_2022_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2023" name="it_2023_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2024" name="it_2024_others">
                                    </td>                                   
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2025" name="it_2025_others">
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="others_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="it_2021_total">0</span></th>
                                    <th><span id="it_2022_total">0</span></th>
                                    <th><span id="it_2023_total">0</span></th>
                                    <th><span id="it_2024_total">0</span></th>
                                    <th><span id="it_2025_total">0</span></th>
                                    <th><span id="it_total_all">0</span></th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

               <!--  <div class="col-lg-4">
                <button type="button" data-toggle="modal" data-target="#rb" class="btn btn-primary">Regional Breakdown</button> 
                </div> -->
                </div>
                <br>
              </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Funding Source and Mode of Implementation <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main Funding Source</label>
                      <div class="col-sm-4">
                        <select id="mainfsource" name="mainfsource" class="form-control" onchange="java_script_:show3(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Main Funding Source</option>
                          @foreach ($fundingsources as $fundingsource)
                          <option value="{{ $fundingsource->fsource_no }}">{{ $fundingsource->fsource_description }}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Mode of Implementation/ <br> Procurement</label>
                      <div class="col-sm-4">
                        <select id="mode" name="modeofimplementation" class="form-control" onchange="java_script_:show4(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Mode of Implementation</option>
                          @foreach ($modes as $mode)
                          <option value="{{ $mode->mode_no }}">{{ $mode->mode_description }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 form-control-label" style="display: none;" id="oda2">ODA Funding Institutions</label>
                      <div class="col-sm-4" style="height: 12em; width: 40em; overflow: auto;display: none;" id="oda">
                        @foreach ($fundings as $funding)
                          <div class="i-checks">
                          <input id="checkFS{{ $funding->fsource_no }}" type="checkbox" value="{{ $funding->fsource_no }}" name="oda_funding[]" class="form-control-custom check-custom"><label for="checkFS{{ $funding->fsource_no }}">{{ $funding->fsource_description }}</label>
                          </div>
                          @endforeach
                      </div>
                      <label class="col-sm-2 form-control-label" style="display: none;" id="others_fs1">Other Funding Source</label>
                      <div class="col-sm-4" style="display: none;" id="others_fs2">
                        <input type="text" class="form-control" name="other_fs">
                      </div>
                      <label class="col-sm-2 form-control-label" style="display: none;" id="others_mode2">Other Mode</label>
                      <div class="col-sm-4" style="display: none;" id="others_mode">
                        <input type="text" class="form-control" name="other_mode">
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>

      <script>
        function show3(mainfsource) {
          if (mainfsource == "2") {
            oda.style.display='block';
            oda2.style.display='block';
            others_fs1.style.display='none';
            others_fs2.style.display='none';
          } else if (mainfsource == "3") {
            oda.style.display='block';
            oda2.style.display='block';
            others_fs1.style.display='none';
            others_fs2.style.display='none';
          } else if (mainfsource == "6") {
            others_fs1.style.display='block';
            others_fs2.style.display='block';
          } else {
            oda.style.display='none';
            oda2.style.display='none';
          }
        }

      function show4(mode) {
          if (mode == "78") {
            others_mode.style.display='block';
            others_mode2.style.display='block';
          } else {
            others_mode.style.display='none';
            others_mode2.style.display='none';
          }
        }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Physical and Financial Status</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Status of Implementation Readiness <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="implementationreadiness" class="form-control" onchange="java_script_:show6(this.options[this.selectedIndex].value)">
                          <option selected value="">Choose Status</option>
                          <option value="1">Ongoing</option>
                          <option id="interOption" value="2">Proposed</option>
                          <option value="3">Completed</option>
                        </select>
                      </div>
                    </div>


                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Updates <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="updates" required></textarea>
                      </div>
                      <!--
                      <label class="col-sm-2 form-control-label">Remarks from the Agency </label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="remarks"></textarea>
                      </div>
                      -->
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" id="tier2cat"
                      
                      >Level of Readiness<br><small class="text-primary">Level 1 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving body but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With NEDA Board and/or ICC project approval but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small> <br/><small class="text-primary">Level 2 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label></small></small><small class="text-primary"><br>Level 3 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 4 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label></small></small></label>
                      <div class="col-sm-4" id="tier2cat2"

                      >
                        <select name="status2" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier2statuses as $tier2statuse)
                          <option value="{{ $tier2statuse->id }}"

                            >{{ $tier2statuse->tier2_status }}</option>
                          @endforeach
                        </select>
                      </div>
<!-- 
                      <label class="col-sm-2 form-control-label" id="tier1cat"

                      >Tier 1 - Status<br><small class="text-primary">Ongoing implementation <label data-toggle="tooltip" data-placement="top" title="A program or project is considered ongoing as follows:a)  if locally funded - upon issuance of notice to proceed to the winning bidder;b) if ODA-assisted - upon effectivity of signed loan or grant agreement; and c) if for implementation through public-private partnership (PPP) or joint venture (JV) arrangement - upon signing of concession / JV agreement, which includes unsolicited PPP PAPs initiated by private sector proponent with signed concession agreement."><i class="fas fa-question-circle flag_tomato"></i></label></small><br/><small class="text-primary"> Level 1 <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving authority but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 2 <label data-toggle="tooltip" data-placement="top" title="For approval of appropriate approving body in 2018 and/or 2019"><i class="fas fa-question-circle flag_tomato"></i></label></small></label>
                      <div class="col-sm-4" id="tier1cat1"

                      >
                        <select name="status1" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier1statuses as $tier1statuse)
                          <option value="{{ $tier1statuse->id }}"

                            >{{ $tier1statuse->tier1_status }}</option>
                          @endforeach
                        </select> -->
                      <!-- </div> -->
                      <label class="col-sm-2 form-control-label">As of</label>
                      <div class="col-sm-4">
                        <input type="date" placeholder="" class="form-control" name="asof" required>
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>

      <script type="text/javascript">
              function show6(implementationreadiness) {
          if (implementationreadiness == "2") {
            tier2cat.style.display='block';
            tier2cat2.style.display='block';
          } else {
            tier2cat.style.display='none';
            tier2cat2.style.display='none';
          }
        }
                      // $(document).ready(function () {


                      // $('#Tier2').click(function () {
                      //         $('#tier2s').show('fast');
                      //         $('#tier2cat').show('fast');
                      //         $('#tier1cat').hide('fast');
                      //         $('#tier2cat2').show('fast');
                      //         $('#tier1cat1').hide('fast');
                      //         $('#uacs').hide('fast');
                      //     });
                      // $('#Tier1').click(function () {
                      //         $('#tier2s').hide('fast');
                      //         $('#tier2cat').hide('fast');
                      //         $('#tier1cat').show('fast');
                      //         $('#tier2cat2').hide('fast');
                      //         $('#tier1cat1').show('fast');
                      //         $('#uacs').show('fast');
                      //     });
                      // $('#Completed').click(function () {
                      //         $('#tier2s').hide('fast');
                      //         $('#tier2cat').hide('fast');
                      //         $('#tier1cat').hide('fast');
                      //         $('#tier2cat2').hide('fast');
                      //         $('#tier1cat1').hide('fast');
                      //         $('#uacs').hide('fast');
                      //     });
                      // $('#Dropped').click(function () {
                      //         $('#tier2s').hide('fast');
                      //         $('#tier2cat').hide('fast');
                      //         $('#tier1cat').hide('fast');
                      //         $('#tier2cat2').hide('fast');
                      //         $('#tier1cat1').hide('fast');
                      //         $('#uacs').hide('fast');
                      //     });
                      // $('#Tier2b').click(function () {
                      //         $('#uacs2').show('fast');
                      //     });
                      // $('#Tier2a').click(function () {
                      //         $('#uacs2').hide('fast');
                      //     });
                      // });
      </script>

<div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Financial Accomplishments <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="NA_fs" type="checkbox" value="1" class="form-control-custom" name="NA_fs">
                            <label for="NA_fs">Not Applicable (For PAPs not for funding in the GAA)</label>
                          </div>
                        </div>
                    </div>
                  <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Year</th>
                                    <th>Amount included in the NEP</th>
                                    <th>Amount Allocated in the Budget/GAA</th>
                                    <th>Actual Amount Disbursed</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      2017
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2017">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2017">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2017">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2018
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2018">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2018">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2018">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                     2019
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2019">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2019">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2019">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2020
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2020">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2020">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2020">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2021
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2021">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2021">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2021">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2022
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2022">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2022">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2022">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Total
                                    </td>
                                    <td>
                                      <span id="nep_total">0</span>
                                    </td>
                                    <td>
                                     <span id="all_total">0</span>
                                    </td>
                                    <td>
                                      <span id="ad_total">0</span>
                                    </td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>

      <div class="row" id=menu>
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <button type="submit" class="btn btn-secondary btn-sm" name="statusofsubmission" value="Draft" onclick="SaveAsDraft();" formnovalidate="formnovalidate" >&nbsp;&nbsp;&nbsp;&nbsp;Save as Draft&nbsp;&nbsp;&nbsp;&nbsp;</button>
                        <div id="endorseButton">
                          <button type="submit" class="btn btn-primary btn-sm" name="statusofsubmission" value="Endorsed" onclick="SaveAsFinal();">Save as Endorsed</button>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
    </div>
  </section>


 <div id="ic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
    <div role="document" class="modal-dialog modal-lg"  style="max-width: 100%;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 id="exampleModalLabel" class="modal-title">Infrastructure Cost</h5>
          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
        </div>
      <div class="modal-body">
            <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Financing Source</th>
                                    <th>2016 and Prior</th>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>2017-2022</th>
                                    <th>Continuing Years</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal1 ic_2016" name="ic_2016_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2017" name="ic_2017_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2018" name="ic_2018_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2019" name="ic_2019_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2020" name="ic_2020_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2021" name="ic_2021_nglocal">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2022" name="ic_2022_nglocal">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-nglocal2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal1 ic_2023" name="ic_2023_nglocal">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-nglocal_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan1 ic_2016" name="ic_2016_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2017" name="ic_2017_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2018" name="ic_2018_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2019" name="ic_2019_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2020" name="ic_2020_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2021" name="ic_2021_ngloan">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2022" name="ic_2022_ngloan">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-ngloan2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan1 ic_2023" name="ic_2023_ngloan">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-ngloan_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant1 ic_2016" name="ic_2016_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2017" name="ic_2017_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2018" name="ic_2018_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2019" name="ic_2019_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2020" name="ic_2020_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2021" name="ic_2021_odagrant">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2022" name="ic_2022_odagrant">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-odagrant2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant1 ic_2023" name="ic_2023_odagrant">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-odagrant_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc1 ic_2016" name="ic_2016_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2017" name="ic_2017_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2018" name="ic_2018_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2019" name="ic_2019_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2020" name="ic_2020_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2021" name="ic_2021_gocc">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2022" name="ic_2022_gocc">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-gocc2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc1 ic_2023" name="ic_2023_gocc">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-gocc_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu1 ic_2016" name="ic_2016_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2017" name="ic_2017_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2018" name="ic_2018_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2019" name="ic_2019_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2020" name="ic_2020_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2021" name="ic_2021_lgu">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2022" name="ic_2022_lgu">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-lgu2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu1 ic_2023" name="ic_2023_lgu">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-lgu_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps1 ic_2016" name="ic_2016_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2017" name="ic_2017_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2018" name="ic_2018_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2019" name="ic_2019_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2020" name="ic_2020_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2021" name="ic_2021_ps">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2022" name="ic_2022_ps">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-ps2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps1 ic_2023" name="ic_2023_ps">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-ps_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others1 ic_2016" name="ic_2016_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2017" name="ic_2017_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2018" name="ic_2018_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2019" name="ic_2019_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2020" name="ic_2020_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2021" name="ic_2021_others">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2022" name="ic_2022_others">
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-others2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others1 ic_2023" name="ic_2023_others">
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-others_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="ic_2016_total">0</span></th>
                                    <th><span id="ic_2017_total">0</span></th>
                                    <th><span id="ic_2018_total">0</span></th>
                                    <th><span id="ic_2019_total">0</span></th>
                                    <th><span id="ic_2020_total">0</span></th>
                                    <th><span id="ic_2021_total">0</span></th>
                                    <th><span id="ic_2022_total">0</span></th>
                                    <th><span id="ic_total2">0</span></th>
                                    <th><span id="ic_2023_total">0</span></th>
                                    <th><span id="ic_total_all">0</span></th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
      </div>
      </div>
    </div>
  </div>

<!--   <div id="rb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
    <div role="document" class="modal-dialog modal-lg" style="max-width: 100%;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 id="exampleModalLabel" class="modal-title">Regional Breakdown</h5>
          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
        </div>
      <div class="modal-body">
            <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Region</th>
                                    <th>2016 and Prior</th>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>2017-2022</th>
                                    <th>Continuing Years</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody id="rbBody">
                                  @foreach ($regions as $region)
                                  <tr>
                                    <td>
                                      {{ $region->region_description }}
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}1 rb_2016" name="2016_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2017" name="2017_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2018" name="2018_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2019" name="2019_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2020" name="2020_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2021" name="2021_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2022" name="2022_{{$region->region_code}}">
                                    </td>
                                    <td>
                                      <span class="rb-total2" id="{{$region->region_code}}2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}1 rb_2023" name="2023_{{$region->region_code}}">
                                    </td>
                                    <td>
                                     <span class="rb-total_all" id="{{$region->region_code}}_total_all">0</span>
                                    </td>
                                  </tr>
                                  @endforeach
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="rb_2016_total">0</span></th>
                                    <th><span id="rb_2017_total">0</span></th>
                                    <th><span id="rb_2018_total">0</span></th>
                                    <th><span id="rb_2019_total">0</span></th>
                                    <th><span id="rb_2020_total">0</span></th>
                                    <th><span id="rb_2021_total">0</span></th>
                                    <th><span id="rb_2022_total">0</span></th>
                                    <th><span id="rb_total2">0</span></th>
                                    <th><span id="rb_2023_total">0</span></th>
                                    <th><span id="rb_total_all">0</span></th>
                                    
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
      </div>
      </div>
    </div>
  </div> -->

  <div class="modal fade interRegionalModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">  
            <h5 class="modal-title" id="exampleModalLabel">Interregional</h5>
            <button type="button" class="close interRegionalModalClose" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group col-lg-12">
                <label for="states">Regions</label>
                     <select class="form-control regionList" name="states[]" multiple="multiple">
                        @foreach ($regions as $region)
                          <option value="{{$region->regional_code}}">{{ $region->region_description }}</option>
                        @endforeach
                     </select>
                <label for="provinces">Provinces</label>
                     <select class="form-control provinceList" name="provinces[]" multiple="multiple">
                     </select>
                <label for="cities">City/Municipality</label>
                     <select class="form-control cityList" name="cities[]" multiple="multiple">
                     </select>
              </div>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade RegionSpec" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">  
            <h5 class="modal-title" id="exampleModalLabel">Region Specific</h5>
            <button type="button" class="close RegionSpecClose" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group col-lg-12">
                <label for="region">Regions</label>
                     <select class="form-control regionSpecList" name="region">
                        <option></option>
                        @foreach ($regions as $region)
                          <option value="{{$region->regional_code}}">{{ $region->region_description }}</option>
                        @endforeach
                     </select>
                <label for="RegionSpecProvincesList">Provinces</label>
                     <select class="form-control RegionSpecProvinceList" name="RegionSpecProvincesList[]" multiple>
                     </select>
              </div>
            </div>
        </div>
    </div>
  </div>

</form>



@endsection
@section('scripts')
<script src="{{ asset('js/c0d3w4rrior.js') }}"></script>

<script type="text/javascript">

$('.it-number').keyup(function() {
  if ($(this).val() == 0 ) {
    $('#endorseButton').hide();
  } else {
    $('#endorseButton').show();
  }
}).keyup();

</script>
@endsection
