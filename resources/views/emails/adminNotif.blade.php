<p>Dear PIP Secretariat</p>
<p>Please be informed that the User Information indicated below is now on the list of user requests.</p>
<ul>
    <li>Last Name: {{$user['lname']}}</li>
    <li>First Name: {{$user['fname']}}</li>
    <li>Middle Name: {{$user['mname']}}</li>
    <li>Designation: {{$user['designation']}}</li>
    <li>Tel Number: {{$user['telnumber']}}</li>
    <li>FAX Number: {{$user['faxnumber']}}</li>
    <li>Email Number: {{$user['email']}}</li>
    <li>Password: {{$user['password']}}</li>
</ul>
<p>Thank you.</p>
<p>PIPOL</p>