<p><strong>Agency: {{$user['agency']->UACS_AGY_DSC}}</strong></p>
<p><strong>Agency PIP/TRIP Focal: {{$user['lname']}}, {{$user['fname']}} {{$user['mname']}} </strong></p>
<br>
<p>Dear Sir/Ma'am:</p>
<p>As the authorized Agency PIP/TRIP Focal, this is to provide your user account information to access the PIPOL System.
Please be informed that the PIPOL System can now be accessed using the account credentials provided since September 9
until October 11, 2019.</p>
<p>Username: {{$user['username']}}</p>
<p>Password: As submitted in the sign-up page</p>
<p>Kindly note as well the following reminders:</p>
<p>A) Each user account cannot be used/accessed simultaneously by more than one user;</p>
<p>B) The PIPOL system generates system logs, which contain electronic record of transactions/interactions of each user
account in the system for monitoring and accountability purposes; and</p>
<p>C) User names and passwords are <strong>case sensitive.</strong></p>
<p>Should you have any inquiries, please do not hesitate to coordinate with the PIP Secretariat of the NEDA-Public
Investment Staff at contact numbers: DL 631-2165 or TL 631-0945 local no.: 404 or through e-mail address:
pip@neda.gov.ph.</p>
<p>Thanks you.</p>
<p><strong>PIP Secretariat</strong></p>
