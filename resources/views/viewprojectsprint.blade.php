<html lang="en" class="app">
<head>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PIPOL v2.0</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
  
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">    
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.default.css') }}" id="theme-stylesheet">
    <script src="{{ asset('js/bootstrap.mins.js') }}"></script>
    <!-- <script src="{{ asset('js/jQuery-3.2.1.min.js') }}"></script> -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.printPage.js') }}"></script>

  </head>
</head>
<body>

<div class="container-fluid">

  <div class="form-group row">
    <div class="col-sm-8">
      PIP
      @if($project->cip == 1)
       | CIP 
      @endif
      @if($project->trip == 1)
       | TRIP 
      @endif
      @if($project->iccable == 1)
        <button type="button" class="btn btn-secondary btn-xs">ICC-able: {{$project->currentlevel}}</button>
      @endif
      @if($project->rdip == 1)
        @if($project->rdc_endorsement == 1)
          <button type="button" class="btn btn-secondary btn-xs">RDC {{$project->rdc_endorsed_notendorsed}}</button>
        @endif
      @endif
    </div>
    <div class="col-sm-4">
      <b>Project Code:</b>
      {{$project->code}}
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12 print-title">
     <h2> Project Title: {{$project->title}} </h2>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> Project Description / Objectives: </b>
      <br><br>
      <div class="print-tabbed">{{$project->description}}</div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-4">
      <b> Implementing Agency: </b>
      <br>
      <div class="print-tabbed">{{$imp_agency->Abbreviation}}</div>
    </div>

    <div class="col-sm-4">
      <b> Co-Implementing Agency: </b>
      <br>
      <div class="print-tabbed">
        @foreach($co_agencies as $agency)
        {{$agency}}
        <br>
        @endforeach
      </div>
    </div>

    <div class="col-sm-4">
      <b> Attached Agency: </b>
      <br>
      <div class="print-tabbed">
       @foreach($att_agencies as $agency)
        {{$agency}}
        <br>
        @endforeach
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-4">
      <b> Spatial Coverage: </b>
      <br>
      <div class="print-tabbed">{{$project->spatial}}</div>
    </div>

    <div class="col-sm-4">
      <b> Region: </b>
      <br>
      <div class="print-tabbed">
        <div class="print-tabbed">
         @foreach($regions as $region)
          {{$region}}
          <br>
          @endforeach
        </div>

      </div>
    </div>

    <div class="col-sm-4">
      <b> Province/Municipalities: </b>
      <br>
      <div class="print-tabbed">
        @foreach($provinces as $province)
          {{$province}}
          <br>
        @endforeach
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> Main PDP Chapter: </b> 
      {{$chapter}}
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> PDP Results Matrices (RM) Indicators Addressed: </b> 
      <div class="print-tabbed">
        
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> Basis from Implementation: </b> 
      <div class="print-tabbed">
        @foreach($bases as $base)
        {{$base}}, 
        @endforeach
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> Expected Output: </b> 
      <div class="print-tabbed">
        {{$project->output}}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b> 10-Point Socioeconomic Agenda: </b> 
      <div class="print-tabbed">
        @if($agendas!=null)
        @foreach($agendas as $agenda)
        @if($agenda!=null)
        {{$agenda->id}}) {{$agenda->agenda}}
        <br/>
        @endif
        @endforeach
        @endif
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-4">
      <b> Implementation Period: </b>
      <br>
      <div class="print-tabbed">{{$project->start}} - {{$project->end}}</div>
    </div>

    <div class="col-sm-4">
      <b> Funding Source/s: </b>
      <br>
      <div class="print-tabbed">
      @foreach($finance_source_projects as $finance_source_project)
        @if($finance_source_project->fsource_id == 1)
            NG-Local / LGU
        @elseif($finance_source_project->fsource_id == 2)
            NG-ODA Loan
        @elseif($finance_source_project->fsource_id == 2)
            NG-ODA Grant
        @elseif($finance_source_project->fsource_id == 2)
            GOCC/GFI
        @elseif($finance_source_project->fsource_id == 2)
            Private Sector
        @elseif($finance_source_project->fsource_id == 2)
            Others
        @endif

      @endforeach
      </div>
    </div>

    <div class="col-sm-4">
      <b> Program/Project: </b>
      <br>
      <div class="print-tabbed">
        {{$progproj}}
      </div>
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-12">
      <b>Project Cost</b>
      <table border="1" align="center">
        <tr>
          <th></th>
          <th>NG-Local / LGU</th>
          <th>NG-ODA Loan</th>
          <th>NG-ODA Grant</th>
          <th>GOCC/GFI</th>
          <th>Private Sector</th>
          <th>Others</th>
          <th>Total</th>
        </tr>
        <?php $_local = 0; ?>
        <?php $_loan = 0; ?>
        <?php $_grant = 0; ?>
        <?php $_gocc = 0; ?>
        <?php $_private = 0; ?>
        <?php $_others = 0; ?>
        <tr>
          <td>2016 and Prior</td>
          @foreach($projectcost2016 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2017</td>
          @foreach($projectcost2017 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2018</td>
          @foreach($projectcost2018 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2019</td>
          @foreach($projectcost2019 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2020</td>
          @foreach($projectcost2020 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2021</td>
          @foreach($projectcost2021 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>2022</td>
          @foreach($projectcost2022 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td>Continuing Years</td>
          @foreach($projectcost2023 as $cost)
          <td>{{$cost->local}}</td>
          <td>{{$cost->loan}}</td>
          <td>{{$cost->grant}}</td>
          <td>{{$cost->gocc}}</td>
          <td>{{$cost->private}}</td>
          <td>{{$cost->others}}</td>
          <td><?php echo $cost->local + $cost->loan + $cost->grant + $cost->gocc + $cost->private + $cost->others; ?></td>
          <?php $_local = $cost->local + $_local; ?>
          <?php $_loan = $cost->loan + $_loan; ?>
          <?php $_grant = $cost->grant + $_grant; ?>
          <?php $_gocc = $cost->gocc + $_gocc; ?>
          <?php $_private = $cost->private + $_private; ?>
          <?php $_others = $cost->others + $_others; ?>
          @endforeach
        </tr>
        <tr>
          <td><b>Overall Total</b></td>
          <td><?php echo $_local; ?></td>
          <td><?php echo $_loan; ?></td>
          <td><?php echo $_grant; ?></td>
          <td><?php echo $_gocc; ?></td>
          <td><?php echo $_private; ?></td>
          <td><?php echo $_others; ?></td>
          <td><b><?php echo $_local + $_loan + $_grant + $_gocc + $_private + $_others; ?></b></td>
        </tr>
      </table>
    </div>
  </div>

</div>


</body>
</html>