<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
Agency: 

Agency PIP/TRIP Focal: 

Dear Sir/Ma'am: 

As the authorized Agency PIP/TRIP Focal, this is to provide your user account information to access the PIPOL System.  Please be informed that the PIPOL System v2.0 will be open to Agency PIP/TRIP Focals starting August 15, 2018 until October 12, 2018 for the submission of priority programs and projects (PAPs) for inclusion in the Updated 2017-2022 PIP and TRIP for FY 2020-2022 as input to the FY 2020 Budget Preparation.

Username: 
Password:  

Kindly note as well the following reminders: 
A) Each user account cannot be used/accessed simultaneously by more than one user; 
B) The PIPOL system generates system logs, which contain electronic record of transactions/interactions of each user account in the system for monitoring and accountability purposes; and
C) User names and passwords are case sensitive.

For reference, you may refer to the Terms of Reference of Agency PIP/TRIP Focals uploaded in the PIP Online System. Should you have any inquiries, please do not hesitate to coordinate with the PIP Secretariat of the NEDA-Public Investment Staff at contact numbers: DL 631-2165 or TL 631-0945 local 404 or through e-mail address: pip@neda.gov.ph

Thank you. 

PIP Secretariat
</body>
</html>