<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PIPOL Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fontastic.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="css/grasp_mobile_progress_circle-1.0.0.min.css">
    <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  </head>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>CHANGE &nbsp;</span><strong class="text-primary">PASSWORD</strong></div>
            <p class="text-body">Password must be atleast 6 alphanumeric characters</p> <br>
            <form class="text-left form-validate" action="{{ asset('/changepassworduser') }}" method="POST" >{{ csrf_field() }}
              <div class="form-group-material">
                <input id="oldPassword" type="password" name="oldPassword" required data-msg="Please enter your password" class="input-material">
                <label for="oldPassword" class="label-material">Password</label>
              </div>
              <div class="form-group-material">
                <input id="newPassword" type="password" name="newPassword" required data-msg="Please enter your password" class="input-material" minlength="6">
                <label class="label-material">New Password</label>
              </div>
              <div class="form-group-material">
                <input id="confirmPassword" type="password" name="confirmPassword" required data-msg="Please enter your password" class="input-material" minlength="6">
                <label for="confirmPassword" class="label-material">Confirm Password</label>
              </div>
              @include('error')
              <div class="form-group text-center">
                <input id="{{ asset('/login') }}" type="submit" value="Save" class="btn btn-primary">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
    <script type="text/javascript">
      $('input').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9@.]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });
    </script>
  </body>
</html>

@include('sweet::alert')