<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Validate Projects </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validate Draft PIP/CIP Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">PIP/CIP</a>
        </li>
      </ul>

      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="example" class="table" style="width: 100%;">
        <thead>
            <tr>
              <th>PIP Code</th>
              <th>Project Title</th>
              <th>Mother Agency</th>
              <th>Attached Agency</th>
              <th>Spatial Coverage</th>
              <th>Head Chapter</th>
              <th>Action</th>
            </tr>
        </thead>
        
        <tbody>
           @foreach ($allpips as $allpip)
            <tr>
              <td>{{$allpip->code}}</td>
              <td>{{$allpip->title}}</td>
              <td>
                @if (empty($allpip->motheragency_id))
                  @foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @else
                  @foreach ($allpip->getAgencyDetails($allpip->motheragency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @endif
              </td>
              <td>
                @if (empty($allpip->motheragency_id))

                @else
                  @foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @endif
              </td>
              <td>{{$allpip->spatial}}</td>
              <td>Chapter {{$allpip->mainpdp}}</td>
              <td>
                <!-- @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
                <a href="{{ asset('/reclassify') }}/{{$allpip->id}}" target="_blank"><button type="button" class="btn btn-primary">R</button></a>
                @endif -->
                <a href="{{ asset('/editproject') }}/{{$allpip->id}}" target="_blank"><button type="button" class="btn btn-primary">V</button></a>
                <a href="{{ asset('/viewprojectsprint') }}/{{$allpip->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
              </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>  
              <th>PIP Code</th>
              <th>Project Title</th>
              <th>Mother Agency</th>
              <th>Attached Agency</th>
              <th>Spatial Coverage</th>
              <th>Head Chapter</th>
              <th>Action</th>
            </tr>
        </tfoot>
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>
@endsection