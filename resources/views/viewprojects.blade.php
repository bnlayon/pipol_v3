@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">View My Projects </li>
      </ul>
    </div>
  </div>

  <section class="forms">
    <span class="errorTxt">
    <button type="button" class="btn btn-danger btn-xs">Draft</button>
    <button type="button" class="btn btn-info btn-xs">Endorsed</button>
  </span>
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">View My Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      @switch(auth()->user()->user_type)
      @case("AG")
      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">PIP/CIP</a>
        </li>
        <!--<li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#trip" role="tab">TRIP</a>
        </li>-->
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#dropped" role="tab">Dropped</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#completed" role="tab">Completed</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#not" role="tab">Not PIP/TRIP</a>
        </li>
      </ul>
      @break
      @endswitch

      <!-- Tab panes -->
      <div class="tab-content tablewrapper">
        <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white;">
          <br/>
          <table id="example" class="table table-responsive" style="width: 100%;">
          <thead>
              <tr>
                  <th>PIPOL Code</th>
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($pipprojects as $project)
              <tr
              @if($project->statusofsubmission == 'Draft')
                style="background-color:#f2dede;"
              @elseif($project->statusofsubmission == 'Endorsed')
                style="background-color:#dff0d8;"
              @endif
              >
              <?php $id = encrypt($project->id); ?>
                <td>{{$project->code}}</td>
                <td>{{$project->title}}</td>
                <td>{{$project->spatial}}</td>
                <td>Chapter {{$project->mainpdp}}</td>
                <td>
                    {{$project->implementation}}
                </td>
                <td>{{$project->statusofsubmission}}</td>
                <?php $idtoapprove = $project->id; ?>
                <td>
                  <a href="{{ asset('/editproject') }}/{{$id}}" target="_blank"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="icon-list-1"></i></button></a>
                  <form action="{{ asset('/markascomplete') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <!-- <button type="button" data-toggle="modal" data-target="#completeproject<?php //echo $idtoapprove; ?>" class="btn btn-primary" disabled>C</button> -->
                  <!--<button type="button" data-toggle="modal" data-target="#completeproject<?php //echo $idtoapprove; ?>" class="btn btn-primary"
                    disabled>C</button> --> 
                  <div id="completeproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="CompleteProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="CompleteProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as complete?</h5>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                  </form>

                  <form action="{{ asset('/markasdropped') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                 <!--  <button type="button" data-toggle="modal" data-target="#droppedproject<?php echo $idtoapprove; ?>" class="btn btn-primary" disabled>D</button> -->
                 <button disabled type="button" data-toggle="modal" data-target="#droppedproject<?php echo $idtoapprove; ?>" class="btn btn-primary">D</button> 
                  <div id="droppedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="DroppedProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="DroppedProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as dropped?</h5>
                                <h6>Reason/s for dropping the project:</h6>
                                <textarea class="form-control" rows="5" id="droppingreasons" name="droppingreasons" required></textarea>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                </form>
                </td>
                <td>
<!--                   <button type="button" data-toggle="modal" data-target="#projectinfo{{$project->id}}" class="btn btn-primary"><i class="fa fa-search"></i></button>  -->
                  <a href="{{ asset('/viewprojectsprint') }}/{{$id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
                </td>
              </tr>

<!--               <div id="projectinfo{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Project Information: {{$project->title}}</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p class="text-project">
                        <h6>Project Status:</h6>
                        <div class="progress" style="height: 20px;">
                          @if ($project->statusofsubmission == 'Draft')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Draft</div>
                          @elseif ($project->statusofsubmission == 'Endorsed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 40%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Endorsed</div>
                          @elseif ($project->statusofsubmission == 'Reviewed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 60%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Reviewed</div>
                          @elseif ($project->statusofsubmission == 'Validated')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Validated</div>
                          @elseif ($project->statusofsubmission == 'Completed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Completed</div>
                          @endif
                        </div>
                      </p>
                      <p class="text-project">
                        <h6>Description:</h6>
                        {{$project->description}}
                      </p>
                      <p class="text-project">
                        <h6>Implementation Period:</h6>
                        {{$project->start}} - {{$project->end}}
                      </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                    </div>
                  </div>
                </div>
              </div> -->
            @endforeach
          </tbody>
          <tfoot>
              <tr>  
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Region</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </tfoot>
          </table>
        </div>
        <div class="tab-pane" id="dropped" role="tabpanel" style="background-color: white">
          <br/>
          <table id="example3" class="table table-striped">
          <thead>
              <tr>
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </thead>
          <tbody>
          @foreach ($droppedprojects as $project)
          <?php $id = encrypt($project->id); ?>
              <tr>
                <td>{{$project->title}}</td>
                <td>{{$project->spatial}}</td>
                <td>Chapter {{$project->mainpdp}}</td>
                <td>
                    {{$project->implementation}}
                </td>
                <td>{{$project->statusofsubmission}}</td>
                <?php $idtoapprove = $project->id; ?>
                <td>
                  <a href="{{ asset('/editproject1') }}/{{$project->id}}" target="_blank"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="icon-list-1"></i></button></a>

                  <form action="{{ asset('/markascomplete') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#completedroppedproject<?php echo $idtoapprove; ?>" class="btn btn-primary" disabled>C</button> 
                  <div id="completedroppedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="CompleteProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="CompleteProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as complete?</h5>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                  </form>

                  <form action="{{ asset('/markasdropped') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#droppeddroppedproject<?php echo $idtoapprove; ?>" class="btn btn-primary" disabled>D</button> 
                  <div id="droppeddroppedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="DroppedProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="DroppedProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as dropped?</h5>
                                <h6>Reason/s for dropping the project:</h6>
                                <textarea class="form-control" rows="5" id="droppingreasons" name="droppingreasons" required></textarea>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                </form>
                </td>
                <td>
<!--                   <button type="button" data-toggle="modal" data-target="#droppedprojectinfo{{$project->id}}" class="btn btn-primary"><i class="fa fa-search"></i></button>  -->
                 <a href="{{ asset('/viewprojectsprint') }}/{{$project->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
                </td>
              </tr>
<!--               <div id="droppedprojectinfo{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Project Information: {{$project->title}}</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p class="text-project">
                        <h6>Project Status:</h6>
                        <div class="progress" style="height: 20px;">
                          @if ($project->statusofsubmission == 'Draft')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Draft</div>
                          @elseif ($project->statusofsubmission == 'Endorsed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 40%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Endorsed</div>
                          @elseif ($project->statusofsubmission == 'Reviewed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 60%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Reviewed</div>
                          @elseif ($project->statusofsubmission == 'Validated')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Validated</div>
                          @elseif ($project->statusofsubmission == 'Completed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Completed</div>
                          @endif
                        </div>
                      </p>
                      <p class="text-project">
                        <h6>Description:</h6>
                        {{$project->description}}
                      </p>
                      <p class="text-project">
                        <h6>Implementation Period:</h6>
                        {{$project->start}} - {{$project->end}}
                      </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                    </div>
                  </div>
                </div>
              </div> -->
            @endforeach
          </tbody>
          <tfoot>
              <tr>  
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </tfoot>
          </table>
        </div>

        <div class="tab-pane" id="completed" role="tabpanel" style="background-color: white">
          <br/>
          <table id="example4" class="table table-striped">
          <thead>
              <tr>
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </thead>
          <tbody>
          @foreach ($completedprojects as $project)
          <?php $id = encrypt($project->id); ?>
              <tr>
                <td>{{$project->title}}</td>
                <td>{{$project->spatial}}</td>
                <td>Chapter {{$project->mainpdp}}</td>
                <td>
                    {{$project->implementation}}
                </td>
                <td>{{$project->statusofsubmission}}</td>
                <?php $idtoapprove = $project->id; ?>
                <td>
                  <a href="{{ asset('/editproject1') }}/{{$project->id}}" target="_blank"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="icon-list-1"></i></button></a>

                  <form action="{{ asset('/markascomplete') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#completecompletedproject<?php echo $idtoapprove; ?>" class="btn btn-primary" disabled>C</button> 
                  <div id="completecompletedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="CompleteProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="CompleteProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as complete?</h5>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                  </form>

                  <form action="{{ asset('/markasdropped') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#droppedcompletedproject<?php echo $idtoapprove; ?>" class="btn btn-primary" disabled>D</button> 
                  <div id="droppedcompletedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="DroppedProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="DroppedProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as dropped?</h5>
                                <h6>Reason/s for dropping the project:</h6>
                                <textarea class="form-control" rows="5" id="droppingreasons" name="droppingreasons" required></textarea>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                </form>
                </td>
                <td>
<!--                   <button type="button" data-toggle="modal" data-target="#completedprojectinfo{{$project->id}}" class="btn btn-primary"><i class="fa fa-search"></i></button>  -->
                  <a href="{{ asset('/viewprojectsprint') }}/{{$project->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
                </td>
              </tr>
<!--               <div id="completedprojectinfo{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Project Information: {{$project->title}}</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p class="text-project">
                        <h6>Project Status:</h6>
                        <div class="progress" style="height: 20px;">
                          @if ($project->statusofsubmission == 'Draft')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Draft</div>
                          @elseif ($project->statusofsubmission == 'Endorsed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 40%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Endorsed</div>
                          @elseif ($project->statusofsubmission == 'Reviewed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 60%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Reviewed</div>
                          @elseif ($project->statusofsubmission == 'Validated')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Validated</div>
                          @elseif ($project->statusofsubmission == 'Completed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Completed</div>
                          @endif
                        </div>
                      </p>
                      <p class="text-project">
                        <h6>Description:</h6>
                        {{$project->description}}
                      </p>
                      <p class="text-project">
                        <h6>Implementation Period:</h6>
                        {{$project->start}} - {{$project->end}}
                      </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                    </div>
                  </div>
                </div>
              </div> -->
            @endforeach
          </tbody>
          <tfoot>
              <tr>  
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </tfoot>
          </table>
        </div>

        <div class="tab-pane" id="not" role="tabpanel" style="background-color: white;">
          <br/>
          <table id="example5" class="table table-responsive" style="width: 100%;">
          <thead>
              <tr>
                  <th>Project Title</th>
                  <th>Spatial Coverage</th>
                  <th>Head Chapter</th>
                  <th>Category</th>
                  <th>Status of Submission</th>
                  <th>Action</th>
                  <th>Function</th>
              </tr>
          </thead>
          <tbody>
            @foreach ($draftprojects as $project)
              <tr>
                <?php $id = encrypt($project->id); ?>
                <td>{{$project->title}}</td>
                <td>{{$project->spatial}}</td>
                <td>Chapter {{$project->mainpdp}}</td>
                <td>
                    {{$project->implementation}}
                </td>
                <td>{{$project->statusofsubmission}}</td>
                <?php $idtoapprove = $project->id; ?>
                <td>
                  <a href="{{ asset('/editproject1') }}/{{$project->id}}" target="_blank"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="icon-list-1"></i></button></a>

                  <form action="{{ asset('/markascomplete') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }} 
                  <div id="completeproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="CompleteProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="CompleteProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as complete?</h5>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                  </form>

                  <form action="{{ asset('/markasdropped') }}/{{$project->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#droppedproject<?php echo $idtoapprove; ?>" class="btn btn-primary">D</button> 
                  <div id="droppedproject<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="DroppedProject" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="DroppedProject" class="modal-title">{{$project->title}}</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to mark this project as dropped?</h5>
                                <h6>Reason/s for dropping the project:</h6>
                                <textarea class="form-control" rows="5" id="droppingreasons" name="droppingreasons" required></textarea>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                    </div>
                  </div>
                </form>
                </td>
                <td>
<!--                   <button type="button" data-toggle="modal" data-target="#projectinfo{{$project->id}}" class="btn btn-primary"><i class="fa fa-search"></i></button>  -->

                  <a href="{{ asset('/viewprojectsprint') }}/{{$project->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" data-target="#reclassify" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
                </td>
              </tr>
<!-- 
              <div id="projectinfo{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Project Information: {{$project->title}}</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p class="text-project">
                        <h6>Project Status:</h6>
                        <div class="progress" style="height: 20px;">
                          @if ($project->statusofsubmission == 'Draft')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Draft</div>
                          @elseif ($project->statusofsubmission == 'Endorsed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 40%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Endorsed</div>
                          @elseif ($project->statusofsubmission == 'Reviewed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 60%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Reviewed</div>
                          @elseif ($project->statusofsubmission == 'Validated')
                          <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Validated</div>
                          @elseif ($project->statusofsubmission == 'Completed')
                          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">Completed</div>
                          @endif
                        </div>
                      </p>
                      <p class="text-project">
                        <h6>Description:</h6>
                        {{$project->description}}
                      </p>
                      <p class="text-project">
                        <h6>Implementation Period:</h6>
                        {{$project->start}} - {{$project->end}}
                      </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                    </div>
                  </div>
                </div>
              </div> -->
            @endforeach
          </tbody>

          </table>
        </div>
      </div>

    </div>
    </section>

    
@endsection