<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Validate Projects </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validate TRIP Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">TRIP</a>
        </li>
      </ul>

      
      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="trip_table" class="table" style="width: 100%;">
        <thead>
            <tr>
              <!-- <th>Project ID</th> -->
              <th>PIP Code</th>
              <th>Project Title</th>
<!--              <th>Mother Agency</th> -->
              <th>Agency</th>
<!-- <th>Spatial Coverage</th>  -->
              <th>Sector</th>
              <!--  <th>Head Chapter</th>   --> 
<!--               <th>Agency Status/Category</th>              
              <th>NRO Status/Category</th>
              <th>NEDA PIP Chapter Focal Status/Category</th>  -->
              <!-- <th>SS Status of Submission</th>  -->
              <th>Action</th> 
            </tr>
        </thead>
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>
@endsection