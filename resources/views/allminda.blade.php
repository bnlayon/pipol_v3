@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">View All Mindanao Projects</li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">View All Mindanao Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">PIP/CIP</a>
        </li>
      </ul>

      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="minda" class="table" style="width: 100%;">
        <thead>
            <tr>
              <th>Project Title</th>
              <th>Agency</th>
              <th>Spatial Coverage</th>
              <th>Head Chapter</th>
              <th>Funding Source</th>
              <th>Category</th>
              <th>Action</th>
              <th>Function</th>
            </tr>
        </thead>
        <tbody>
           @foreach ($allpips as $allpip)
            <tr>
              <td>{{$allpip->title}}</td>
              <td>@foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                @endforeach
              </td>
              <td>{{$allpip->spatial}}</td>
              <td>Chapter {{$allpip->mainpdp}}</td>
              <td>
                @foreach ($fundingsources as $fundingsource)
                    @if($fundingsource->fsource_no == $allpip->mainfsource)
                      {{$fundingsource->fsource_description}}
                    @endif
                  @endforeach
              </td>
              <td>{{$allpip->category}}</td>
              <td>
                <a href="{{ asset('/editproject') }}/{{$allpip->id}}" target="_blank"><button type="button" class="btn btn-primary"><i class="icon-list-1"></i></button></a>
              </td>
              <td>
                <a href="{{ asset('/viewprojectsprint') }}/{{$allpip->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
              </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>  
              <th>Project Title</th>
              <th>Agency</th>
              <th>Spatial Coverage</th>
              <th>Head Chapter</th>
              <th>Funding Source</th>
              <th>Category</th>
              <th>Action</th>
              <th>Function</th>
            </tr>
        </tfoot>
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>
@endsection