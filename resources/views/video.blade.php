@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Video Tutorial </li>
      </ul>
    </div>
  </div>

  <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Video Tutorial</h1>
      </header>
     
    </div>
  </section>

  <section class="statistics">
        <div class="container-fluid">
          <div class="row d-flex">
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-video"></i></div>
                <strong class="text-primary">Add Project Module</strong>
                <a href="https://drive.google.com/file/d/15HzHlOrOBHaly941sciDu8Mgsr6CNTx1/view?usp=sharing" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-video"></i></div>
                <strong class="text-primary">NRO Validation</strong>
                <a href="https://drive.google.com/file/d/1kShuDuueFWrs-2PRFjXfQWblQR9-EgOM/view?usp=sharing" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-video"></i></div>
                <strong class="text-primary">NCO Validation</strong>
                <a href="https://drive.google.com/file/d/1gie8bFwD4DVoAUuF3ydyR7kbBP3MSXIg/view?usp=sharing" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-video"></i></div>
                <strong class="text-primary">Reclassification</strong>
                <a href="https://drive.google.com/file/d/1EyXg35EDDEHRPQnsa6Wy8My77FM_QPGO/view?usp=sharing" target="_blank">Click Here!</a>
              </div>
            </div>
          </div>
        </div>
      </section>


@endsection