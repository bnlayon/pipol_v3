@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Related Links </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Related Links</h1>
      </header>
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>List of Related Documents</h4>
                </div>
                <div class="card-body">
                  <table id="example_link1" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($links as $link)
                        <tr>
                          <td>{{$link->title}}</td>
                          @if($link->iflinks == 1)
                            <td align="center">
                            <a href="{{$link->attachment}}" target="_blank" >
                            <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </a>
                          </td>
                          @else
                            <td align="center">
                            <a href="{{ asset('storage/storage/links/') }}/{{$link->attachment}}" target="_blank" >
                            <button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </a>
                            </td>
                          @endif
                          
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      @if (auth()->user()->user_type == "ADM")
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Archive of PIP-Related References</h4>
                </div>
                <div class="card-body">
                  <table id="example_link2" class="table table-striped table-bordered" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($links_archives as $links_archive)
                        <tr>
                          <td>{{$links_archive->title}}</td>
                          <td align="center"><button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button> {{$links_archive->attachment}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>
            </div>
              </div>
        </div>
      </div>
      @endif
    </div>

    

  </section>


@endsection