<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PIPOL Sign Up</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fontastic.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="css/grasp_mobile_progress_circle-1.0.0.min.css">
    <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <div class="page register2-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><strong class="text-primary">PIPOL System Online Sign-up Page</strong></div>
            <p class="text-body">The PIPOL System v2.0 will be open to Agency PIP Focals starting September 17, 2018 for the submission of priority progarms and projects (PAPs) for inclusion in the Updated 2017-2022 PIP and TRIP for FY2020-2022  as input to the FY 2020 Budget Preparation.</p> <br>
            <p class="text-body">For this PIP Updating, all concerned agencies are required to reconfirm/designate their PIP/TRIP Focals who will be provided with the new username and password by the PIP and TRIP Secretariats to access the PIPOL System.          
            </p> <br>
            <p class="text-body">Kindly provide all <strong class="text-primary">required information</strong> and <strong class="text-primary">upload a copy of the duly signed authorization <a href="{{ asset('/storage/authform.docx') }}" target="_blank"><u>form</u></a></strong> in PDF or JPEG format. Please be informed that all requests for PIPOL System account username and password will be subject to review and approval by the PIP and TRIP Secretariats. Once approved, an email will be sent to the provided email addresses of the authorized Agency PIP/TRIP Focals with the account username and password.</p> <br>

            <p align="left"><strong class="text-danger"><i>All fields with * are required. </i></strong></p>

            @include('error')

            <form class="text-left form-validate" action="{{ asset('/register') }}" method="POST" enctype="multipart/form-data" onsubmit="check_if_capcha_is_filled">{{ csrf_field() }}
              <div class="form-group row">
                 <div class="col-sm-12"><label class="form-control-label">Agency*</label></div>
                  <div class="col-sm-12">
                    <select id="agency" name="agency" class="form-control" required>
                      <option disabled selected>Choose Agency</option>
                        @foreach ($all_agencies as $agency)
                        <option value="{{ $agency->id }}">{{ $agency->UACS_DPT_ID }}
                          @if ($agency->Category != 'HEAD')
                          {{ $agency->UACS_AGY_ID }}@endif
                          - {{ $agency->UACS_AGY_DSC }}</option>
                        @endforeach
                    </select>
                  </div>

                  <div id="withMotherAgency1">
                    <div class="col-sm-12"><label class="form-control-label">Mother Agency </label></div>
                    <div class="col-sm-12">
                      <label class="form-control-label"><div id="mother_agency_desc"></div></label>
                      <input type="hidden" id="mother_agency" name="mother_agency" value="0">
                    </div>
                  </div>


              </div>

              <div class="form-group row">
                <div class="col-sm-12"><label class="form-control-label">1.  Agency PIP/TRIP Focal 1  (Director IV and above)*</label></div>
                <div class="col-sm-4">
                  <input id="login-1_loginLastname" type="text" name="1_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-1_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="login-1_loginFirstname" type="text" name="1_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="1_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="1_loginMiddlename" type="text" name="1_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="1_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-4">
                  <input id="1_loginDes" type="text" name="1_loginDes" required data-msg="Please enter your Designation" class="input-material">
                <label for="1_loginDes" class="label-material">Designation:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="1_loginTel" type="text" name="1_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="1_loginTel" class="label-material">Telephone Number <small>(Please include area code e.g., 083-1234567)</small>:* </label>
                </div>
                <div class="col-sm-4">
                  <input id="1_loginMFax" type="text" name="1_loginMFax" class="input-material">
                <label for="1_loginMFax" class="label-material">Fax Number: </label>
                </div>
                <div class="col-sm-12">
                  <input id="1_email" type="email" name="1_email" required class="input-material">
                <label for="1_email" class="label-material">Email Address (please double check):*</label>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-12"><label class="form-control-label">2.  Agency PIP/TRIP Focal 2 </label></div>
                <div class="col-sm-4">
                  <input id="login-2_loginLastname" type="text" name="2_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-2_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="login-2_loginFirstname" type="text" name="2_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="2_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="2_loginMiddlename" type="text" name="2_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="2_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-4">
                  <input id="2_loginDes" type="text" name="2_loginDes" required data-msg="Please enter your Designation" class="input-material">
                <label for="2_loginDes" class="label-material">Designation:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="2_loginTel" type="text" name="2_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="2_loginTel" class="label-material">Telephone Number <small>(Please include area code e.g., 083-1234567)</small>:* </label>
                </div>
                <div class="col-sm-4">
                  <input id="2_loginMFax" type="text" name="2_loginMFax" class="input-material">
                <label for="2_loginMFax" class="label-material">Fax Number: </label>
                </div>
                <div class="col-sm-12">
                  <input id="2_email" type="email" name="2_email" required class="input-material">
                <label for="2_email" class="label-material">Email Address (please double check):*</label>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-12"><label class="form-control-label">3.  Agency PIP/TRIP Focal 3 </label></div>
                <div class="col-sm-4">
                  <input id="login-3_loginLastname" type="text" name="3_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-3_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="login-3_loginFirstname" type="text" name="3_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="3_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="3_loginMiddlename" type="text" name="3_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="3_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-4">
                  <input id="3_loginDes" type="text" name="3_loginDes" required data-msg="Please enter your Designation" class="input-material">
                <label for="3_loginDes" class="label-material">Designation:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="3_loginTel" type="text" name="3_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="3_loginTel" class="label-material">Telephone Number <small>(Please include area code e.g., 083-1234567)</small>:* </label>
                </div>
                <div class="col-sm-4">
                  <input id="3_loginMFax" type="text" name="3_loginMFax" class="input-material">
                <label for="3_loginMFax" class="label-material">Fax Number: </label>
                </div>
                <div class="col-sm-12">
                  <input id="3_email" type="email" name="3_email" required class="input-material">
                <label for="3_email" class="label-material">Email Address (please double check):*</label>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-12"><label class="form-control-label">Contact Information of the Head of the Agency<br><small class="text-primary">Should be consistent with the signatory of the Authorization Form. All advisories will be sent to the Head of Agency through the e-mail address to be provided below.</small></label>
                  </div>
                <div class="col-sm-4">
                  <input id="login-4_loginLastname" type="text" name="4_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-4_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="login-4_loginFirstname" type="text" name="4_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="4_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="4_loginMiddlename" type="text" name="4_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="4_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-4">
                  <input id="4_loginDes" type="text" name="4_loginDes" required data-msg="Please enter your Designation" class="input-material">
                <label for="4_loginDes" class="label-material">Designation:(Secretary, SUC President, etc.)*</label>
                </div>
                <div class="col-sm-4">
                  <input id="4_loginTel" type="text" name="4_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="4_loginTel" class="label-material">Telephone Number <small>(Please include area code e.g., 083-1234567)</small>:* </label>
                </div>
                <div class="col-sm-4">
                  <input id="4_loginMFax" type="text" name="4_loginMFax" class="input-material">
                <label for="4_loginMFax" class="label-material">Fax Number: </label>
                </div>
                <div class="col-sm-12">
                  <input id="4_email" type="email" name="4_email" required class="input-material">
                <label for="4_email" class="label-material">Email Address (please double check):*</label>
                </div>
              </div>

              <div class="form-group row" id="withMotherAgency2">
                <div class="col-sm-12"><label class="form-control-label">Contact Information of the Head of the Mother Agency (if applicable)<br><small class="text-primary">Should be consistent with the signatory of the Authorization Form. All advisories will be sent to the Head of Agency through the e-mail address to be provided below.</small></label>
                  </div>
                <div class="col-sm-4">
                  <input id="login-5_loginLastname" type="text" name="5_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-5_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="login-5_loginFirstname" type="text" name="5_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="5_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-4">
                  <input id="5_loginMiddlename" type="text" name="5_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="5_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-4">
                  <input id="5_loginDes" type="text" name="5_loginDes" required data-msg="Please enter your Designation" class="input-material">
                <label for="5_loginDes" class="label-material">Designation:(Secretary, SUC President, etc.)*</label>
                </div>
                <div class="col-sm-4">
                  <input id="5_loginTel" type="text" name="5_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="5_loginTel" class="label-material">Telephone Number <small>(Please include area code e.g., 083-1234567)</small>:* </label>
                </div>
                <div class="col-sm-4">
                  <input id="5_loginMFax" type="text" name="5_loginMFax" class="input-material">
                <label for="5_loginMFax" class="label-material">Fax Number: </label>
                </div>
                <div class="col-sm-12">
                  <input id="5_email" type="email" name="5_email" required class="input-material">
                <label for="5_email" class="label-material">Email Address (please double check):*</label>
                </div>
              </div>

              <div class="form-group-material">
                <label class="form-control-label">Upload Authorization Form</label> | <a href="{{ asset('/storage/authform.docx') }}" target="_blank">Download Form</a><br><small class="text-primary">Duly signed authorization form by the Head of Agency. File should be in PDF or JPEG Format and should not exceed the maximum size of 5 MB.</small></p>
                <input id="registerAttachment" type="file" accept=".pdf,.jpg,.jpeg" id="registerAttachment" name="registerAttachment" required data-msg="Please upload your designation form" class="input-material" onchange="return FileValidation()">
                <label for="register-attachment" class="label-material"></label>
              </div>

              <strong class="text-danger"><label class="form-control-label">Please check reCAPTCHA below:</label></strong>
              <div class="g-recaptcha"
                   data-callback="capcha_filled"
                   data-expired-callback="capcha_expired"
                   data-sitekey="6LfJA3AUAAAAAGht99VY1X_mWZiuzf4mSCKjRAZj" style="align-content: center;"></div>

              <div class="form-group text-center">
                <input id="{{ asset('/register') }}" type="submit" value="Register" class="btn btn-primary">
              </div>
            </form><small>Already have an account? </small><a href="/login" class="signup">Login</a>
          </div>

          <div class="copyrights text-center">
            <p>National Economic and Development Authority <a href="#" class="external">Information and Communications Technology Staff</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
    <script type="text/javascript">
     
      $(document).ready(function() {
        $('select[name="agency"]').on('change', function() {
            var maValue = $(this).val();        

            $.ajax({
              url: './getmother',
              type: 'GET',
              dataType: 'json',
              data: {
                '_token': $('input[name=_token]').val(),
                'id'    : maValue
              },
              success: function(data) {

                if(data.Category == "ATT" || data.Category == "ATT_GOCC" || data.Category == "SUC") {
                  Display("show");
                  $('#mother_agency_desc').html(data.UACS_DPT_DSC);
                  $('#mother_agency').val(data.motheragency_id);
                } else {
                  Display("hidden");
                  $('#mother_agency_desc').html("");
                  $('#mother_agency').val(0);
                }                
                
              }
            });	 
            
        });
    
        Display("hidden");

      });

      function Display(visibility) {
        if(visibility == "hidden")
          {
            $('#withMotherAgency1').hide();
            $('#withMotherAgency2').hide();
            $('#login-5_loginLastname').removeAttr("required");
            $('#login-5_loginFirstname').removeAttr("required");
            $('#login-5_loginMiddlename').removeAttr("required");
            $('#login-5_loginDes').removeAttr("required");
            $('#login-5_loginTel').removeAttr("required");
            $('#login-4_email').removeAttr("required");
          }
          else {
            $('#withMotherAgency1').show();
            $('#withMotherAgency2').show();
            $('#login-5_loginLastname').prop('required', true);
            $('#login-5_loginFirstname').prop('required', true);
            $('#login-5_loginMiddlename').prop('required', true);
            $('#login-5_loginDes').prop('required', true);
            $('#login-5_loginTel').prop('required', true);
            $('#login-4_email').prop('required', true);
          }
      }

      function FileValidation(){
          var fileInput = document.getElementById('registerAttachment');
          var filePath = fileInput.value;
          var allowedExtensions = /(\.jpg|\.jpeg|\.pdf)$/i;
          if(!allowedExtensions.exec(filePath)){
              alert('File should be in PDF or JPEG Format only.');
              fileInput.value = '';
              return false;
          }
      }

    $('input').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9@.- ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });

    var allowSubmit = false;

    function capcha_filled () {
      allowSubmit = true;
    }

    function capcha_expired () {
      allowSubmit = false;
    }

    function check_if_capcha_is_filled (e) {
    if(allowSubmit) return true;
    e.preventDefault();
    alert('Fill in the capcha!');
}

   </script>
  </body>
</html>

@include('sweet::alert')