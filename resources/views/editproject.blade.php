<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Edit Project</li>
      </ul>
    </div>
  </div>
    <?php $isOkAgency = 0; ?>
@switch(auth()->user()->user_type)
  @case("AG")
    @if($agency_agency->id != $projects->agency_id)
    
      @if($agencytype->Category == 'HEAD')
        @foreach ($getIdsOfAttached as $getIdsOfAttache)
          <?php $isOK = 0; ?>
          <?php $isOkAgency = 0; ?>
          @if($projects->agency_id == $getIdsOfAttache->id)
            <?php $isOK = 1; ?>
            <?php $isOkAgency = $getIdsOfAttache->id; ?>
            <?php break; ?>
          @endif
        @endforeach
        @if($isOK == 1)

        @else
        <script>window.location = "{{ asset('/dashboard') }}";</script>
        @endif
      @else
        <script>window.location = "{{ asset('/dashboard') }}";</script>
      @endif
    @endif
  @break
@endswitch


<form class="text-left needs-validation" action="{{ asset('/editproject') }}/{{$projects->id}}" method="POST" enctype="multipart/form-data" novalidate id="addProjForm">{{ csrf_field() }}

  <span class="errorTxt">
    <button type="button" class="btn btn-secondary btn-xs">For Draft Projects, please fill up the projec title</button>
    <button type="button" class="btn btn-primary btn-xs">For Finalized Projects, please fill up fields with <i class="fas fa-flag"></i> </button>
    <button type="button" data-toggle="modal" data-target="#logs" class="btn btn-secondary btn-xs">Project Logs</button>
  </span>
  <br/><br/>

  <div id="logs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
      <div role="document" class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-ticle">Project Logs</h5>
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Username</th>
                      <th>Activity</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($logs_projects as $logs_project)
                    <tr>
                      <td>{{ $logs_project->created_at }}</td>
                      <td>{{ $logs_project->username }}</td>
                      <td>{{ $logs_project->activity }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        </div>
        </div>
      </div>
  </div>

  <div class="row">
      <div class="col-lg-12">
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#about">General Information</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ia">Implementing Agency</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#sc">Spatial Coverage</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#loa">Level of Approval</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#piwpd">Project for Inclusion in Which Programming Document</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pdp">Philippine Development Plan (PDP) Chapter</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ag">0-10 Point Socioeconomic Agenda </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#sdgs">Sustainable Development Goals (SDG) </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#rms">Results Matrices </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ip">Implementation Period</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ppd">Project Preparation Details</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pcc">Pre-construction Costs</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#eg">Employment Generation</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#it">Investment Targets</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#fss">Funding Source </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pa">Physical Accomplishments </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#fa">Financial Accomplishments</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#val">Validation</a></button>
      </div>
  </div>

  <section class="forms" id="forms" 
  <?php
     if(auth()->user()->user_type == 'NRO' || auth()->user()->user_type == 'MINDA' || auth()->user()->user_type == 'OS')
      echo "style='pointer-events:none;'";
  ?>
  >
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Project Code: {{ $projects->code }}
        </h1>
      </header>

      <section class="bg-primary" id="about">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>General Information</h4>
                </div>
                <div class="card-body">

                    <div class="form-group row">
                      <label class="col-sm-2 control-label" for="title">Project Title <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="title" id="title" required value="{{ $projects->title }}">
                        <span class="text-small text-gray help-block-none">The project title should be identical with the project's title in the budget proposal submitted to DBM.</span>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Is it a Program or a Project? <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <div class="i-checks">
                          <input id="radioProg" type="radio" value="1" name="prog_proj" class="form-control-custom radio-custom form-check-input" required
                          @if ($projects->prog_proj == 1)
                            checked
                          @endif
                          >
                          <label for="radioProg" data-toggle="tooltip" data-placement="top" title="A program is a group of activities and projects that contribute to a common particular outcome. A program should have the following: (a) unique expected results or outcomes; (b) a clear target population or client group external to the agency; (c) a defined method of intervention to achieve the desired result; and (d) a clear management structure that defines accountabilities.">Program </label>
                          <input id="radioProj" type="radio" value="2" name="prog_proj" class="form-control-custom radio-custom form-check-input" required
                          @if ($projects->prog_proj == 2)
                            checked
                          @endif
                          >
                          <label for="radioProj" data-toggle="tooltip" data-placement="top" title="A project is a special undertaking carried out within a definite time frame and intended to result in some pre -determined measure of goods and services.">Project</label>
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>
                      </div>
                      <label class="col-sm-2 form-control-label" id="reg_prog_choices" 
                      @if ($projects->prog_proj == 1)
                        style="display: block;"
                      @else
                        style="display: none;"
                      @endif
                      >Is it a Regular Program? <label data-toggle="tooltip" data-placement="top" title="A regular program refers to a program being implemented by agencies on a regular basis."><i class="fas fa-question-circle flag_tomato"></i></label>&nbsp;<i class="fas fa-flag flag_tomato"></i></label>
                        <div class="col-sm-10"  id="reg_prog_choices2"
                        @if ($projects->prog_proj == 1)
                        style="display: block;"
                      @else
                        style="display: none;"
                      @endif
                        >
                          <div class="i-checks">
                            <input id="radioProgYes" type="radio" value="1" name="reg_prog" class="form-control-custom radio-custom form-check-input"
                            @if ($projects->reg_prog == 1)
                              checked
                            @endif
                            >
                            <label for="radioProgYes">Yes </label>
                            &nbsp;&nbsp;&nbsp;<input id="radioProgNo" type="radio" value="0" name="reg_prog" class="form-control-custom radio-custom form-check-input"
                            @if ($projects->reg_prog == 0)
                              checked
                            @endif
                            >
                            <label for="radioProgNo">No</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                      $(document).ready(function () {
                          $('#radioProg').click(function () {
                              $('#reg_prog_choices').show('fast');
                              $('#reg_prog_choices2').show('fast');
                          });
                          $('#radioProj').click(function () {
                              $('#reg_prog_choices').hide('fast');
                              $('#reg_prog_choices2').hide('fast');
                          });
                      });
                    </script>

                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Basis for Implementation <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Identify the basis for the implementation of the program/ project. Check all applicable</small></label>
                      <div class="col-sm-10">
                         <div class="i-checks basisImp">
                        @foreach ($basis_ as $basis)                       
                          <input id="checkboxCustomBasis{{ $basis->id }}" type="checkbox" value="{{ $basis->id }}" class="form-control-custom form-check-input" name="basis_id[]" onchange="java_script_:BasisImpCheckboxes(this.checked ? '1' : '0')" required
                          @foreach ($basis_projects as $basis_project)
                            @if($basis->id == $basis_project->basis_id)
                              checked
                            @endif
                          @endforeach
                          >
                          <label for="checkboxCustomBasis{{ $basis->id }}">{{ $basis->basis }}</label>
                          <br />                                    
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>  
                                              
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Components and Objectives <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Identify the Components of the Program/Project. If a Program, please identify the sub-programs/projects and explain the objective of the program/project in terms of responding to the PDP/ RM.</small></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="description" required>{{ $projects->description }}</textarea>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="ia">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementing Agency</h4>
                </div>
                <div class="card-body">
                  @switch(auth()->user()->user_type)
                    @case("AG")
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Mother/Oversight Agency </label>
                      <div class="col-sm-10">
                        @if($submission->motheragency_id == 0)
                          {{$agency_agency->UACS_AGY_DSC}}
                          <input type="hidden" name="agency_id" value="{{$submission->agency_id}}">
                          <input type="hidden" name="motheragency_id" value="0">
                          @if($projects->code == '')
                            <input type="hidden" name="code" value="{{$agency_agency->UACS_DPT_ID}}-">
                          @else
                            <input type="hidden" name="code" value="{{$projects->code}}">
                          @endif
                        @else
                          {{$agency_mother->UACS_AGY_DSC}}
                          <input type="hidden" name="agency_id" value="{{$submission->agency_id}}">
                          <input type="hidden" name="motheragency_id" value="{{$submission->motheragency_id}}">
                          @if($projects->code == '')
                            <input type="hidden" name="code" value="{{$agency_agency->UACS_DPT_ID}}{{$agency_agency->UACS_AGY_ID}}-">
                          @else
                            <input type="hidden" name="code" value="{{$projects->code}}">
                          @endif
                        @endif
                      </div>
                      <label class="col-sm-2 form-control-label">Co-Implementing Agency </label>
                      <div class="col-sm-10">
                        <select id="coimp" name="coimp[]" multiple="multiple">
                          @foreach ($agencies as $agency)
                            <option value="{{$agency->id}}"
                              @foreach ($agencies_projects as $agencies_project)
                                @if($agency->id == $agencies_project->agency_id)
                                  selected
                                @endif
                              @endforeach
                              >{{$agency->UACS_AGY_DSC}}</option>
                          @endforeach
                        </select>
                      </div>

                      <label class="col-sm-2 form-control-label">Attached Agency </label>
                        @if($submission->motheragency_id == 0)
                          <div class="col-sm-10">
                            <select id="att" name="att[]" multiple="multiple">
                              @foreach ($agencies as $agency)
                                <option value="{{$agency->id}}"
                                  @foreach ($attagencies_projects as $attagencies_project)
                                    @if($agency->id == $attagencies_project->agency_id)
                                      selected
                                    @endif
                                  @endforeach
                                    @if($agency->id == $isOkAgency)
                                      selected
                                    @endif
                                  >{{$agency->UACS_AGY_DSC}}</option>
                              @endforeach

                            </select>
                          </div>
                        @else
                          <div class="col-sm-10">
                          {{$agency_agency->UACS_AGY_DSC}}
                          </div>
                        @endif
                    </div>
                    @break

                    @default
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">
                          <input type="hidden" name="agency_id" value="{{$projects->agency_id}}">
                          <input type="hidden" name="motheragency_id" value="{{$projects->motheragency_id}}">
                        @foreach ($projects->getAgencyDetails($projects->agency_id) as $agencydetail)
                          {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      </label>
                    </div>
                  @endswitch

                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="sc"
        <?php
          if(auth()->user()->user_type == 'NRO')
            echo "style='pointer-events:auto;'";
        ?>
      >
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Spatial Coverage <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Coverage<br><small class="text-primary">Nationwide <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or project covers all regions (in parts or as a whole)."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"> Interregional <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or project pertains to more than one region (in parts or as a whole) but not all regions."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Region Specific <label data-toggle="tooltip" data-placement="top" title="If spatial coverage/impact of a program or a project pertains to one region (in parts or as a whole)."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"> Abroad <label data-toggle="tooltip" data-placement="top" title="If spatial coverage of a program or project is outside the country that will have an impact to Filipinos outside of the country (e.g., Overseas Filipino Workers)."><i class="fas fa-question-circle flag_tomato"></i></label></small></label>
                      <div class="col-sm-10">
                        <select id="proj_cov_main" name="spatial" class="form-control" onchange="java_script_:show(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Coverage</option>
                          <label data-toggle="tooltip" data-placement="top" title="A program is a group of activities and projects that contribute to a common particular outcome. A program should have the following: (a) unique expected results or outcomes; (b) a clear target population or client group external to the agency; (c) a defined method of intervention to achieve the desired result; and (d) a clear management structure that defines accountabilities.">A</label>
                          <option value="Nationwide"
                          @if ($projects->spatial == 'Nationwide')
                            selected
                          @endif
                          >Nationwide</option>
                          <option id="interOption" value="Interregional"
                          @if ($projects->spatial == 'Interregional')
                            selected
                          @endif
                          >Interregional</option>
                          <option value="Region Specific"
                          @if ($projects->spatial == 'Region Specific')
                            selected
                          @endif
                          >Region Specific</option>
                          <option value="Abroad"
                          @if ($projects->spatial == 'Abroad')
                            selected
                          @endif
                          >Abroad</option>
                        </select>
                      </div>
                    </div>
<!--                     {{ csrf_field() }}
                    <select class="form-control regionList2" name="states_id[]" multiple="multiple">
                        @foreach ($regions as $region)
                          <option value="{{$region->regional_code}}"
                            @foreach ($states_projects as $states_project)
                            @if($region->regional_code == $states_project->region_id)
                              selected
                            @endif
                          @endforeach
                            >{{ $region->region_description }}</option>
                        @endforeach
                     </select>
                     <select class="form-control provinceList2" name="provinces_id[]" multiple="multiple">
                        @foreach ($provinces as $prov)
                          <option value={{$prov->id}} 
                            @foreach ($province_projects as $province_project)
                              @if ($prov->id == $province_project->province_id)
                                selected
                              @endif
                            @endforeach
                          >{{$prov->provName}}</option>
                        @endforeach
                     </select> -->
                    <div class="form-group row" id="inter" style="display: none;">
                      <label class="col-sm-2 form-control-label">Interregional</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input class="interRegion" id="checkCustomInterRegion{{ $region->region_no }}" type="checkbox" value="{{ $region->regional_code }}" name="inter">
                          <label for="checkCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                            {{-- <div class="card" id="reg{{ $region->regional_code }}" style="display: none;">
                              <div class="card-body reg{{ $region->regional_code }}">
                              </div>
                            </div> --}}
                        </div>
                        @endforeach
                      </div>
                    </div>
                    {{-- <div class="form-group row" id="rs" style="display: none;">
                      <label class="col-sm-2 form-control-label">Region Specific</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input id="radioCustomInterRegion{{ $region->region_no }}" type="radio" value="{{ $region->region_no }}" name="rs" class="form-control-custom radio-custom">
                          <label for="radioCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div> --}}
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function show(proj_cov_main) {
          if (proj_cov_main == 'Interregional') {
            //inter.style.display='block';
            //rs.style.display='none';
          } else if(proj_cov_main == 'Region Specific') {
            inter.style.display='none';
            //rs.style.display='block';
          } else {
            inter.style.display='none';
            //rs.style.display='none';
          }
        }
      </script>

      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Level of Approval <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                            <div class="form-group row">
                              <div class="col-sm-12">
                                  <div class="i-checks">
                                    <input id="ICCNBcheckboxYes" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="iccable" required
                                    @if ($projects->iccable == 1)
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBcheckboxYes">Will require Investment Coordination Committee/NEDA Board Approval (ICC-able)? </label> &nbsp;&nbsp;&nbsp;
                                    <input id="ICCNBcheckboxNo" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="iccable" required
                                    @if ($projects->iccable == 0)
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBcheckboxNo">Not Applicable </label>
                                    <div class="invalid-feedback">
                                     This is a required field.
                                   </div>
                                  </div>
                                  <div id="icccheckboxes"
                                  @if ($projects->iccable == 1)
                                    style="display: block;"
                                  @else
                                    style="display: none;"
                                  @endif
                                  >
                                  <div class="i-checks">
                                    <input id="ICCNBradio1" type="radio" value="Yet to be submitted to NEDA Secretariat" name="currentlevel" class="form-control-custom radio-custom form-check-input"
                                    @if ($projects->currentlevel == "Yet to be submitted to NEDA Secretariat")
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBradio1">Yet to be submitted to the NEDA Secretariat</label>
                                    <div id="tdos"
                                    @if ($projects->currentlevel == "Yet to be submitted to NEDA Secretariat")
                                      style="display: block;"
                                    @else
                                      style="display: none;"
                                    @endif
                                    >
                                       <label for="tdos">Target Date of Submission</label>
                                          <div class="col-sm-4">
                                          <input type="date" class="form-control" name="approval_date1" value="{{ $projects->approval_date1 }}">
                                          </div>
                                    </div>
                                    <br />
                                    <input id="ICCNBradio2" type="radio" value="Under the NEDA Secretariat Review" name="currentlevel" class="form-control-custom radio-custom form-check-input"
                                    @if ($projects->currentlevel == "Under the NEDA Secretariat Review")
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBradio2">Under the NEDA Secretariat Review</label>
                                    <div id="dostns"
                                    @if ($projects->currentlevel == "Under the NEDA Secretariat Review")
                                      style="display: block;"
                                    @else
                                      style="display: none;"
                                    @endif
                                    >
                                       <label for="dostns">Date of Submission to NEDA Secretariat</label>
                                       <div class="col-sm-4">
                                          <input type="date" class="form-control" name="approval_date2" value="{{ $projects->approval_date2 }}">
                                        </div>
                                    </div>
                                    <br />
                                    <input id="ICCNBradio3" type="radio" value="ICC-TB Endorsed" name="currentlevel" class="form-control-custom radio-custom form-check-input"
                                    @if ($projects->currentlevel == "ICC-TB Endorsed")
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBradio3">ICC-TB Endorsed</label>
                                    <div id="doa"
                                    @if ($projects->currentlevel == "ICC-TB Endorsed")
                                      style="display: block;"
                                    @else
                                      style="display: none;"
                                    @endif
                                    >
                                       <label for="doa">Date of Approval</label>
                                       <div class="col-sm-4">
                                          <input type="date" class="form-control" name="approval_date3" value="{{ $projects->approval_date3 }}">
                                        </div>
                                    </div>
                                    <br />
                                    <input id="ICCNBradio4" type="radio" value="ICC-CC Approved" name="currentlevel" class="form-control-custom radio-custom form-check-input"
                                    @if ($projects->currentlevel == "ICC-CC Approved")
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBradio4">ICC-CC Approved</label>
                                    <div id="doa2"
                                    @if ($projects->currentlevel == "ICC-CC Approved")
                                      style="display: block;"
                                    @else
                                      style="display: none;"
                                    @endif
                                    >
                                       <label for="doa2">Date of Approval</label>
                                       <div class="col-sm-4">
                                          <input type="date" class="form-control" name="approval_date4" value="{{ $projects->approval_date4 }}">
                                        </div>
                                    </div>
                                    <br />
                                    <input id="ICCNBradio5" type="radio" value="NEDA Board Confirmed" name="currentlevel" class="form-control-custom radio-custom form-check-input"
                                    @if ($projects->currentlevel == "NEDA Board Confirmed")
                                      checked
                                    @endif
                                    >
                                    <label for="ICCNBradio5">NEDA Board Confirmed</label>
                                    <div id="doa3"
                                    @if ($projects->currentlevel == "NEDA Board Confirmed")
                                      style="display: block;"
                                    @else
                                      style="display: none;"
                                    @endif
                                    >
                                       <label for="doa3">Date of Approval</label>
                                       <div class="col-sm-4">
                                          <input type="date" class="form-control" name="approval_date5" value="{{ $projects->approval_date5 }}">
                                        </div>
                                    </div>
                                    <div class="invalid-feedback">
                                      This is a required field.
                                    </div>
                                  </div>
                                  
                                </div>
                                
                              </div>
                            </div>
                            <script src="http://code.jquery.com/jquery-latest.js"></script>
                            <script type="text/javascript">
                              $(document).ready(function () {
                                  $('#ICCNBcheckboxYes').click(function () {
                                      $('#icccheckboxes').show('fast');
                                  });
                                  $('#ICCNBcheckboxNo').click(function () {
                                      $('#icccheckboxes').hide('fast');
                                  });
                                  $('#ICCNBradio1').click(function () {
                                      $('#tdos').show('fast');
                                      $('#dostns').hide('fast');
                                      $('#doa').hide('fast');
                                      $('#doa2').hide('fast');
                                      $('#doa3').hide('fast');
                                  });
                                  $('#ICCNBradio2').click(function () {
                                      $('#tdos').hide('fast');
                                      $('#dostns').show('fast');
                                      $('#doa').hide('fast');
                                      $('#doa2').hide('fast');
                                      $('#doa3').hide('fast');
                                  });
                                  $('#ICCNBradio3').click(function () {
                                      $('#tdos').hide('fast');
                                      $('#dostns').hide('fast');
                                      $('#doa').show('fast');
                                      $('#doa2').hide('fast');
                                      $('#doa3').hide('fast');
                                  });
                                  $('#ICCNBradio4').click(function () {
                                      $('#tdos').hide('fast');
                                      $('#dostns').hide('fast');
                                      $('#doa').hide('fast');
                                      $('#doa2').show('fast');
                                      $('#doa3').hide('fast');
                                  });
                                  $('#ICCNBradio5').click(function () {
                                      $('#tdos').hide('fast');
                                      $('#dostns').hide('fast');
                                      $('#doa').hide('fast');
                                      $('#doa2').hide('fast');
                                      $('#doa3').show('fast');
                                  });
                              });
                            </script>
                </div>
              </div>
        </div>
      </div>

      <section class="bg-primary" id="piwpd">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project for Inclusion in Which Programming Document </h4>
                </div>
                <div class="card-body">
                  
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <div style="pointer-events: none;">
                          <input id="PIPcheckbox" type="checkbox" value="1" checked class="form-control-custom" name="pip">
                          <label for="PIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains priority programs and projects to be implemented by NG, GOCC, GFI and other national government offices that are responsive to the PDP and RM"><b>Public Investment Program (PIP)</b> <i class="fas fa-flag flag_tomato"></i></label>
                          </div>
                          <div class="i-checks">
                          @foreach ($piptypologies as $piptypology)
                          <input id="radioCustomPIPtypo{{ $piptypology->id }}" type="radio" value="{{ $piptypology->id }}" name="pip_typo" class="form-control-custom radio-custom form-check-input" required
                          @if ($projects->pip_typo == $piptypology->id)
                              checked
                          @endif
                          >
                          <label for="radioCustomPIPtypo{{ $piptypology->id }}">{{ $piptypology->typologies }}
                            @if ($piptypology->id == 7)
                              <label data-toggle="tooltip" data-placement="top" title="Government Buildings be used as offices, housing (military and civilian), and for other technical purposes by NGAs, GOCCs, GFI, other NG offices and instrumentalities and SUCs"><i class="fas fa-question-circle flag_tomato"></i></label>
                            @endif
                          </label>
                          <br />
                          @if ($piptypology->id == 2)
                             
                            @if ($piptypology->id == 2)
                              <div id="RDYesNo" style="display: block;">
                            @else
                              <div id="RDYesNo" style="display: none;">
                            @endif
                              <div class="i-checks">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Is it a Research and Development Program/Project </label><br/>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="RDYes" type="radio" value="1" name="rd" class="form-control-custom radio-custom form-check-input"
                                @if ($projects->rd == 1)
                                  checked
                                @endif
                                >
                                <label for="RDYes">Yes </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="RDNo" type="radio" value="0" name="rd" class="form-control-custom radio-custom form-check-input"
                                @if ($projects->rd == 0)
                                  checked
                                @endif
                                >
                                <label for="RDNo">No</label>
                              </div>
                            </div>
                          @endif
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                           </div>
                          </div>
                        </div>
                      </div>

                      <script type="text/javascript">
                      $(document).ready(function () {
                          $('#radioCustomPIPtypo2').click(function () {
                              $('#RDYesNo').show('fast');
                          });
                          $('#radioCustomPIPtypo1').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                          $('#radioCustomPIPtypo6').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                          $('#radioCustomPIPtypo7').click(function () {
                              $('#RDYesNo').hide('fast');
                          });
                      });
                    </script>

                      <div class="col-sm-6">
                        <div class="i-checks">
                          <input id="CIPcheckbox" type="checkbox" value="1" class="form-control-custom" onclick="CIPCheckboxFunction()" name="cip"
                          @if ($projects->cip == 1)
                              checked
                          @endif
                          >
                          <label for="CIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains the big ticket programs and projects of the PIP that serves as pipeline for the NEDA Board and Investment Coordination Committee (ICC)"><b>Core Investment Programs/Projects (CIP)</b></label>
                          <div id="cipcheckboxes"
                          @if ($projects->cip == 1)
                              style="display: block;"
                          @else
                              style="display: none;"
                          @endif
                          >
                            <div class="i-checks cipRbtn">
                            @foreach ($ciptypologies as $ciptypology)
                            <input id="radioCustomCIPtypo{{ $ciptypology->id }}" type="radio" value="{{ $ciptypology->id }}" name="cip_typo" class="form-control-custom radio-custom form-check-input"
                            @if ($projects->cip_typo == $ciptypology->id)
                              checked
                            @endif
                            ><label for="radioCustomCIPtypo{{ $ciptypology->id }}">{{ $ciptypology->typologies }}</label><br/>
                            @endforeach
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <script type="text/javascript">
                        function CIPCheckboxFunction() {
                        var checkBox = document.getElementById("CIPcheckbox");
                        var text = document.getElementById("cipcheckboxes");
                        var text2 = document.getElementById("gad");
                        var cipRbtn = $('.cipRbtn :radio');
                        var gadRbtn = $('.gadRbtn :radio');
                        var rowa1Rbtn = $('.rowa1Rbtn :radio');
                        var rowa2Rbtn = $('.rowa2Rbtn :radio');
                        var rowa3Rbtn = $('.rowa3Rbtn :radio');
                        var ppdSelect = $('#projdocument');

                        if (checkBox.checked == true){
                          text.style.display = "block";
                          text2.style.display = "block";
                          cipRbtn.attr('required', 'required');
                          gadRbtn.attr('required', 'required');
                          ppdSelect.attr('required', 'required');
                          rowa1Rbtn.attr('required', 'required');
                          rowa2Rbtn.attr('required', 'required');
                          rowa3Rbtn.attr('required', 'required');
                        } else {
                          text.style.display = "none";
                          text2.style.display = "none";
                          cipRbtn.removeAttr('required');
                          gadRbtn.removeAttr('required');
                          ppdSelect.removeAttr('required');
                          rowa1Rbtn.removeAttr('required');
                          rowa2Rbtn.removeAttr('required');
                          rowa3Rbtn.removeAttr('required');
                        }
                      }
                      </script>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6">
                          <div class="i-checks">
                            <input id="TRIPcheckbox" type="checkbox" value="1" class="form-control-custom" onclick="TRIPCheckboxFunction()" name="trip"
                            @if ($projects->trip == 1)
                              checked
                            @endif
                            >
                            <label for="TRIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains nationally funded infrastructure projects irrespective of cost and financing with emphasis on immediate priorities to be undertaken in three-year periods"><b>Three-year Rolling Infrastructure Program (TRIP)</b></label>
                          </div>
                      </div>
                      <div class="col-sm-6" 
                      <?php
                        if(auth()->user()->user_type == 'NRO')
                          echo "style='pointer-events:auto;'";
                      ?>>
                          <div class="i-checks">
                            <label for="TRIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains nationally funded infrastructure projects irrespective of cost and financing with emphasis on immediate priorities to be undertaken in three-year periods"><b>Is the Program/Project included in the RDIP? <i class="fas fa-flag flag_tomato"></i></b></label>
                            <input id="RDIPcheckboxYes" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="rdip" required
                            @if ($projects->rdip == 1)
                              checked
                            @endif
                            >
                            <label for="RDIPcheckboxYes">Yes </label> &nbsp;&nbsp;&nbsp;
                            <input id="RDIPcheckboxNo" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="rdip" required
                            @if ($projects->rdip == 0)
                              checked
                            @endif
                            >
                            <label for="RDIPcheckboxNo">No </label>
                            <div class="invalid-feedback">
                             This is a required field.
                            </div>
                          </div>
                          <div class="i-checks" id="rdipcheckboxes">
                            <input id="RDIPcheckbox1" type="checkbox" value="1" class="form-control-custom" onclick="RDIPCheckboxFunction2()" name="rdc_endorsement"
                            @if ($projects->rdc_endorsement == 1)
                              checked
                            @endif
                            >
                            <label for="RDIPcheckbox1">Will require Regional Development Council (RDC) Endorsement?</label>
                          </div>
                          <div id="rdipcheckboxes2"
                          @if ($projects->rdc_endorsement == 1)
                              style="display: block;"
                          @else
                              style="display: none;"
                          @endif
                          >
                            <div class="i-checks">
                              <input id="RDCEndorseed1" type="radio" value="Endorsed" name="rdc_endorsed_notendorsed" class="form-control-custom radio-custom"
                              @if ($projects->rdc_endorsed_notendorsed == 'Endorsed')
                              checked
                              @endif
                              >
                              <label for="RDCEndorseed1">Endorsed</label><br/>
                              <div id="RDCEndorseedDate"
                              @if ($projects->rdc_endorsed_notendorsed == 'Endorsed')
                                style="display: block;"
                              @else
                                  style="display: none;"
                              @endif
                              >
                              <label for="RDCEndorseedDate">Date of Endorsement</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" name="rdc_date_endorsement" value="{{$projects->rdc_date_endorsement}}">
                              </div>
                              </div>
                              
                            </div>
                            <div class="i-checks">
                              <input id="RDCEndorseed2" type="radio" value="Yet to be Endorsed" name="rdc_endorsed_notendorsed" class="form-control-custom radio-custom"
                              @if ($projects->rdc_endorsed_notendorsed == 'Yet to be Endorsed')
                              checked
                              @endif
                              >
                              <label for="RDCEndorseed2">Yet to be Endorsed</label>
                            </div>
                          </div>
                      </div>
                      
                      <script type="text/javascript">
                        function TRIPCheckboxFunction() {
                        var checkBox = document.getElementById("TRIPcheckbox");
                        var text = document.getElementById("tripdiv");
                        var infraSecChbxs = $('.infraSecChbxs :checkbox[name="sectors[]"]');
                        var impReadinessChbxs = $('.impReadiness :checkbox[name="status[]"]');
                        var risk =  $('textarea#risk');

                        if (checkBox.checked == true){
                          text.style.display = "block";
                          infraSecChbxs.attr('required', 'required');
                          impReadinessChbxs.attr('required', 'required');
                          risk.attr('required', 'required');
                        } else {
                          text.style.display = "none";
                          infraSecChbxs.removeAttr('required');
                          impReadinessChbxs.removeAttr('required');
                          risk.removeAttr('required');
                        }
                      }

                        function RDIPCheckboxFunction() {
                        var checkBox = document.getElementById("RDIPcheckbox");
                        var text = document.getElementById("rdipcheckboxes");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      // $(document).ready(function () {
                      //     $('#RDIPcheckboxYes').click(function () {
                      //         $('#rdipcheckboxes').show('fast');
                      //     });
                      //     $('#RDIPcheckboxNo').click(function () {
                      //         $('#rdipcheckboxes').hide('fast');
                      //         $('#rdipcheckboxes2').hide('fast');
                      //         RDIPcheckbox1.removeAttr('checked');
                      //     });
                      //   });

                      function RDIPCheckboxFunction2() {
                        var checkBox = document.getElementById("RDIPcheckbox1");
                        var text = document.getElementById("rdipcheckboxes2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      $(document).ready(function () {
                          $('#RDCEndorseed1').click(function () {
                              $('#RDCEndorseedDate').show('fast');
                          });
                          $('#RDCEndorseed2').click(function () {
                              $('#RDCEndorseedDate').hide('fast');
                          });
                        });
                      </script>
                    </div>
                </div>
              </div>
        </div>
      </div>
      <div class="row" id="tripdiv"
      @if ($projects->trip == 1)
        style="display: block;"
      @else
        style="display: none;"
      @endif
      >
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Three-year Rolling Infrastructure Program</h4>
                </div>
                <div class="card-body">
                  
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Infrastructure Sector </label>
                          <div class="i-checks infraSecChbxs">
                          @foreach ($sectors as $sector)
                          <input id="checkboxCustomSector{{ $sector->id }}" type="checkbox" value="{{ $sector->id }}" class="form-control-custom form-check-input testsec" onclick="InfraCheckboxFunction{{ $sector->id }}()" name="sectors[]" onchange="java_script_:InfraSectorCheckboxes()"
                            @foreach ($sector_projects as $sector_project)
                                @if($sector->id == $sector_project->sector_id)
                                  checked
                                @endif
                              @endforeach
                          >
                          <label for="checkboxCustomSector{{ $sector->id }}">{{ $sector->sector }}</label>
                          <br />
                          <div style="display: none;" id="mothersector{{ $sector->id }}">
                            <div class="i-checks infraSubSecChbxs{{ $sector->id }}">
                          @foreach ($subsectors as $subsector)
                          @if ($sector->id == $subsector->sector)
                            &nbsp;&nbsp;&nbsp;<input id="checkboxCustomSubSector{{ $subsector->id }}" type="checkbox" value="{{ $subsector->id }}" class="form-control-custom form-check-input" name="subsectors[]" onchange="java_script_:InfraSubSectorCheckboxes{{ $sector->id }}()"
                              @foreach ($subsector_projects as $subsector_project)
                                @if($subsector->id == $subsector_project->subsector_id)
                                  checked
                                @endif
                              @endforeach
                            >
                            <label for="checkboxCustomSubSector{{ $subsector->id }}">{{ $subsector->subsector }}</label>
                            <br />
                          @endif
                          @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                          </div>
                        
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Status of Implementation Readiness</label>
                          <div class="i-checks impReadiness">
                         @foreach ($statuses as $status)
                          <input id="checkboxCustomStatus{{ $status->id }}" type="checkbox" value="{{ $status->id }}" class="form-control-custom form-check-input" name="status[]" onchange="java_script_:ImpReadinessCheckboxes()"
                            @foreach ($status_projects as $status_project)
                                @if($status->id == $status_project->status_id)
                                  checked
                                @endif
                              @endforeach
                          >
                          <label for="checkboxCustomStatus{{ $status->id }}">{{ $status->statuses }}</label>
                          <br />
                        @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                         </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Implementation Risks and Mitigation Strategies</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="risk" name="risk">{{ $projects->risk }}</textarea>
                      </div>
                    </div>
                    <div class="form-group row" align="center">
                      <div class="col-sm-10">
                      <button type="button" data-toggle="modal" data-target="#ic" class="btn btn-primary">Infrastructure Cost</button> 
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>



      <section class="bg-primary" id="pa"
        <?php
        if(auth()->user()->user_type == 'NRO' || auth()->user()->user_type == 'SS')
            echo "style='pointer-events:none;'";
        ?>
      >


      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Physical and Financial Status</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Status of Implementation Readiness <i class="fas fa-flag flag_tomato"></i></label>
                      <!-- <div class="col-sm-10">
                        <div class="i-checks">
                          <input id="Tier1" type="radio" value="Tier 1" name="category" class="form-control-custom radio-custom form-check-input" onclick="tier2Radios()" required
                          @if ($projects->category == 'Tier 1')
                              checked
                          @endif
                          >
                          <label for="Tier1" data-toggle="tooltip" data-placement="top" title="PAPs with known budgetary amounts that are essential for the continued implementation of existing approved PAPs, including ROW acquisition, and FS preparation.">Tier 1</label>
                          <div id="uacs" value="{{$projects->category}}"
                            @if ($projects->category == 'Tier 1')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                            >
                          <input type="text" placeholder="UACS Code" class="form-control" name="tier1_uacs" value="{{$projects->tier1_uacs}}">
                          </div>
                          <br />
                          <input id="Tier2" type="radio" value="Tier 2" name="category" class="form-control-custom radio-custom form-check-input" onclick="tier2Radios()" required
                          @if ($projects->category == 'Tier 2')
                              checked
                          @endif
                          >
                          <label for="Tier2" data-toggle="tooltip" data-placement="top" title="New priority PAPs or scale-up of existing PAPs in terms of scope, timing, number or type of beneficiaries, design or implementation.  ">Tier 2 (New and Expanded) </label><br />
                          <div id="tier2s"
                          @if ($projects->category == 'Tier 2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                          >
                            <div class="i-checks">
                              &nbsp;&nbsp;&nbsp;<input id="Tier2a" type="radio" value="New" name="tier2_type" class="form-control-custom radio-custom form-check-input"
                              @if ($projects->tier2_type == 'New')
                                  checked
                              @endif
                              >
                              <label for="Tier2a" data-toggle="tooltip" data-placement="top" title="">Tier 2 - New </label>
                              <br />
                              &nbsp;&nbsp;&nbsp;<input id="Tier2b" type="radio" value="Expanded" name="tier2_type" class="form-control-custom radio-custom form-check-input"
                              @if ($projects->tier2_type == 'Expanded')
                                  checked
                              @endif
                              >
                              <label for="Tier2b" data-toggle="tooltip" data-placement="top" title="">Tier 2 - Expanded </label>
                              <div id="uacs2"
                              @if ($projects->tier2_type == 'Expanded')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                              >
                              <input type="text" placeholder="UACS Code" class="form-control" name="tier2_uacs" value="{{$projects->tier2_uacs}}">
                              </div>
                              <div class="invalid-feedback">
                                This is a required field.
                              </div>
                            </div>
                          </div>
                          <input id="Completed" type="radio" value="Completed" name="category" class="form-control-custom radio-custom form-check-input"required
                          @if ($projects->category == 'Completed')
                              checked
                          @endif
                          >
                          <label for="Completed" data-toggle="tooltip" data-placement="top">Completed </label><br />
                          <input id="Dropped" type="radio" value="Dropped" name="category" class="form-control-custom radio-custom form-check-input"required
                          @if ($projects->category == 'Dropped')
                              checked
                          @endif
                          >
                          <label for="Dropped" data-toggle="tooltip" data-placement="top">Dropped </label>
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                        </div>
                      </div> -->
                      <div class="col-sm-10">
                        <select name="implementationreadiness" class="form-control" onchange="java_script_:show6(this.options[this.selectedIndex].value)">
                          <option selected value="">Choose Status</option>
                          <option value="1"
                          @if ($projects->implementationreadiness == '1')
                            selected
                          @endif
                          >Ongoing</option>
                          <option id="interOption" value="2"
                          @if ($projects->implementationreadiness == '2')
                            selected
                          @endif
                          >Proposed</option>
                          <option value="3"
                          @if ($projects->implementationreadiness == '3')
                            selected
                          @endif
                          >Completed</option>
                        </select>
                      </div>
                    </div>


                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Updates <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="updates" required>{{ $projects->updates }}</textarea>
                      </div>
                      <!--
                      <label class="col-sm-2 form-control-label">Remarks from the Agency </label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="remarks"></textarea>
                      </div>
                      -->
                    </div>

                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" id="tier2cat"
                      @if ($projects->implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >Level of Readiness<br><small class="text-primary">Level 1 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving body but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With NEDA Board and/or ICC project approval but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small> <br/><small class="text-primary">Level 2 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label></small></small><small class="text-primary"><br>Level 3 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 4 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label></small></small></label>
                      <div class="col-sm-4" id="tier2cat2"
                      @if ($projects->implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >
                        <select name="status2" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier2statuses as $tier2statuse)
                          <option value="{{ $tier2statuse->id }}"
                            @if($projects->tier2_status == $tier2statuse->id)
                              selected
                            @endif
                            >{{ $tier2statuse->tier2_status }}</option>
                          @endforeach
                        </select>
                      </div>

                      <label class="col-sm-2 form-control-label" id="tier1cat"
                      @if ($projects->category == 'Tier 1')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >Tier 1 - Status<br><small class="text-primary">Ongoing implementation <label data-toggle="tooltip" data-placement="top" title="A program or project is considered ongoing as follows:a)  if locally funded - upon issuance of notice to proceed to the winning bidder;b) if ODA-assisted - upon effectivity of signed loan or grant agreement; and c) if for implementation through public-private partnership (PPP) or joint venture (JV) arrangement - upon signing of concession / JV agreement, which includes unsolicited PPP PAPs initiated by private sector proponent with signed concession agreement."><i class="fas fa-question-circle flag_tomato"></i></label></small><br/><small class="text-primary"> Level 1 <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving authority but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 2 <label data-toggle="tooltip" data-placement="top" title="For approval of appropriate approving body in 2018 and/or 2019"><i class="fas fa-question-circle flag_tomato"></i></label></small></label>
                      <div class="col-sm-4" id="tier1cat1"
                      @if ($projects->category == 'Tier 1')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >
                        <select name="status1" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier1statuses as $tier1statuse)
                          <option value="{{ $tier1statuse->id }}"
                            @if ($projects->tier1_type == $tier1statuse->id)
                              selected
                            @endif
                            >{{ $tier1statuse->tier1_status }}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">As of</label>
                      <div class="col-sm-4">
                        <input type="date" placeholder="" class="form-control" name="asof" required value="{{$projects->asof}}">
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script type="text/javascript">
                      $(document).ready(function () {
                      $('#Tier2').click(function () {
                              $('#tier2s').show('fast');
                              $('#tier2cat').show('fast');
                              $('#tier1cat').hide('fast');
                              $('#tier2cat2').show('fast');
                              $('#tier1cat1').hide('fast');
                              $('#uacs').hide('fast');
                          });
                      $('#Tier1').click(function () {
                              $('#tier2s').hide('fast');
                              $('#tier2cat').hide('fast');
                              $('#tier1cat').show('fast');
                              $('#tier2cat2').hide('fast');
                              $('#tier1cat1').show('fast');
                              $('#uacs').show('fast');
                          });
                      $('#Completed').click(function () {
                              $('#tier2s').hide('fast');
                              $('#tier2cat').hide('fast');
                              $('#tier1cat').hide('fast');
                              $('#tier2cat2').hide('fast');
                              $('#tier1cat1').hide('fast');
                              $('#uacs').hide('fast');
                          });
                      $('#Dropped').click(function () {
                              $('#tier2s').hide('fast');
                              $('#tier2cat').hide('fast');
                              $('#tier1cat').hide('fast');
                              $('#tier2cat2').hide('fast');
                              $('#tier1cat1').hide('fast');
                              $('#uacs').hide('fast');
                          });
                      $('#Tier2b').click(function () {
                              $('#uacs2').show('fast');
                          });
                      $('#Tier2a').click(function () {
                              $('#uacs2').hide('fast');
                          });
                      });
      </script>


      <section class="bg-primary" id="loa">

      </section>

      <script type="text/javascript">
        function iccCheckboxFunction() {
        var checkBox = document.getElementById("ICCNBcheckbox");
        var text = document.getElementById("icccheckboxes");
        if (checkBox.checked == true){
          text.style.display = "block";
        } else {
          text.style.display = "none";
        }
      }
      </script>



      <script type="text/javascript">
        function InfraCheckboxFunction1() {
          
          if (checkboxCustomSector1.checked == true){
            mothersector1.style.display = "block";
          } else {
            mothersector1.style.display = "none";
          }

          InfraSubSectorCheckboxes1();

        }

        function InfraCheckboxFunction2() {

        }

        function InfraCheckboxFunction3() {
          var infraSubSecChbxs3 = $('.infraSubSecChbxs3 :checkbox');
          if (checkboxCustomSector3.checked == true){
            mothersector3.style.display = "block";
          } else {
            mothersector3.style.display = "none";
          }

          InfraSubSectorCheckboxes3();
        }

        function InfraCheckboxFunction4() {
          var infraSubSecChbxs4 = $('.infraSubSecChbxs4 :checkbox');
          if (checkboxCustomSector4.checked == true){
            mothersector4.style.display = "block";
          } else {
            mothersector4.style.display = "none";
          }

          InfraSubSectorCheckboxes4();
        }

        function InfraCheckboxFunction5() {

        }

        function InfraCheckboxFunction6() {
          var infraSubSecChbxs6 = $('.infraSubSecChbxs6 :checkbox');
          if (checkboxCustomSector6.checked == true){
            mothersector6.style.display = "block";
          } else {
            mothersector6.style.display = "none";
          }

          InfraSubSectorCheckboxes6();
        }
      </script>

      <section class="bg-primary" id="pdp">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Philippine Development Plan (PDP) Chapter</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main PDP Chapter <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="mainpdp" class="form-control" onchange="java_script_:show5(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Chapter</option>
                          @foreach ($chapters as $chapter)
                          <option value="{{ $chapter->chap_no }}"
                            @if ($projects->mainpdp == $chapter->chap_no)
                              selected
                            @endif
                            >{{ $chapter->chap_description }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Other PDP Chapters <br><small class="text-primary">Select as many as applicable</small></label>
                      <div class="col-sm-10">
                        <select id="otherpdp"name="otherpdp[]" multiple="multiple">
                          @foreach ($chapters as $chapter)
                            <option value="{{$chapter->chap_no}}"
                              @foreach ($chapters_projects as $chapters_project)
                                @if($chapter->chap_no == $chapters_project->chap_id)
                                  selected
                                @endif
                              @endforeach
                              >{{$chapter->chap_description}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>
      <script type="text/javascript">
        var expanded = false;
        function show_proj_co_agency() {
          var checkboxes = document.getElementById("proj_co_agency");
          if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
          } else {
            checkboxes.style.display = "none";
            expanded = false;
          }
        }
        $("#proj_co_agency input").click(function () {
        $("#proj_co_agency_output").text($("#proj_co_agency input:checked").map(function () {
          return $(this).val();
        }).get().join());
      }).click().click();
      </script>

      <section class="bg-primary" id="rms">
      @for ($i = 5; $i <= 20; $i++)
        <div class="row" id="ResultsChapter{{ $i }}"
        @if ($i == $projects->mainpdp)
          style="display: block;"
        @else
          style="display: none;"
        @endif
        >
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>PDP Chapter {{ $i }} Outcome Statements/Outputs <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-10">
                        @foreach ($_1matrices as $_1matrice)
                          @if ($_1matrice->chapter == $i)
                          <div class="i-checks">
                          <input id="checkCustom1RM{{ $_1matrice->id }}" type="checkbox" value="{{ $_1matrice->id }}" name="_1rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"
                          @foreach ($_1matrices_projects as $_1matrices_project)
                              @if($_1matrices_project->_1rm_id == $_1matrice->id)
                                checked
                              @endif
                            @endforeach
                          ><label for="checkCustom1RM{{ $_1matrice->id }}">{{ $_1matrice->chapter_outcome }}</label>
                          </div>
                          @foreach ($_2matrices as $_2matrice)
                            @if ($_2matrice->chapter_outcome_id == $_1matrice->id)
                            <div class="i-checks">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom2RM{{ $_2matrice->id }}" type="checkbox" value="{{ $_2matrice->id }}" name="_2rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"
                            @foreach ($_2matrices_projects as $_2matrices_project)
                              @if($_2matrices_project->_2rm_id == $_2matrice->id)
                                checked
                              @endif
                            @endforeach
                            ><label for="checkCustom2RM{{ $_2matrice->id }}">{{ $_2matrice->intermediate_outcome }}</label>
                            </div>
                            @foreach ($_3matrices as $_3matrice)
                              @if ($_3matrice->intermediate_outcome_id == $_2matrice->id)
                              <div class="i-checks">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom3RM{{ $_3matrice->id }}" type="checkbox" value="{{ $_3matrice->id }}" name="_3rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"
                              @foreach ($_3matrices_projects as $_3matrices_project)
                                @if($_3matrices_project->_3rm_id == $_3matrice->id)
                                  checked
                                @endif
                              @endforeach
                              ><label for="checkCustom3RM{{ $_3matrice->id }}">{{ $_3matrice->intermediate_outcome }}</label>
                              </div>
                                @foreach ($_4matrices as $_4matrice)
                                @if ($_4matrice->intermediate_outcome_id == $_3matrice->id)
                                <div class="i-checks">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom4RM{{ $_4matrice->id }}" type="checkbox" value="{{ $_4matrice->id }}" name="_4rm{{$_1matrice->chapter}}[]" class="form-control-custom check-custom"
                                @foreach ($_4matrices_projects as $_4matrices_project)
                                  @if($_4matrices_project->_4rm_id == $_4matrice->id)
                                    checked
                                  @endif
                                @endforeach
                                ><label for="checkCustom4RM{{ $_4matrice->id }}">{{ $_4matrice->intermediate_outcome }}</label>
                                </div>
                                @endif
                                @endforeach
                              @endif
                            @endforeach
                            @endif
                          @endforeach
                          @endif
                        @endforeach
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>
      @endfor
      </section>

      <div class="form-group row">
        <div class="col-sm-12">
          <div class="i-checks">
            <input id="NA_rm" type="checkbox" value="1" class="form-control-custom" name="NA_rm"
            @if($projects->NA_rm == 1)
              checked
            @endif
            >
            <label for="NA_rm"><strong>No PDP Output Statement applicable</strong></label>
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label class="col-sm-2 form-control-label">Expected Outputs <label data-toggle="tooltip" data-placement="top" title="Expected outputs should directly contribute to identified RM outcome statement/output"><i class="fas fa-question-circle flag_tomato"></i></label> <br><small class="text-primary">Actual Deliverables, i.e. 100km of paved roads</small></label>
        <div class="col-sm-10">
          <textarea class="form-control" rows="5" id="output" name="output" required>{{ $projects->output }}</textarea>
        </div>
      </div>

      <section class="bg-primary" id="ag">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>0-10 Point Socioeconomic Agenda <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <div class="i-checks socoPoints">
                          @foreach ($agendas as $agenda)
                          <input id="checkCustomAgenda{{ $agenda->id }}" type="checkbox" value="{{ $agenda->id }}" name="agenda[]" class="form-control-custom checkbox-custom form-check-input" onchange="java_script_:SocioPointsCheckboxes()" required
                          @foreach ($agenda_projects as $agenda_project)
                                @if($agenda->id == $agenda_project->agenda_id)
                                  checked
                                @endif
                              @endforeach
                          ><label for="checkCustomAgenda{{ $agenda->id }}">{{ $agenda->agenda }}</label>
                          <br />
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="sdgs">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Sustainable Development Goals (SDG) <i class="fas fa-flag flag_tomato"></i> <br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <div class="i-checks susPoints">
                          @foreach ($goals as $goal)
                          <input id="checkCustomGoals{{ $goal->goal_id }}" type="checkbox" value="{{ $goal->goal_id }}" name="sdg[]" class="form-control-custom checkbox-custom form-check-input" onchange="java_script_:SusPointsCheckboxes()" required
                            @foreach ($sdg_projects as $sdg_project)
                                @if($goal->goal_id == $sdg_project->sdg_id)
                                  checked
                                @endif
                              @endforeach
                          ><label for="checkCustomGoals{{ $goal->goal_id }}">{{ $goal->goal_description }}</label>
                          <br />
                          @endforeach
                           <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <div class="row" style="display: none;" id="gad">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center"> 
                  <h4>Level of GAD Responsiveness <br><small class="text-primary">Based on the score of the program/ project using the GAD checklist accessible through this <a href="http://www.neda.gov.ph/wp-content/uploads/2015/10/DEVELOPMENT-PLANNING-CHECKLIST.pdf" target="_blank"><u>link</u></a>, kindly identify the GAD responsiveness of the program/project. </small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                          <div class="i-checks gadRbtn">
                          @foreach ($levels as $level)
                          <input id="radioCustomLevel{{ $level->level_id }}" type="radio" value="{{ $level->level_id }}" name="gender" class="form-control-custom radio-custom form-check-input"
                          @if ($projects->gender == $level->level_id)
                              checked
                            @endif
                          ><label for="radioCustomLevel{{ $level->level_id }}">{{ $level->level_description }}</label>
                          <br />
                          @endforeach
                          <div class="invalid-feedback">
                            This is a required field.
                          </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>



      <section class="bg-primary" id="ip">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementation Period <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Start of Project Implementation</label>
                      <div class="col-sm-2">
                        <input type="number" class="form-control" name="start" required value="{{ $projects->start }}">
                      </div>
                      <label class="col-sm-2 form-control-label">Year of Project Completion</label>
                      <div class="col-sm-2">
                        <input type="number" class="form-control" name="end" required value="{{ $projects->end }}">
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="ppd">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project Preparation Details <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Preparation Document </label>
                      <div class="col-sm-10">
                        <select id="projdocument" class="form-control" onchange="java_script_:show2(this.options[this.selectedIndex].value)" name="ppdetails">
                          <option disabled selected value="">Choose Project Document</option>
                          @foreach ($projectdocuments as $projectdocument)
                          <option value="{{ $projectdocument->id }}"
                            @if ($projects->ppdetails == $projectdocument->id)
                              selected
                            @endif
                            >{{ $projectdocument->projectdocument }}</option>
                          @endforeach
                        </select>
                      </div>
                      
                    </div>
                    <div class="invalid-feedback">
                        This is a required field.
                      </div>

                    <div class="form-group row" id="fs"
                    @if ($projects->ppdetails == 1)
                      style="display: block;"
                    @else
                      style="display: none;"
                    @endif
                    >
                      <div class="col-sm-12">
                        <input id="fsassistance" type="checkbox" value="1" class="form-control-custom" name="fsassistance"
                        @if ($projects->fsassistance == 1)
                          checked
                        @endif
                        >
                        <label for="fsassistance">Will require assistance for the conduct of Feasibility Study?</label>
                      </div>
                      <div class="col-sm-12">
                        <label class="col-sm-2 form-control-label">Status </label>
                        <div class="col-sm-10">
                          <div class="i-checks">
                            @foreach ($fsstatuses as $fsstatus)
                            <div class="i-checks">
                            <input id="radioCustomFS{{ $fsstatus->id }}" type="radio" value="{{ $fsstatus->id }}" name="fsstatus" class="form-control-custom radio-custom"
                            @if ($projects->fsstatus == $fsstatus->id)
                              checked
                            @endif
                            ><label for="radioCustomFS{{ $fsstatus->id }}">{{ $fsstatus->fs_status }}</label>
                            </div>
                            @if ($fsstatus->id == '2')
                            <div class="col-sm-6" id="fsongoing_expected_date"
                            @if ($projects->fsstatus == 2)
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                            >
                            <div class="i-checks">
                              Expected completion date: <input type="date" name="fsstatus_ongoing" class="form-control" value="{{$projects->fsstatus_ongoing}}">
                            </div>
                            </div>
                            @endif
                            @if ($fsstatus->id == '3')
                            <div class="col-sm-6" id="fsongoing_forprep_date"
                            @if ($projects->fsstatus == 3)
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                            >
                            <div class="i-checks">
                              Start date: <input type="date" name="fsstatus_prep" class="form-control" value="{{$projects->fsstatus_prep}}">
                            </div>
                            </div>
                            @endif
                            @endforeach
                          </div>
                        </div>
                      </div>
                        <script type="text/javascript">
                        $(document).ready(function () {
                          $('#radioCustomFS1').click(function () {
                              $('#fsongoing_expected_date').hide('fast');
                              $('#fsongoing_forprep_date').hide('fast');
                          });
                          $('#radioCustomFS2').click(function () {
                              $('#fsongoing_expected_date').show('fast');
                              $('#fsongoing_forprep_date').hide('fast');
                          });
                          $('#radioCustomFS3').click(function () {
                              $('#fsongoing_expected_date').hide('fast');
                              $('#fsongoing_forprep_date').show('fast');
                          });
                        });
                        </script>

                      <label class="col-sm-12 form-control-label">Schedule of F/S Cost (In Exact Amount in PhP) </label>
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2017"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2017)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2018"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2018)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2019"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2019)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2020"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2020)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2021"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2021)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="fs_2022"
                                        @foreach ($fscost_projects as $fscost_project)
                                          @if($fscost_project->fsyear == 2022)
                                            value="{{$fscost_project->fscost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      
                    </div>
                    <div class="form-group row" id="others" style="display: none;">
                      <label class="col-sm-2 form-control-label">Others</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="ppdetails_others" value="{{$projects->ppdetails_others}}">
                      </div>                    
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function show2(projdocument) {
          if (projdocument == "1") {
            fs.style.display='block';
            others.style.display='none';
          } else if (projdocument == "5") {
            fs.style.display='none';
            others.style.display='flex';
          } else {
            fs.style.display='none';
            others.style.display='none';
          }
        }
      </script>

      <section class="bg-primary" id="pcc">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Pre-construction Costs</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa1Rbtn">
                            <input id="ROWA1" type="radio" value="1" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox()" name="rowa"
                            @if ($projects->rowa == 1)
                              checked
                            @endif
                            >
                            <label for="ROWA1">With ROWA Component?</label>&nbsp;&nbsp;&nbsp;
                            <input id="ROWA1_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox()" name="rowa"
                            @if ($projects->rowa == 0)
                              checked
                            @endif
                            >
                            <label for="ROWA1_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement"
                    @if ($projects->rowa == 1)
                      style="display: block;"
                    @else
                      style="display: none;"
                    @endif
                    >
                      <label class="col-sm-12 form-control-label">Schedule of ROWA Cost (In Exact Amount in PhP) </label>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2017"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2017)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2018"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2018)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2019"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2019)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2020"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2020)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2021"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2021)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rowa_2022"
                                        @foreach ($rowa_projects as $rowa_project)
                                          @if($rowa_project->rowayear == 2022)
                                            value="{{$rowa_project->rowacost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="number" class="form-control" name="rowa_affected" value="{{$projects->rowa_affected}}">
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox() {
                        var checkBox = document.getElementById("ROWA1");
                        var text = document.getElementById("rowarequirement");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa2Rbtn">
                            <input id="ROWA2" type="radio" value="1" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox2()" name="rc"
                            @if ($projects->rc == 1)
                              checked
                            @endif
                            >
                            <label for="ROWA2">With Resettlement Component?</label>&nbsp;&nbsp;&nbsp;
                            <input id="ROWA2_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" onclick="ROWAComponentCheckbox2()" name="rc"
                            @if ($projects->rc == 0)
                              checked
                            @endif
                            >
                            <label for="ROWA2_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement2"
                    @if ($projects->rc == 1)
                      style="display: block;"
                    @else
                      style="display: none;"
                    @endif
                    >
                    
                    <label class="col-sm-12 form-control-label">Schedule of Resettlement Cost (In Exact Amount in PhP) </label>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2017"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2017)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2018"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2018)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2019"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2019)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2020"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2020)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2021"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2021)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                        <input type="number" placeholder="" class="form-control" name="rc_2022"
                                        @foreach ($rc_projects as $rc_project)
                                          @if($rc_project->rcyear == 2022)
                                            value="{{$rc_project->rccost}}"
                                          @endif
                                        @endforeach
                                        >
                                      </td>
                                      <td>
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="text" class="form-control" name="rc_affected" value="{{$projects->rc_affected}}">
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox2() {
                        var checkBox = document.getElementById("ROWA2");
                        var text = document.getElementById("rowarequirement2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks rowa3Rbtn">
                            <input id="ROWA3" type="radio" value="1" class="form-control-custom radio-custom form-check-input" name="wrrc"
                            @if ($projects->wrrc == 1)
                              checked
                            @endif
                            >
                            <label for="ROWA3">With ROWA and Resettlement Action Plan?</label>
                            <input id="ROWA3_No" type="radio" value="0" class="form-control-custom radio-custom form-check-input" name="wrrc"
                            @if ($projects->wrrc == 0)
                              checked
                            @endif
                            >
                            <label for="ROWA3_No">Not Applicable</label>
                            <div class="invalid-feedback">
                              This is a required field.
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>


      <section class="bg-primary" id="eg">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Employment Generation<br><small class="text-primary">Please indicate the no. of persons to be employed by the project outside of the implementing agency</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of persons to be employed:</label>
                      <div class="col-sm-4">
                        <input type="number" class="form-control" name="employment" value="{{ $projects->employment }}">
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="fss">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Funding Source and Mode of Implementation <i class="fas fa-flag flag_tomato"></i></h4>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main Funding Source</label>
                      <div class="col-sm-4">
                        <select id="mainfsource" name="mainfsource" class="form-control" onchange="java_script_:show3(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Main Funding Source</option>
                          @foreach ($fundingsources as $fundingsource)
                          <option value="{{ $fundingsource->fsource_no }}"
                              @if( $fundingsource->fsource_no == $projects->mainfsource)
                                  selected
                              @endif
                            >{{ $fundingsource->fsource_description }}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Mode of Implementation/ <br> Procurement</label>
                      <div class="col-sm-4">
                        <select id="mode" name="modeofimplementation" class="form-control" onchange="java_script_:show4(this.options[this.selectedIndex].value)" required>
                          <option disabled selected value="">Choose Mode of Implementation</option>
                          @foreach ($modes as $mode)
                          <option value="{{ $mode->mode_no }}"
                              @if( $mode->mode_no == $projects->modeofimplementation)
                                  selected
                              @endif
                            >{{ $mode->mode_description }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group row">


                      <label class="col-sm-2 form-control-label" id="oda2"
                      @if($projects->mainfsource == 2 || $projects->mainfsource == 3)
                         style="display: auto;"
                      @else
                        style="display: none;"
                      @endif

                      >ODA Funding Institutions</label>
                      <div class="col-sm-4" id="oda"
                      @if($projects->mainfsource == 2 || $projects->mainfsource == 3)
                         style="height: 12em; width: 40em; display: auto;"
                      @else
                        style="height: 12em; width: 40em; display: none;"
                      @endif
                      >
                    
                        @foreach ($fundings as $funding)
                          <div class="i-checks">
                          <input id="checkFS{{ $funding->fsource_no }}" type="checkbox" value="{{ $funding->fsource_no }}" name="oda_funding[]" class="form-control-custom check-custom"

                          @foreach($f_projects as $f_project)
                            @if($f_project->fsource_no == $funding->fsource_no)
                              checked
                            @endif
                          @endforeach

                          ><label for="checkFS{{ $funding->fsource_no }}">{{ $funding->fsource_description }}</label>
                          </div>
                          @endforeach
                      </div>


                      <label class="col-sm-2 form-control-label" id="others_fs1"
                      @if($projects->mainfsource == 6)
                         style="display: auto;"
                      @else
                        style="display: none;"
                      @endif
                      >Other Funding Source</label>
                      <div class="col-sm-4" id="others_fs2"
                      @if($projects->mainfsource == 6)
                         style="display: auto;"
                      @else
                        style="display: none;"
                      @endif
                      >
                        <input type="text" class="form-control" name="other_fs" value="{{ $projects->other_fs }}">
                      </div>

                      <label class="col-sm-2 form-control-label" id="others_mode2"
                      @if($projects->modeofimplementation == 78)
                         style="display: auto;"
                      @else
                        style="display: none;"
                      @endif
                      >Other Mode</label>
                      <div class="col-sm-4" id="others_mode"
                      @if($projects->modeofimplementation == 78)
                         style="display: auto;"
                      @else
                        style="display: none;"
                      @endif
                      >
                        <input type="text" class="form-control" name="other_mode" value="{{ $projects->other_mode }}">
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function FundingInst(){
          var checkBox = document.getElementById("checkCustomFS2");
          var checkBox2 = document.getElementById("checkCustomFS3");
          var oda = document.getElementById("oda");
          if (checkBox.checked == true && checkBox2.checked == false){
            oda.style.display='block';
            oda2.style.display='block';
          }else if(checkBox.checked == false && checkBox2.checked == true){
            oda.style.display='block';
            oda2.style.display='block';
          }else if(checkBox.checked == true && checkBox2.checked == true){
            oda.style.display='block';
            oda2.style.display='block';
          }else{
            oda.style.display='none';
            oda2.style.display='none';
          }
        }

        function FundingInst_Others(){
          var checkBox = document.getElementById("checkCustomFS6");
          if (checkBox.checked == true){
            others_fs1.style.display='block';
            others_fs2.style.display='block';
          }else{
            others_fs1.style.display='none';
            others_fs2.style.display='none';
          }
        }

        function show3(mainfsource) {
          if (mainfsource == "2") {
            oda.style.display='block';
            oda2.style.display='block';
            others_fs1.style.display='none';
            others_fs2.style.display='none';
          } else if (mainfsource == "3") {
            oda.style.display='block';
            oda2.style.display='block';
            others_fs1.style.display='none';
            others_fs2.style.display='none';
          } else if (mainfsource == "6") {
            others_fs1.style.display='block';
            others_fs2.style.display='block';
          } else {
            oda.style.display='none';
            oda2.style.display='none';
          }
        }

      function show4(mode) {
          if (mode == "78") {
            others_mode.style.display='block';
            others_mode2.style.display='block';
          } else {
            others_mode.style.display='none';
            others_mode2.style.display='none';
          }
        }

      function show6(implementationreadiness) {
          if (implementationreadiness == "2") {
            tier2cat.style.display='block';
            tier2cat2.style.display='block';
          } else {
            tier2cat.style.display='none';
            tier2cat2.style.display='none';
          }
        }

      function show7(SS_implementationreadiness) {
          if (SS_implementationreadiness == "2") {
            tier2cat_v.style.display='block';
            tier2cat2_v.style.display='block';
          } else {
            tier2cat_v.style.display='none';
            tier2cat2_v.style.display='none';
          }
        }

      function show8(NRO_implementationreadiness) {
          if (NRO_implementationreadiness == "2") {
            tier2cat_nro.style.display='block';
            tier2cat2_nro.style.display='block';
          } else {
            tier2cat_nro.style.display='none';
            tier2cat2_nro.style.display='none';
          }
        }

      function show10(PIS_implementationreadiness) {
          if (PIS_implementationreadiness == "2") {
            tier2cat_pis.style.display='block';
            tier2cat2_pis.style.display='block';
          } else {
            tier2cat_pis.style.display='none';
            tier2cat2_pis.style.display='none';
          }
        }
      </script>

      <section class="bg-primary" id="it">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project Cost <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                <br>
                <div class="row" align="center">
                
                  <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Financing Source</th>
                                    <th>2021 and Prior</th>
                                    <th>2022</th>
                                    <th>2023</th>
                                    <th>2024</th>
                                    <th>Continuing Years</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2021" name="it_2021_nglocal"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2022" name="it_2022_nglocal"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2023" name="it_2023_nglocal"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal2 it_2024" name="it_2024_nglocal"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number nglocal1 it_2025" name="it_2025_nglocal"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                      @foreach ($it_projects2 as $it_project)
                                        @if($it_project->year == 2024)
                                          <input type="hidden" name="checker_if_new" value="1">
                                        @else
                                          <input type="hidden" name="checker_if_new" value="0">
                                        @endif
                                      @endforeach
                                    </td>
                                    <td>
                                       <span class="it-total_all" id="nglocal_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2021" name="it_2021_ngloan"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->loan}}"
                                          @endif
                                        @endforeach
                                      > 
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2022" name="it_2022_ngloan"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2023" name="it_2023_ngloan"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->loan}}"
                                          @endif
                                        @endforeach
                                      > 
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan2 it_2024" name="it_2024_ngloan"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ngloan1 it_2025" name="it_2025_ngloan"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ngloan_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2021" name="it_2021_odagrant"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2022" name="it_2022_odagrant"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2023" name="it_2023_odagrant"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant2 it_2024" name="it_2024_odagrant"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number odagrant1 it_2025" name="it_2025_odagrant"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                     <span class="it-total_all" id="odagrant_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2021" name="it_2021_gocc"
                                       @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2022" name="it_2022_gocc"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2023" name="it_2023_gocc"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc2 it_2024" name="it_2024_gocc"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number gocc1 it_2025" name="it_2025_gocc"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="gocc_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2021" name="it_2021_lgu"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2022" name="it_2022_lgu"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2023" name="it_2023_lgu"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu2 it_2024" name="it_2024_lgu"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number lgu1 it_2025" name="it_2025_lgu"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="lgu_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2021" name="it_2021_ps"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2022" name="it_2022_ps"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2023" name="it_2023_ps"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps2 it_2024" name="it_2024_ps"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number ps1 it_2025" name="it_2025_ps"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="ps_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2021" name="it_2021_others"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2021)
                                            value="{{$it_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2022" name="it_2022_others"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2022)
                                            value="{{$it_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2023" name="it_2023_others"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2023)
                                            value="{{$it_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others2 it_2024" name="it_2024_others"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2024)
                                            value="{{$it_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control it-number others1 it_2025" name="it_2025_others"
                                      @foreach ($it_projects as $it_project)
                                          @if($it_project->year == 2025)
                                            value="{{$it_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="it-total_all" id="others_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="it_2021_total">0</span></th>
                                    <th><span id="it_2022_total">0</span></th>
                                    <th><span id="it_2023_total">0</span></th>
                                    <th><span id="it_2024_total">0</span></th>
                                    <th><span id="it_2025_total">0</span></th>
                                    <th><span id="it_total_all">0</span></th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

               <!--  <div class="col-lg-4">
                <button type="button" data-toggle="modal" data-target="#rb" class="btn btn-primary">Regional Breakdown</button> 
                </div> -->
                </div>
                <br>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="fa"
<?php
          if(auth()->user()->user_type == 'NRO' || auth()->user()->user_type == 'SS')
            echo "style='pointer-events:none;'";
        ?>
      >
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Financial Accomplishments <i class="fas fa-flag flag_tomato"></i><br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                <div class="card-body">
<!--                   <div class="form-group row">r
                        <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="NA_fs" type="checkbox" value="1" class="form-control-custom" name="NA_fs" 
                            @if ($projects->NA_fs == 1)
                              checked
                            @endif
                            >
                            <label for="NA_fs">Not Applicable (For PAPs not for funding in the GAA)</label>
                          </div>
                        </div>
                    </div> -->
                  <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Year</th>
                                    <th>Amount included in the NEP</th>
                                    <th>Amount Allocated in the Budget/GAA</th>
                                    <th>Actual Amount Disbursed</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      2017
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2017"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2017)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2017"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2017)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2017"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2017)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2018
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2018"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2018)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2018"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2018)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2018"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2018)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2019
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2019"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2019)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2019"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2019)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2019"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2019)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2020
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2020"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2020)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2020"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2020)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2020"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2020)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2021
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2021"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2021)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2021"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2021)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2021"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2021)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2022
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number nep" name="nep_2022"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2022)
                                            value="{{$fa_project->facost_nep}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number all" name="all_2022"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2022)
                                            value="{{$fa_project->facost_all}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control number ad" name="ad_2022"
                                      @foreach ($fa_projects as $fa_project)
                                          @if($fa_project->fayear == 2022)
                                            value="{{$fa_project->facost_ad}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Total
                                    </td>
                                    <td>
                                      <span id="nep_total">0</span>
                                    </td>
                                    <td>
                                     <span id="all_total">0</span>
                                    </td>
                                    <td>
                                      <span id="ad_total">0</span>
                                    </td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>
@if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "OS" || auth()->user()->user_type == "NRO" || auth()->user()->user_type == "AG" || auth()->user()->user_type == "PIS")
      <section class="bg-primary" id="val"
      <?php
          if(auth()->user()->user_type == 'NRO')
            echo "style='pointer-events:auto;'";
        ?>
      >
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>
                    @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "OS")
                      Main PIP Chapter Focal-Validated Status
                    @elseif(auth()->user()->user_type == "NRO")
                      NRO PIP Focal-Validated Status
                    @elseif(auth()->user()->user_type == "PIS")
                      PIS Validated Status  
                    @else
                    @endif
                  </h4>
                </div>
                <div class="card-body">
                      @if(auth()->user()->user_type == 'SS' || auth()->user()->user_type == "OS")
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Status of Implementation Readiness <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="SS_implementationreadiness" class="form-control" required onchange="java_script_:show7(this.options[this.selectedIndex].value)">
                          <option selected value="">Choose Status</option>
                          <option value="1"
                          @if ($projects->SS_implementationreadiness == '1')
                            selected
                          @endif
                          >Ongoing</option>
                          <option id="interOption" value="2"
                          @if ($projects->SS_implementationreadiness == '2')
                            selected
                          @endif
                          >Proposed</option>
                          <option value="3"
                          @if ($projects->SS_implementationreadiness == '3')
                            selected
                          @endif
                          >Completed</option>
                        </select>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Financial and Physical Status Updates <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="SS_updates" required>{{ $projects->SS_updates }}</textarea>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Remarks from Main PIP Chapter Focal</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="SS_remarks">{{ $projects->SS_remarks }}</textarea>
                      </div>

                      <!-- <label class="col-sm-2 form-control-label">If Uncategorized</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="uncat" required></textarea>
                      </div> -->
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label" id="tier2cat_v"
                      @if ($projects->SS_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >Level of Readiness<br><small class="text-primary">Level 1 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving body but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With NEDA Board and/or ICC project approval but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small> <br/><small class="text-primary">Level 2 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label></small></small><small class="text-primary"><br>Level 3 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 4 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label></small></small></label>
                      <div class="col-sm-4" id="tier2cat2_v"
                      @if ($projects->SS_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >
                        <select name="SS_status2" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier2statuses as $tier2statuse)
                          <option value="{{ $tier2statuse->id }}"
                            @if($projects->SS_status2 == $tier2statuse->id)
                              selected
                            @endif
                            >{{ $tier2statuse->tier2_status }}</option>
                          @endforeach
                        </select>
                      </div>
                      @if(auth()->user()->user_type == 'OS')

                      @else
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <b>NRO PIP Focal-Validated Status</b>
                        </div>
                        <div class="col-sm-12">
                          Status of Implementation Readiness:
                          @if ($projects->NRO_implementationreadiness == '1')
                            Ongoing
                          @elseif($projects->NRO_implementationreadiness == '2')
                            Proposed
                          @elseif($projects->NRO_implementationreadiness == '3')
                            Completed
                          @endif
                        </div>
                        <div class="col-sm-12">
                          Financial and Physical Status Updates:
                          {{ $projects->NRO_updates }}
                        </div>
                        <div class="col-sm-12">
                          Remarks from NRO PIP Focal:
                          {{ $projects->NRO_remarks }}
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-12">
                          <b>PIS-Validated Status</b>
                        </div>
                        <div class="col-sm-12">
                          Status of Implementation Readiness:
                          @if ($projects->PIS_implementationreadiness == '1')
                            Ongoing
                          @elseif($projects->PIS_implementationreadiness == '2')
                            Proposed
                          @elseif($projects->PIS_implementationreadiness == '3')
                            Completed
                          @endif
                        </div>
                        <div class="col-sm-12">
                          Financial and Physical Status Updates:
                          {{ $projects->PIS_updates }}
                        </div>
                        <div class="col-sm-12">
                          Remarks from NRO PIP Focal:
                          {{ $projects->PIS_remarks }}
                        </div>
                      </div>
                      @endif
                      

                     
                      @endif
                      @if(auth()->user()->user_type == 'NRO')
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Status of Implementation Readiness <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="NRO_implementationreadiness" class="form-control" required onchange="java_script_:show8(this.options[this.selectedIndex].value)">
                          <option selected value="">Choose Status</option>
                          <option value="1"
                          @if ($projects->NRO_implementationreadiness == '1')
                            selected
                          @endif
                          >Ongoing</option>
                          <option id="interOption" value="2"
                          @if ($projects->NRO_implementationreadiness == '2')
                            selected
                          @endif
                          >Proposed</option>
                          <option value="3"
                          @if ($projects->NRO_implementationreadiness == '3')
                            selected
                          @endif
                          >Completed</option>
                        </select>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Financial and Physical Status Updates <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="NRO_updates" required>{{ $projects->NRO_updates }}</textarea>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Remarks from NRO PIP Focal</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="NRO_remarks">{{ $projects->NRO_remarks }}</textarea>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label" id="tier2cat_nro"
                      @if ($projects->NRO_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >Level of Readiness<br><small class="text-primary">Level 1 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving body but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With NEDA Board and/or ICC project approval but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small> <br/><small class="text-primary">Level 2 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label></small></small><small class="text-primary"><br>Level 3 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 4 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label></small></small></label>
                      <div class="col-sm-4" id="tier2cat2_nro"
                      @if ($projects->NRO_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >
                        <select name="NRO_status2" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier2statuses as $tier2statuse)
                          <option value="{{ $tier2statuse->id }}"
                            @if($projects->NRO_status2 == $tier2statuse->id)
                              selected
                            @endif
                            >{{ $tier2statuse->tier2_status }}</option>
                          @endforeach
                        </select>
                      </div>
                  @endif
                  @if(auth()->user()->user_type == 'PIS')
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Status of Implementation Readiness <i class="fas fa-flag flag_tomato"></i></label>
                      <div class="col-sm-10">
                        <select name="PIS_implementationreadiness" class="form-control" required onchange="java_script_:show10(this.options[this.selectedIndex].value)">
                          <option selected value="">Choose Status</option>
                          <option value="1"
                          @if ($projects->PIS_implementationreadiness == '1')
                            selected
                          @endif
                          >Ongoing</option>
                          <option id="interOption" value="2"
                          @if ($projects->PIS_implementationreadiness == '2')
                            selected
                          @endif
                          >Proposed</option>
                          <option value="3"
                          @if ($projects->PIS_implementationreadiness == '3')
                            selected
                          @endif
                          >Completed</option>
                        </select>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Financial and Physical Status Updates <i class="fas fa-flag flag_tomato" required></i></label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="PIS_updates" required>{{ $projects->PIS_updates }}</textarea>
                      </div>
                      </div>
                       <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Remarks from PIS</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="PIS_remarks">{{ $projects->PIS_remarks }}</textarea>
                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label" id="tier2cat_pis"
                      @if ($projects->PIS_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >Level of Readiness<br><small class="text-primary">Level 1 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With approval of appropriate approving body but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With NEDA Board and/or ICC project approval but not yet ongoing."><i class="fas fa-question-circle flag_tomato"></i></label></small> <br/><small class="text-primary">Level 2 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document completed, for ICC processing in 2018 and/or 2019,  included in the NEP for FY 2019 or for inclusion in NEP for FY 2020."><i class="fas fa-question-circle flag_tomato"></i></label></small></small><small class="text-primary"><br>Level 3 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document currently being prepared and to be completed in 2019, for ICC processing in 2020 and/or for inclusion in the NEP for FY 2021."><i class="fas fa-question-circle flag_tomato"></i></label></small><small class="text-primary"><br>Level 4 (Non-CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label> (CIP) <label data-toggle="tooltip" data-placement="top" title="With project preparation document for completion in 2020, for ICC processing in 2021 and/or for inclusion in the NEP for FY 2022."><i class="fas fa-question-circle flag_tomato"></i></label></small></small></label>
                      <div class="col-sm-4" id="tier2cat2_pis"
                      @if ($projects->PIS_implementationreadiness == '2')
                              style="display: block;"
                            @else
                              style="display: none;"
                            @endif
                      >
                        <select name="PIS_status2" class="form-control">
                          <option disabled selected>Choose Level</option>
                          @foreach ($tier2statuses as $tier2statuse)
                          <option value="{{ $tier2statuse->id }}"
                            @if($projects->PIS_status2 == $tier2statuse->id)
                              selected
                            @endif
                            >{{ $tier2statuse->tier2_status }}</option>
                          @endforeach
                        </select>
                      </div>
                  @endif
                  
                  @if(auth()->user()->user_type != 'AG')
                  <div class="form-group row">
                      <div class="col-sm-12">
                        <div class="i-checks">
                          <input id="NFI" type="checkbox" value="1" class="form-control-custom" name="NFI"
                          @if(auth()->user()->user_type == 'NRO')
                            @if ($projects->NRO_no == 1)
                              checked
                            @endif
                          @endif

                          @if(auth()->user()->user_type == 'SS')
                            @if ($projects->SS_no == 1)
                              checked
                            @endif
                          @endif

                          @if(auth()->user()->user_type == 'PIS')
                            @if ($projects->PIS_no == 1)
                              checked
                            @endif
                          @endif
                          >
                          <label for="NFI"><strong>No further inputs from NEDA</strong></label>
                        </div>
                      </div>
                  </div>
                  @endif

                </div>
              </div>
        </div>
      </div>
      </section>
      <!-- <script type="text/javascript">
                      $(document).ready(function () {
                      $('#Tier2_v').click(function () {
                              $('#tier2s_v').show('fast');
                              $('#tier2cat_v').show('fast');
                              $('#tier1cat_v').hide('fast');
                              $('#tier2cat2_v').show('fast');
                              $('#tier1cat1_v').hide('fast');
                              $('#uacs_v').hide('fast');
                          });
                      $('#Tier1_v').click(function () {
                              $('#tier2s_v').hide('fast');
                              $('#tier2cat_v').hide('fast');
                              $('#tier1cat_v').show('fast');
                              $('#tier2cat2_v').hide('fast');
                              $('#tier1cat1_v').show('fast');
                              $('#uacs_v').show('fast');
                          });
                      $('#Completed_v').click(function () {
                              $('#tier2s_v').hide('fast');
                              $('#tier2cat_v').hide('fast');
                              $('#tier1cat_v').hide('fast');
                              $('#tier2cat2_v').hide('fast');
                              $('#tier1cat1_v').hide('fast');
                              $('#uacs_v').hide('fast');
                          });
                      $('#Dropped_v').click(function () {
                              $('#tier2s_v').hide('fast');
                              $('#tier2cat_v').hide('fast');
                              $('#tier1cat_v').hide('fast');
                              $('#tier2cat2_v').hide('fast');
                              $('#tier1cat1_v').hide('fast');
                              $('#uacs_v').hide('fast');
                          });
                      $('#Tier2b_v').click(function () {
                              $('#uacs2_v').show('fast');
                          });
                      $('#Tier2a_v').click(function () {
                              $('#uacs2_v').hide('fast');
                          });
                      });
      </script> -->
@endif

@if(auth()->user()->user_type == "AG")
      <div class="row" id=menu>
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <button type="submit" class="btn btn-secondary btn-sm" name="statusofsubmission" value="Draft" onclick="SaveAsDraft();" formnovalidate="formnovalidate" >&nbsp;&nbsp;&nbsp;&nbsp;Save as Draft&nbsp;&nbsp;&nbsp;&nbsp;</button>
                        <button type="submit" class="btn btn-primary btn-sm" name="statusofsubmission" value="Endorsed" onclick="SaveAsFinal();">Save as Endorsed</button>
                        <button type="button" class="btn btn-secondary btn-sm"><a class="js-scroll-trigger" href="#forms" style="color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Back on Top&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></button>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
@elseif(auth()->user()->user_type == "NRO")
      <div class="row" id=menu
        <?php
          if(auth()->user()->user_type == 'NRO')
            echo "style='pointer-events:auto;'";
        ?>
      >
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <input type="hidden" name="statusofsubmission" value="{{$projects->statusofsubmission}}">
                        <button type="submit" class="btn btn-secondary btn-sm" name="NRO_statusofsubmission" value="Reviewed">Save as Reviewed</button>
                        <button type="submit" class="btn btn-primary btn-sm" name="NRO_statusofsubmission" value="Validated">Save as Validated</button>
                        <button type="button" class="btn btn-secondary btn-sm"><a class="js-scroll-trigger" href="#forms" style="color:white;">Back on Top</a></button>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
@elseif(auth()->user()->user_type == "SS")
      <div class="row" id=menu
        <?php
          if(auth()->user()->user_type == 'NRO')
            echo "style='pointer-events:auto;'";
        ?>
      >
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <input type="hidden" name="statusofsubmission" value="{{$projects->statusofsubmission}}">
                        <button type="submit" class="btn btn-secondary btn-sm" name="SS_statusofsubmission" value="Reviewed" onclick="SaveAsReviewed();" formnovalidate="formnovalidate">Save as Reviewed</button>
                        <button type="submit" class="btn btn-primary btn-sm" name="SS_statusofsubmission" value="Validated" onclick="SaveAsValidated();">Save as Validated</button>
                        <button type="button" data-toggle="modal" data-target="#uncat" class="btn btn-secondary btn-sm">Save as Uncategorized</button> 
                        <button type="button" class="btn btn-primary btn-sm"><a class="js-scroll-trigger" href="#forms" style="color:white;">Back on Top</a></button>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
@elseif(auth()->user()->user_type == "PIS")
      <div class="row" id=menu
        <?php
          if(auth()->user()->user_type == 'NRO')
            echo "style='pointer-events:auto;'";
        ?>
      >
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-4 offset-sm-2">
                        <input type="hidden" name="statusofsubmission" value="{{$projects->statusofsubmission}}">
                        <button type="submit" class="btn btn-secondary btn-sm" name="PIS_statusofsubmission" value="Reviewed" onclick="SaveAsReviewed();">Save as Reviewed</button>
                        <button type="submit" class="btn btn-primary btn-sm" name="PIS_statusofsubmission" value="Validated" onclick="SaveAsValidated();">Save as Validated</button>
                        <button type="button" data-toggle="modal" data-target="#uncat" class="btn btn-secondary btn-sm">Save as Uncategorized</button> 
                        <button type="button" class="btn btn-primary btn-sm"><a class="js-scroll-trigger" href="#forms" style="color:white;">Back on Top</a></button>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
@endif
    </div>
  </section>

  <div id="ic" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
      <div role="document" class="modal-dialog modal-lg"  style="max-width: 100%;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-ticle">Infrastructure Cost</h5>
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
          </div>
        <div class="modal-body">
              <div class="row">
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Financing Source</th>
                                      <th>2016 and Prior</th>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>2017-2022</th>
                                      <th>Continuing Years</th>
                                      <th>Overall Total</th>
                                    </tr>
                                  </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal1 ic_2016" name="ic_2016_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2017" name="ic_2017_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2018" name="ic_2018_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2019" name="ic_2019_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2020" name="ic_2020_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2021" name="ic_2021_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal2 ic_2022" name="ic_2022_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-nglocal2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-nglocal1 ic_2023" name="ic_2023_nglocal"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->local}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                       <span class="ic-total_all" id="ic-nglocal_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan1 ic_2016" name="ic_2016_ngloan"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      > 
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2017" name="ic_2017_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2018" name="ic_2018_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      > 
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2019" name="ic_2019_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2020" name="ic_2020_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2021" name="ic_2021_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan2 ic_2022" name="ic_2022_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-ngloan2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ngloan1 ic_2023" name="ic_2023_ngloan"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->loan}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-ngloan_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant1 ic_2016" name="ic_2016_odagrant"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2017" name="ic_2017_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2018" name="ic_2018_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2019" name="ic_2019_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2020" name="ic_2020_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2021" name="ic_2021_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant2 ic_2022" name="ic_2022_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-odagrant2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-odagrant1 ic_2023" name="ic_2023_odagrant"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->grant}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                     <span class="ic-total_all" id="ic-odagrant_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc1 ic_2016" name="ic_2016_gocc"
                                       @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2017" name="ic_2017_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2018" name="ic_2018_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2019" name="ic_2019_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2020" name="ic_2020_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2021" name="ic_2021_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc2 ic_2022" name="ic_2022_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-gocc2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-gocc1 ic_2023" name="ic_2023_gocc"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->gocc}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-gocc_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu1 ic_2016" name="ic_2016_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2017" name="ic_2017_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2018" name="ic_2018_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2019" name="ic_2019_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2020" name="ic_2020_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2021" name="ic_2021_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu2 ic_2022" name="ic_2022_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-lgu2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-lgu1 ic_2023" name="ic_2023_lgu"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->lgu}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-lgu_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps1 ic_2016" name="ic_2016_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2017" name="ic_2017_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2018" name="ic_2018_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2019" name="ic_2019_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2020" name="ic_2020_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2021" name="ic_2021_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps2 ic_2022" name="ic_2022_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-ps2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-ps1 ic_2023" name="ic_2023_ps"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->private}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-ps_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others1 ic_2016" name="ic_2016_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2016)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2017" name="ic_2017_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2017)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2018" name="ic_2018_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2018)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2019" name="ic_2019_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2019)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2020" name="ic_2020_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2020)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2021" name="ic_2021_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2021)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others2 ic_2022" name="ic_2022_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2022)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total2" id="ic-others2_total">0</span>
                                    </td>
                                    <td>
                                      <input type="text" placeholder="" class="form-control ic-number ic-others1 ic_2023" name="ic_2023_others"
                                      @foreach ($ic_projects as $ic_project)
                                          @if($ic_project->year == 2023)
                                            value="{{$ic_project->others}}"
                                          @endif
                                        @endforeach
                                      >
                                    </td>
                                    <td>
                                      <span class="ic-total_all" id="ic-others_total_all">0</span>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th><span id="ic_2016_total">0</span></th>
                                    <th><span id="ic_2017_total">0</span></th>
                                    <th><span id="ic_2018_total">0</span></th>
                                    <th><span id="ic_2019_total">0</span></th>
                                    <th><span id="ic_2020_total">0</span></th>
                                    <th><span id="ic_2021_total">0</span></th>
                                    <th><span id="ic_2022_total">0</span></th>
                                    <th><span id="ic_total2">0</span></th>
                                    <th><span id="ic_2023_total">0</span></th>
                                    <th><span id="ic_total_all">0</span></th>
                                  </tr>
                                </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        </div>
        </div>
      </div>
  </div>

<!--   <div id="rb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
      <div role="document" class="modal-dialog modal-lg" style="max-width: 100%;">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-title">Regional Breakdown</h5>
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
          </div>
        <div class="modal-body">
              <div class="row">
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Region</th>
                                      <th>2016 and Prior</th>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>2017-2022</th>
                                      <th>Continuing Years</th>
                                      <th>Overall Total</th>
                                    </tr>
                                  </thead>
                                  <tbody id="rbBody">
                                    @foreach ($regions as $region)
                                    <tr>
                                      <td>
                                        {{ $region->region_description }}
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}1 rb_2016" name="2016_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2017" name="2017_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2018" name="2018_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2019" name="2019_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2020" name="2020_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2021" name="2021_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}2 rb_2022" name="2022_{{$region->region_code}}">
                                      </td>
                                      <td>
                                        <span class="rb-total2" id="{{$region->region_code}}2_total">0</span>
                                      </td>
                                      <td>
                                        <input type="text" placeholder="" class="form-control rb-number {{$region->region_code}}1 rb_2023" name="2023_{{$region->region_code}}">
                                      </td>
                                      <td>
                                       <span class="rb-total_all" id="{{$region->region_code}}_total_all">0</span>
                                      </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                      <th>Total</th>
                                      <th><span id="rb_2016_total">0</span></th>
                                      <th><span id="rb_2017_total">0</span></th>
                                      <th><span id="rb_2018_total">0</span></th>
                                      <th><span id="rb_2019_total">0</span></th>
                                      <th><span id="rb_2020_total">0</span></th>
                                      <th><span id="rb_2021_total">0</span></th>
                                      <th><span id="rb_2022_total">0</span></th>
                                      <th><span id="rb_total2">0</span></th>
                                      <th><span id="rb_2023_total">0</span></th>
                                      <th><span id="rb_total_all">0</span></th>
                                      
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        </div>
        </div>
      </div>
  </div> -->

  <div class="modal fade interRegionalModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">  
            <h5 class="modal-title" id="exampleModalLabel">Interregional</h5>
            <button type="button" class="close interRegionalModalClose" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group col-lg-12">
                <label for="states">Regions</label>
                     <select class="form-control regionList2" name="states_id[]" multiple="multiple">
                        @foreach ($regions as $region)
                          <option value="{{$region->regional_code}}"
                            @foreach ($states_projects as $states_project)
                            @if($region->regional_code == $states_project->region_id)
                              selected
                            @endif
                          @endforeach
                            >{{ $region->region_description }}</option>
                        @endforeach
                     </select>
                <label for="provinces">Provinces</label>
                     <select class="form-control provinceList2" name="provinces_id[]" multiple="multiple">
                        @foreach ($provinces as $prov)
                          <option value={{$prov->id}} 
                            @foreach ($province_projects as $province_project)
                              @if ($prov->id == $province_project->province_id)
                                selected
                              @endif
                            @endforeach
                          >{{$prov->provName}}</option>
                        @endforeach
                     </select>
                <label for="cities">City/Municipality</label>
                     <select class="form-control cityList2" name="cities_id[]" multiple="multiple">
                     @foreach ($cities as $city)
                        <option value={{$city->id}}
                          @foreach ($city_projects as $city_project)
                            @if ($city->id == $city_project->cities_id)
                              selected
                            @endif
                          @endforeach 
                        >{{$city->cityName}}  
                        </option>
                        }
                        }
                     @endforeach
                     </select>
              </div>
            </div>
        </div>
    </div>
  </div>

  <div class="modal fade RegionSpec" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">  
            <h5 class="modal-title" id="exampleModalLabel">Region Specific</h5>
            <button type="button" class="close RegionSpecClose" data-dismiss="modal">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div class="form-group col-lg-12">
                <label for="region">Regions</label>
                     <select class="form-control regionSpecList2" name="region">
                        @foreach ($regions as $region)
                          <option value="{{$region->regional_code}}"
                            @foreach ($states_projects as $states_project)
                              @if($region->regional_code == $states_project->region_id)
                                selected
                              @endif
                          @endforeach
                          >{{ $region->region_description }}
                          </option>
                        @endforeach
                     </select>
                <label for="RegionSpecProvincesList">Provinces</label>
                     <select class="form-control RegionSpecProvinceList" name="RegionSpecProvincesList[]" multiple>
                        @foreach ($provinces as $prov)
                          <option value="{{$prov->id}}"
                            @foreach ($province_projects as $province_project)
                              @if ($prov->id == $province_project->province_id)
                                selected
                              @endif
                            @endforeach
                          >{{$prov->provName}}
                          </option>
                        @endforeach
                     </select>
              </div>
            </div>
        </div>
    </div>
  </div>
</form> 
                <div id="uncat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                  <div role="document" class="modal-dialog modal-lg">
                    <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">Save as Uncategorized</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                  </div>
                                  <div class="modal-body">
                                    <form class="text-left needs-validation" action="{{ asset('/uncatproject') }}/{{$projects->id}}" method="POST" enctype="multipart/form-data" novalidate id="uncatProjForm">{{ csrf_field() }}
                                     <label class="col-sm-12 form-control-label">If Uncategorized</label>
                                      <div class="col-sm-12">
                                          <textarea class="form-control" rows="5" id="comment" name="uncat" required>{{$projects->uncat}}</textarea>
                                      </div>   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-secondary btn-sm" name="PIS_statusofsubmission" value="Uncategorized" onclick="SaveAsUncat();">Save</button>
                                    </form>
                                  </div>
                    </div>
                  </div>
                </div>

@endsection
@section('scripts')
<script type="text/javascript">
  
  function OnLoadPage() {
    InfraCheckboxFunction1();
    InfraCheckboxFunction3();
    InfraCheckboxFunction4();
    InfraCheckboxFunction6();

    BasisImpCheckboxes();
    SocioPointsCheckboxes();
    SusPointsCheckboxes();    
  }

  window.onload = OnLoadPage;


</script>
<script src="{{ asset('js/c0d3w4rrior.js') }}"></script>
@endsection
