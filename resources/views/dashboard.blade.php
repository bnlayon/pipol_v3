@extends ('layouts')

@section ('content')
  @if(auth()->user()->user_type == 'AG')
  <section class="dashboard-counts section-padding">
      <div class="container-fluid">
            <div class="row">
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-file"></i></div>
                  <div class="name"><strong class="text-uppercase">Draft Projects</strong>
                    <div class="count-number">{{$countdraftproject}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-padnote"></i></div>
                  <div class="name"><strong class="text-uppercase">Endorsed Projects</strong>
                    <div class="count-number">{{$countendorsedproject}} 
                    </div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-check"></i></div>
                  <div class="name"><strong class="text-uppercase">Completed Projects</strong>
                    <div class="count-number">{{$countcompletedproject}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-chart-line"></i></div>
                  <div class="name"><strong class="text-uppercase">PIP</strong>
                    <div class="count-number">{{$countpipproject}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fab fa-creative-commons-remix"></i></div>
                  <div class="name"><strong class="text-uppercase">CIP</strong>
                    <div class="count-number">{{$countcipproject}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-cogs"></i></div>
                  <div class="name"><strong class="text-uppercase">TRIP</strong>
                    <div class="count-number">{{$counttripproject}}</div>
                  </div>
                </div>
              </div>
            </div>
       </div>
  </section>
  @endif
  @if(auth()->user()->user_type == 'SS')
  <section class="dashboard-counts section-padding">
      <div class="container-fluid">
            <div class="row">
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-file"></i></div>
                  <div class="name"><strong class="text-uppercase">Chapter Projects</strong>
                    <div class="count-number">{{$countmychap}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-padnote"></i></div>
                  <div class="name"><strong class="text-uppercase">Validated</strong>
                    <div class="count-number">{{$countvalidated}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-check"></i></div>
                  <div class="name"><strong class="text-uppercase">For Reclassification</strong>
                    <div class="count-number">{{$countforreclassification}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-chart-line"></i></div>
                  <div class="name"><strong class="text-uppercase">Reclassification - For Your Action</strong>
                    <div class="count-number">{{$countforyouraction}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fab fa-creative-commons-remix"></i></div>
                  <div class="name"><strong class="text-uppercase">Reclassification - Accepted</strong>
                    <div class="count-number">{{$countaccepted}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-cogs"></i></div>
                  <div class="name"><strong class="text-uppercase">Reclassification - Declined</strong>
                    <div class="count-number">{{$countdeclined}}</div>
                  </div>
                </div>
              </div>
            </div>
       </div>
  </section>
  @endif
  @if(auth()->user()->user_type == 'NRO')
  <section class="dashboard-counts section-padding">
      <div class="container-fluid">
            <div class="row">
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="fas fa-file"></i></div>
                  <div class="name"><strong class="text-uppercase">Nationwide Projects</strong>
                    <div class="count-number">{{$countnationwide}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-padnote"></i></div>
                  <div class="name"><strong class="text-uppercase">Interregional</strong>
                    <div class="count-number">{{$countinter}}</div>
                  </div>
                </div>
              </div>
              <!-- Count item widget-->
              <div class="col-xl-2 col-md-4 col-6">
                <div class="wrapper count-title d-flex">
                  <div class="icon"><i class="icon-check"></i></div>
                  <div class="name"><strong class="text-uppercase">Region Specific</strong>
                    <div class="count-number">{{$countreg}}</div>
                  </div>
                </div>
              </div>
            </div>
       </div>
  </section>
  @endif
      <section class="dashboard-header section-padding">
        <div class="container-fluid">
          <div class="row d-flex align-items-md-stretch">
            <!-- To Do List-->
            <div class="col-lg-6 col-md-6">
              <div class="card to-do">
                <h2 class="display h4">Welcome to Public Investment Program Online (PIPOL) System</h2>
                <p>
                  The 2017-2022 Public Investment Program (PIP) contains the rolling list of priority programs and projects (PAPs) to be implemented by the national government (NG), government-owned and controlled corporations (GOCCs), government financial institutions (GFIs), and other national government offices and instrumentalities within the medium term (or the plan period from 2017 to 2022), which respond to the outcomes in the Philippine Development Plan (PDP) and its Results Matrices (RM). These PAPs may be financed using national government funds, including internal cash generated by GOCCs, in partnership with the private sector or through Official Development Assistance (ODA).</p>

                  <p>Pursuant to Section 3 of the Executive Order (EO) No. 27 on the implementation of the 2017-2022 Development Plan (PDP) and PIP , and the Special Provision in the General Appropriations Act (GAA) for FY 2018 , NEDA issued on February 7, 2018 a call to agencies for the updating of the 2017-2022 PIP.</p>
                </p>
              </div>
            </div>
            <div class="col-lg-6 col-md-6" align="center">
              <a href="http://www.neda.gov.ph/2018/09/28/updated-2017-2022-public-investment-program-as-input-to-the-fiscal-year-2019-budget-preparation/" target="_blank"><img src="img/portfolio/fullsize/header.jpg" class="img-responsive" style="width:60%"></a>
            </div>
            <!-- 
            <div class="col-lg-3 col-md-6">
              <div class="card project-progress">
                <h2 class="display h4">Project Beta progress</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div class="pie-chart">
                  <canvas id="pieChart" width="300" height="300"> </canvas>
                </div>
              </div>
            </div>
            <!-- Line Chart
            <div class="col-lg-6 col-md-12 flex-lg-last flex-md-first align-self-baseline">
              <div class="card sales-report">
                <h2 class="display h4">Sales marketing report</h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor amet officiis</p>
                <div class="line-chart">
                  <canvas id="lineCahrt"></canvas>
                </div>
              </div>
            </div>
             -->
          </div>
        </div>
      </section>
      <!-- Statistics Section-->
      <section class="statistics">
        <div class="container-fluid">
          <div class="row d-flex">
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-phone-square"></i></div>
                <strong class="text-primary">Secretariat Focal Directory</strong>
                <a href="{{ asset('storage/storage/files/NSFocal2.pdf') }}" target="_blank">Click Here!</a>
                <strong class="text-primary">NEDA RO Focal Directory</strong>
                <a href="{{ asset('storage/storage/files/NROFocal.pdf') }}" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-book"></i></div>
                <strong class="text-primary">2017-2022 PIP (Formulation and Annual Update)</strong>
                <a href="http://www.neda.gov.ph/public-investment-programs/" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-table"></i></div>
                <strong class="text-primary">August 27, 2019 NEDA Memorandum on PIP/TRIP Call as Input to FY 2021 Budget Preparation</strong>
                <a href="http://www.neda.gov.ph/joint-call-for-the-updating-of-the-2017-2022-pip-and-formulation-of-fy-2021-2023-trip-as-input-to-the-fy-2021-budget-preparation/" target="_blank">Click Here!</a>
              </div>
            </div>
            <div class="col-lg-3">
              <!-- Income-->
              <div class="card income text-center">
                <div class="icon"><i class="fas fa-building"></i></div>
                <strong class="text-primary">Regional Development Investment Program</strong>
                <a href="{{ asset('/rdip') }}" target="_blank">Click Here!</a>
              </div>
            </div>
          </div>
        </div>
      </section>
@endsection