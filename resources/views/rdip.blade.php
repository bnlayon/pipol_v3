@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Regional Development Investment Program </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Regional Development Investment Program</h1>
      </header>
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Links to RDIP</h4>
                </div>
                <div class="card-body">
                  <table id="example_link1" class="table table-striped table-bordered" style="width: 100%;">
                    <tr>
                      <th>Region</th>
                      <th>Link</th>
                    </tr>
                    <tr>
                      <td>CAR</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                    <tr>
                      <td>Region I</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                    <tr>
                      <td>Region II</td>
                      <td><a href="https://drive.google.com/file/d/1UYjR_x8MzsxrNI8HwdEoRB1LO5HtyVk5/view?usp=sharing" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region III</td>
                      <td><a href="https://onedrive.live.com/embed?cid=0AC857A6179BC9A1&resid=AC857A6179BC9A1%211030&authkey=AOONUTHfPCe8BHY&em=2" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>CALABARZON</td>
                      <td><a href="http://calabarzon.neda.gov.ph/wp-content/uploads/2018/09/Updated-RDIP-2018-2022.pdf" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>MIMAROPA</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                    <tr>
                      <td>Region V</td>
                      <td><a href="https://drive.google.com/drive/folders/1XYnjfDqp4y1qJcCXJA17B3LH8kpKv1db?usp=sharing" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region VI</td>
                      <td><a href="https://drive.google.com/file/d/1DDUW4Fh9MjhtP24fUNme0D4JUuXPNXp-/view" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region VII</td>
                      <td><a href="https://drive.google.com/file/d/1-eBgHPXsFhi4JIJFaH0xhruNYS1ShUn5/view" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region VIII</td>
                      <td><a href="https://drive.google.com/file/d/1yyvphMosPePGDS_PK9OIzAWol1OpYI6T/view" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region IX</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                    <tr>
                      <td>Region X</td>
                      <td><a href="https://drive.google.com/file/d/1TD0imrjkQMrt25aojYu6EOW8IVOr2v2o/view" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a>
                      <a href="https://drive.google.com/file/d/1UQmIC3ShLK4rSvOjKaks9tIM5H6Un31V/view" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region XI</td>
                      <td><a href="http://nro11.neda.gov.ph/wp-content/uploads/2018/02/Davao-RDIP-2017-2022.pdf" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a>
                      <a href="https://drive.google.com/drive/folders/1zjMaKj6lAryL3NBOTqe91lXqQnTOWT-F?usp=drive_open" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-link"></i></button></a></td>
                    </tr>
                    <tr>
                      <td>Region XII</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                    <tr>
                      <td>CARAGA</td>
                      <td><a href="#"><button type="button" class="btn btn-default"><i class="fa fa-link"></i></button disabled></a></td>
                    </tr>
                  </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection