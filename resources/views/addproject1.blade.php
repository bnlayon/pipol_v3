@extends ('layouts2')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Add Project</li>
      </ul>
    </div>
  </div>

  <div id="app">
    <addproject 
      :route="'{{ asset('/addproject') }}'"
      :routetoview="'{{ asset('/viewprojects') }}'"
      :basis="{{$basis_}}"
      @if ($submission != '')
      :submission="{{$submission}}"
      @endif
      @if ($agency_agency != '')
      :agency_agency="{{$agency_agency}}"
      @endif
      @if ($agency_mother != '')
      :agency_mother="{{$agency_mother}}"
      @endif
      @if ($piptypologies != '')
      :piptypologies = "{{$piptypologies}}"
      @endif
      @if ($ciptypologies != '')
      :ciptypologies = "{{$ciptypologies}}"
      @endif
      @if ($sectors != '')
      :sectors = "{{$sectors}}"
      @endif
      @if ($subsectors != '')
      :subsectors = "{{$subsectors}}"
      @endif
      @if ($statuses != '')
      :statuses = "{{$statuses}}"
      @endif
      @if ($chapters != '')
      :chapters = "{{$chapters}}"
      @endif
      @if ($_1matrices != '')
      :_1matrices = "{{$_1matrices}}"
      @endif
      @if ($_2matrices != '')
      :_2matrices = "{{$_2matrices}}"
      @endif
      @if ($_3matrices != '')
      :_3matrices = "{{$_3matrices}}"
      @endif
      @if ($_4matrices != '')
      :_4matrices = "{{$_4matrices}}"
      @endif
      @if ($agendas != '')
      :agendas = "{{$agendas}}"
      @endif
      @if ($agendas != '')
      :agendas = "{{$agendas}}"
      @endif
      @if ($goals != '')
      :goals = "{{$goals}}"
      @endif
      @if ($projectdocuments != '')
      :projectdocuments = "{{$projectdocuments}}"
      @endif
      @if ($fsstatuses != '')
      :fsstatuses = "{{$fsstatuses}}"
      @endif
      @if ($fundingsources != '')
      :fundingsources = "{{$fundingsources}}"
      @endif
      @if ($modes != '')
      :modes = "{{$modes}}"
      @endif
      @if ($fundings != '')
      :fundings = "{{$fundings}}"
      @endif
    ></addproject>
  </div>  

@endsection
@section('scripts')
{{-- <script src="{{ asset('js/c0d3w4rrior.js') }}"></script> --}}
@endsection