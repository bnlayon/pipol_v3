<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Validate Projects </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validate Uncategorized Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">Uncategorized</a>
        </li>
      </ul>

      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="example" class="table" style="width: 100%;">
        <thead>
            <tr>
              <th>PIP Code</th>
              <th>Project Title</th>
              <th>Mother Agency</th>
              <th>Attached Agency</th>
              <th>Spatial Coverage</th>
              <th>Region</th>
              <th>Head Chapter</th>
              <th>Agency Status/Category</th>
              <th>NRO Status/Category</th>
              @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
              <th>NEDA PIP Chapter Focal Status/Category</th>
              @endif
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
           @foreach ($allpips as $allpip)
            <tr
              @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
                @if($allpip->SS_statusofsubmission == 'Reviewed')
                  style="background-color:#ffefc0;"
                @elseif($allpip->SS_statusofsubmission == 'Validated')
                  style="background-color:#dff0d8;"
                @elseif($allpip->SS_statusofsubmission == 'For Reclassification')
                  style="background-color:#a3cfff;"
                @endif
              @elseif(auth()->user()->user_type == "NRO")
                @if($allpip->SS_statusofsubmission == 'Reviewed')
                  style="background-color:#ffefc0;"
                @elseif($allpip->SS_statusofsubmission == 'Validated')
                  style="background-color:#dff0d8;"
                @endif
              @endif
              >
              <td>{{$allpip->code}}</td>
              <td>{{$allpip->title}}</td>
              <td>
                @if (empty($allpip->motheragency_id))
                  @foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @else
                  @foreach ($allpip->getAgencyDetails($allpip->motheragency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @endif
              </td>
              <td>
                @if (empty($allpip->motheragency_id))

                @else
                  @foreach ($allpip->getAgencyDetails($allpip->agency_id) as $agencydetail)
                  {{ $agencydetail->UACS_AGY_DSC }}
                  @endforeach
                @endif
              </td>
              <td>{{$allpip->spatial}}</td>
              <td>
                @if($allpip->spatial == 'Region Specific' || $allpip->spatial == 'Interregional')
                  @foreach ($allpip->getRegionsProjects($allpip->id) as $regionDetail)
                    @if($regionDetail->region_id == 14)
                      ARMM
                    @elseif($regionDetail->region_id == 15)
                      CAR
                    @elseif($regionDetail->region_id == 16)
                      NCR
                    @elseif($regionDetail->region_id == 40)
                      Region 4A
                    @elseif($regionDetail->region_id == 41)
                      Region 4B
                    @else
                      Region {{ $regionDetail->region_id }}
                    @endif
                  @endforeach       
                @endif         
              </td>
              <td>Chapter {{$allpip->mainpdp}}</td>
              <td>{{$allpip->category}} 
                @if($allpip->category == 'Tier 1')
                  @if($allpip->tier1_type == 1)
                    (Ongoing)
                  @elseif($allpip->tier1_type == 2)
                    (Level 1)
                  @elseif($allpip->tier1_type == 3) 
                    (Level 2)
                  @else
                  @endif 
                @elseif($allpip->category == 'Tier 2')
                  (Level {{$allpip->tier2_status}}) 
                @else

                @endif
                - {{$allpip->statusofsubmission}}</td>
              
              <td>{{$allpip->NRO_category}} 
                @if($allpip->NRO_category == 'Tier 1')
                  @if($allpip->NRO_tier1_stat == 1)
                    (Ongoing)
                  @elseif($allpip->NRO_tier1_stat == 2)
                    (Level 1)
                  @elseif($allpip->NRO_tier1_stat == 3) 
                    (Level 2)
                  @else
                  @endif 
                @elseif($allpip->NRO_category == 'Tier 2')
                  (Level {{$allpip->NRO_tier2_stat}}) 
                @else

                @endif
                - {{$allpip->NRO_statusofsubmission}}</td>
              @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
              <td>{{$allpip->SS_category}} 
                @if($allpip->SS_category == 'Tier 1')
                  @if($allpip->SS_tier1_stat == 1)
                    (Ongoing)
                  @elseif($allpip->SS_tier1_stat == 2)
                    (Level 1)
                  @elseif($allpip->SS_tier1_stat == 3) 
                    (Level 2)
                  @else
                  @endif 
                @elseif($allpip->SS_category == 'Tier 2')
                  (Level {{$allpip->SS_tier2_stat}}) 
                @else

                @endif
                - {{$allpip->SS_statusofsubmission}}</td>
              @endif
              <td>
                @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
                <a href="{{ asset('/reclassify') }}/{{$allpip->id}}" target="_blank"><button disabled type="button" class="btn btn-primary">R</button></a>
                @endif
                <a href="{{ asset('/editproject') }}/{{$allpip->id}}" target="_blank"><button type="button" class="btn btn-primary">V</button></a>
                <a href="{{ asset('/viewprojectsprint') }}/{{$allpip->id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="fa fa-print"></i></button></a>
              </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>  
              <th>PIP Code</th>
              <th>Project Title</th>
              <th>Mother Agency</th>
              <th>Attached Agency</th>
              <th>Spatial Coverage</th>
              <th>Region</th>
              <th>Head Chapter</th>
              <th>Agency Status/Category</th>
              <th>NRO Status/Category</th>
              @if(auth()->user()->user_type == "SS" || auth()->user()->user_type == "ADM" || auth()->user()->user_type == "IS")
              <th>NEDA PIP Chapter Focal Status/Category</th>
              @endif
              <th>Action</th>
            </tr>
        </tfoot>
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>
@endsection