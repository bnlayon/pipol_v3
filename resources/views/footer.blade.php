      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>National Economic and Development Authority &copy; 2018</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="" class="external">NEDA-ICTS</a></p>
            </div>
          </div>
        </div>
      </footer>