@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Related Links </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Reclassify Project</h1>
      </header>
      <form action="{{ asset('/reclassify') }}/{{$projects->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
        <div class="row">
          <div class="col-lg-12">
                <div class="card">
                  <div class="card-header d-flex align-items-center">
                    <h4>Project Information</h4>
                  </div>
                  <div class="card-body">
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Project Title</label>
                        <div class="col-sm-10">
                          <label>{{ $projects->title }}</label>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Project Description</label>
                        <div class="col-sm-10">
                          <label>{{ $projects->description }}</label>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Original Main PDP Chapter</label>
                        <div class="col-sm-10">
                          <label>Chapter {{ $projects->mainpdp }}</label>
                          <input type="hidden" name="orig_pdp" value="{{ $projects->mainpdp }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Original Other PDP Chapter</label>
                        <div class="col-sm-10">
                          <label>
                            <?php $pdps = ''; ?>
                            @foreach ($chapters as $chapter)
                                @foreach ($chapters_projects as $chapters_project)
                                  @if($chapter->chap_no == $chapters_project->chap_id)
                                    Chapter {{ $chapter->chap_no }}
                                    <?php $pdps .= $chapter->chap_no.', '; ?>
                                  @endif
                                @endforeach
                            @endforeach
                            <?php $pdps = rtrim($pdps,", ");?>
                            <input type="hidden" name="orig_other_pdp" value="<?php echo $pdps; ?>">
                          </label>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header d-flex align-items-center">
                    <h4>Proposed Reclassification</h4>
                  </div>
                  <div class="card-body">
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Proposed Main PDP Chapter</label>
                        <div class="col-sm-10">
                          <select name="proposed_pdp" class="form-control" onchange="java_script_:show5(this.options[this.selectedIndex].value)" required>
                            <option disabled selected value="">Choose Chapter</option>
                            @foreach ($chapters as $chapter)
                              @if($chapter->chap_no == 99)
                            <option value="{{ $chapter->chap_no }}"
                              >{{ $chapter->chap_description }}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Proposed Other PDP Chapter</label>
                        <div class="col-sm-10">
                          <select id="proposed_other_pdp" name="proposed_other_pdp[]" multiple="multiple">
                            @foreach ($chapters as $chapter)
                              <option value="{{$chapter->chap_no}}"
                                >{{$chapter->chap_description}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-2 control-label" for="title">Remarks</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="remarks_proposed" name="remarks_proposed" required></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-10">
                          <center><button type="submit" data-toggle="modal" class="btn btn-danger">Reclassify</button></center>
                        </div>
                      </div>
                  </div>
                </div>
          </div>
        </div>
      </form>
    </div>
  </section>


@endsection