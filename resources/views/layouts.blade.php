<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PIPOL v2.0</title>

    <style>
        #menu {
            position: fixed;
            right: 0;
            top: 50%;
            width: 15em;
            margin-top: -2.5em;
        }

        #menuButton {
            position: fixed;
            padding-left: 180px;
            right: 0;
            top: 50%;
            width: 15em;
            margin-top: -2.5em;
        }

        .select2-search--inline input {
            width: 700px !important;
        }

        .select2-selection__rendered {
            width: 700px !important
        }

    </style>
    

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="{{ asset('js/sweealert2.all.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/fontastic.css') }}">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/own.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="{{ asset('/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.default.css') }}" id="theme-stylesheet">
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">

</head>

<body>
    

    <div class="se-pre-con"></div>
    @include ('sidebar')
    <div class="page">
        @include ('header')
        @yield ('content')
        @yield('scripts')
        @include ('footer')
        @include('sweet::alert')
        <!-- @yield('c0d3w4rrior89-1')  -->
         <!-- @yield('jQuery2')  -->
    </div>

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
    <script src="{{ asset('js/own.js') }}"></script>
    <script src="{{ asset('js/kc.js') }}"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}" type="text/css">

    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.fixedColumns.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/scrollreveal/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<!--         
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  -->

 <!-- TRIP table in Validation Module  -->
    <script>
        $(document).ready(function () {

            $('#trip_table thead tr').clone(true).appendTo( '#trip_table thead' );
            $('#trip_table thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
            
        var table = $('#trip_table').DataTable({

                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     dom: 'Blfrtip',           
                            buttons: [
                                {
                                    extend: 'excelHtml5',
                                    title: 'TRIP Projects',
                                    exportOptions: {
                                        columns: ':visible'
                                    }
                                },
                                'colvis'
                                ],                  
                processing: true,
                serverSide: true,
                ajax: "{{ url('appraisal2-list') }}",    
                deferRender: true,             
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'code', name: 'code'},
                    {data: 'title', name: 'title'},
                    // {data: 'UACS_DPT_DSC', name: 'UACS_DPT_DSC'},
                    {data: 'UACS_AGY_DSC', name: 'UACS_AGY_DSC'},
                    // {data: 'spatial', name: 'spatial'},
                     {data: 'sector', name: 'sector'},                    
                   //  {data: 'chap_description', name: 'chap_description'},                    
                    // {data: 'cat', name: 'cat'},                    
                    // {data: 'NRO_category', name: 'NRO_category'},
                    // {data: 'SS_category', name: 'SS_category'},
                     //{data: 'SS_statusofsubmission', name: 'SS_statusofsubmission'},
                    {data: 'actions', name: 'actions'},
                ],

                "createdRow": function( row, data, dataIndex){
                            if( data['SS_statusofsubmission'] == "Reviewed"  ){
                                $(row).css('background-color', '#ffefc0');
                            }
                            if( data['SS_statusofsubmission'] == "Validated"  ){
                                $(row).css('background-color', '#dff0d8');
                            }
                            if( data['SS_statusofsubmission'] == "For Reclassification"  ){
                                $(row).css('background-color', '#a3cfff');
                            }  
                        }
                
            });
        });
    </script>

    
    <script>
        $(document).ready(function () {

            $('#completed_table thead tr').clone(true).appendTo( '#completed_table thead' );
            $('#completed_table thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
            
        var table = $('#completed_table').DataTable({

                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     dom: 'Blfrtip',           
                            buttons: [
                                {
                                    extend: 'excelHtml5',
                                    title: 'Completed Projects',
                                    exportOptions: {
                                        columns: ':visible'
                                    }
                                },
                                'colvis'
                                ],                  
                processing: true,
                serverSide: true,
                ajax: "{{ url('appraisal4-list') }}",    
                deferRender: true,             
                columns: [
                    // {data: 'id', name: 'id'},
                    {data: 'code', name: 'code'},
                    {data: 'title', name: 'title'},
                    // {data: 'UACS_DPT_DSC', name: 'UACS_DPT_DSC'},
                    {data: 'UACS_AGY_DSC', name: 'UACS_AGY_DSC'},
                    // {data: 'spatial', name: 'spatial'},
                    // {data: 'sector', name: 'sector'},                    
                   //  {data: 'chap_description', name: 'chap_description'},                    
                    // {data: 'cat', name: 'cat'},                    
                    // {data: 'NRO_category', name: 'NRO_category'},
                    // {data: 'SS_category', name: 'SS_category'},
                    {data: 'SS_statusofsubmission', name: 'SS_statusofsubmission'},
                    {data: 'actions', name: 'actions'},
                ],

                "createdRow": function( row, data, dataIndex){
                            if( data['SS_statusofsubmission'] == "Reviewed"  ){
                                $(row).css('background-color', '#ffefc0');
                            }
                            if( data['SS_statusofsubmission'] == "Validated"  ){
                                $(row).css('background-color', '#dff0d8');
                            }
                            if( data['SS_statusofsubmission'] == "For Reclassification"  ){
                                $(row).css('background-color', '#a3cfff');
                            }  
                        }
                
            });
        });
    </script>

    <script>
        $(document).ready(function () {

            $('#oversight_table thead tr').clone(true).appendTo( '#oversight_table thead' );
            $('#oversight_table thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
            
        var table = $('#oversight_table').DataTable({

                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     dom: 'Blfrtip',           
                            buttons: [
                                {
                                    extend: 'excelHtml5',
                                    title: 'TRIP Projects',
                                    exportOptions: {
                                        columns: ':visible'
                                    }
                                },
                                'colvis'
                                ],                  
                processing: true,
                serverSide: true,
                "ajax":{
                     "url": "{{ route('product_catalog') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
                deferRender: true,             
                columns: [
                {data: 'id', name: 'id'},
                    {data: 'code', name: 'code'},
                    {data: 'title', name: 'title'},
                     //{data: 'UACS_DPT_DSC', name: 'UACS_DPT_DSC'},
                    {data: 'UACS_AGY_DSC', name: 'UACS_AGY_DSC'},
                   {data: 'spatial', name: 'spatial'},
                //{data: 'sector', name: 'sector'},                    
                   //  {data: 'chap_description', name: 'chap_description'},                    
                    // {data: 'cat', name: 'cat'},                    
                    // {data: 'NRO_category', name: 'NRO_category'},
                    // {data: 'SS_category', name: 'SS_category'},
                    // {data: 'SS_statusofsubmission', name: 'SS_statusofsubmission'},
                    {data: 'actions', name: 'actions'},
                ],

                "createdRow": function( row, data, dataIndex){
                            if( data['SS_statusofsubmission'] == "Reviewed"  ){
                                $(row).css('background-color', '#ffefc0');
                            }
                            if( data['SS_statusofsubmission'] == "Validated"  ){
                                $(row).css('background-color', '#dff0d8');
                            }
                            if( data['SS_statusofsubmission'] == "For Reclassification"  ){
                                $(row).css('background-color', '#a3cfff');
                            }  
                        }
                
            });
        });
    </script>
    <script>
        $(document).ready(function () {

            $('#dropped_table thead tr').clone(true).appendTo( '#dropped_table thead' );
            $('#dropped_table thead tr:eq(0) th').each( function (i) {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
     
            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } );
            
        var table = $('#dropped_table').DataTable({

                    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     dom: 'Blfrtip',           
                            buttons: [
                                {
                                    extend: 'excelHtml5',
                                    title: 'Dropped Projects',
                                    exportOptions: {
                                        columns: ':visible'
                                    }
                                },
                                'colvis'
                                ],                  
                processing: true,
                serverSide: true,
                "ajax":{
                     "url": "{{ route('appraisal_dropped') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                   },
                deferRender: true,             
                columns: [
                {data: 'id', name: 'id'},
                {data: 'code', name: 'code'},
                {data: 'title', name: 'title'},
                     //{data: 'UACS_DPT_DSC', name: 'UACS_DPT_DSC'},
                {data: 'UACS_AGY_DSC', name: 'UACS_AGY_DSC'},
                   // {data: 'spatial', name: 'spatial'},
                //{data: 'sector', name: 'sector'},                    
                   //  {data: 'chap_description', name: 'chap_description'},                    
                    // {data: 'cat', name: 'cat'},                    
                    // {data: 'NRO_category', name: 'NRO_category'},
                    // {data: 'SS_category', name: 'SS_category'},
                {data: 'SS_statusofsubmission', name: 'SS_statusofsubmission'},
                {data: 'actions', name: 'actions'},
                ],

                "createdRow": function( row, data, dataIndex){
                            if( data['SS_statusofsubmission'] == "Reviewed"  ){
                                $(row).css('background-color', '#ffefc0');
                            }
                            if( data['SS_statusofsubmission'] == "Validated"  ){
                                $(row).css('background-color', '#dff0d8');
                            }
                            if( data['SS_statusofsubmission'] == "For Reclassification"  ){
                                $(row).css('background-color', '#a3cfff');
                            }  
                        }
                
            });
        });
    </script>
</body>
</html>
