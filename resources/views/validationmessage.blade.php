<?php ini_set('max_execution_time', 6000); ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
      <div class="container">
        <br>
        <div class="card">
          <div class="card-body">
            <center><img src="http://pipol.neda.gov.ph/img/headeremail.png" height="232px" align="center"></center><br/><br/>
            <h5><b><?php echo date("d F Y"); ?></b></h5>
            <br>

            <?php if (!empty($submission->motheragency_id)){ ?>                 
                <span style="text-transform:uppercase;"><b>
                    <?php echo $submission->mother_fname." ".$submission->mother_mname." ".$submission->mother_lname; ?>
                </b>
                </span><br>
                <span style="text-transform:uppercase;">
                    <?php echo $submission->mother_designation; ?>
                </span>
                <br>
                        <?php 
                            foreach ($submission->getAgencyDetails($submission->motheragency_id) as $agencydetail){
                                echo $agencydetail->UACS_AGY_DSC;
                            }?>
                
                <?php } ?>

            <br><br>
            <span style="text-transform:uppercase;"><b>
                <?php echo $submission->head_fname." ".$submission->head_mname." ".$submission->head_lname; ?>
            </b>
            </span><br>
                <span style="text-transform:uppercase;">
                    <?php echo $submission->head_designation; ?>
                </span>
                <br>
                    <?php 
                        foreach ($submission->getAgencyDetails($submission->agency_id) as $agencydetail){
                            echo $agencydetail->UACS_AGY_DSC;
                        }
                    ?>
            
            <br> <br>  
            <p>Dear Ma'am(s)/Sir(s):</p>
            <p>Following the NEDA Secretariat’s validation of your submission and the inter-agency bodies’ confirmation of respective PIP Chapters, this is to provide the <b>validated list of priority programs and projects (PAPs) included in the <b>Updated 2017-2022 Public Investment Program (PIP)</b>, as input to Fiscal Year (FY) 2021 Budget Preparation,</b> as <b>of March 19, 2020<sup>1</sup></b>:</p>

            <center><strong>Ongoing PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PIP Code</th>
                    <th>PAP Title</th>
                    <th>Mode of Implementation</th>
                    <th>Spatial Coverage</th>
                    <th>Total Project Cost</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectOngoing_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->code; ?></td> 
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td style="text-align: center">
                         <?php
                            if($agencyTier1->modeofimplementation == 74){
                                echo "Through Local Funds in accordance with RA 9184 or the GPRA"; 
                            }
                            elseif ($agencyTier1->modeofimplementation == 75) {
                                echo "Through ODA pursuant to RA 8182 or the ODA Act of 1996";
                            }
                            elseif ($agencyTier1->modeofimplementation == 76) {
                                echo "Through PPP under the Amended BOT Law and Its IRR";
                            }
                            elseif ($agencyTier1->modeofimplementation == 77) {
                                echo "Through Joint Venture Arrangement";
                            }
                            elseif ($agencyTier1->modeofimplementation == 78) {
                                echo "Others";
                            }
                        ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                    <td>
                        <?php
                         $local_inv = $agencyTier1->projects_investments->sum('local');
                         $loan_inv = $agencyTier1->projects_investments->sum('loan');
                         $grant_inv = $agencyTier1->projects_investments->sum('grant');
                         $gocc_inv = $agencyTier1->projects_investments->sum('gocc');
                         $lgu_inv = $agencyTier1->projects_investments->sum('lgu');
                         $private_inv = $agencyTier1->projects_investments->sum('private');
                         $others_inv = $agencyTier1->projects_investments->sum('others');

                         $total_investment = $local_inv+$loan_inv+$grant_inv+$gocc_inv+$lgu_inv+$private_inv+$others_inv;

                         $local_infra = $agencyTier1->projects_infrastructures->sum('local');
                         $loan_infra = $agencyTier1->projects_infrastructures->sum('loan');
                         $grant_infra = $agencyTier1->projects_infrastructures->sum('grant');
                         $gocc_infra = $agencyTier1->projects_infrastructures->sum('gocc');
                         $lgu_infra = $agencyTier1->projects_infrastructures->sum('lgu');
                         $private_infra = $agencyTier1->projects_infrastructures->sum('private');
                         $others_infra = $agencyTier1->projects_infrastructures->sum('others');

                         $total_infra = $local_infra + $loan_infra+$grant_infra+$gocc_infra+$lgu_infra+$private_infra+$others_infra;

                             
                         if($total_investment == 0){
                            echo '₱'.number_format($total_infra,2);
                         }
                         else {
                            echo '₱'.number_format($total_investment,2);
                         }
                         
                        ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <br>
            <center><strong>Proposed PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PIP Code</th> 
                    <th>PAP Title</th>
                    <th>Mode of Implementation</th>                   
                    <th>Spatial Coverage</th>
                    <!-- <th>Level of Readiness</th> -->
                    <th>Total Project Cost</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectProposed_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->code; ?></td> 
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td style="text-align: center">
                          <?php
                            if($agencyTier1->modeofimplementation == 74){
                                echo "Through Local Funds in accordance with RA 9184 or the GPRA"; 
                            }
                            elseif ($agencyTier1->modeofimplementation == 75) {
                                echo "Through ODA pursuant to RA 8182 or the ODA Act of 1996";
                            }
                            elseif ($agencyTier1->modeofimplementation == 76) {
                                echo "Through PPP under the Amended BOT Law and Its IRR";
                            }
                            elseif ($agencyTier1->modeofimplementation == 77) {
                                echo "Through Joint Venture Arrangement";
                            }
                            elseif ($agencyTier1->modeofimplementation == 78) {
                                echo "Others";
                            }
                        ?>
                        </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                    <!-- <td>
                        <?php 
                        if($agencyTier1->tier2_status == 1){
                            echo "Level 1 - CIP";
                        }elseif($agencyTier1->tier2_status == 2){
                            echo "Level 2 - CIP";
                        }elseif($agencyTier1->tier2_status == 3){
                            echo "Level 3 - CIP";
                        }elseif($agencyTier1->tier2_status == 4){
                            echo "Level 4 - CIP"; 
                        }elseif($agencyTier1->tier2_status == 5){
                            echo "Level 1 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 6){
                            echo "Level 2 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 7){
                            echo "Level 3 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 8){
                            echo "Level 4 - Non-CIP";
                        } ?>
                    </td> -->

                    <td>
                          <?php
                         $local_inv = $agencyTier1->projects_investments->sum('local');
                         $loan_inv = $agencyTier1->projects_investments->sum('loan');
                         $grant_inv = $agencyTier1->projects_investments->sum('grant');
                         $gocc_inv = $agencyTier1->projects_investments->sum('gocc');
                         $lgu_inv = $agencyTier1->projects_investments->sum('lgu');
                         $private_inv = $agencyTier1->projects_investments->sum('private');
                         $others_inv = $agencyTier1->projects_investments->sum('others');

                         $total_investment = $local_inv+$loan_inv+$grant_inv+$gocc_inv+$lgu_inv+$private_inv+$others_inv;

                         // $local_infra = $agencyTier1->projects_infrastructures->sum('local');
                         // $loan_infra = $agencyTier1->projects_infrastructures->sum('loan');
                         // $grant_infra = $agencyTier1->projects_infrastructures->sum('grant');
                         // $gocc_infra = $agencyTier1->projects_infrastructures->sum('gocc');
                         // $lgu_infra = $agencyTier1->projects_infrastructures->sum('lgu');
                         // $private_infra = $agencyTier1->projects_infrastructures->sum('private');
                         // $others_infra = $agencyTier1->projects_infrastructures->sum('others');

                         // $total_infra = $local_infra + $loan_infra+$grant_infra+$gocc_infra+$lgu_infra+$private_infra+$others_infra;
                         echo '₱'.number_format($total_investment,2);
                             
                         // if($total_investment == 0){
                         //    echo '₱'.number_format($total_infra,2);
                         // }
                         // else {
                            
                         // }
                         
                        ?>
                    </td>

                </tr>
                <?php } ?>
            </table>
            <br>            
            <br>
            <p>Below is/are the PAP(s) reviewed by the NEDA Secretariat but was/were not qualified for inclusion in the Updated PIP based on the criteria provided in the PIP Updating Guidelines<sup>2</sup> and /or due to incomplete information in the submission: </p>

            <center><strong>Not Qualified as PIP/TRIP</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                    <th>Reason</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectUncategorized($submission->agency_id) as $agencyUncategorized){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyUncategorized->title; ?></td>
                    <td><?php echo $agencyUncategorized->uncat; ?></td>
                </tr>
                <?php } ?>                
            </table>
            <br>
            <br>

            <p>Furthermore, below is/are the PAP(s) submitted by the agency as completed/dropped: </p>
             
            <center><strong>Completed PAPs as of 30 June 2019</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectCompleted_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->title; ?></td>                    
                </tr>
                <?php } ?>
            </table><br>

            <center><strong>Dropped PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                    <th>Reason</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectDropped($submission->agency_id) as $agencyDropped){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyDropped->title; ?></td>
                    <td><?php echo $submission->getDroppingReasons($agencyDropped->id)->reasons; ?></td>
                </tr>
                <?php } ?>                
            </table>
            <br>           

            <p>The list of PAPs as validated by the NEDA Secretariat and confirmed by appropriate inter-agency bodies will be provided to the Department of Budget and Management, Senate and House of Representatives, as input to the FY 2021 budget preparation. </p>
            
            <p>For the details of the PAPs, please be informed that Agency PIP/TRIP Focals may now access their accounts in view mode.</p>
            
            <p>Thank you and warm regards.</p>

            <br>

            <p>Very truly yours,<br>
            <b>PIP and TRIP Secretariats</b></p>

            <br>
            <small>______________________________</small><br>
            <small><sup>1</sup>Confirmation of PIP Chapters by inter-agency bodies following NEDA Secretariat validation of Agency submission as of October 2019.<br>
            <sup>2</sup>27 August 2019 NEDA Memorandum to heads of all agencies and offices.</small><br>
            <!-- <small> *LFP  - Through Local Funds in accordance with RA 9184 or the Government Procurement Act;  ODA - Through ODA pursuant to RA 8182 or the ODA Act of 1996; PPP - Through PPP under the Amended BOT Law and Its IRR; JVA - Through Joint Venture Arrangement; and Others</small> -->
            <center><img src="http://pipol.neda.gov.ph/img/nedafooter.png" height="150px" align="center"></center>
          </div>
        </div>
      </div>

      <form action="{{ asset('/midpointmessage') }}/{{$submission->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                          <button type="submit" class="btn btn-danger">Email</button>  
                        </form>
</body>
</html>