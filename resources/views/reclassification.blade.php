@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Reclassification </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">PDP Chapter Reclassification</h1>
      </header>
    </div>

    <div class="panel-body">
      <ul class="nav nav-pills nav-justified" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#inbox" role="tab">Inbox</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#outbox" role="tab">Outbox</a>
          </li>
        </ul>

      <div class="tab-content tablewrapper">
        <div class="tab-pane active" id="inbox" role="tabpanel" style="background-color: white">
          <table id="reclassify_inbox" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Username of the Requesting PIP Focal</th>
                    <th>Title</th>
                    <th>Agency</th>
                    <th>Original PDP Chapter</th>
                    <th>Original Other PDP Chapters</th>
                    <th>Proposed PDP Chapter</th>
                    <th>Proposed Other PDP Chapters</th>
                    <th>Reclassification Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                  @foreach ($chapters_focals as $chapters_focal)
                    @if($request->proposed_pdp == $chapters_focal->chap)
                      <tr
                @if($request->status == 'For Action')
                  style="background-color:#ffefc0;"
                @elseif($request->status == 'Accepted')
                  style="background-color:#dff0d8;"
                @elseif($request->status == 'Declined')
                  style="background-color:#f2dede;"
                @endif
                >
                  <td>{{$request->requestor}}</td>
                  <td>
                    @foreach ($request->getProjectDetails($request->proj_id) as $projectdetail)
                      <?php echo $title = $projectdetail->title; ?>
                      <?php $des = $projectdetail->description; ?>
                      <?php $id = $projectdetail->id; ?>
                      <?php $aid = $projectdetail->agency_id; ?>
                    @endforeach
                  </td>
                  <td>
                    @foreach ($request->getAgencyDetails($aid) as $agencydetail)
                      {{ $agencydetail->UACS_AGY_DSC }}
                     @endforeach
                  <td>{{$request->orig_pdp}}</td>
                  <td>{{$request->orig_other_pdp}}</td>
                  <td>{{$request->proposed_pdp}}</td>
                  <td>{{$request->proposed_other_pdp}}</td>
                  <td>{{$request->status}}</td>
                  <td><button type="button" data-toggle="modal" data-target="#viewinboxreclassify<?php echo $id; ?>" class="btn btn-info"><i class="fa fa-search"></i></button> 
                                    <div id="viewinboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                  <div role="document" class="modal-dialog modal-lg">
                    <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">Project Information</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                  </div>
                                  <div class="modal-body">
                                    <?php echo $title; ?><br><br>
                                    <?php echo $des; ?><br><br>
                                    Proposed Main PDP Chapter: {{$request->proposed_pdp}}<br><br>
                                    Proposed Other PDP Chapter: {{$request->proposed_other_pdp}}<br><br>
                                    Remarks: {{$request->remarks_proposed}}<br><br>
                                    Reclassification Status: {{$request->status}}<br><br>
                                    @if($request->status == "Declined")
                                    Reason for Declining the Proposed Reclassification: {{$request->reasons_declined}}
                                    @endif
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                                      <a href="{{ asset('/editproject') }}/<?php echo $id; ?>" target="_blank"><button type="button" data-toggle="modal" class="btn btn-info">View Entire Project Details</button></a>
                                  </div>
                    </div>
                  </div>
                </div>
                  @if($request->status == "For Action")
                  
                  <form action="{{ asset('/acceptrequest') }}/{{$request->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#acceptinboxreclassify<?php echo $id; ?>" class="btn btn-primary"><i class="fa fa-check"></i></button> 
                  <div id="acceptinboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                  <div role="document" class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 id="exampleModalLabel" class="modal-title">Project Reclassification Acceptance</h5>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                        <h5>Are you sure you want to accept the project?</h5>
                        <input type="hidden" name="proposed_pdp" value="{{$request->proposed_pdp}}">
                        <input type="hidden" name="proposed_other_pdp" value="{{$request->proposed_other_pdp}}">
                      </div>
                      <div class="modal-footer">
                          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                          <button type="submit" class="btn btn-primary">Accept</button>
                      </div>
                    </div>
                  </div>
                </div>
                </form>

                <form action="{{ asset('/declinerequest') }}/{{$request->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <button type="button" data-toggle="modal" data-target="#declineinboxreclassify<?php echo $id; ?>" class="btn btn-danger"><i class="fa fa-close"></i></button>
                  <div id="declineinboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                      <div class="modal-content">
                       <div class="modal-header">
                         <h5 id="exampleModalLabel" class="modal-title">Decline Project</h5>
                         <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                       </div>
                       <div class="modal-body">
                         <h5>Are you sure you want to decline the project?</h5><br><br>
                         <form>
                           <div class="form-group row">
                             <label class="col-sm-2 form-control-label">Reason/s <br></label>
                             <div class="col-sm-10">
                               <textarea class="form-control" rows="5" id="reasons_declined" name="reasons_declined" required></textarea>
                               <input type="hidden" name="declineid" value="{{$request->id}}">
                             </div>
                           </div>
                         </form>
                       </div>
                       <div class="modal-footer">
                         <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                         <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                      </div>
                    </div>
                  </div>
                </form>

                  @endif
                </tr>
                    @else
                    @endif
                  @endforeach
                @endforeach
            </tbody>
            <tfoot>
                <tr>  
                    <th>Username of the Requesting PIP Focal</th>
                    <th>Title</th>
                    <th>Agency</th>
                    <th>Original PDP Chapter</th>
                    <th>Original Other PDP Chapters</th>
                    <th>Proposed PDP Chapter</th>
                    <th>Proposed Other PDP Chapters</th>
                    <th>Reclassification Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
          </table>
        </div>

        <div class="tab-pane" id="outbox" role="tabpanel" style="background-color: white">
          <table id="reclassify_outbox" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Agency</th>
                    <th>Original PDP Chapter</th>
                    <th>Original Other PDP Chapters</th>
                    <th>Proposed PDP Chapter</th>
                    <th>Proposed Other PDP Chapters</th>
                    <th>Reclassification Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requests as $request)
                  @foreach ($chapters_focals as $chapters_focal)
                    @if($request->orig_pdp == $chapters_focal->chap)
                      <tr
                @if($request->status == 'For Action')
                  style="background-color:#ffefc0;"
                @elseif($request->status == 'Accepted')
                  style="background-color:#dff0d8;"
                @elseif($request->status == 'Declined')
                  style="background-color:#f2dede;"
                @endif>
                  <td>
                    @foreach ($request->getProjectDetails($request->proj_id) as $projectdetail)
                      <?php echo $title = $projectdetail->title; ?>
                      <?php $des = $projectdetail->description; ?>
                      <?php $id = $projectdetail->id; ?>
                      <?php $aid = $projectdetail->agency_id; ?>
                    @endforeach
                  </td>
                  <td>
                    @foreach ($request->getAgencyDetails($aid) as $agencydetail)
                      {{ $agencydetail->UACS_AGY_DSC }}
                     @endforeach
                  </td>
                  <td>{{$request->orig_pdp}}</td>
                  <td>{{$request->orig_other_pdp}}</td>
                  <td>{{$request->proposed_pdp}}</td>
                  <td>{{$request->proposed_other_pdp}}</td>
                  <td>{{$request->status}}</td>
                  <td><button type="button" data-toggle="modal" data-target="#viewoutboxreclassify<?php echo $id; ?>" class="btn btn-info"><i class="fa fa-search"></i></button> 
                  @if($request->status == "For Action")
                  <button type="button" data-toggle="modal" data-target="#editoutboxreclassify<?php echo $id; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></button>

                  <button type="button" data-toggle="modal" data-target="#canceloutboxreclassify<?php echo $id; ?>" class="btn btn-danger"><i class="fa fa-close"></i></button>

                  <form action="{{ asset('/cancelrequest') }}/{{$request->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <div id="canceloutboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true" class="modal fade text-left">
                                  <div role="document" class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 id="exampleModalLabel" class="modal-title">Cancel Request</h5>
                                            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                          </div>
                                          <div class="modal-body">
                                            <h5>Are you sure you want to cancel the request?</h5>
                                            <input type="hidden" name="cancelid" value="{{$request->id}}">
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                                            <button type="submit" class="btn btn-danger">Submit</button>
                                          </div>
                                        </div>
                                  </div>
                  </div>
                  </form>

                  <form action="{{ asset('/editrequest') }}/{{$request->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                  <div id="editoutboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 id="exampleModalLabel" class="modal-title">Edit Request</h5>
                                          <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                          <h6><?php echo $title; ?><br><br>
                                          <?php echo $des; ?><br><br></h6>
                                          <form>
                      <div class="form-group row">
                                      <label class="col-sm-2 form-control-label">Main PDP Chapter </label>
                                      <div class="col-sm-10">
                                        <select name="mainpdp_r" class="form-control">
                                          <option disabled selected>Choose Chapter</option>
                                          @foreach ($chapters as $chapter)
                                          <option value="{{ $chapter->chap_no }}"
                                            @if($chapter->chap_no == $request->proposed_pdp)
                                              selected
                                            @endif
                                            >{{ $chapter->chap_description }}</option>
                                          @endforeach
                                        </select>
                                      </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Other PDP Chapters <br><small class="text-primary">Select as many as applicable</small></label>
                      <div class="col-sm-10">
                        <select id="proposed_other_pdp_edit" name="proposed_other_pdp_edit[]" multiple="multiple" class="form-control">
                            @foreach ($chapters as $chapter)
                              <?php $arr_pdp_prposed = explode(", ",$request->proposed_other_pdp); ?>
                              <option value="{{$chapter->chap_no}}"
                                @foreach($arr_pdp_prposed as $a)
                                  @if($a == $chapter->chap_no)
                                    selected
                                  @endif
                                @endforeach
                                >{{$chapter->chap_description}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>
                      <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Remarks <br></label>
                        <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="remarks_proposed_r" name="remarks_proposed_r">{{$request->remarks_proposed}}</textarea>
                        </div>
                      </div>
                      </form>
                      </div>
                      <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                      </div>
                      </div>
                    </div>
                  </div>
                  </form>
                  @endif
                  <div id="viewoutboxreclassify<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                    <div role="document" class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 id="exampleModalLabel" class="modal-title">Project Information</h5>
                                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                      <h6><?php echo $title; ?><br><br>
                                      <?php echo $des; ?></h6>
                                      Reclassification Status: {{$request->status}}<br><br>
                                    @if($request->status == "Declined")
                                    Reason for Declining the Proposed Reclassification: {{$request->reasons_declined}}
                                    @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                                        <a href="{{ asset('/editproject') }}/<?php echo $id; ?>" target="_blank"><button type="button" data-toggle="modal" class="btn btn-info">View Entire Project Details</button></a>
                                    </div>
                                  </div>
                    </div>
                  </div>
                  </td>
                </tr>
                    @endif
                  @endforeach
                @endforeach
            </tbody>
            <tfoot>
                <tr>  
                    <th>Title</th>
                    <th>Agency</th>
                    <th>Original PDP Chapter</th>
                    <th>Original Other PDP Chapters</th>
                    <th>Proposed PDP Chapter</th>
                    <th>Proposed Other PDP Chapters</th>
                    <th>Reclassification Status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
          </table>
        </div>
      </div>

    </div>
    </section>
@endsection