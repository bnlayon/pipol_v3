@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item">Validate Projects </li>
        <li class="breadcrumb-item"><a href="/trip">TRIP Prioritization</a></li>
        <li class="breadcrumb-item active">Validate Project</li>
      </ul>
    </div>
  </div>

  <form>

  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#about">General Information</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ia">Implementing Agency</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#sc">Spatial Coverage</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#loa">Level of Approval</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#piwpd">Project for Inclusion in Which Programming Document</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pdp">Philippine Development Plan (PDP) Chapter</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ag">0-10 Point Socioeconomic Agenda </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#sdgs">Sustainable Development Goals (SDG) </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#rms">Results Matrices </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ip">Implementation Period</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#ppd">Project Preparation Details</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pcc">Pre-construction Costs</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#eg">Employment Generation</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#it">Investment Targets</a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#fss">Funding Source </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#pa">Physical Accomplishments </a></button>
        <button type="button" class="btn btn-primary btn-xs"><a class="js-scroll-trigger " style="color:white;" href="#fa">Financial Accomplishments</a></button>
      </div>
    </div>
  </div>

  <section class="forms" id="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validate Project</h1>
      </header>

      <section class="bg-primary" id="about">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>General Information</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Title</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" required><span class="text-small text-gray help-block-none">The project title should be identical with the project's title in the budget proposal submitted to DBM.</span>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Is it a Program or a Project? </label>
                      <div class="col-sm-10">
                        <div class="i-checks">
                          <input id="radioCustom1" type="radio" value="option1" name="programorproject" class="form-control-custom radio-custom">
                          <label for="radioCustom1" data-toggle="tooltip" data-placement="top" title="A program is a group of activities and projects that contribute to a common particular outcome. A program should have the following: (a) unique expected results or outcomes; (b) a clear target population or client group external to the agency; (c) a defined method of intervention to achieve the desired result; and (d) a clear management structure that defines accountabilities.">Program </label>
                          <input id="radioCustom2" type="radio" value="option2" name="programorproject" class="form-control-custom radio-custom">
                          <label for="radioCustom2" data-toggle="tooltip" data-placement="top" title="A project is a special undertaking carried out within a definite time frame and intended to result in some pre -determined measure of goods and services.">Project</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Basis for Implementation <br><small class="text-primary">included in any of the following documents</small></label>
                      <div class="col-sm-10">
                        @foreach ($basis_ as $basis)
                        <div class="i-checks">
                          <input id="checkboxCustomBasis{{ $basis->id }}" type="checkbox" value="{{ $basis->basis }}" class="form-control-custom">
                          <label for="checkboxCustomBasis{{ $basis->id }}">{{ $basis->basis }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Description <br><small class="text-primary">Overview, Purpose, and/or Rationale of the Undertaking, Sub-programs/Components)</small></label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                      </div>
                      <label class="col-sm-2 form-control-label">Expected Outputs <br><small class="text-primary">Actual Deliverables, ie. 100km of paved roads</small></label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="ia">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementing Agency</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Mother/Oversight Agency </label>
                      <div class="col-sm-10">
                        <select name="account" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Co-Implementing Agency </label>
                      <div class="col-sm-10">
                        <select name="account" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Attached Agency </label>
                      <div class="col-sm-10">
                        <select name="account" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                        </select>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="sc">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Spatial Coverage</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Coverage</label>
                      <div class="col-sm-10">
                        <select id="proj_cov_main" name="account" class="form-control" onchange="java_script_:show(this.options[this.selectedIndex].value)">
                          <option disabled selected>Choose Coverage</option>
                          <option value="Nationwide">Nationwide</option>
                          <option value="Interregional">Interregional</option>
                          <option value="Region Specific">Region Specific</option>
                          <option value="Abroad">Abroad</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="inter" style="display: none;">
                      <label class="col-sm-2 form-control-label">Interregional</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input id="checkCustomInterRegion{{ $region->region_no }}" type="checkbox" value="{{ $region->region_no }}" name="inter" class="form-control-custom checkbox-custom">
                          <label for="checkCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    <div class="form-group row" id="rs" style="display: none;">
                      <label class="col-sm-2 form-control-label">Region Specific</label>
                      <div class="col-sm-10">
                        @foreach ($regions as $region)
                        <div class="i-checks">
                          <input id="radioCustomInterRegion{{ $region->region_no }}" type="radio" value="{{ $region->region_no }}" name="rs" class="form-control-custom radio-custom">
                          <label for="radioCustomInterRegion{{ $region->region_no }}">{{ $region->region_description }}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function show(proj_cov_main) {
          if (proj_cov_main == 'Interregional') {
            inter.style.display='block';
            rs.style.display='none';
          } else if(proj_cov_main == 'Region Specific') {
            inter.style.display='none';
            rs.style.display='block';
          } else {
            inter.style.display='none';
            rs.style.display='none';
          }
        }
      </script>

      <section class="bg-primary" id="loa">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Level of Approval</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="ICCNBcheckbox" type="checkbox" value="" class="form-control-custom" onclick="iccCheckboxFunction()">
                            <label for="ICCNBcheckbox">Will require Investment Coordination Committee/NEDA Board Approval (ICC-able) ?</label>
                          </div>
                          <div style="display:none" id="icccheckboxes">
                          <div class="i-checks">
                            <input id="ICCNBradio1" type="radio" value="levelofapproval1" name="levelofapproval" class="form-control-custom radio-custom">
                            <label for="ICCNBradio1">Yet to be submitted to NEDA Secretariat</label>
                            <div id="tdos" style="display:none">
                               <label for="tdos">Target Date of Submission</label>
                                  <div class="col-sm-4">
                                  <input type="date" class="form-control" required>
                                  </div>
                            </div>
                          </div>
                          <div class="i-checks">
                            <input id="ICCNBradio2" type="radio" value="" name="levelofapproval" class="form-control-custom radio-custom">
                            <label for="ICCNBradio2">Under the NEDA Secretariat Review</label>
                            <div id="dostns" style="display:none">
                               <label for="dostns">Date of Submission to NEDA Secretariat</label>
                               <div class="col-sm-4">
                                  <input type="date" class="form-control" required>
                                </div>
                            </div>
                          </div>
                          <div class="i-checks">
                            <input id="ICCNBradio3" type="radio" value="" name="levelofapproval" class="form-control-custom radio-custom">
                            <label for="ICCNBradio3">ICC-TB Endorsed</label>
                            <div id="doa" style="display:none">
                               <label for="doa">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="month" class="form-control" required>
                                </div>
                            </div>
                          </div>
                          <div class="i-checks">
                            <input id="ICCNBradio4" type="radio" value="" name="levelofapproval" class="form-control-custom radio-custom">
                            <label for="ICCNBradio4">ICC-CC Approved</label>
                            <div id="doa2" style="display:none">
                               <label for="doa2">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="month" class="form-control" required>
                                </div>
                            </div>
                          </div>
                          <div class="i-checks">
                            <input id="ICCNBradio5" type="radio" value="" name="levelofapproval" class="form-control-custom radio-custom">
                            <label for="ICCNBradio5">NEDA Board Confirmed</label>
                            <div id="doa3" style="display:none">
                               <label for="doa3">Date of Approval</label>
                               <div class="col-sm-4">
                                  <input type="month" class="form-control" required>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <script src="http://code.jquery.com/jquery-latest.js"></script>
                    <script type="text/javascript">
                      $(document).ready(function () {
                          $('#ICCNBradio1').click(function () {
                              $('#tdos').show('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio2').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').show('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio3').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').show('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio4').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').show('fast');
                              $('#doa3').hide('fast');
                          });
                          $('#ICCNBradio5').click(function () {
                              $('#tdos').hide('fast');
                              $('#dostns').hide('fast');
                              $('#doa').hide('fast');
                              $('#doa2').hide('fast');
                              $('#doa3').show('fast');
                          });
                      });
                    </script>
                  </form>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script type="text/javascript">
        function iccCheckboxFunction() {
        var checkBox = document.getElementById("ICCNBcheckbox");
        var text = document.getElementById("icccheckboxes");
        if (checkBox.checked == true){
          text.style.display = "block";
        } else {
          text.style.display = "none";
        }
      }
      </script>

      <section class="bg-primary" id="piwpd">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project for Inclusion in Which Programming Document</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <input id="PIPcheckbox" type="checkbox" value="" checked disabled class="form-control-custom">
                          <label for="PIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains priority programs and projects to be implemented by NG, GOCC, GFI and other national government offices that are responsive to the PDP and RM">Public Investment Program (PIP)</label>
                          @foreach ($piptypologies as $piptypology)
                        <div class="i-checks">
                          <input id="radioCustomPIPtypo{{ $piptypology->id }}" type="radio" value="{{ $piptypology->id }}" name="a" class="form-control-custom radio-custom">
                          <label for="radioCustomPIPtypo{{ $piptypology->id }}">{{ $piptypology->typologies }}</label>
                        </div>
                        @endforeach
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <input id="CIPcheckbox" type="checkbox" value="" class="form-control-custom" onclick="CIPCheckboxFunction()">
                          <label for="CIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains the big ticket programs and projects of the PIP that serves as pipeline for the NEDA Board and Investment Coordination Committee (ICC)">Core Investment Programs/Projects (CIP)</label>
                          <div id="cipcheckboxes" style="display:none" >
                          @foreach ($ciptypologies as $ciptypology)
                          <div class="i-checks">
                          <input id="radioCustomCIPtypo{{ $ciptypology->id }}" type="radio" value="{{ $ciptypology->id }}" name="b" class="form-control-custom radio-custom"><label for="radioCustomCIPtypo{{ $ciptypology->id }}">{{ $ciptypology->typologies }}</label>
                          </div>
                          @endforeach
                          </div>
                        </div>
                      </div>
                      <script type="text/javascript">
                        function CIPCheckboxFunction() {
                        var checkBox = document.getElementById("CIPcheckbox");
                        var text = document.getElementById("cipcheckboxes");
                        var text2 = document.getElementById("gad");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                          text2.style.display = "block";
                        } else {
                          text.style.display = "none";
                          text2.style.display = "none";
                        }
                      }
                      </script>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6">
                          <div class="i-checks">
                            <input id="TRIPcheckbox" type="checkbox" value="" class="form-control-custom" onclick="TRIPCheckboxFunction()">
                            <label for="TRIPcheckbox" data-toggle="tooltip" data-placement="top" title="Contains nationally funded infrastructure projects irrespective of cost and financing with emphasis on immediate priorities to be undertaken in three-year periods">Three-year Rolling Infrastructure Program (TRIP)</label>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="i-checks">
                            <input id="RDIPcheckbox" type="checkbox" value="" class="form-control-custom" onclick="RDIPCheckboxFunction()">
                            <label for="RDIPcheckbox">Is the Program/Project included in the RDIP?</label>
                          </div>
                          <div class="i-checks" style="display:none" id="rdipcheckboxes">
                            <input id="RDIPcheckbox1" type="checkbox" value="" class="form-control-custom" onclick="RDIPCheckboxFunction2()">
                            <label for="RDIPcheckbox1">Will require Regional Development Council (RDC) Endorsement?</label>
                          </div>
                          <div style="display:none" id="rdipcheckboxes2">
                            <div class="i-checks">
                              <input id="RDCEndorseed1" type="radio" value="" name="d" class="form-control-custom radio-custom">
                              <label for="RDCEndorseed1">Endorsed</label><br/>
                              <div id="RDCEndorseedDate" style="display:none">
                              <label for="RDCEndorseedDate">Date of Endorsement</label>
                              <div class="col-sm-4">
                                  <input type="date" class="form-control" required>
                              </div>
                              </div>
                              
                            </div>
                            <div class="i-checks">
                              <input id="RDCEndorseed2" type="radio" value="" name="d" class="form-control-custom radio-custom">
                              <label for="RDCEndorseed2">Yet to be Endorsed</label>
                            </div>
                          </div>
                      </div>
                      <script type="text/javascript">
                        function TRIPCheckboxFunction() {
                        var checkBox = document.getElementById("TRIPcheckbox");
                        var text = document.getElementById("tripdiv");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                        function RDIPCheckboxFunction() {
                        var checkBox = document.getElementById("RDIPcheckbox");
                        var text = document.getElementById("rdipcheckboxes");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      function RDIPCheckboxFunction2() {
                        var checkBox = document.getElementById("RDIPcheckbox1");
                        var text = document.getElementById("rdipcheckboxes2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }

                      $(document).ready(function () {
                          $('#RDCEndorseed1').click(function () {
                              $('#RDCEndorseedDate').show('fast');
                          });
                          $('#RDCEndorseed2').click(function () {
                              $('#RDCEndorseedDate').hide('fast');
                          });
                        });
                      </script>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>
      </section>

      <div class="row" style="display: none;" id="tripdiv">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Three-year Rolling Infrastructure Program</h4>
                </div>
                <div class="card-body">
                  <form class="form-horizontal">
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Infrastructure Sector </label>
                          @foreach ($sectors as $sector)
                        <div class="i-checks">
                          <input id="checkboxCustomSector{{ $sector->id }}" type="checkbox" value="{{ $sector->sector }}" class="form-control-custom" onclick="InfraCheckboxFunction()">
                          <label for="checkboxCustomSector{{ $sector->id }}">{{ $sector->sector }}</label>
                          <div style="display: none;" id="mothersector{{ $sector->id }}">
                          @foreach ($subsectors as $subsector)
                          @if ($sector->id == $subsector->sector)
                          <div class="i-checks">
                            &nbsp;&nbsp;&nbsp;<input id="checkboxCustomSubSector{{ $subsector->id }}" type="checkbox" value="{{ $subsector->subsector }}" class="form-control-custom">
                            <label for="checkboxCustomSubSector{{ $subsector->id }}">{{ $subsector->subsector }}</label>
                          </div>
                          @endif
                          @endforeach
                          </div>
                        </div>
                        @endforeach
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="i-checks">
                          <label class="form-control-label">Status of Implementation Readiness</label>
                          @foreach ($statuses as $status)
                        <div class="i-checks">
                          <input id="checkboxCustomStatus{{ $status->id }}" type="checkbox" value="{{ $status->statuses }}" class="form-control-custom">
                          <label for="checkboxCustomStatus{{ $status->id }}">{{ $status->statuses }}</label>
                        </div>
                        @endforeach
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Implementation Risks and Mitigation Strategies</label>
                      <div class="col-sm-10">
                          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>

      <script type="text/javascript">
        function InfraCheckboxFunction() {
          if (checkboxCustomSector1.checked == true){
            mothersector1.style.display = "block";
          } else {
            mothersector1.style.display = "none";
          }

          if (checkboxCustomSector3.checked == true){
            mothersector3.style.display = "block";
          } else {
            mothersector3.style.display = "none";
          }

          if (checkboxCustomSector4.checked == true){
            mothersector4.style.display = "block";
          } else {
            mothersector4.style.display = "none";
          }

          if (checkboxCustomSector6.checked == true){
            mothersector6.style.display = "block";
          } else {
            mothersector6.style.display = "none";
          }
        }
      </script>

      <section class="bg-primary" id="pdp">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Philippine Development Plan (PDP) Chapter</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main PDP Chapter </label>
                      <div class="col-sm-10">
                        <select name="mainpdp" class="form-control" onchange="java_script_:show5(this.options[this.selectedIndex].value)">
                          <option disabled selected>Choose Chapter</option>
                          @foreach ($chapters as $chapter)
                          <option value="{{ $chapter->chap_no }}">{{ $chapter->chap_description }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Other PDP Chapters <br><small class="text-primary">Select as many as applicable</small></label>
                      <div class="col-sm-10">
                      <div class="form-control" onclick="show_proj_co_agency()">
                        <label id="proj_co_agency_output"></label>
                      </div>
                      <div class="col-sm-10 scroll-div-coagency" id="proj_co_agency" name="proj_co_agency">
                        @foreach ($chapters as $chapter)
                        <input type='checkbox' id='checkChapter{{ $chapter->chap_no }}' name='coimpagency[]' value="{{ $chapter->chap_no }}" class="form-control-custom checkbox-custom">
                        <label for="checkChapter{{ $chapter->chap_no }}">{{ $chapter->chap_description }}</label>
                        @endforeach
                      </div>
                    </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script type="text/javascript">
        var expanded = false;
        function show_proj_co_agency() {
          var checkboxes = document.getElementById("proj_co_agency");
          if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
          } else {
            checkboxes.style.display = "none";
            expanded = false;
          }
        }
        $("#proj_co_agency input").click(function () {
        $("#proj_co_agency_output").text($("#proj_co_agency input:checked").map(function () {
          return $(this).val();
        }).get().join());
      }).click().click();

      </script>

      <section class="bg-primary" id="ag">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>0-10 Point Socioeconomic Agenda <br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                          @foreach ($agendas as $agenda)
                          <div class="i-checks">
                          <input id="checkCustomAgenda{{ $agenda->id }}" type="checkbox" value="{{ $agenda->id }}" name="agenda" class="form-control-custom checkbox-custom"><label for="checkCustomAgenda{{ $agenda->id }}">{{ $agenda->agenda }}</label>
                          </div>
                          @endforeach
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="sdgs">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Sustainable Development Goals (SDG) <br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                        
                          @foreach ($goals as $goal)
                          <div class="i-checks">
                          <input id="checkCustomGoals{{ $goal->goal_id }}" type="checkbox" value="{{ $goal->goal_id }}" name="sdg" class="form-control-custom check-custom"><label for="checkCustomGoals{{ $goal->goal_id }}">{{ $goal->goal_description }}</label>
                          </div>
                          @endforeach

                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>
      
      <div class="row" style="display: none;" id="gad">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Level of Gender Responsiveness <br></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <div class="col-sm-10">
                          @foreach ($levels as $level)
                          <div class="i-checks">
                          <input id="radioCustomLevel{{ $level->level_id }}" type="radio" value="{{ $level->level_id }}" name="level" class="form-control-custom radio-custom"><label for="radioCustomLevel{{ $level->level_id }}">{{ $level->level_description }}</label>
                          </div>
                          @endforeach
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>

      <section class="bg-primary" id="rms">
      @for ($i = 5; $i <= 20; $i++)
        <div class="row" style="display: none;" id="ResultsChapter{{ $i }}">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>PDP Chapter {{ $i }} Results Matrices Outcome Statements <br><small class="text-primary">Select as many as applicable</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-10">
                        @foreach ($_1matrices as $_1matrice)
                          @if ($_1matrice->chapter == $i)
                          <div class="i-checks">
                          <input id="checkCustom1RM{{ $_1matrice->id }}" type="checkbox" value="{{ $_1matrice->id }}" name="1rm" class="form-control-custom check-custom"><label for="checkCustom1RM{{ $_1matrice->id }}">{{ $_1matrice->chapter_outcome }}</label>
                          </div>
                          @foreach ($_2matrices as $_2matrice)
                            @if ($_2matrice->chapter_outcome_id == $_1matrice->id)
                            <div class="i-checks">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom2RM{{ $_2matrice->id }}" type="checkbox" value="{{ $_2matrice->id }}" name="2rm" class="form-control-custom check-custom"><label for="checkCustom2RM{{ $_2matrice->id }}">{{ $_2matrice->intermediate_outcome }}</label>
                            </div>
                            @foreach ($_3matrices as $_3matrice)
                              @if ($_3matrice->intermediate_outcome_id == $_2matrice->id)
                              <div class="i-checks">
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom3RM{{ $_3matrice->id }}" type="checkbox" value="{{ $_3matrice->id }}" name="3rm" class="form-control-custom check-custom"><label for="checkCustom3RM{{ $_3matrice->id }}">{{ $_3matrice->intermediate_outcome }}</label>
                              </div>
                                @foreach ($_4matrices as $_4matrice)
                                @if ($_4matrice->intermediate_outcome_id == $_3matrice->id)
                                <div class="i-checks">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="checkCustom4RM{{ $_4matrice->id }}" type="checkbox" value="{{ $_4matrice->id }}" name="4rm" class="form-control-custom check-custom"><label for="checkCustom4RM{{ $_4matrice->id }}">{{ $_4matrice->intermediate_outcome }}</label>
                                </div>
                                @endif
                                @endforeach
                              @endif
                            @endforeach
                            @endif
                          @endforeach
                          @endif
                        @endforeach
                        </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      @endfor
      </section>

      <section class="bg-primary" id="ip">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Implementation Period</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Start</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" required>
                      </div>
                      <label class="col-sm-2 form-control-label">End</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" required>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="ppd">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Project Preparation Details</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Project Preparation Document </label>
                      <div class="col-sm-10">
                        <select id="projdocument" name="projdocument" class="form-control" onchange="java_script_:show2(this.options[this.selectedIndex].value)">
                          <option disabled selected>Choose Project Document</option>
                          @foreach ($projectdocuments as $projectdocument)
                          <option value="{{ $projectdocument->id }}">{{ $projectdocument->projectdocument }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="fs" style="display: none;">
                      <label class="col-sm-12 form-control-label">Schedule of F/S Cost (In Exact Amount in PhP) </label>
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group row" id="others" style="display: none;">
                      <label class="col-sm-2 form-control-label">Others</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" required>
                      </div>                    
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function show2(projdocument) {
          if (projdocument == "1") {
            fs.style.display='block';
            others.style.display='none';
          } else if (projdocument == "5") {
            fs.style.display='none';
            others.style.display='flex';
          } else {
            fs.style.display='none';
            others.style.display='none';
          }
        }
      </script>

      <section class="bg-primary" id="pcc">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Pre-construction Costs</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="ROWA1" type="checkbox" value="" class="form-control-custom" onclick="ROWAComponentCheckbox()">
                            <label for="ROWA1">With ROWA Component?</label>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement" style="display: none;">
                      <label class="col-sm-12 form-control-label"><strong>ROWA Requirement </strong>Schedule of ROWA Cost (In Exact Amount in PhP) </label>
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="table-responsive">
                                <table class="table">
                                  <thead>
                                    <tr>
                                      <th>2017</th>
                                      <th>2018</th>
                                      <th>2019</th>
                                      <th>2020</th>
                                      <th>2021</th>
                                      <th>2022</th>
                                      <th>Total</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                      <td>
                                        <input type="text" placeholder=".col-md-2" class="form-control">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="text" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox() {
                        var checkBox = document.getElementById("ROWA1");
                        var text = document.getElementById("rowarequirement");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="ROWA2" type="checkbox" value="" class="form-control-custom" onclick="ROWAComponentCheckbox2()">
                            <label for="ROWA2">With Resettlement Component?</label>
                          </div>
                        </div>
                    </div>
                    <div id="rowarequirement2" style="display: none;">
                    <label class="col-sm-12 form-control-label"><strong>With Resettlement Component </strong>Schedule of Resettlement Cost (In Exact Amount in PhP) </label>
                    <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of household affected</label>
                        <div class="col-sm-2">
                        <input type="text" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <script type="text/javascript">
                        function ROWAComponentCheckbox2() {
                        var checkBox = document.getElementById("ROWA2");
                        var text = document.getElementById("rowarequirement2");
                        if (checkBox.checked == true){
                          text.style.display = "block";
                        } else {
                          text.style.display = "none";
                        }
                      }
                    </script>
                    <div class="form-group row">
                        <div class="col-sm-12">
                          <div class="i-checks">
                            <input id="ROWA3" type="checkbox" value="" class="form-control-custom">
                            <label for="ROWA3">With ROWA and Resettlement Action Plan?</label>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="eg">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Employment Generation<br><small class="text-primary">Please indicate the no. Of persons to be employed by the project outside of the implementing agency</small></h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">No. of persons to be employed:</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" required>
                      </div>
                    </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="it">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Investment Targets<br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                  <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Financing Source</th>
                                    <th>2016 and Prior</th>
                                    <th>2017</th>
                                    <th>2018</th>
                                    <th>2019</th>
                                    <th>2020</th>
                                    <th>2021</th>
                                    <th>2022</th>
                                    <th>2017-2022</th>
                                    <th>2023</th>
                                    <th>Overall Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      NG - Local
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Loan
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      NG - ODA Grant
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      GOCC/GFIs
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      LGUs
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Private Sector
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Others
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                    <th>0.00</th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
        </div>
      </div>
      </section>

      <section class="bg-primary" id="fss">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Funding Source</h4>
                </div>
                <div class="card-body">
                  <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Main Funding Source</label>
                      <div class="col-sm-4">
                        <select id="mainfundingsource" name="mainfundingsource" class="form-control" onchange="java_script_:show3(this.options[this.selectedIndex].value)">
                          <option disabled selected>Choose Project Document</option>
                          @foreach ($fundingsources as $fundingsource)
                          <option value="{{ $fundingsource->fsource_code }}">{{ $fundingsource->fsource_description }}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">Mode of Implementation/Procurement</label>
                      <div class="col-sm-4">
                        <select id="mode" name="mode" class="form-control" onchange="java_script_:show4(this.options[this.selectedIndex].value)">
                          <option disabled selected>Choose Mode of Implementation</option>
                          @foreach ($modes as $mode)
                          <option value="{{ $mode->mode_code }}">{{ $mode->mode_description }}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-2 form-control-label" style="display: none;" id="oda2">ODA Funding Institutions</label>
                      <div class="col-sm-4" style="height: 12em; width: 40em; overflow: auto;display: none;" id="oda">
                        @foreach ($fundings as $funding)
                          <div class="i-checks">
                          <input id="checkFS{{ $funding->fsource_no }}" type="checkbox" value="{{ $funding->fsource_no }}" name="oda" class="form-control-custom check-custom"><label for="checkFS{{ $funding->fsource_no }}">{{ $funding->fsource_description }}</label>
                          </div>
                          @endforeach
                      </div>
                      <label class="col-sm-2 form-control-label" style="display: none;" id="others_mode2">Other Mode</label>
                      <div class="col-sm-4" style="display: none;" id="others_mode">
                        <input type="text" class="form-control" required>
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script>
        function show3(mainfundingsource) {
          if (mainfundingsource == "ODA") {
            oda.style.display='block';
            oda2.style.display='block';
          } else {
            oda.style.display='none';
            oda2.style.display='none';
          }
        }

      function show4(mode) {
          if (mode == "Other-Mode") {
            others_mode.style.display='block';
            others_mode2.style.display='block';
          } else {
            others_mode.style.display='none';
            others_mode2.style.display='none';
          }
        }
      </script>

      <section class="bg-primary" id="pa">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Physical Accomplishments</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Categorization </label>
                      <div class="col-sm-10">
                        <div class="i-checks">
                          <input id="Tier1" type="radio" value="option1" name="tier" class="form-control-custom radio-custom" onclick="tier2Radios()">
                          <label for="Tier1" data-toggle="tooltip" data-placement="top" title="">Tier 1 (Ongoing)  </label>
                          <div style="display:none" id="uacs">
                          <input type="text" placeholder=".col-md-2" class="form-control">
                          </div>
                        </div>
                        <div class="i-checks">
                          <input id="Tier2" type="radio" value="option2" name="tier" class="form-control-custom radio-custom" onclick="tier2Radios()">
                          <label for="Tier2" data-toggle="tooltip" data-placement="top" title="">Tier 2 (New and Expanded) </label>
                        </div>
                        <div style="display:none" id="tier2s">
                        <div class="i-checks">
                          <input id="Tier2a" type="radio" value="option2" name="tier2" class="form-control-custom radio-custom">
                          <label for="Tier2a" data-toggle="tooltip" data-placement="top" title="">Tier 2 - New </label>
                        </div>
                        <div class="i-checks">
                          <input id="Tier2b" type="radio" value="option2" name="tier2" class="form-control-custom radio-custom">
                          <label for="Tier2b" data-toggle="tooltip" data-placement="top" title="">Tier 2 - Expanded </label>
                          <div style="display:none" id="uacs2">
                          <input type="text" placeholder=".col-md-2" class="form-control">
                          </div>
                        </div>
                      </div>
                      </div>
                      
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label">Updates </label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                      </div>
                      <label class="col-sm-2 form-control-label">Remarks from the Agency </label>
                      <div class="col-sm-4">
                          <textarea class="form-control" rows="5" id="comment" name="text"></textarea>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 form-control-label" style="display:none" id="tier2cat">Tier 2 - Status</label>
                      <div class="col-sm-4" style="display:none" id="tier2cat2">
                        <select name="account" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                        </select>
                      </div>
                      <label class="col-sm-2 form-control-label">As of</label>
                      <div class="col-sm-4">
                        <input type="date" placeholder=".col-md-2" class="form-control">
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <script type="text/javascript">
                      $(document).ready(function () {
                      $('#Tier2').click(function () {
                              $('#tier2s').show('fast');
                              $('#tier2cat').show('fast');
                              $('#tier2cat2').show('fast');
                              $('#uacs').hide('fast');
                          });
                      $('#Tier1').click(function () {
                              $('#tier2s').hide('fast');
                              $('#tier2cat').hide('fast');
                              $('#tier2cat2').hide('fast');
                              $('#uacs').show('fast');
                          });
                      $('#Tier2b').click(function () {
                              $('#uacs2').show('fast');
                          });
                      $('#Tier2a').click(function () {
                              $('#uacs2').hide('fast');
                          });
                      });
                      </script>

      <section class="bg-primary" id="fa">
      <div class="row">
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Financial Accomplishments<br><small class="text-primary">In exact amount in PhP</small></h4>
                </div>
                <div class="card-body">
                  <div class="row">
                      <div class="col-lg-12">
                        <div class="card">
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th>Year</th>
                                    <th>Amount for Inclusion in the NEP</th>
                                    <th>Amount Allocated in the Budget/GAA</th>
                                    <th>Amount Obligated By the Agency for the Program/Project</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>
                                      2017
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2018
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                     2019
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2020
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2021
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      2022
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                    <td>
                                      <input type="text" placeholder=".col-md-2" class="form-control">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>
                                      Total
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                    <td>
                                      0.00
                                    </td>
                                  </tr>
                                  
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
      </section>

      <div class="row" id=menu>
        <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h4>Finalize</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row" align="center">
                      <div class="col-sm-4 offset-sm-2">
                        <button type="submit" class="btn btn-secondary btn-sm">Save as Reviewed</button>
                        <button type="submit" class="btn btn-primary btn-sm">Save as Validated</button>
                        <button type="button" class="btn btn-secondary btn-sm"><a class="js-scroll-trigger" href="#forms" style="color:white;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Back on Top&nbsp;&nbsp;&nbsp;&nbsp;</a></button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
        </div>
      </div>
    </div>
  </section>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/scrollreveal/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('/js/creative.min.js') }}"></script>

</form>

@endsection