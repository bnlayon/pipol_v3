@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Users </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">User Management </h1>
      </header>
    </div>

    <div class="panel-body">
      <ul class="nav nav-pills nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#all" role="tab">Approved Agency Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#req" role="tab">User Requests</a>
            </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content tablewrapper">

            <div class="tab-pane" id="all" role="tabpanel" style="background-color: white">
              <table id="userstable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Username</th>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Agency</th>
                      <th>Designation</th>
                      <th>Telephone Number</th>
                      <th>Fax Number</th>
                      <th>Email Address</th>
                      <th>Date</th>
                      <th>Activate</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($app_userrequests as $app_userrequest)
                  <tr>
                    <td>{{ $app_userrequest->username }}</td>
                    <td>{{ $app_userrequest->lname }}</td>
                    <td>{{ $app_userrequest->fname }}</td>
                    <td>
                        @foreach ($app_userrequest->getSubmissiondetails($app_userrequest->submission_id) as $submission)
                          @foreach ($app_userrequest->getAgencyDetails($submission->agency_id) as $agencydetail)
                            {{ $agencydetail->UACS_AGY_DSC }}
                          @endforeach
                        @endforeach
                    </td>
                    <td>{{ $app_userrequest->designation }}</td>
                    <td>{{ $app_userrequest->telnumber }}</td>
                    <td>{{ $app_userrequest->faxnumber }}</td>
                    <td>{{ $app_userrequest->email }}</td>
                    <td>{{ $app_userrequest->created_at }}</td>
                    <td align="center">
                      <a href="{{ asset('storage/storage/registration/') }}/{{$submission->authorization_form}}" target="_blank" ><button type="button" class="btn btn-info"><i class="fa fa-search"></i></button></a>
                      @if($app_userrequest->status == 2)
                      <form action="{{ asset('/deactivate') }}/{{$app_userrequest->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                      <button type="submit" class="btn btn-danger">Deactivate</button>
                      </form>
                      @elseif($app_userrequest->status == 1)
                      <form action="{{ asset('/reactivate') }}/{{$app_userrequest->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                      <button type="submit" class="btn btn-info">Activate</button>
                      </form>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                  <tr>  
                      <th>Username</th>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Agency</th>
                      <th>Designation</th>
                      <th>Telephone Number</th>
                      <th>Fax Number</th>
                      <th>Email Address</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
              </tfoot>
              </table>
            </div>
            
            <div class="tab-pane active" id="req" role="tabpanel" style="background-color: white">
              <table id="usersreqtable" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Email Address</th>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Agency</th>
                      <th>Designation</th>
                      <th>Telephone Number</th>
                      <th>Fax Number</th>
                      <th>User Number</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody>   
                    @foreach ($userrequests as $userrequest)
                    <tr>
                      <td>{{ $userrequest->email }}</td>
                      <td>{{ $userrequest->lname }}</td>
                      <td>{{ $userrequest->fname }}</td>
                      <td>  
                        @foreach ($userrequest->getSubmissiondetails($userrequest->submission_id) as $submission)
                          @foreach ($userrequest->getAgencyDetails($submission->agency_id) as $agencydetail)
                            {{ $agencydetail->UACS_AGY_DSC }}
                          @endforeach
                        @endforeach
                      </td>
                      <td>{{ $userrequest->designation }}</td>
                      <td>{{ $userrequest->telnumber }}</td>
                      <td>{{ $userrequest->faxnumber }}</td>
                      <td>{{ $userrequest->focal_no }}
                        <?php $idtoapprove = $userrequest->id ?></td>
                      <td>
                        <a href="{{ asset('storage/storage/registration/') }}/{{$submission->authorization_form}}" target="_blank" ><button type="button" class="btn btn-info"><i class="fa fa-search"></i></button></a>
                        <form action="{{ asset('/activate') }}/{{$userrequest->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                        <button type="button" data-toggle="modal" data-target="#approveaccount<?php echo $idtoapprove; ?>" class="btn btn-primary"><i class="fa fa-check"></i></button> 
                        <div id="approveaccount<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                          <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="exampleModalLabel" class="modal-title">Approve Account Request</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <h5>Are you sure you want to approve this account?</h5>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        </form>

                        <form action="{{ asset('/decline') }}/{{$userrequest->id}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                        <button type="button" data-toggle="modal" data-target="#declineremarks<?php echo $idtoapprove; ?>" class="btn btn-danger"><i class="fa fa-close"></i></button> 
                        <div id="declineremarks<?php echo $idtoapprove; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php echo $idtoapprove; ?>" aria-hidden="true" class="modal fade text-left">
                          <div role="document" class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 id="exampleModalLabel<?php echo $idtoapprove; ?>" class="modal-title">Decline Account Request</h5>
                                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="card">
                                      <div class="card-body">
                                        <div class="i-checks">
                                          <input id="reason1<?php echo $idtoapprove; ?>" type="radio" value="The authorization form uploaded in the PIPOL System Online Sign-up Sheet is not duly signed by the Head of your Agency.and/or Mother Agency" name="reason" class="form-control-custom radio-custom">
                                          <label for="reason1<?php echo $idtoapprove; ?>">The authorization form uploaded in the PIPOL System Online Sign-up Sheet is not duly signed by the Head of your Agency.and/or Mother Agency. </label>
                                        </div>
                                        <div class="i-checks">
                                          <input id="reason2<?php echo $idtoapprove; ?>" type="radio" value="The file uploaded in the PIP System Online Sign-up Sheet does not contain the prescribed authorization form. Please download the Authorization Form through this link" name="reason" class="form-control-custom radio-custom">
                                          <label for="reason2<?php echo $idtoapprove; ?>">The file uploaded in the PIP System Online Sign-up Sheet does not contain the prescribed authorization form. Please download the Authorization Form through this link. </label>
                                        </div><br/>
                                        <div class="i-checks">
                                          <input id="reason3<?php echo $idtoapprove; ?>" type="radio" value="Others" name="reason" class="form-control-custom radio-custom">
                                          <label for="reason3<?php echo $idtoapprove; ?>">Others </label>
                                        </div>
                                        <div class="col-sm-12">
                                          <textarea class="form-control" rows="5" id="comment" name="reason_others"></textarea>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Submit</button>  
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        </form>
                      </td>
                    </tr>
                    @endforeach
              </tbody>
              <tfoot>
                  <tr>  
                      <th>Username</th>
                      <th>Last Name</th>
                      <th>First Name</th>
                      <th>Agency</th>
                      <th>Designation</th>
                      <th>Telephone Number</th>
                      <th>Fax Number</th>
                      <th>Email Address</th>
                      <th>Action</th>
                  </tr>
              </tfoot>
              </table>
            </div>      
      </div>
    </div>

  </section>


@endsection