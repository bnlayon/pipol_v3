<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')

  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item">Validate Projects </li>
        <li class="breadcrumb-item active">TRIP Prioritization </li>
      </ul>
    </div>
  </div>

  <section class="forms">
      <div class="container-fluid">
        <header> 
          <h1 class="h3 display">TRIP Prioritization - Category 2</h1>
        </header>

        <div class="panel-body">
          <ul class="nav nav-pills nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#trip2" role="tab">Category 2</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content tablewrapper">
            <div class="tab-pane active" id="trip2" role="tabpanel" style="background-color: white">
              <br/>
              <table id="tripTableCat2" class="table table-striped row-border order-column " cellspacing="0" style="width:100%; background-color: white">
              <thead>
                  <tr>
                      <th>Project Title</th>
                      <th>Mother Agency</th>
                      <th>Attached Agency</th>
                      <th>Spatial Coverage</th>
                      <th>Sector</th>
                      <th>Score</th>

                      <th>Contributes to identified gaps to achieve development targets</th>
                      <th>Readiness</th>

                      <th>Immediate/short-term</th>
                      <th>Medium-term</th>
                      <th>Long-term</th>
                      <th>Not anchored on a programmatic approach</th>

                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($alltrips as $alltrip)
                 <tr>
                      <td>{{$alltrip->title}}</td>
                      <td>
                         @if (empty($alltrip->motheragency_id))
                        @foreach ($alltrip->getAgencyDetails($alltrip->agency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @else
                        @foreach ($alltrip->getAgencyDetails($alltrip->motheragency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @endif
                      </td>
                      <td>
                        @if (empty($alltrip->motheragency_id))

                      @else
                        @foreach ($alltrip->getAgencyDetails($alltrip->agency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @endif
                      </td>
                      <td>{{$alltrip->spatial}}</td>
                      <td>
                        @foreach ($alltrip->getSectorProjects($alltrip->id) as $sectorDetail)
                        @if($sectorDetail->sector_id == 1)
                          Social Infrastructure
                        @elseif($sectorDetail->sector_id == 2)
                          Power - Electrification
                        @elseif($sectorDetail->sector_id == 3)
                          Transportation
                        @elseif($sectorDetail->sector_id == 4)
                          Water Resources
                        @elseif($sectorDetail->sector_id == 5)
                          Information and Communications Technology
                        @elseif($sectorDetail->sector_id == 6)
                          Others
                        @else
                        @endif
                      @endforeach
                      </td>
                      <td>@foreach ($alltrip->getScores2($alltrip->id) as $sectorDetail)
                          {{$sectorDetail->criteria3}}
                        @endforeach</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                  </tr>
                @endforeach
              </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </section>



@endsection