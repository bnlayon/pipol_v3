<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PIPOL v2.0</title>
    <style>
    #menu {
      position: fixed;
      right: 0;
      top: 50%;
      width: 15em;  
      margin-top: -2.5em;
    }
    #menuButton {
      position: fixed;
      padding-left: 180px; 
      right: 0;
      top: 50%;
      width: 15em;  
      margin-top: -2.5em;
    }
    .select2-search--inline input {
    width: 700px !important;
    }
    .select2-selection__rendered {width: 700px !important}
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script src="{{ asset('js/sweealert2.all.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/fontastic.css') }}">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/own.css') }}">
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700"> --}}
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.default.css') }}" id="theme-stylesheet">
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  </head>
  <body>
    <div class="se-pre-con"></div>
    @include ('sidebar')
    <div class="page">
      @include ('header')
      @yield ('content')
      {{-- @yield('scripts') --}}
      @include ('footer')
      {{-- @include('sweet::alert') --}}
      {{-- @yield('c0d3w4rrior89-1') --}}
      {{-- @yield('jQuery2') --}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    {{-- <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/select2.min.js') }}"></script>  --}}
    {{-- <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"> </script> --}}
    {{-- <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"> </script> --}}
    {{-- <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/front.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/own.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/kc.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}" type="text/css"> --}}

    {{-- <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.fixedColumns.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script> --}}

    {{-- <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('/vendor/scrollreveal/scrollreveal.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script> --}}
  </body>
</html>