<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')

  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item">Validate Projects </li>
        <li class="breadcrumb-item active">TRIP Prioritization </li>
      </ul>
    </div>
  </div>

  <section class="forms">
      <div class="container-fluid">
        <header> 
          <h1 class="h3 display">TRIP Prioritization - Category 1</h1>
        </header>

        <div class="panel-body">
          <ul class="nav nav-pills nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#trip1" role="tab">Category 1</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content tablewrapper">
            <div class="tab-pane active" id="trip1" role="tabpanel" style="background-color: white">
              <br/>
              <table id="tripTableCat1" class="table table-striped row-border order-column " cellspacing="0" style="width:100%; background-color: white">
              <thead>
                  <tr>
                      <th>Project Title</th>
                      <th>Mother Agency</th>
                      <th>Attached Agency</th>
                      <th>Spatial Coverage</th>
                      <th>Sector</th>
                      <th>Score</th>

                      <th>Contributes to identified gaps to achieve development targets</th>
                      <th>Readiness</th>

                      <th>Institutional/Governance Improvements</th>
                      <th>Climate Resiliency</th>
                      <th>Competitiveness</th>
                      <th>Agricultural Sustainability</th>
                      <th>Basic Infrastructure Facilities/Services</th>
                      <th>Bridging Infrastructure Development Gaps in Identified Areas</th>
                      <th>Safety and Security</th>
                      <th>Compliance with laws/international agreements</th>
                      <th>Rehabilitation /reconstruction</th>

                      <th>Highest magnitude of poor families</th>
                      <th>Highest poverty incidence</th>
                      <th>Exposed and prone to multiple hazard</th>
                      <th>Emerging growth corridor/area or Located in Mindanao</th>

                      <th>Immediate/short-term</th>
                      <th>Medium-term</th>
                      <th>Long-term</th>
                      <th>Not anchored on a programmatic approach</th>

                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
                @foreach ($alltrips as $alltrip)
                                    <tr>
                    <td>{{$alltrip->title}}</td>
                    <td>
                      @if (empty($alltrip->motheragency_id))
                        @foreach ($alltrip->getAgencyDetails($alltrip->agency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @else
                        @foreach ($alltrip->getAgencyDetails($alltrip->motheragency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @endif
                    </td>
                    <td>
                      @if (empty($alltrip->motheragency_id))

                      @else
                        @foreach ($alltrip->getAgencyDetails($alltrip->agency_id) as $agencydetail)
                        {{ $agencydetail->UACS_AGY_DSC }}
                        @endforeach
                      @endif
                    </td>
                    <td>{{$alltrip->spatial}}</td>
                    <td>
                      @foreach ($alltrip->getSectorProjects($alltrip->id) as $sectorDetail)
                        @if($sectorDetail->sector_id == 1)
                          Social Infrastructure
                        @elseif($sectorDetail->sector_id == 2)
                          Power - Electrification
                        @elseif($sectorDetail->sector_id == 3)
                          Transportation
                        @elseif($sectorDetail->sector_id == 4)
                          Water Resources
                        @elseif($sectorDetail->sector_id == 5)
                          Information and Communications Technology
                        @elseif($sectorDetail->sector_id == 6)
                          Others
                        @else
                        @endif
                      @endforeach
                    </td>
                    <td>@foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          {{$sectorDetail->score}}
                        @endforeach</td>
                    <form action="{{ asset('/cat1_save') }}/{{$alltrip->id}}" method="POST" >{{ csrf_field() }}
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria1" id="{{$alltrip->id}}_chbxFL1_1" value="1"
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria1 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria2" id="{{$alltrip->id}}chbxFL1_1" value="1"
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria2 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>

                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria3" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria3 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria4" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria4 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria5" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria5 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria6" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria6 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria7" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria7 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria8" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria8 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria9" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria9 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria10" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria10 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria11" id="{{$alltrip->id}}chbxSL1_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria11 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>

                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria12" id="{{$alltrip->id}}chbxSL2_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria12 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria13" id="{{$alltrip->id}}chbxSL2_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria13 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria14" id="{{$alltrip->id}}chbxSL2_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria14 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample" type="checkbox" name="criteria15" id="{{$alltrip->id}}chbxSL2_1" value="1" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria15 == 1)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample2" type="radio" name="criteria16_{{$alltrip->id}}" id="{{$alltrip->id}}rdBtnSL3_1" name="rdBtnSL3" value="20" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria16 == 20)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample2" type="radio" name="criteria16_{{$alltrip->id}}" id="{{$alltrip->id}}rdBtnSL3_1" name="rdBtnSL3" value="12" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria16 == 12)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample2" type="radio" name="criteria16_{{$alltrip->id}}" id="{{$alltrip->id}}rdBtnSL3_1" name="rdBtnSL3" value="8" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria16 == 8)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>
                    <td class="buttons"> 
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sample2" type="radio" name="criteria16_{{$alltrip->id}}" id="{{$alltrip->id}}rdBtnSL3_1" name="rdBtnSL3" value="5" disabled
                        @foreach ($alltrip->getScores1($alltrip->id) as $sectorDetail)
                          @if($sectorDetail->criteria16 == 5)
                            checked
                          @endif
                        @endforeach>
                      </div>
                    </td>

                    <td align="center">
                      <a href="{{ asset('/editproject') }}/{{$alltrip->id}}" target="_blank"><button type="button" class="btn btn-primary"><i class="fa fa-search"></i></button></a>
                      <button type="submit" class="btn btn-primary" id="btn_cat1"><i class="fa fa-save"></i></button> 
                    </td>
                    </form>
                    <script type="text/javascript">
                      $('input[id={{$alltrip->id}}chbxFL1_'+"1"+']').click(function() {
                        if ($('input[id={{$alltrip->id}}chbxFL1_'+"1"+']:checked').length == $('input[id={{$alltrip->id}}chbxFL1_'+"1"+']').length ){
                            $('input[id={{$alltrip->id}}chbxSL1_'+"1"+']').each(function(){ 
                                this.disabled = false; 
                            });
                            $('input[id={{$alltrip->id}}chbxSL2_'+"1"+']').each(function(){ 
                                this.disabled = false; 
                            });
                            $('input[id={{$alltrip->id}}rdBtnSL3_'+"1"+']').each(function(){ 
                                this.disabled = false; 
                            });
                        } else {
                            $('input[id={{$alltrip->id}}chbxSL1_'+"1"+']').each(function(){ 
                                this.disabled = true; 
                                this.checked = false;
                            });
                            $('input[id={{$alltrip->id}}chbxSL2_'+"1"+']').each(function(){ 
                                this.disabled = true; 
                                this.checked = false;
                            });
                            $('input[id={{$alltrip->id}}rdBtnSL3_'+"1"+']').each(function(){ 
                                this.disabled = true; 
                                this.checked = false;
                            });
                        }
                    });
                    </script> 
                  </tr>
                @endforeach
              </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
  </section>



@endsection