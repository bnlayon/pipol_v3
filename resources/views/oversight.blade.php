<?php ini_set('max_execution_time', 6000); ?>
@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Validated Projects </li>
      </ul>
    </div>
  </div>

    <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Validated PIP/CIP Projects</h1>
      </header>
    </div>

    <div class="panel-body">

      <ul class="nav nav-pills nav-justified" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#pip" role="tab">PIP/CIP</a>
        </li>
      </ul>

      <!-- Tab panes -->
    <div class="tab-content tablewrapper">

      <div class="tab-pane active" id="pip" role="tabpanel" style="background-color: white">
        <br/>
        <div class="table-responsive">
        <table id="oversight_table" class="table" style="width: 100%;">
        <thead>
            <tr>
              <th>ID</th>             
              <th>PIP Code</th>
              <th>Project Title</th>
              <th>Mother Agency</th>
              <th>Spatial Coverage</th>
              <th>Action</th>
            </tr>
        </thead>     
        </table>
        </div>
      </div>
      
    </div>
    </div>

  </section>
@endsection