@extends ('layouts2')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Edit Project</li>
      </ul>
    </div>
  </div>

{{-- 'levels', 'tier1statuses',

  'attagencies_projects',
  'fa_projects',
  'states_projects'
  'provinces', 'cities','agencytype','getIdsOfAttached', 'logs_projects' --}}

  <div id="app">
      <button type="button" data-toggle="modal" data-target="#logs" class="btn btn-secondary btn-xs">Project Logs</button>
      <div id="logs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade number-left">
      <div role="document" class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-ticle">Project Logs</h5>
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Username</th>
                      <th>Activity</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($logs_projects as $logs_project)
                    <tr>
                      <td>{{ $logs_project->created_at }}</td>
                      <td>{{ $logs_project->username }}</td>
                      <td>{{ $logs_project->activity }}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
        </div>
        </div>
      </div>
  </div>
    <editproject 
      :route="'{{ asset('/addproject') }}'"
      :basis="{{$basis_}}"
      @if ($submission != '')
      :submission="{{$submission}}"
      @endif
      @if ($agency_agency != '')
      :agency_agency="{{$agency_agency}}"
      @endif
      @if ($agency_mother != '')
      :agency_mother="{{$agency_mother}}"
      @endif
      @if ($piptypologies != '')
      :piptypologies = "{{$piptypologies}}"
      @endif
      @if ($ciptypologies != '')
      :ciptypologies = "{{$ciptypologies}}"
      @endif
      @if ($sectors != '')
      :sectors = "{{$sectors}}"
      @endif
      @if ($subsectors != '')
      :subsectors = "{{$subsectors}}"
      @endif
      @if ($statuses != '')
      :statuses = "{{$statuses}}"
      @endif
      @if ($chapters != '')
      :chapters = "{{$chapters}}"
      @endif
      @if ($_1matrices != '')
      :_1matrices = "{{$_1matrices}}"
      @endif
      @if ($_2matrices != '')
      :_2matrices = "{{$_2matrices}}"
      @endif
      @if ($_3matrices != '')
      :_3matrices = "{{$_3matrices}}"
      @endif
      @if ($_4matrices != '')
      :_4matrices = "{{$_4matrices}}"
      @endif
      @if ($_1matrices_projects != '')
      :_1matrices_projects = "{{$_1matrices_projects}}"
      @endif
      @if ($_2matrices_projects != '')
      :_2matrices_projects = "{{$_2matrices_projects}}"
      @endif
      @if ($_3matrices_projects != '')
      :_3matrices_projects = "{{$_3matrices_projects}}"
      @endif
      @if ($_4matrices_projects != '')
      :_4matrices_projects = "{{$_4matrices_projects}}"
      @endif
      @if ($agendas != '')
      :agendas = "{{$agendas}}"
      @endif
      @if ($agendas != '')
      :agendas = "{{$agendas}}"
      @endif
      @if ($goals != '')
      :goals = "{{$goals}}"
      @endif
      @if ($projectdocuments != '')
      :projectdocuments = "{{$projectdocuments}}"
      @endif
      @if ($fsstatuses != '')
      :fsstatuses = "{{$fsstatuses}}"
      @endif
      @if ($fundingsources != '')
      :fundingsources = "{{$fundingsources}}"
      @endif
      @if ($modes != ''):projects = "{{$projects}}"
      :modes = "{{$modes}}"
      @endif
      @if ($fundings != '')
      :fundings = "{{$fundings}}"
      @endif
      :projects = "{{$projects}}"
      @if ($basis_projects != '')
      :basis_projects = "{{$basis_projects}}"
      @endif
      @if ($agencies_projects != '')
      :agencies_projects = "{{$agencies_projects}}"
      @endif
      @if ($regions_projects != '')
      :regions_projects = "{{$regions_projects}}"
      @endif
      @if ($province_projects != '')
      :province_projects = "{{$province_projects}}"
      @endif
      @if ($city_projects != '')
      :city_projects = "{{$city_projects}}"
      @endif
      @if ($sector_projects != '')
      :sector_projects = "{{$sector_projects}}"
      @endif
      @if ($subsector_projects != '')
      :subsector_projects = "{{$subsector_projects}}"
      @endif
      @if ($status_projects != '')
      :status_projects = "{{$status_projects}}"
      @endif
      @if ($ic_projects != '')
      :ic_projects = "{{$ic_projects}}"
      @endif
      @if ($tier2statuses != '')
      :tier2statuses = "{{$tier2statuses}}"
      @endif
      @if ($chapters_projects != '')
      :chapters_projects = "{{$chapters_projects}}"
      @endif
      @if ($sdg_projects != '')
      :sdg_projects = "{{$sdg_projects}}"
      @endif
      @if ($agenda_projects != '')
      :agenda_projects = "{{$agenda_projects}}"
      @endif
      @if ($fscost_projects != '')
      :fscost_projects = "{{$fscost_projects}}"
      @endif
      @if ($rowa_projects != '')
      :rowa_projects = "{{$rowa_projects}}"
      @endif
      @if ($rc_projects != '')
      :rc_projects = "{{$rc_projects}}"
      @endif
      @if ($financesourceprojects != '')
      :financesourceprojects = "{{$financesourceprojects}}"
      @endif
      @if ($f_projects != '')
      :f_projects = "{{$f_projects}}"
      @endif
      @if ($it_projects != '')
      :it_projects = "{{$it_projects}}"
      @endif
      @if ($regionalcost != '')
      :regionalcost = "{{$regionalcost}}"
      @endif
      @if ($fa_projects != '')
      :fa_projects = "{{$fa_projects}}"
      @endif
      @if ($attagencies_projects != '')
      :attagencies_projects = "{{$attagencies_projects}}"
      @endif
    ></editproject>
  </div>  

@endsection
@section('scripts')
{{-- <script src="{{ asset('js/c0d3w4rrior.js') }}"></script> --}}
@endsection