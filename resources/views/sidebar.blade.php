    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{ asset('img/neda.jpg') }}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5">{{ auth()->user()->SiderbarName() }}</h2>
            @switch(auth()->user()->user_type)
              @case("AG")
                <span>{{auth()->user()->user_type}} | {{auth()->user()->username}}</span>
              @break
              @default
                <span>{{auth()->user()->user_type}} | {{auth()->user()->username}}</span>
            @endswitch
          </div>
          <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>P</strong><strong class="text-primary">v2</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          <h5 class="sidenav-heading">Main</h5>
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="{{ asset('/dashboard') }}"> <i class="icon-home"></i>Dashboard</a></li>

      @switch(auth()->user()->user_type)
        @case("AG")
          @if(auth()->user()->username == 'BNLayon')
            <li><a href="{{ asset('/addproject') }}"> <i class="icon-form"></i>Add Project</a></li>
          @endif
            <li><a href="{{ asset('/viewprojects') }}"> <i class="icon-list"></i>View/Update My Projects</a></li>
            @if($agencytype->Category == 'HEAD')
            <li><a href="{{ asset('/viewprojectsattached') }}"> <i class="icon-list"></i>View/Update Projects of Attached Agencies</a></li>
            @endif
            <!--<li><a href="#"> <i class="icon-presentation"></i>Report Generation</a></li>-->
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
            <li><a href="{{ asset('/video') }}"> <i class="icon-bill"></i>Video Tutorial</a></li>
          </ul>
        </div>
      @break

      @case("NRO")            
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>Validate Module </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/nationwide') }}"> Nationwide PIP/CIP </a></li>
                <li><a href="{{ asset('/interregional') }}"> Interregional PIP/CIP </a></li>
                <li><a href="{{ asset('/region') }}"> Region Specific PIP/CIP </a></li>
                <li><a href="{{ asset('/appraisal2') }}"> TRIP </a></li>
                <li><a href="{{ asset('/appraisal3') }}"> Dropped </a></li>
                <li><a href="{{ asset('/appraisal4') }}"> Completed </a></li>
                <li><a href="{{ asset('/appraisal5') }}"> Uncategorized </a></li>
              </ul>
            </li>
            <!--<li><a href="/report"> <i class="icon-presentation"></i>Report Generation</a></li>-->
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
            <li><a href="{{ asset('/video') }}"> <i class="icon-bill"></i>Video Tutorial</a></li>
          </ul>
        </div>
      @break

      @case("OS")
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>View Projects </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/oversight') }}"> PIP/CIP </a></li>
                <!-- <li><a href="{{ asset('/appraisal2') }}"> TRIP </a></li> -->
              </ul>
            </li>
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
          </ul>
        </div>
      @break

      @case("SS")
            <li><a href="{{ asset('/addproject1') }}"> <i class="icon-form"></i>Add Project</a></li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>Validate Module </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/appraisal') }}"> PIP/CIP </a></li>
                <li><a href="{{ asset('/cip') }}"> CIP </a></li>
                <li><a href="{{ asset('/appraisal2') }}"> TRIP </a></li>
                <li><a href="{{ asset('/appraisal3') }}"> Dropped </a></li>
                <li><a href="{{ asset('/appraisal4') }}"> Completed </a></li>
                <li><a href="{{ asset('/appraisal5') }}"> Uncategorized </a></li>
                <li><a href="{{ asset('/drafts') }}">Draft Projects</a></li>
              </ul>
            </li>
            <!-- <li><a href="{{ asset('/reclassification') }}"> <i class="icon-presentation"></i>Reclassify Projects</a></li> -->
           
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
          </ul>
        </div>
      @break

      @case("PIS")
            <li><a href="{{ asset('/addproject1') }}"> <i class="icon-form"></i>Add Project</a></li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>Validate Module </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/appraisal') }}"> PIP/CIP </a></li>
                <li><a href="{{ asset('/cip') }}"> CIP </a></li>
                <li><a href="{{ asset('/appraisal2') }}"> TRIP </a></li>
                <li><a href="{{ asset('/appraisal3') }}"> Dropped </a></li>
                <li><a href="{{ asset('/appraisal4') }}"> Completed </a></li>
                <li><a href="{{ asset('/appraisal5') }}"> Uncategorized </a></li>
                <li><a href="{{ asset('/drafts') }}">Draft Projects</a></li>
              </ul>
            </li>
            <li><a href="{{ asset('/reclassification') }}"> <i class="icon-presentation"></i>Reclassify Projects</a></li>
            
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
          </ul>
        </div>
        <div class="admin-menu">
          <h5 class="sidenav-heading">Admin</h5>
          <ul id="side-admin-menu" class="side-menu list-unstyled"> 
            <li> <a href="{{ asset('/users') }}"> <i class="icon-user"> </i>Users</a></li>
            <li> <a href="#"> <i class="icon-grid"> </i>Reference Tables</a></li>
          </ul>
        </div>
      </div>
      @break

      @case("IS")
            <li><a href="{{ asset('/addproject') }}"> <i class="icon-form"></i>Add Project</a></li>            
            
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>Validate Module </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/appraisal') }}"> PIP/CIP </a></li>
                <li><a href="{{ asset('/appraisal') }}2"> TRIP </a></li>
                <li><a href="{{ asset('/appraisal') }}3"> Dropped </a></li>
                <li><a href="{{ asset('/appraisal') }}4"> Completed </a></li>
                <li><a href="{{ asset('/appraisal') }}5"> Uncategorized </a></li>
                <li><a href="{{ asset('/reclassification') }}">Reclassify Projects</a></li>
                <li><a href="{{ asset('/trip') }}">TRIP Prioritization 1</a></li>
                <li><a href="{{ asset('/tripcat2') }}">TRIP Prioritization 2</a></li>
              </ul>
            </li>


            <li><a href="/report"> <i class="icon-presentation"></i>Report Generation</a></li>
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
            <li><a href="{{ asset('/video') }}"> <i class="icon-bill"></i>Video Tutorial</a></li>
          </ul>
        </div>
      @break

      @case("MINDA")          
            <li><a href="{{ asset('/allminda') }}"> <i class="icon-list-1"></i>View MINDA Projects</a></li>
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
            <li><a href="{{ asset('/video') }}"> <i class="icon-bill"></i>Video Tutorial</a></li>
          </ul>
        </div>
      @break

      @case("ADM") 
            <li><a href="#exampledropdownDropdown2" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>View All Projects </a>
              <ul id="exampledropdownDropdown2" class="collapse list-unstyled ">
                <li><a href="{{ asset('/all') }}"> PIP/CIP </a></li>
                <li><a href="{{ asset('/all2') }}"> TRIP </a></li>
                <li><a href="{{ asset('/all3') }}"> Dropped </a></li>
                <li><a href="{{ asset('/all4') }}"> Completed </a></li>
                <li><a href="{{ asset('/all5') }}"> Uncategorized </a></li>
              </ul>
            </li>
            <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-list-1"></i>Validate Module </a>
              <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                <li><a href="{{ asset('/appraisal') }}"> PIP/CIP </a></li>
                <li><a href="{{ asset('/appraisal') }}2"> TRIP </a></li>
                <li><a href="{{ asset('/appraisal') }}3"> Dropped </a></li>
                <li><a href="{{ asset('/appraisal') }}4"> Completed </a></li>
                <li><a href="{{ asset('/appraisal') }}5"> Uncategorized </a></li>
                <li><a href="{{ asset('/reclassification') }}">Reclassify Projects</a></li>
                <li><a href="{{ asset('/trip') }}">TRIP Prioritization 1</a></li>
                <li><a href="{{ asset('/tripcat2') }}">TRIP Prioritization 2</a></li>
              </ul>
            </li>
            <li><a href="/report"> <i class="icon-presentation"></i>Report Generation</a></li>
            <li><a href="{{ asset('/links') }}"> <i class="icon-line-chart"></i>Related Links</a></li>
            <li><a href="{{ asset('/manual') }}"> <i class="icon-bill"></i>User Manual</a></li>
            <li><a href="{{ asset('/video') }}"> <i class="icon-bill"></i>Video Tutorial</a></li>
          </ul>
        </div>
        <div class="admin-menu">
          <h5 class="sidenav-heading">Admin</h5>
          <ul id="side-admin-menu" class="side-menu list-unstyled"> 
            <li> <a href="{{ asset('/emailnotif') }}"> <i class="far fa-envelope"></i>Send Email Notification</a></li>
            <li> <a href="{{ asset('/users') }}"> <i class="icon-user"> </i>Users</a></li>
            <li> <a href="#"> <i class="icon-grid"> </i>Reference Tables</a></li>
          </ul>
        </div>
      </div>
    @break

    @default
    @endswitch

    </nav>