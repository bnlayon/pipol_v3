<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('papcode')->after('tripcategory')->default(null)->nullable();
            $table->string('regprogtitle')->default(null)->nullable();
            $table->string('implementationreadiness')->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropIfExists('papcode');
            $table->dropIfExists('regprogtitle');
            $table->dropIfExists('implementationreadiness');
        });
    }
}
