/*
 Navicat Premium Data Transfer

 Source Server         : Local
 Source Server Type    : MySQL
 Source Server Version : 100136
 Source Host           : localhost:3306
 Source Schema         : pipol090919

 Target Server Type    : MySQL
 Target Server Version : 100136
 File Encoding         : 65001

 Date: 21/10/2020 20:15:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for _1matrices
-- ----------------------------
DROP TABLE IF EXISTS `_1matrices`;
CREATE TABLE `_1matrices`  (
  `id` int(11) NOT NULL DEFAULT 0,
  `chapter` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `chapter_outcome` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _1matricesprojects
-- ----------------------------
DROP TABLE IF EXISTS `_1matricesprojects`;
CREATE TABLE `_1matricesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `_1rm_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53103 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _2matrices
-- ----------------------------
DROP TABLE IF EXISTS `_2matrices`;
CREATE TABLE `_2matrices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intermediate_outcome` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chapter_outcome_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 138 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _2matricesprojects
-- ----------------------------
DROP TABLE IF EXISTS `_2matricesprojects`;
CREATE TABLE `_2matricesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `_2rm_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60592 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _3matrices
-- ----------------------------
DROP TABLE IF EXISTS `_3matrices`;
CREATE TABLE `_3matrices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intermediate_outcome` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `intermediate_outcome_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 403 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _3matricesprojects
-- ----------------------------
DROP TABLE IF EXISTS `_3matricesprojects`;
CREATE TABLE `_3matricesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `_3rm_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29253 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _4matrices
-- ----------------------------
DROP TABLE IF EXISTS `_4matrices`;
CREATE TABLE `_4matrices`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intermediate_outcome` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `intermediate_outcome_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 392 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for _4matricesprojects
-- ----------------------------
DROP TABLE IF EXISTS `_4matricesprojects`;
CREATE TABLE `_4matricesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `_4rm_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16164 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for agencies
-- ----------------------------
DROP TABLE IF EXISTS `agencies`;
CREATE TABLE `agencies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `motheragency_id` int(11) NULL DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Category` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Abbreviation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 441 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for agenciesprojects
-- ----------------------------
DROP TABLE IF EXISTS `agenciesprojects`;
CREATE TABLE `agenciesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `agency_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6875 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for agendaprojects
-- ----------------------------
DROP TABLE IF EXISTS `agendaprojects`;
CREATE TABLE `agendaprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `agenda_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 73512 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for agendas
-- ----------------------------
DROP TABLE IF EXISTS `agendas`;
CREATE TABLE `agendas`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agenda` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for attagenciesprojects
-- ----------------------------
DROP TABLE IF EXISTS `attagenciesprojects`;
CREATE TABLE `attagenciesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `agency_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18975 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bases
-- ----------------------------
DROP TABLE IF EXISTS `bases`;
CREATE TABLE `bases`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `basis` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for basisprojects
-- ----------------------------
DROP TABLE IF EXISTS `basisprojects`;
CREATE TABLE `basisprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `basis_id` int(11) NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 76585 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for chapters
-- ----------------------------
DROP TABLE IF EXISTS `chapters`;
CREATE TABLE `chapters`  (
  `chap_no` int(2) NOT NULL AUTO_INCREMENT,
  `chap_description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`chap_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ciptypologies
-- ----------------------------
DROP TABLE IF EXISTS `ciptypologies`;
CREATE TABLE `ciptypologies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typologies` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cities
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities`  (
  `id` int(10) UNSIGNED NOT NULL,
  `cityName` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provNo` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for citiesprojects
-- ----------------------------
DROP TABLE IF EXISTS `citiesprojects`;
CREATE TABLE `citiesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `cities_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 48478 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for droppingreasons
-- ----------------------------
DROP TABLE IF EXISTS `droppingreasons`;
CREATE TABLE `droppingreasons`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `reasons` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `updated_at` date NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3792 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for facosts
-- ----------------------------
DROP TABLE IF EXISTS `facosts`;
CREATE TABLE `facosts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `fayear` int(11) NULL DEFAULT NULL,
  `facost_nep` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `facost_ad` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `facost_all` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 310268 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for finance_source_projects
-- ----------------------------
DROP TABLE IF EXISTS `finance_source_projects`;
CREATE TABLE `finance_source_projects`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL,
  `fsource_id` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30366 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for focals
-- ----------------------------
DROP TABLE IF EXISTS `focals`;
CREATE TABLE `focals`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chap` int(2) NULL DEFAULT NULL,
  `focal_id` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fprojects
-- ----------------------------
DROP TABLE IF EXISTS `fprojects`;
CREATE TABLE `fprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `fsource_no` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1253 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fscosts
-- ----------------------------
DROP TABLE IF EXISTS `fscosts`;
CREATE TABLE `fscosts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `fsyear` int(11) NULL DEFAULT NULL,
  `fscost` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115375 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fsstatuses
-- ----------------------------
DROP TABLE IF EXISTS `fsstatuses`;
CREATE TABLE `fsstatuses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fs_status` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fundings
-- ----------------------------
DROP TABLE IF EXISTS `fundings`;
CREATE TABLE `fundings`  (
  `fsource_no` int(11) NOT NULL AUTO_INCREMENT,
  `fsource_description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`fsource_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for fundingsources
-- ----------------------------
DROP TABLE IF EXISTS `fundingsources`;
CREATE TABLE `fundingsources`  (
  `fsource_no` int(11) NOT NULL AUTO_INCREMENT,
  `fsource_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fsource_description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`fsource_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for goals
-- ----------------------------
DROP TABLE IF EXISTS `goals`;
CREATE TABLE `goals`  (
  `goal_id` int(11) NOT NULL,
  `goal_description` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`goal_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for implementations
-- ----------------------------
DROP TABLE IF EXISTS `implementations`;
CREATE TABLE `implementations`  (
  `id` int(11) NOT NULL,
  `implementation` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for infrastructures
-- ----------------------------
DROP TABLE IF EXISTS `infrastructures`;
CREATE TABLE `infrastructures`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  `local` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `grant` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gocc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lgu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `private` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `others` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413337 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for investments
-- ----------------------------
DROP TABLE IF EXISTS `investments`;
CREATE TABLE `investments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  `local` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `loan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `grant` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gocc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `lgu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `private` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `others` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413544 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for levels
-- ----------------------------
DROP TABLE IF EXISTS `levels`;
CREATE TABLE `levels`  (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`level_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `attachment` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `archive` int(1) NULL DEFAULT NULL,
  `iflinks` int(1) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_id_model_type_index`(`model_id`, `model_type`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for modes
-- ----------------------------
DROP TABLE IF EXISTS `modes`;
CREATE TABLE `modes`  (
  `mode_no` int(11) NOT NULL AUTO_INCREMENT,
  `mode_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mode_description` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`mode_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for otherpdpprojects
-- ----------------------------
DROP TABLE IF EXISTS `otherpdpprojects`;
CREATE TABLE `otherpdpprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `chap_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65586 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for piptypologies
-- ----------------------------
DROP TABLE IF EXISTS `piptypologies`;
CREATE TABLE `piptypologies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typologies` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for projectdocuments
-- ----------------------------
DROP TABLE IF EXISTS `projectdocuments`;
CREATE TABLE `projectdocuments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectdocument` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for projectlogs
-- ----------------------------
DROP TABLE IF EXISTS `projectlogs`;
CREATE TABLE `projectlogs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `activity` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ipaddress` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `proj_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78889 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `agency_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `motheragency_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `title` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prog_proj` int(1) NULL DEFAULT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `output` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `spatial` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `iccable` int(1) NULL DEFAULT NULL,
  `currentlevel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approval_date1` date NULL DEFAULT NULL,
  `approval_date2` date NULL DEFAULT NULL,
  `approval_date3` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approval_date4` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approval_date5` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pip` int(11) NULL DEFAULT NULL,
  `pip_typo` int(11) NULL DEFAULT NULL,
  `cip` int(11) NULL DEFAULT NULL,
  `cip_typo` int(11) NULL DEFAULT NULL,
  `trip` int(11) NULL DEFAULT NULL,
  `rdip` int(11) NULL DEFAULT NULL,
  `rdc_endorsement` int(11) NULL DEFAULT NULL,
  `rdc_endorsed_notendorsed` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rdc_date_endorsement` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `risk` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `mainpdp` int(11) NULL DEFAULT NULL,
  `gender` int(11) NULL DEFAULT NULL,
  `start` int(11) NULL DEFAULT NULL,
  `end` int(11) NULL DEFAULT NULL,
  `ppdetails` int(11) NULL DEFAULT NULL,
  `ppdetails_others` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `rowa` int(11) NULL DEFAULT NULL,
  `rc` int(11) NULL DEFAULT NULL,
  `wrrc` int(11) NULL DEFAULT NULL,
  `rowa_affected` int(11) NULL DEFAULT NULL,
  `rc_affected` int(11) NULL DEFAULT NULL,
  `employment` int(11) NULL DEFAULT NULL,
  `mainfsource` int(11) NULL DEFAULT NULL,
  `modeofimplementation` int(11) NULL DEFAULT NULL,
  `category` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tier2_status` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updates` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `asof` date NULL DEFAULT NULL,
  `tier1_uacs` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tier2_uacs` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tier2_type` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `statusofsubmission` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `fsassistance` int(1) NULL DEFAULT NULL,
  `fsstatus` int(11) NULL DEFAULT NULL,
  `fsstatus_ongoing` date NULL DEFAULT NULL,
  `fsstatus_prep` date NULL DEFAULT NULL,
  `tier1_type` int(11) NULL DEFAULT NULL,
  `NA_fs` int(11) NULL DEFAULT NULL,
  `other_fs` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_mode` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reg_prog` int(11) NULL DEFAULT NULL,
  `rd` int(11) NULL DEFAULT NULL,
  `NA_rm` int(11) NULL DEFAULT NULL,
  `cost` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tripcategory` int(1) NULL DEFAULT NULL,
  `papcode` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `regprogtitle` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `implementationreadiness` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_updates` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `SS_tier1_stat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_tier2_stat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_category` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_no` int(1) NULL DEFAULT NULL,
  `SS_tier2_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_implementationreadiness` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_statusofsubmission` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `SS_remarks` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `SS_status2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_category` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_updates` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `NRO_tier1_stat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_tier2_stat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_implementationreadiness` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_remarks` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `NRO_no` int(1) NULL DEFAULT NULL,
  `NRO_status2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_statusofsubmission` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `NRO_tier2_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `PIS_implementationreadiness` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `PIS_status2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `PIS_updates` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `PIS_statusofsubmission` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `PIS_no` int(11) NULL DEFAULT NULL,
  `PIS_remarks` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uncat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32918 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces`  (
  `id` int(10) UNSIGNED NOT NULL,
  `provName` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `regionNo` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for provincesprojects
-- ----------------------------
DROP TABLE IF EXISTS `provincesprojects`;
CREATE TABLE `provincesprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `province_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47613 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rccosts
-- ----------------------------
DROP TABLE IF EXISTS `rccosts`;
CREATE TABLE `rccosts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `rcyear` int(11) NULL DEFAULT NULL,
  `rccost` varchar(22) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107287 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for reclassifications
-- ----------------------------
DROP TABLE IF EXISTS `reclassifications`;
CREATE TABLE `reclassifications`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `proj_id` int(11) NULL DEFAULT NULL,
  `orig_pdp` int(11) NULL DEFAULT NULL,
  `orig_other_pdp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `proposed_pdp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `proposed_other_pdp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `remarks_proposed` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reasons_declined` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4000 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for regionalcosts
-- ----------------------------
DROP TABLE IF EXISTS `regionalcosts`;
CREATE TABLE `regionalcosts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `region` int(11) NULL DEFAULT NULL,
  `year` int(11) NULL DEFAULT NULL,
  `cost` bigint(255) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 702368 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for regions
-- ----------------------------
DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions`  (
  `region_no` int(11) NOT NULL AUTO_INCREMENT,
  `region_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `region_description` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `regional_code` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`region_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for regionsprojects
-- ----------------------------
DROP TABLE IF EXISTS `regionsprojects`;
CREATE TABLE `regionsprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `region_id` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56319 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rowacosts
-- ----------------------------
DROP TABLE IF EXISTS `rowacosts`;
CREATE TABLE `rowacosts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `rowayear` int(11) NULL DEFAULT NULL,
  `rowacost` varchar(33) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111108 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sdgprojects
-- ----------------------------
DROP TABLE IF EXISTS `sdgprojects`;
CREATE TABLE `sdgprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `sdg_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105742 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sectorprojects
-- ----------------------------
DROP TABLE IF EXISTS `sectorprojects`;
CREATE TABLE `sectorprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `sector_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34940 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sectors
-- ----------------------------
DROP TABLE IF EXISTS `sectors`;
CREATE TABLE `sectors`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE `statuses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statuses` varchar(75) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for statusprojects
-- ----------------------------
DROP TABLE IF EXISTS `statusprojects`;
CREATE TABLE `statusprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `status_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39509 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for submissions
-- ----------------------------
DROP TABLE IF EXISTS `submissions`;
CREATE TABLE `submissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_id` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `motheragency_id` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_lname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_mname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_fname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_designation` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_telnumber` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_faxnumber` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head_email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_lname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_mname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_fname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_designation` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_telnumber` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_faxnumber` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mother_email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `authorization_form` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `focal_no` int(1) NULL DEFAULT NULL,
  `midpoint` int(1) NULL DEFAULT NULL,
  `midpoint_time` timestamp(0) NULL DEFAULT NULL,
  `final` int(1) NULL DEFAULT NULL,
  `final_time` timestamp(0) NULL DEFAULT NULL,
  `validated` int(1) NULL DEFAULT NULL,
  `validated_time` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 994 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for subsectorprojects
-- ----------------------------
DROP TABLE IF EXISTS `subsectorprojects`;
CREATE TABLE `subsectorprojects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `subsector_id` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33588 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for subsectors
-- ----------------------------
DROP TABLE IF EXISTS `subsectors`;
CREATE TABLE `subsectors`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subsector` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sector` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for substatuses
-- ----------------------------
DROP TABLE IF EXISTS `substatuses`;
CREATE TABLE `substatuses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `substatuses` varchar(75) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for systemlogs
-- ----------------------------
DROP TABLE IF EXISTS `systemlogs`;
CREATE TABLE `systemlogs`  (
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `activity` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ipaddress` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0)
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tier1statuses
-- ----------------------------
DROP TABLE IF EXISTS `tier1statuses`;
CREATE TABLE `tier1statuses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tier1_status` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tier2statuses
-- ----------------------------
DROP TABLE IF EXISTS `tier2statuses`;
CREATE TABLE `tier2statuses`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tier2_status` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tripcat1projects
-- ----------------------------
DROP TABLE IF EXISTS `tripcat1projects`;
CREATE TABLE `tripcat1projects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `criteria1` int(1) NULL DEFAULT NULL,
  `criteria2` int(1) NULL DEFAULT NULL,
  `criteria3` int(1) NULL DEFAULT NULL,
  `criteria4` int(1) NULL DEFAULT NULL,
  `criteria5` int(1) NULL DEFAULT NULL,
  `criteria6` int(1) NULL DEFAULT NULL,
  `criteria7` int(1) NULL DEFAULT NULL,
  `criteria8` int(1) NULL DEFAULT NULL,
  `criteria9` int(1) NULL DEFAULT NULL,
  `criteria10` int(1) NULL DEFAULT NULL,
  `criteria11` int(1) NULL DEFAULT NULL,
  `criteria12` int(1) NULL DEFAULT NULL,
  `criteria13` int(1) NULL DEFAULT NULL,
  `criteria14` int(1) NULL DEFAULT NULL,
  `criteria15` int(1) NULL DEFAULT NULL,
  `criteria16` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  `score` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 572 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tripcat2projects
-- ----------------------------
DROP TABLE IF EXISTS `tripcat2projects`;
CREATE TABLE `tripcat2projects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NULL DEFAULT NULL,
  `criteria1` int(11) NULL DEFAULT NULL,
  `criteria2` int(11) NULL DEFAULT NULL,
  `criteria3` int(11) NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7134 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `fname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `mname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `designation` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `telnumber` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `faxnumber` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `submission_id` int(4) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 1,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `focal_no` int(1) NULL DEFAULT NULL,
  `user_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_first` int(11) NULL DEFAULT 1,
  `gender` varchar(191) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3464 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- View structure for agendas_pivot
-- ----------------------------
DROP VIEW IF EXISTS `agendas_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `agendas_pivot` AS SELECT
	agendaprojects.proj_id,
	GROUP_CONCAT( DISTINCT agendas.agenda SEPARATOR ' / ' ) as Agendas
FROM
	agendaprojects
	LEFT JOIN projects ON agendaprojects.proj_id = projects.id 
	LEFT JOIN agendas ON agendas.id = agendaprojects.agenda_id
GROUP BY
	agendaprojects.proj_id ;

-- ----------------------------
-- View structure for allmatrices_pivot
-- ----------------------------
DROP VIEW IF EXISTS `allmatrices_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `allmatrices_pivot` AS SELECT
	projects.id,
	matrices1_pivot.Matrices1,
	matrices2_pivot.Matrices2,
	matrices3_pivot.Matrices3,
	matrices4_pivot.Matrices4 
FROM
	projects
	LEFT JOIN matrices1_pivot ON projects.id = matrices1_pivot.proj_id
	LEFT JOIN matrices2_pivot ON projects.id = matrices2_pivot.proj_id
	LEFT JOIN matrices3_pivot ON projects.id = matrices3_pivot.proj_id
	LEFT JOIN matrices4_pivot ON projects.id = matrices4_pivot.proj_id 
WHERE
	(projects.SS_statusofsubmission = 'Validated' 
	OR projects.SS_statusofsubmission = 'Reviewed' 
	OR projects.statusofsubmission = 'Endorsed') ;

-- ----------------------------
-- View structure for basis_pivot
-- ----------------------------
DROP VIEW IF EXISTS `basis_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `basis_pivot` AS SELECT
	basisprojects.proj_id,
-- 	GROUP_CONCAT( DISTINCT basisprojects.basis_id SEPARATOR ',' ) as BasisID,
	GROUP_CONCAT( DISTINCT bases.basis SEPARATOR '/' ) as BasisImplementation
FROM
	basisprojects
	LEFT JOIN projects ON basisprojects.proj_id = projects.id 
	LEFT JOIN bases ON bases.id = basisprojects.basis_id
GROUP BY
	basisprojects.proj_id ;

-- ----------------------------
-- View structure for chapters_pivot
-- ----------------------------
DROP VIEW IF EXISTS `chapters_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `chapters_pivot` AS SELECT
	otherpdpprojects.proj_id,
	GROUP_CONCAT( DISTINCT chapters.chap_no SEPARATOR ' / ' ) as OTHERPDP
FROM
	otherpdpprojects
	LEFT JOIN projects ON otherpdpprojects.proj_id = projects.id 
	LEFT JOIN chapters ON chapters.chap_no = otherpdpprojects.chap_id
GROUP BY
	otherpdpprojects.proj_id ;

-- ----------------------------
-- View structure for facost_extended
-- ----------------------------
DROP VIEW IF EXISTS `facost_extended`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `facost_extended` AS (
SELECT
facosts.proj_id,

case when facosts.`fayear` = '2017' then facosts.facost_nep end as 2017facost_nep,
case when facosts.`fayear` = '2017' then facosts.facost_all end as 2017facost_alloc,
case when facosts.`fayear` = '2017' then facosts.facost_ad end as 2017facost_disbursed,

case when facosts.`fayear` = '2018' then facosts.facost_nep end as 2018facost_nep,
case when facosts.`fayear` = '2018' then facosts.facost_all end as 2018facost_alloc,
case when facosts.`fayear` = '2018' then facosts.facost_ad end as 2018facost_disbursed,

case when facosts.`fayear` = '2019' then facosts.facost_nep end as 2019facost_nep,
case when facosts.`fayear` = '2019' then facosts.facost_all end as 2019facost_alloc,
case when facosts.`fayear` = '2019' then facosts.facost_ad end as 2019facost_disbursed,

case when facosts.`fayear` = '2020' then facosts.facost_nep end as 2020facost_nep,
case when facosts.`fayear` = '2020' then facosts.facost_all end as 2020facost_alloc,
case when facosts.`fayear` = '2020' then facosts.facost_ad end as 2020facost_disbursed,

case when facosts.`fayear` = '2021' then facosts.facost_nep end as 2021facost_nep,
case when facosts.`fayear` = '2021' then facosts.facost_all end as 2021facost_alloc,
case when facosts.`fayear` = '2021' then facosts.facost_ad end as 2021facost_disbursed,

case when facosts.`fayear` = '2022' then facosts.facost_nep end as 2022facost_nep,
case when facosts.`fayear` = '2022' then facosts.facost_all end as 2022facost_alloc,
case when facosts.`fayear` = '2022' then facosts.facost_ad end as 2022facost_disbursed



from facosts
) ;

-- ----------------------------
-- View structure for facost_pivot
-- ----------------------------
DROP VIEW IF EXISTS `facost_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `facost_pivot` AS (
SELECT
proj_id,

SUM(2017facost_nep) as 2017facost_nep,
SUM(2017facost_alloc) as 2017facost_alloc,
SUM(2017facost_disbursed) as 2017facost_disbursed,
SUM(2018facost_nep) as 2018facost_nep,
SUM(2018facost_alloc) as 2018facost_alloc,
SUM(2018facost_disbursed) as 2018facost_disbursed,
SUM(2019facost_nep) as 2019facost_nep,
SUM(2019facost_alloc) as 2019facost_alloc,
SUM(2019facost_disbursed) as 2019facost_disbursed,
SUM(2020facost_nep) as 2020facost_nep,
SUM(2020facost_alloc) as 2020facost_alloc,
SUM(2020facost_disbursed) as 2020facost_disbursed,
SUM(2021facost_nep) as 2021facost_nep,
SUM(2021facost_alloc) as 2021facost_alloc,
SUM(2021facost_disbursed) as 2021facost_disbursed,
SUM(2022facost_nep) as 2022facost_nep,
SUM(2022facost_alloc) as 2022facost_alloc,
SUM(2022facost_disbursed) as 2022facost_disbursed
-- 
-- (SUM(2017facost_nep) + 
-- SUM(2017facost_alloc) +
-- SUM(2017facost_disbursed) + 
-- SUM(2018facost_nep) + 
-- SUM(2018facost_alloc) + 
-- SUM(2018facost_disbursed) + 
-- SUM(2019facost_nep) +
-- SUM(2019facost_alloc) +
-- SUM(2019facost_disbursed) +
-- SUM(2020facost_nep) +
-- SUM(2020facost_alloc) +
-- SUM(2020facost_disbursed) +
-- SUM(2021facost_nep) +
-- SUM(2021facost_alloc) +
-- SUM(2021facost_disbursed) +
-- SUM(2022facost_nep) +
-- SUM(2022facost_alloc) +
-- SUM(2022facost_disbursed)) as TotalFACost
from facost_extended
group by proj_id
) ;

-- ----------------------------
-- View structure for fsource_pivot
-- ----------------------------
DROP VIEW IF EXISTS `fsource_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `fsource_pivot` AS SELECT
	finance_source_projects.proj_id,
	GROUP_CONCAT( DISTINCT fundingsources.fsource_description SEPARATOR ' / ' ) as FSOURCE
FROM
	finance_source_projects
	LEFT JOIN projects ON finance_source_projects.proj_id = projects.id 
	LEFT JOIN fundingsources ON fundingsources.fsource_no = finance_source_projects.fsource_id
GROUP BY
	finance_source_projects.proj_id ;

-- ----------------------------
-- View structure for funding_pivot
-- ----------------------------
DROP VIEW IF EXISTS `funding_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `funding_pivot` AS SELECT
	fprojects.proj_id,
	GROUP_CONCAT( DISTINCT fundings.fsource_description SEPARATOR ' / ' ) as FUNDINGINST
FROM
	fprojects
	LEFT JOIN projects ON fprojects.proj_id = projects.id 
	LEFT JOIN fundings ON fundings.fsource_no = fprojects.fsource_no
GROUP BY
	fprojects.proj_id ;

-- ----------------------------
-- View structure for infrastructures_extended
-- ----------------------------
DROP VIEW IF EXISTS `infrastructures_extended`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `infrastructures_extended` AS (
SELECT
infrastructures.proj_id,
case when infrastructures.`year` = '2016' then infrastructures.`local` end as 2016local,
case when infrastructures.`year` = '2016' then infrastructures.`loan` end as 2016loan,
case when infrastructures.`year` = '2016' then infrastructures.`grant` end as 2016grant,
case when infrastructures.`year` = '2016' then infrastructures.`gocc` end as 2016gocc,
case when infrastructures.`year` = '2016' then infrastructures.`lgu` end as 2016lgu,
case when infrastructures.`year` = '2016' then infrastructures.`private` end as 2016private,
case when infrastructures.`year` = '2016' then infrastructures.`others` end as 2016others,
case when infrastructures.`year` = '2017' then infrastructures.`local` end as 2017local,
case when infrastructures.`year` = '2017' then infrastructures.`loan` end as 2017loan,
case when infrastructures.`year` = '2017' then infrastructures.`grant` end as 2017grant,
case when infrastructures.`year` = '2017' then infrastructures.`gocc` end as 2017gocc,
case when infrastructures.`year` = '2017' then infrastructures.`lgu` end as 2017lgu,
case when infrastructures.`year` = '2017' then infrastructures.`private` end as 2017private,
case when infrastructures.`year` = '2017' then infrastructures.`others` end as 2017others,
case when infrastructures.`year` = '2018' then infrastructures.`local` end as 2018local,
case when infrastructures.`year` = '2018' then infrastructures.`loan` end as 2018loan,
case when infrastructures.`year` = '2018' then infrastructures.`grant` end as 2018grant,
case when infrastructures.`year` = '2018' then infrastructures.`gocc` end as 2018gocc,
case when infrastructures.`year` = '2018' then infrastructures.`lgu` end as 2018lgu,
case when infrastructures.`year` = '2018' then infrastructures.`private` end as 2018private,
case when infrastructures.`year` = '2018' then infrastructures.`others` end as 2018others,
case when infrastructures.`year` = '2019' then infrastructures.`local` end as 2019local,
case when infrastructures.`year` = '2019' then infrastructures.`loan` end as 2019loan,
case when infrastructures.`year` = '2019' then infrastructures.`grant` end as 2019grant,
case when infrastructures.`year` = '2019' then infrastructures.`gocc` end as 2019gocc,
case when infrastructures.`year` = '2019' then infrastructures.`lgu` end as 2019lgu,
case when infrastructures.`year` = '2019' then infrastructures.`private` end as 2019private,
case when infrastructures.`year` = '2019' then infrastructures.`others` end as 2019others,
case when infrastructures.`year` = '2020' then infrastructures.`local` end as 2020local,
case when infrastructures.`year` = '2020' then infrastructures.`loan` end as 2020loan,
case when infrastructures.`year` = '2020' then infrastructures.`grant` end as 2020grant,
case when infrastructures.`year` = '2020' then infrastructures.`gocc` end as 2020gocc,
case when infrastructures.`year` = '2020' then infrastructures.`lgu` end as 2020lgu,
case when infrastructures.`year` = '2020' then infrastructures.`private` end as 2020private,
case when infrastructures.`year` = '2020' then infrastructures.`others` end as 2020others,
case when infrastructures.`year` = '2021' then infrastructures.`local` end as 2021local,
case when infrastructures.`year` = '2021' then infrastructures.`loan` end as 2021loan,
case when infrastructures.`year` = '2021' then infrastructures.`grant` end as 2021grant,
case when infrastructures.`year` = '2021' then infrastructures.`gocc` end as 2021gocc,
case when infrastructures.`year` = '2021' then infrastructures.`lgu` end as 2021lgu,
case when infrastructures.`year` = '2021' then infrastructures.`private` end as 2021private,
case when infrastructures.`year` = '2021' then infrastructures.`others` end as 2021others,
case when infrastructures.`year` = '2022' then infrastructures.`local` end as 2022local,
case when infrastructures.`year` = '2022' then infrastructures.`loan` end as 2022loan,
case when infrastructures.`year` = '2022' then infrastructures.`grant` end as 2022grant,
case when infrastructures.`year` = '2022' then infrastructures.`gocc` end as 2022gocc,
case when infrastructures.`year` = '2022' then infrastructures.`lgu` end as 2022lgu,
case when infrastructures.`year` = '2022' then infrastructures.`private` end as 2022private,
case when infrastructures.`year` = '2022' then infrastructures.`others` end as 2022others,
case when infrastructures.`year` = '2023' then infrastructures.`local` end as 2023local,
case when infrastructures.`year` = '2023' then infrastructures.`loan` end as 2023loan,
case when infrastructures.`year` = '2023' then infrastructures.`grant` end as 2023grant,
case when infrastructures.`year` = '2023' then infrastructures.`gocc` end as 2023gocc,
case when infrastructures.`year` = '2023' then infrastructures.`lgu` end as 2023lgu,
case when infrastructures.`year` = '2023' then infrastructures.`private` end as 2023private,
case when infrastructures.`year` = '2023' then infrastructures.`others` end as 2023others
from infrastructures
) ;

-- ----------------------------
-- View structure for infrastructures_pivot
-- ----------------------------
DROP VIEW IF EXISTS `infrastructures_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `infrastructures_pivot` AS (
SELECT
proj_id,
SUM(2016local) as Infra2016local,
SUM(2016loan) as Infra2016loan,
SUM(2016grant) as Infra2016grant,
SUM(2016gocc) as Infra2016gocc,
SUM(2016lgu) as Infra2016lgu,
SUM(2016private) as Infra2016private,
SUM(2016others) as Infra2016others,
(SUM(2016local) + SUM(2016loan) + SUM(2016grant) + SUM(2016gocc) + SUM(2016lgu) + SUM(2016private) + SUM(2016others)) as 2016TOTAL,
SUM(2017local) as Infra2017local,
SUM(2017loan) as Infra2017loan,
SUM(2017grant) as Infra2017grant,
SUM(2017gocc) as Infra2017gocc,
SUM(2017lgu) as Infra2017lgu,
SUM(2017private) as Infra2017private,
SUM(2017others) as Infra2017others,
(SUM(2017local) + SUM(2017loan) + SUM(2017grant) + SUM(2017gocc) + SUM(2017lgu) + SUM(2017private) + SUM(2017others)) as 2017TOTAL,
SUM(2018local) as Infra2018local,
SUM(2018loan) as Infra2018loan,
SUM(2018grant) as Infra2018grant,
SUM(2018gocc) as Infra2018gocc,
SUM(2018lgu) as Infra2018lgu,
SUM(2018private) as Infra2018private,
SUM(2018others) as Infra2018others,
(SUM(2018local) + SUM(2018loan) + SUM(2018grant) + SUM(2018gocc) + SUM(2018lgu) + SUM(2018private) + SUM(2018others)) as 2018TOTAL,
SUM(2019local) as Infra2019local,
SUM(2019loan) as Infra2019loan,
SUM(2019grant) as Infra2019grant,
SUM(2019gocc) as Infra2019gocc,
SUM(2019lgu) as Infra2019lgu,
SUM(2019private) as Infra2019private,
SUM(2019others) as Infra2019others,
(SUM(2019local) + SUM(2019loan) + SUM(2019grant) + SUM(2019gocc) + SUM(2019lgu) + SUM(2019private) + SUM(2019others)) as 2019TOTAL,
SUM(2020local) as Infra2020local,
SUM(2020loan) as Infra2020loan,
SUM(2020grant) as Infra2020grant,
SUM(2020gocc) as Infra2020gocc,
SUM(2020lgu) as Infra2020lgu,
SUM(2020private) as Infra2020private,
SUM(2020others) as Infra2020others,
(SUM(2020local) + SUM(2020loan) + SUM(2020grant) + SUM(2020gocc) + SUM(2020lgu) + SUM(2020private) + SUM(2020others)) as 2020TOTAL,
SUM(2021local) as Infra2021local,
SUM(2021loan) as Infra2021loan,
SUM(2021grant) as Infra2021grant,
SUM(2021gocc) as Infra2021gocc,
SUM(2021lgu) as Infra2021lgu,
SUM(2021private) as Infra2021private,
SUM(2021others) as Infra2021others,
(SUM(2021local) + SUM(2021loan) + SUM(2021grant) + SUM(2021gocc) + SUM(2021lgu) + SUM(2021private) + SUM(2021others)) as 2021TOTAL,
SUM(2022local) as Infra2022local,
SUM(2022loan) as Infra2022loan,
SUM(2022grant) as Infra2022grant,
SUM(2022gocc) as Infra2022gocc,
SUM(2022lgu) as Infra2022lgu,
SUM(2022private) as Infra2022private,
SUM(2022others) as Infra2022others,
(SUM(2022local) + SUM(2022loan) + SUM(2022grant) + SUM(2022gocc) + SUM(2022lgu) + SUM(2022private) + SUM(2022others)) as 2022TOTAL,
SUM(2023local) as Infra2023local,
SUM(2023loan) as Infra2023loan,
SUM(2023grant) as Infra2023grant,
SUM(2023gocc) as Infra2023gocc,
SUM(2023lgu) as Infra2023lgu,
SUM(2023private) as Infra2023private,
SUM(2023others) as Infra2023others,
(SUM(2023local) + SUM(2023loan) + SUM(2023grant) + SUM(2023gocc) + SUM(2023lgu) + SUM(2023private) + SUM(2023others)) as 2023TOTAL,
(SUM(2016local) + SUM(2016loan) + SUM(2016grant) + SUM(2016gocc) + SUM(2016lgu) + SUM(2016private) + SUM(2016others) + SUM(2017local) + SUM(2017loan) + SUM(2017grant) + SUM(2017gocc) + SUM(2017lgu) + SUM(2017private) + SUM(2017others) + SUM(2018local) + SUM(2018loan) + SUM(2018grant) + SUM(2018gocc) + SUM(2018lgu) + SUM(2018private) + SUM(2018others) + SUM(2019local) + SUM(2019loan) + SUM(2019grant) + SUM(2019gocc) + SUM(2019lgu) + SUM(2019private) + SUM(2019others) + SUM(2020local) + SUM(2020loan) + SUM(2020grant) + SUM(2020gocc) + SUM(2020lgu) + SUM(2020private) + SUM(2020others) + SUM(2021local) + SUM(2021loan) + SUM(2021grant) + SUM(2021gocc) + SUM(2021lgu) + SUM(2021private) + SUM(2021others) + SUM(2022local) + SUM(2022loan) + SUM(2022grant) + SUM(2022gocc) + SUM(2022lgu) + SUM(2022private) + SUM(2022others) + SUM(2023local) + SUM(2023loan) + SUM(2023grant) + SUM(2023gocc) + SUM(2023lgu) + SUM(2023private) + SUM(2023others)) as TotalInfraCost
from infrastructures_extended
group by proj_id
) ;

-- ----------------------------
-- View structure for investments_extended
-- ----------------------------
DROP VIEW IF EXISTS `investments_extended`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `investments_extended` AS (
SELECT
investments.proj_id,
case when investments.`year` = '2016' then investments.`local` end as 2016local,
case when investments.`year` = '2016' then investments.`loan` end as 2016loan,
case when investments.`year` = '2016' then investments.`grant` end as 2016grant,
case when investments.`year` = '2016' then investments.`gocc` end as 2016gocc,
case when investments.`year` = '2016' then investments.`lgu` end as 2016lgu,
case when investments.`year` = '2016' then investments.`private` end as 2016private,
case when investments.`year` = '2016' then investments.`others` end as 2016others,
case when investments.`year` = '2017' then investments.`local` end as 2017local,
case when investments.`year` = '2017' then investments.`loan` end as 2017loan,
case when investments.`year` = '2017' then investments.`grant` end as 2017grant,
case when investments.`year` = '2017' then investments.`gocc` end as 2017gocc,
case when investments.`year` = '2017' then investments.`lgu` end as 2017lgu,
case when investments.`year` = '2017' then investments.`private` end as 2017private,
case when investments.`year` = '2017' then investments.`others` end as 2017others,
case when investments.`year` = '2018' then investments.`local` end as 2018local,
case when investments.`year` = '2018' then investments.`loan` end as 2018loan,
case when investments.`year` = '2018' then investments.`grant` end as 2018grant,
case when investments.`year` = '2018' then investments.`gocc` end as 2018gocc,
case when investments.`year` = '2018' then investments.`lgu` end as 2018lgu,
case when investments.`year` = '2018' then investments.`private` end as 2018private,
case when investments.`year` = '2018' then investments.`others` end as 2018others,
case when investments.`year` = '2019' then investments.`local` end as 2019local,
case when investments.`year` = '2019' then investments.`loan` end as 2019loan,
case when investments.`year` = '2019' then investments.`grant` end as 2019grant,
case when investments.`year` = '2019' then investments.`gocc` end as 2019gocc,
case when investments.`year` = '2019' then investments.`lgu` end as 2019lgu,
case when investments.`year` = '2019' then investments.`private` end as 2019private,
case when investments.`year` = '2019' then investments.`others` end as 2019others,
case when investments.`year` = '2020' then investments.`local` end as 2020local,
case when investments.`year` = '2020' then investments.`loan` end as 2020loan,
case when investments.`year` = '2020' then investments.`grant` end as 2020grant,
case when investments.`year` = '2020' then investments.`gocc` end as 2020gocc,
case when investments.`year` = '2020' then investments.`lgu` end as 2020lgu,
case when investments.`year` = '2020' then investments.`private` end as 2020private,
case when investments.`year` = '2020' then investments.`others` end as 2020others,
case when investments.`year` = '2021' then investments.`local` end as 2021local,
case when investments.`year` = '2021' then investments.`loan` end as 2021loan,
case when investments.`year` = '2021' then investments.`grant` end as 2021grant,
case when investments.`year` = '2021' then investments.`gocc` end as 2021gocc,
case when investments.`year` = '2021' then investments.`lgu` end as 2021lgu,
case when investments.`year` = '2021' then investments.`private` end as 2021private,
case when investments.`year` = '2021' then investments.`others` end as 2021others,
case when investments.`year` = '2022' then investments.`local` end as 2022local,
case when investments.`year` = '2022' then investments.`loan` end as 2022loan,
case when investments.`year` = '2022' then investments.`grant` end as 2022grant,
case when investments.`year` = '2022' then investments.`gocc` end as 2022gocc,
case when investments.`year` = '2022' then investments.`lgu` end as 2022lgu,
case when investments.`year` = '2022' then investments.`private` end as 2022private,
case when investments.`year` = '2022' then investments.`others` end as 2022others,
case when investments.`year` = '2023' then investments.`local` end as 2023local,
case when investments.`year` = '2023' then investments.`loan` end as 2023loan,
case when investments.`year` = '2023' then investments.`grant` end as 2023grant,
case when investments.`year` = '2023' then investments.`gocc` end as 2023gocc,
case when investments.`year` = '2023' then investments.`lgu` end as 2023lgu,
case when investments.`year` = '2023' then investments.`private` end as 2023private,
case when investments.`year` = '2023' then investments.`others` end as 2023others
from investments
) ;

-- ----------------------------
-- View structure for investments_pivot
-- ----------------------------
DROP VIEW IF EXISTS `investments_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `investments_pivot` AS (
SELECT
proj_id,
SUM(2016local) as 2016local,
SUM(2016loan) as 2016loan,
SUM(2016grant) as 2016grant,
SUM(2016gocc) as 2016gocc,
SUM(2016lgu) as 2016lgu,
SUM(2016private) as 2016private,
SUM(2016others) as 2016others,
(SUM(2016local) + SUM(2016loan) + SUM(2016grant) + SUM(2016gocc) + SUM(2016lgu) + SUM(2016private) + SUM(2016others)) as 2016TOTAL,
SUM(2017local) as 2017local,
SUM(2017loan) as 2017loan,
SUM(2017grant) as 2017grant,
SUM(2017gocc) as 2017gocc,
SUM(2017lgu) as 2017lgu,
SUM(2017private) as 2017private,
SUM(2017others) as 2017others,
(SUM(2017local) + SUM(2017loan) + SUM(2017grant) + SUM(2017gocc) + SUM(2017lgu) + SUM(2017private) + SUM(2017others)) as 2017TOTAL,
SUM(2018local) as 2018local,
SUM(2018loan) as 2018loan,
SUM(2018grant) as 2018grant,
SUM(2018gocc) as 2018gocc,
SUM(2018lgu) as 2018lgu,
SUM(2018private) as 2018private,
SUM(2018others) as 2018others,
(SUM(2018local) + SUM(2018loan) + SUM(2018grant) + SUM(2018gocc) + SUM(2018lgu) + SUM(2018private) + SUM(2018others)) as 2018TOTAL,
SUM(2019local) as 2019local,
SUM(2019loan) as 2019loan,
SUM(2019grant) as 2019grant,
SUM(2019gocc) as 2019gocc,
SUM(2019lgu) as 2019lgu,
SUM(2019private) as 2019private,
SUM(2019others) as 2019others,
(SUM(2019local) + SUM(2019loan) + SUM(2019grant) + SUM(2019gocc) + SUM(2019lgu) + SUM(2019private) + SUM(2019others)) as 2019TOTAL,
SUM(2020local) as 2020local,
SUM(2020loan) as 2020loan,
SUM(2020grant) as 2020grant,
SUM(2020gocc) as 2020gocc,
SUM(2020lgu) as 2020lgu,
SUM(2020private) as 2020private,
SUM(2020others) as 2020others,
(SUM(2020local) + SUM(2020loan) + SUM(2020grant) + SUM(2020gocc) + SUM(2020lgu) + SUM(2020private) + SUM(2020others)) as 2020TOTAL,
SUM(2021local) as 2021local,
SUM(2021loan) as 2021loan,
SUM(2021grant) as 2021grant,
SUM(2021gocc) as 2021gocc,
SUM(2021lgu) as 2021lgu,
SUM(2021private) as 2021private,
SUM(2021others) as 2021others,
(SUM(2021local) + SUM(2021loan) + SUM(2021grant) + SUM(2021gocc) + SUM(2021lgu) + SUM(2021private) + SUM(2021others)) as 2021TOTAL,
SUM(2022local) as 2022local,
SUM(2022loan) as 2022loan,
SUM(2022grant) as 2022grant,
SUM(2022gocc) as 2022gocc,
SUM(2022lgu) as 2022lgu,
SUM(2022private) as 2022private,
SUM(2022others) as 2022others,
(SUM(2022local) + SUM(2022loan) + SUM(2022grant) + SUM(2022gocc) + SUM(2022lgu) + SUM(2022private) + SUM(2022others)) as 2022TOTAL,
SUM(2023local) as 2023local,
SUM(2023loan) as 2023loan,
SUM(2023grant) as 2023grant,
SUM(2023gocc) as 2023gocc,
SUM(2023lgu) as 2023lgu,
SUM(2023private) as 2023private,
SUM(2023others) as 2023others,
(SUM(2023local) + SUM(2023loan) + SUM(2023grant) + SUM(2023gocc) + SUM(2023lgu) + SUM(2023private) + SUM(2023others)) as 2023TOTAL,
(SUM(2016local) + SUM(2016loan) + SUM(2016grant) + SUM(2016gocc) + SUM(2016lgu) + SUM(2016private) + SUM(2016others) + SUM(2017local) + SUM(2017loan) + SUM(2017grant) + SUM(2017gocc) + SUM(2017lgu) + SUM(2017private) + SUM(2017others) + SUM(2018local) + SUM(2018loan) + SUM(2018grant) + SUM(2018gocc) + SUM(2018lgu) + SUM(2018private) + SUM(2018others) + SUM(2019local) + SUM(2019loan) + SUM(2019grant) + SUM(2019gocc) + SUM(2019lgu) + SUM(2019private) + SUM(2019others) + SUM(2020local) + SUM(2020loan) + SUM(2020grant) + SUM(2020gocc) + SUM(2020lgu) + SUM(2020private) + SUM(2020others) + SUM(2021local) + SUM(2021loan) + SUM(2021grant) + SUM(2021gocc) + SUM(2021lgu) + SUM(2021private) + SUM(2021others) + SUM(2022local) + SUM(2022loan) + SUM(2022grant) + SUM(2022gocc) + SUM(2022lgu) + SUM(2022private) + SUM(2022others) + SUM(2023local) + SUM(2023loan) + SUM(2023grant) + SUM(2023gocc) + SUM(2023lgu) + SUM(2023private) + SUM(2023others)) as TotalInvCost
from investments_extended
group by proj_id
) ;

-- ----------------------------
-- View structure for matrices1_pivot
-- ----------------------------
DROP VIEW IF EXISTS `matrices1_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `matrices1_pivot` AS SELECT
	_1matricesprojects.proj_id,
	GROUP_CONCAT( DISTINCT _1matrices.chapter_outcome SEPARATOR ' / ' ) as Matrices1
FROM
	_1matricesprojects
	LEFT JOIN projects ON _1matricesprojects.proj_id = projects.id 
	LEFT JOIN _1matrices ON _1matricesprojects._1rm_id = _1matrices.id
GROUP BY
	_1matricesprojects.proj_id ;

-- ----------------------------
-- View structure for matrices2_pivot
-- ----------------------------
DROP VIEW IF EXISTS `matrices2_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `matrices2_pivot` AS SELECT
	_2matricesprojects.proj_id,
	GROUP_CONCAT( DISTINCT _2matrices.intermediate_outcome SEPARATOR ' / ' ) as Matrices2
FROM
	_2matricesprojects
	LEFT JOIN projects ON _2matricesprojects.proj_id = projects.id 
	LEFT JOIN _2matrices ON _2matricesprojects._2rm_id = _2matrices.id
GROUP BY
	_2matricesprojects.proj_id ;

-- ----------------------------
-- View structure for matrices3_pivot
-- ----------------------------
DROP VIEW IF EXISTS `matrices3_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `matrices3_pivot` AS SELECT
	_3matricesprojects.proj_id,
	GROUP_CONCAT( DISTINCT _3matrices.intermediate_outcome SEPARATOR ' / ' ) as Matrices3
FROM
	_3matricesprojects
	LEFT JOIN projects ON _3matricesprojects.proj_id = projects.id 
	LEFT JOIN _3matrices ON _3matricesprojects._3rm_id = _3matrices.id
GROUP BY
	_3matricesprojects.proj_id ;

-- ----------------------------
-- View structure for matrices4_pivot
-- ----------------------------
DROP VIEW IF EXISTS `matrices4_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `matrices4_pivot` AS SELECT
	_4matricesprojects.proj_id,
	GROUP_CONCAT( DISTINCT _4matrices.intermediate_outcome SEPARATOR ' / ' ) as Matrices4
FROM
	_4matricesprojects
	LEFT JOIN projects ON _4matricesprojects.proj_id = projects.id 
	LEFT JOIN _4matrices ON _4matricesprojects._4rm_id = _4matrices.id
GROUP BY
	_4matricesprojects.proj_id ;

-- ----------------------------
-- View structure for provinces_pivot
-- ----------------------------
DROP VIEW IF EXISTS `provinces_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `provinces_pivot` AS SELECT
	provincesprojects.proj_id,
	GROUP_CONCAT( DISTINCT provinces.provName SEPARATOR ' / ' ) as PROVINCES
FROM
	provincesprojects
	LEFT JOIN projects ON provincesprojects.proj_id = projects.id 
	LEFT JOIN provinces ON provinces.id = provincesprojects.province_id
GROUP BY
	provincesprojects.proj_id ;

-- ----------------------------
-- View structure for regions_pivot
-- ----------------------------
DROP VIEW IF EXISTS `regions_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `regions_pivot` AS SELECT
	regionsprojects.proj_id,
	GROUP_CONCAT( DISTINCT regions.region_description SEPARATOR ' / ' ) as REGIONS
FROM
	regionsprojects
	LEFT JOIN projects ON regionsprojects.proj_id = projects.id 
	LEFT JOIN regions ON regions.regional_code = regionsprojects.region_id
GROUP BY
	regionsprojects.proj_id ;

-- ----------------------------
-- View structure for sdg_pivot
-- ----------------------------
DROP VIEW IF EXISTS `sdg_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `sdg_pivot` AS SELECT
	sdgprojects.proj_id,
	GROUP_CONCAT( DISTINCT goals.goal_description SEPARATOR ' / ' ) as SDGs
FROM
	sdgprojects
	LEFT JOIN projects ON sdgprojects.proj_id = projects.id 
	LEFT JOIN goals ON goals.goal_id = sdgprojects.sdg_id
GROUP BY
	sdgprojects.proj_id ;

-- ----------------------------
-- View structure for sectors_pivot
-- ----------------------------
DROP VIEW IF EXISTS `sectors_pivot`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `sectors_pivot` AS SELECT
	sectorprojects.proj_id,
	GROUP_CONCAT( DISTINCT sectors.sector SEPARATOR ' / ' ) as SECTORS
FROM
	sectorprojects
	LEFT JOIN projects ON sectorprojects.proj_id = projects.id 
	LEFT JOIN sectors ON sectors.id = sectorprojects.sector_id
GROUP BY
	sectorprojects.proj_id ;

SET FOREIGN_KEY_CHECKS = 1;
